<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//Route::middleware(['checkIp'])->group(function () {
    Auth::routes();
    //Route::get('/', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@ssoLogin');
    Route::get('/promotions', 'DiscountsController@promotions');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/cart-items', 'HomeController@cartItems');
    Route::get('/mobile-plans', 'MobileController@index')->name('mobile');
    Route::get('/business-mobile-plan', 'MobileController@businessMobilePlans');
    Route::get('/upload-mobile-broadbands', 'MobileBroadbandController@uploadMobileBroadbandPlans');
    Route::get('/upload-hro', 'MobileController@uploadHro')->name('uploadHro');
    Route::get('/upload-hro-console', 'MobileController@uploadConsoleHro')->name('uploadConsoleHro');
    Route::post('/get-mobile-devices', 'MobileController@getMobileDevices');
    Route::post('/get-business-mobile-devices', 'MobileController@getBusinessMobileDevices');
    Route::post('/checkout-items', 'MobileController@checkout');

    Route::get('/get-mobile-plans', 'MobileController@getMobilePlans');
    Route::get('/get-business-mobile-plans', 'MobileController@getBusinessMobilePlans');
    Route::get('/get-hro', 'MobileController@getHro');
    Route::get('/get-all-hro', 'MobileController@getAllHro');
    Route::get('/get-hro-homepage', 'MobileController@getHroHomepage');
    Route::get('/get-hro-console', 'MobileController@getHroConsole');
    // Route::post('/get-mobile-devices', 'MobileController@getMobileDevices');
    Route::post('/add-cart-item', 'MobileController@addCartItem');
    Route::post('/add-to-cart-item', 'MobileController@addToCartItem');
    Route::post('/add-to-cart-broadband-item', 'MobileController@addToCartBroadbandItem');
    Route::post('/add-to-cart-business-broadband-item', 'MobileController@addToCartBusinessBroadbandItem');

    Route::get('/get-all-mobile-devices', 'MobileController@getAllMobileDevices');
    Route::get('/get-all-temp-mobile-devices', 'MobileController@getAllTempMobileDevices');

    Route::get('/hro', 'HroController@index');
    Route::get('/business-hro', 'HroController@businessHRO');

    Route::get('/sim-plans', 'SimController@index');
    Route::get('/mobile-broadband', 'MobileBroadbandController@index');
    Route::get('/business-mobile-broadband', 'MobileBroadbandController@businessMobileBroadband');
    Route::get('/get-mobile-broadband-categories', 'MobileBroadbandController@getMobileBroadbandCategories');
    Route::get('/get-business-mobile-broadband-categories', 'MobileBroadbandController@getBusinessMobileBroadbandCategories');

    Route::post('/get-mobile-broadbands', 'MobileBroadbandController@getMobileBroadbands');    
    Route::post('/get-business-mobile-broadbands', 'MobileBroadbandController@getBusinessMobileBroadbands');
    Route::post('/add-mobile-broadband-item', 'MobileBroadbandController@addMobileBroadbandItem');
    Route::get('/get-all-mobile-broadband-devices', 'MobileBroadbandController@getAllBroadbandDevices');
    Route::get('/get-all-business-mobile-broadband-devices', 'MobileBroadbandController@getAllBusinessBroadbandDevices');

    Route::get('/get-mobile-page-content', 'MobileController@getMobilePageContent');
    Route::get('/get-business-mobile-page-content', 'MobileController@getBusinessMobilePageContent');

    Route::get('/fixed-plan', 'FixedController@index');
    Route::get('/business-fixed-plan', 'FixedController@businessFixedPlan');
    Route::get('/entertainment', 'EntertainmentController@index');

    Route::get('/add-order', 'OrderController@save');
    Route::get('/get-ip-address', 'OrderController@getIPAddress');
    Route::post('/get-grouped-mobile-devices', 'MobileController@getGroupedMobileDevices');    
    Route::get('/get-proposal-history', 'MobileController@getProposalHistory');    
    Route::get('/get-proposal-history-breakdown', 'MobileController@getProposalHistoryBreakdown');    

    Route::get('/cart-history', 'MobileController@cartHistory');   
    Route::get('/cart-reports', 'AdminController@cartReports');    
    Route::post('/filter-by-category', 'AdminController@filterByCategory');    
//});

Route::middleware(['checkIp'])->group(function () {
    // Route::get('/hro/upload', 'UploadController@hroUpload');
    // Route::post('/upload-hro', 'UploadController@uploadHro');

    Route::get('/admin', 'AdminController@admin')->name('admin');

    //mobile device upload
    Route::get('/mobile-plan/upload', 'AdminController@uploadPage');
    // Route::post('/upload-mobile-plans', 'AdminController@uploadMobilePlans');

    Route::get('/business-mobile-plan/upload', 'UploadController@businessMobileDeviceUpload');
    Route::post('/upload-business-mobile-plans', 'UploadController@uploadBusinessMobilePlans');
    //end mobile device upload

    //mobile broadband upload
    Route::get('/mobile-broadband/upload', 'UploadController@mobileBroadbandPlanUpload');

    Route::post('/upload-mobile-broadband-plans', 'UploadController@uploadMobileBroadbandPlan');

    Route::get('/business-mobile-broadband/upload', 'UploadController@businessMobileBroadbandUpload');

    Route::post('/upload-business-mobile-broadband-plans', 'UploadController@uploadBusinessMobileBroadbandPlan');
    //end mobile broadband upload

    //redemption products
    Route::get('/upload-redemptions-products', 'UploadController@uploadRedemptionsProducts');
    Route::get('/update-redemptions-product-images', 'UploadController@updateRedemptionProductImage');

    //end redemption products

    //business mobile upload
        // Route::get('/get-all-mobile-devices', 'MobileController@getAllMobileDevices');
        Route::get('/get-all-temp-business-mobile-devices', 'MobileController@getAllTempBusinessMobileDevices');
        Route::get('/get-all-temp-mobile-broadband-devices', 'MobileController@getAllTempMobileBroadbandDevices');
        Route::get('/get-all-temp-business-mobile-broadband-devices', 'MobileController@getAllTempBusinessMobileBroadbandDevices');

    //end business mobile upload
    Route::get('/device-categories/{id}', 'AdminController@getDeviceCategories');
    Route::post('/mobile-device/update-category/', 'AdminController@updateDeviceCategory');
    Route::post('/business-mobile-device/update-category/', 'AdminController@updateBusinessMobileDeviceCategory');

    Route::get('/category/mobile-devices', 'AdminController@getMobilePlanCategory');
    Route::get('/category/business/mobile-devices', 'AdminController@getBusinessMobilePlanCategory');

    Route::get('/admin/pages/mobile-plan', 'AdminController@mobilePlanPage');

    Route::get('/admin/pages/business-mobile-plan', 'AdminController@businessMobilePlanPage');

    Route::get('/pages/mobile-broadband', 'AdminController@broadbandPlanPage');
    Route::post('/pages/mobile-broadband-plan/edit', 'AdminController@editMobileBroadbandPlanPage');
    Route::get('/get-broadband-page-content', 'AdminController@getBroadbandPageContent');

    Route::get('/pages/business-mobile-broadband', 'AdminController@businessBroadbandPlanPage');
    Route::post('/pages/business-mobile-broadband-plan/edit', 'AdminController@editBusinessMobileBroadbandPlanPage');
    Route::get('/get-business-broadband-page-content', 'AdminController@getBusinessBroadbandPageContent');

    Route::post('/pages/business-mobile-plan/edit', 'AdminController@editBusinessMobilePlanPage');

    Route::post('/pages/mobile-plan/edit', 'AdminController@editMobilePlanPage');

    Route::post('/test-upload', 'AdminController@testUpload');

    Route::get('/reject-upload', 'AdminController@rejectUpload');

    Route::get('/reject-business-mobile-upload', 'UploadController@rejectBusinessMobileUpload');

    Route::get('/reject-mobile-broadband-upload', 'UploadController@rejectMobileBroadBandUpload');

    Route::get('/reject-business-mobile-broadband-upload', 'UploadController@rejectBusinessMobileBroadBandUpload');

    Route::get('/accept-upload', 'AdminController@uploadMobilePlans');

    Route::get('/accept-upload-business-mobile-plans', 'UploadController@acceptUploadBusinessMobilePlans');

    Route::get('/accept-upload-mobile-broadband-plans', 'UploadController@acceptUploadMobileBroadbandPlans');    

    Route::get('/accept-upload-business-mobile-broadband-plans', 'UploadController@acceptUploadBusinessMobileBroadbandPlans');  

    Route::post('/schedule-upload', 'AdminController@scheduleUpload');
    Route::get('/discounts', 'DiscountsController@index');
    Route::post('/add-discount', 'DiscountsController@addDiscount');
    Route::post('/remove-discount', 'DiscountsController@removeDiscount');

    Route::get('/redemption-products', 'TelstraRedemptionController@index');
    Route::get('/get-redemption-products', 'TelstraRedemptionController@getRedemptionProducts');
    Route::get('/admin-promotions', 'DiscountsController@adminPromotions');
    Route::get('/outright-mobile', 'OutrightController@mobile');
    Route::get('/outright-mobile-broadband', 'OutrightController@mobilebroadband');
    Route::get('/outright-accessories', 'OutrightController@accessories');

    Route::get('/get-outright-accessories', 'OutrightController@getOutrightAccessories');
    Route::post('/outright-edit-accessories', 'OutrightController@editOutrightAccessories');

    Route::get('/get-outright-mobile-devices', 'OutrightController@getOutrightMobileDevices');
    Route::get('/get-business-mobile-devices', 'OutrightController@getBusinessMobileDevices');
    Route::get('/get-outright-mobile-broadband-devices', 'OutrightController@getOutrightMobileBroadbandDevices');
    Route::get('/get-business-mobile-broadband-devices', 'OutrightController@getBusinessMobileBroadbandDevices');
    Route::post('/outright-edit-mobile-pricing', 'OutrightController@editOutrightMobileDevices');
    Route::post('/edit-business-mobile-pricing', 'OutrightController@editBusinessMobileDevices');
    Route::post('/outright-edit-mobile-broadband-pricing', 'OutrightController@editOutrightMobileBroadbandDevices');
    Route::post('/edit-business-mobile-broadband-pricing', 'OutrightController@editBusinessMobileBroadbandDevices');
    Route::get('/telstra-platinum', 'TelstraRedemptionController@telstraPlatinum');
    Route::get('/upload-tls-accessories', 'UploadController@uploadTLSAccessories');    
    Route::get('/compareAccessories', 'UploadController@compareAccessories');
    Route::get('/quicklinks', 'AdminController@quicklinks');
    Route::get('/get-quicklinks', 'AdminController@getQuicklinks');
    Route::post('/add-quicklink', 'AdminController@addQuicklinks');
    Route::post('/remove-quicklink', 'AdminController@removeQuicklinks');
    Route::get('/upload-accessory-supplier-codes', 'AdminController@uploadAccessorySupplierCodes');
    Route::get('/uploader', 'UploadController@uploader');
    Route::get('/check-mobile-promotions', 'UploadController@testMobileUploadPromotions');
    Route::post('/use-proposal-history', 'MobileController@useProposalHistory');
    Route::get('/reports', 'AdminController@reports');
    Route::post('/get-reports', 'AdminController@getReports');
    Route::post('/get-filtered-reports', 'AdminController@getFilteredReports');
    Route::post('/export-reports', 'AdminController@exportReports');
    Route::post('/get-available-store-filters', 'AdminController@getStoreType');
    Route::get('/get-consoles', 'AdminController@getConsoles');
});

Route::middleware('token')->get('/protected', function (Illuminate\Http\Request $request) {
    return "You are on protected zone";
});
Route::get('/sso-logout', 'HomeController@ssoLogout');
Route::get('/permission-denied', 'HomeController@permissionDenied');
Route::get('/_oauth/oidc', 'HomeController@test');
Route::post('/save-branch', 'HomeController@saveBranch');
Route::post('/save-client', 'HomeController@saveClient');
Route::get('/get-client', 'AdminController@getClient');

Route::post('/get-branches', 'HomeController@getBranches');

Route::get('/get-clickpos-stock', 'HomeController@getClickPosStock');
Route::get('/get-stock', 'HomeController@getStock');
Route::get('/change-branch', 'HomeController@changeBranch');
Route::post('/send-cart-template', 'OrderController@sendCartTemplate');
Route::post('/aro-acknowledgement', 'MobileController@aroAcknowledgement');
Route::post('/use-cart-template', 'OrderController@useCartTemplate');
Route::get('/clear-cart', 'OrderController@clearCart');
Route::get('/verify-template-change', 'OrderController@verifyTemplateChange');
Route::get('/get-active-cart-template', 'HomeController@getActiveCartTemplate');
Route::get('/clear-active-cart-template', 'HomeController@clearActiveCartTemplate');
Route::get('/get-branch-users', 'HomeController@getBranchUsers');
Route::get('/telstra-redemptions-products', 'OrderController@telstraRedemptionPage');
Route::get('/get-redemptions-products', 'OrderController@getRedemptionProducts');
Route::get('/get-redemptions-unique-products', 'OrderController@getRedemptionUniqueProducts');
Route::get('/get-redemptions-products-categories', 'OrderController@getRedemptionProductsCategories');

Route::get('/aro-form', 'MobileController@aroForm');
Route::post('/print-aro-form', 'MobileController@printAROForm');
Route::get('/office-use-only', 'MobileController@officeUseOnly');
Route::post('/print-office-use-only', 'MobileController@printOfficeUseOnly');

Route::post('/send-proposal', 'MobileController@sendProposal');
Route::get('/get-discounts', 'DiscountsController@getDiscounts');
Route::get('/get-admin-discounts', 'DiscountsController@getAdminDiscounts');
Route::get('/get-admin-discounts-by-end-date', 'DiscountsController@getAdminDiscountsByEndDate');
Route::get('/get-admin-discounts-no-expiry', 'DiscountsController@getAdminDiscountsNoExpiry');
Route::get('/get-admin-discounts-by-type', 'DiscountsController@getAdminDiscountsByType');
Route::get('/get-admin-archived-discounts', 'DiscountsController@getArchivedAdminDiscounts');
Route::get('/delete-cart-template', 'OrderController@deleteCartTemplate');
Route::get('/get-user-details', 'HomeController@getUserDetails');
Route::post('/upload-batching', 'UploadController@uploadBatching');

Route::get('/get-accessory-master-list', 'OrderController@getMasterListAccessories');
Route::get('/get-accessory-clickpos-list', 'OrderController@uploadAccessorySupplierCodes');
Route::get('/get-accessory-filtered-list', 'OrderController@getRedemptionProducts');
Route::get('/get-accessory-filtered-list-console', 'OrderController@getRedemptionProductsConsole');

Route::get('/get-consumer-hro', 'HroController@getConsumerHro');
Route::get('/get-console-hro', 'HroController@getConsoleHro');
Route::get('/upload-consumer-hro', 'HroController@uploadConsumerHro');
Route::get('/upload-console-hro', 'HroController@uploadConsoleHro');
Route::get('/reject-console-hro-upload', 'HroController@rejectConsoleHro');
Route::get('/reject-consumer-hro-upload', 'HroController@rejectConsumerHro');
Route::post('/upload-consumer-hro-data', 'HroController@uploadConsumerHroData');
Route::post('/upload-console-hro-data', 'HroController@uploadConsoleHroData');

Route::get('/upload-telstra-plus', 'TelstraRedemptionController@uploadTelstraRedemption');
Route::get('/get-telstra-redemption', 'TelstraRedemptionController@getTelstraRedemption');
Route::post('/upload-telstra-redemption-data', 'TelstraRedemptionController@uploadTelstraRedemptionData');
Route::get('/reject-telstra-redemption-upload', 'TelstraRedemptionController@rejectTelstraRedemptionData');



