import camelot
tables = camelot.read_pdf('plans.pdf', pages='all', shift_text=['r', 'b'])
tables[1].to_csv('plans-page-1-table-1.csv')
tables[2].to_csv('plans-page-1-table-2.csv')
tables[3].to_csv('plans-page-2-table-1.csv')
tables[4].to_csv('plans-page-3-table-1.csv')
