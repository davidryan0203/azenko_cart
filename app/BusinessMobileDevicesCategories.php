<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class BusinessMobileDevicesCategories extends Model
    {
        protected $table = 'business_mobile_devices_category';

        public function categories()
	    {
	        return $this->hasMany('App\DeviceCategories', 'category_id');
	    }
    }
?>