<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class MobileBroadband extends Model
    {
        protected $table = 'mobile_broadband';

        public function discounts()
        {
            return $this->hasOne('App\Discounts','id','broadband_device_id');
        }
    }
?>