<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempMobileDevices extends Model
    {
        protected $table = 'temp_mobile_devices';

        public function device_category()
	    {
	        return $this->belongsTo('App\TempMobileDevicesCategories', 'category_id');
	    }
    }
?>