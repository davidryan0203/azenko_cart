<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class CartTemplates extends Model
    {
        protected $table = 'cart_templates';

        public function getDataAttribute($value){
	        return json_decode($value, true);
	    }
    }
?>