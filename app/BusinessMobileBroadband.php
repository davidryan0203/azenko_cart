<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class BusinessMobileBroadband extends Model
    {
        protected $table = 'business_mobile_broadband';
        
        public function discounts()
        {
            return $this->hasOne('App\Discounts','id','broadband_device_id');
        }
    }
?>