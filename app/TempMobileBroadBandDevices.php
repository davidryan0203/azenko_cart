<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempMobileBroadBandDevices extends Model
    {
        protected $table = 'temp_mobile_broadband';

        public function device_category()
	    {
	        return $this->belongsTo('App\TempMobileBroadBandCategories', 'category_id');
	    }
    }
?>