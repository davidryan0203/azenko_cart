<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempBusinessMobileDevicesCategories extends Model
    {
        protected $table = 'temp_business_mobile_devices_category';

        public function categories()
	    {
	        return $this->hasMany('App\DeviceCategories', 'category_id');
	    }
    }
?>