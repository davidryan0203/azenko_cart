<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class MobileDevices extends Model
    {
        protected $table = 'mobile_devices';

        public function device_category()
	    {
	        return $this->belongsTo('App\MobileDevicesCategories', 'category_id');
	    }

        public function discounts()
        {
            return $this->hasOne('App\Discounts','mobile_device_id');
        }
    }
?>