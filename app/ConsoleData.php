<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class ConsoleData extends Model
    {
        protected $table = 'consoles';

     //    protected $casts = [
	    //     'data' => 'array',
	    // ];

        public function getDataAttribute($value){
	        return json_decode($value, true);
	    }
    }
?>