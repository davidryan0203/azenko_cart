<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class AdminPromotions extends Model
    {
        protected $table = 'promotions';

        public function mobile_devices()
	    {
	        return $this->belongsTo('App\MobileDevices', 'device_id');
	    }

        public function mobile_broadband()
        {
            return $this->belongsTo('App\MobileBroadband', 'broadband_device_id');
        }

        public function getVasAttribute($value){
            return json_decode($value, true);
        }

        public function getPlanTypeAttribute($value){
            return json_decode($value, true);
        }

        public function getTypeAttribute($value){
            return json_decode($value, true);
        }

        public function getCategoryAttribute($value){
            return json_decode($value, true);
        }

        public function getAroDeviceAttribute($value){
            return json_decode($value, true);
        }
    }
?>