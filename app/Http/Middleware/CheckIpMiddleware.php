<?php
   
namespace App\Http\Middleware;
   
use Closure;
use App\IPAddress;

class CheckIpMiddleware
{
    
    public $whiteIps = ['110.141.230.190','111.125.111.41','111.125.121.150','121.210.77.72','120.29.91.116','111.125.123.32'];
        
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

    public function handle($request, Closure $next)
    {
        session_start();
        //dd($_SESSION);
        //$ipAddress = IPAddress::all()->pluck('ip_address')->toArray();
        //if(!in_array($this->getIp(), $ipAddress)) {
            /*
                 You can redirect to any error page. 
            */
            //return response()->json(['your ip address is not valid.']);
        //}
        if(!isset($_SESSION['userInfo'])){
            session_unset();
            session_destroy();
            return redirect('/sso-logout');
        }

        if($_SESSION['userInfo']['email'] == 'cassie@azenko.com.au' || $_SESSION['userInfo']['email'] == 'trent@azenko.com.au' || $_SESSION['userInfo']['email'] == 'KymB@tlsbendigo.com.au' || $_SESSION['userInfo']['email'] == 'rachel@azenko.com.au' || $_SESSION['userInfo']['email'] == 'Repairs@tlsbendigo.com.au' || $_SESSION['userInfo']['email'] == 'jasonj@tlsbendigo.com.au'){
            return $next($request);
        }
        if($_SESSION['userInfo']['role'] != 'Administrator' && $_SESSION['userInfo']['role'] != 'SuperAdmin'){
            return redirect('/home');
        }
       
    
        return $next($request);
    }
}