<?php
   
namespace App\Http\Middleware;
   
use Closure;
   
class IpMiddleware
{
    
    public $restrictIps = ['111.125.111.41'];
        
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->ip(), $this->restrictIps)) {
    
            return response()->json(['you dont have permission to access this application.']);
        }
    
        return $next($request);
    }
}