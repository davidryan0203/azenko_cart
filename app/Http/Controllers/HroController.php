<?php
namespace App\Http\Controllers;

use App\Hro;
use App\ConsoleHRO;
use App\MobileBroadbandCategories;
use App\MobileBroadband;
use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;

class HroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        session_start();
        return view('mobile_plans.hro');  
    }

    public function businessHRO()
    {
        session_start();
        return view('mobile_plans.business-hro');  
    }

    public function uploadConsumerHro(){
        session_start();
        return view('admin.uploads.consumer-hro');  
    }

    public function uploadConsoleHro(){
        session_start();
        return view('admin.uploads.console-hro');  
    }

    public function getConsumerHro(){
        $hro = Hro::all();
        return $hro;
    }

    public function getConsoleHro(){
        $hro = ConsoleHRO::all();
        return $hro;
    }

    public function rejectConsumerHro(){
        Hro::query()->truncate();
        $hro = Hro::all();
        return $hro;
    }

    public function rejectConsoleHro(){
        ConsoleHRO::query()->truncate();
        $hro = ConsoleHRO::all();
        return $hro;
    }

    public function uploadConsumerHroData(Request $request){
        $input = $request->all();
        //dd($input);
        Hro::query()->truncate();
        $fileData = $request->file('link');   
        //dd($file);
            config(['excel.import.startRow' => 1]);
        \Excel::load($fileData, function($reader) {
            $file = $reader->get()->toArray();
            foreach ($file as $key => $data) {
                if($data['product_name'] && $data['product_codes']){
                    $hro = new Hro;
                    $hro->name = $data['product_name'];
                    $hro->links = $data['link'];
                    $hro->category = $data['category'];
                    $hro->product_code = $data['product_codes'];
                    $hro->dbp_ex_gst = number_format( floatval(str_replace(['US$', ' ',','], '', $data['dbp_ex_gst'])) ,2);
                    $hro->rrp = number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp'])) ,2);
                    //$hro->price_24_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2);
                    $hro->price_24_months = ($data['24_months'] != 'N/A') ? number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2) : 0;
                    $hro->price_12_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['12_months'])) ,2);
                    $hro->save();
                }
            }
            //dd($numbers);
        });
    }
    
    public function uploadConsoleHroData(Request $request){
        $input = $request->all();
        //dd($input);
        ConsoleHRO::query()->truncate();
        $fileData = $request->file('link');   
        //dd($file);
            config(['excel.import.startRow' => 1]);
        \Excel::load($fileData, function($reader) {
            $file = $reader->get()->toArray();
            foreach ($file as $key => $data) {
                if($data['product_name'] && $data['product_codes']){
                    $hro = new ConsoleHRO;
                    $hro->name = $data['product_name'];
                    $hro->links = $data['link'];
                    $hro->category = $data['category'];
                    $hro->product_code = $data['product_codes'];
                    $hro->dbp_ex_gst = number_format( floatval(str_replace(['US$', ' ',','], '', $data['dbp_ex_gst'])) ,2);
                    $hro->rrp = number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp'])) ,2);
                    //$hro->price_24_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2);
                    $hro->price_24_months = ($data['24_months'] != 'N/A') ? number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2) : 0;
                    $hro->price_12_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['12_months'])) ,2);
                    $hro->save();
                }
            }
            //dd($numbers);
        });
    }
}