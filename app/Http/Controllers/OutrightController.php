<?php
namespace App\Http\Controllers;

use App\Orders;
use App\OutrightMobileDevices;
use App\OutrightAccessories;
use App\MobileDevices;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use App\CartTemplates;
use App\RedemptionProducts;
use PDF;
use App\BusinessMobileDevices;
use App\MobileBroadband;
use App\BusinessMobileBroadband;

class OutrightController extends Controller
{
	public function mobile()
    {
        return view('outright.mobile');
    }
    public function mobilebroadband()
    {
        return view('outright.mobile-broadband');
    }

    public function accessories()
    {
        return view('outright.accessories');
    }

    public function getOutrightMobileDevices(){
    	$devices = MobileDevices::all();
    	return $devices;
    }

    public function getBusinessMobileDevices(){
        $devices = BusinessMobileDevices::all();
        return $devices;
    }

    public function getOutrightMobileBroadbandDevices(){
        $devices = MobileBroadband::all();
        return $devices;
    }

    public function getBusinessMobileBroadbandDevices(){
        $devices = BusinessMobileBroadband::all();
        return $devices;
    }

    public function getOutrightAccessories(){
    	$devices = RedemptionProducts::all();
    	return $devices;
    }

    public function editOutrightAccessories(Request $request){
    	$input = $request->all();
    	$device = RedemptionProducts::where('id',$input['id'])->update(['rrp' => $input['price']]);
    	return 'success';
    }    

    public function editOutrightMobileDevices(Request $request){
    	$input = $request->all();
        //dd($input);
    	$device = MobileDevices::where('id',$input['id'])->update(['rrp_incl_gst' => $input['price'], 'mths_12_device_payment_contract' => $input['mths_12_device_payment_contract'],'mths_24_device_payment_contract' => $input['mths_24_device_payment_contract'] ]);
    	return 'success';
    }

    public function editBusinessMobileDevices(Request $request){
        $input = $request->all();
        $device = BusinessMobileDevices::where('id',$input['id'])->update(['rrp_incl_gst' => $input['price'], 'mths_12_device_payment_contract' => $input['mths_12_device_payment_contract'],'mths_24_device_payment_contract' => $input['mths_24_device_payment_contract'] ]);
        return 'success';
    }

    public function editOutrightMobileBroadbandDevices(Request $request){
        $input = $request->all();
        //dd($input);
        $device = MobileBroadband::where('id',$input['id'])->update(['rrp_incl_gst' => $input['price'], 'mths_12_device_payment_contract' => $input['mths_12_device_payment_contract'],'mths_24_device_payment_contract' => $input['mths_24_device_payment_contract'] ]);
        return 'success';
    }

    public function editBusinessMobileBroadbandDevices(Request $request){
        $input = $request->all();
        $device = BusinessMobileBroadband::where('id',$input['id'])->update(['rrp_incl_gst' => $input['price'], 'mths_12_device_payment_contract' => $input['mths_12_device_payment_contract'],'mths_24_device_payment_contract' => $input['mths_24_device_payment_contract'] ]);
        return 'success';
    }
}