<?php
namespace App\Http\Controllers;

use App\Discounts;
use App\AdminPromotions;
use App\BusinessMobileBroadband;
use App\MobileDevices;
use App\MobileBroadband;
use App\Hro;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DiscountsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.discounts');  
    }

    public function adminPromotions()
    {
        return view('admin.promotions');  
    }

    public function promotions()
    {
        session_start();
        return view('promotions');  
    }

    public function getDiscounts(){
        $discounts = AdminPromotions::where('is_active',1)->get()->sortByDesc('end_date');
        //dd($discounts);
        $now = Carbon::parse(Carbon::now())->startOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {

            if($value['is_end_date_enabled'] == 0 || $value['is_end_date_enabled'] == 1 && Carbon::parse($value['end_date'])->toDateTimeString() >= $now && Carbon::parse($value['start_date'])->toDateTimeString() <= $now){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    if($value['device_id'] != 999 && $value['device_id'] != 998){
                        $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                        //dd($value);
                    }

                    if($value['broadband_device_id'] != 0 && $value['broadband_device_id'] != 999 && $value['broadband_device_id'] != 998){
                        $value['mobile_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }

                    if($value['hardware_device_id'] != 0 && $value['hardware_device_id'] != 999 && $value['hardware_device_id'] != 998){
                        $value['mobile_devices'] = Hro::where('id', $value['hardware_device_id'])->first();
                        $value['hro_device'] = Hro::where('id', $value['hardware_device_id'])->first();
                    }
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    $value['broadband_devices'] = MobileBroadband::where('id', $value['device_id'])->first();
                }
                $results[] = $value;
            }
        }   
        $resultsData = [];
        foreach (collect($results)->groupBy('name')->toArray() as $key => $data) {
            $resultsData[] = ['name' => $key , 'data' => $data, 'id' => $key];
        }
        //dd($resultsData);
        return $results;
    }

    public function getArchivedAdminDiscounts(){
        //$discounts = AdminPromotions::with('mobile_devices')->where('is_active',1)->get();

        //$discounts = AdminPromotions::where('is_active',0)->get();
        $discounts = AdminPromotions::all();
        $now = Carbon::parse(Carbon::now())->endOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {
            // dd($now);
            // dd(Carbon::parse($value['end_date'])->toDateTimeString());
            if(Carbon::parse($value['end_date'])->toDateTimeString() < $now || $value['is_active'] == 0 && Carbon::parse($value['end_date'])->toDateTimeString() < $now){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    $value['broadband_devices'] = MobileBroadband::where('id', $value['device_id'])->first();
                }
                $results[] = $value;
            }
        }   
        //dd(collect($results)->toArray());
        $resultsData = [];
        foreach (collect($results)->groupBy('name')->toArray() as $key => $data) {
            $resultsData[] = ['name' => $key , 'data' => $data, 'id' => $key];
        }
        //dd($resultsData);
        return $resultsData;
        dd($resultsData);
        return ['name' => collect($results)->groupBy('name')->toArray()];
    }

    public function getAdminDiscounts(){
        $discounts = AdminPromotions::where('is_active',1)->get();
        $now = Carbon::parse(Carbon::now())->startOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {

            if($value['is_end_date_enabled'] == 0 || Carbon::parse($value['end_date'])->toDateTimeString() >= $now){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    if($value['device_id'] != 999 && $value['device_id'] != 998){
                        $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                        //dd($value);
                    }

                    if($value['broadband_device_id'] != 0 && $value['broadband_device_id'] != 999 && $value['broadband_device_id'] != 998){
                        $value['mobile_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }

                    if($value['hardware_device_id'] != 0 && $value['hardware_device_id'] != 999 && $value['hardware_device_id'] != 998){
                        $value['mobile_devices'] = Hro::where('id', $value['hardware_device_id'])->first();
                        $value['hro_device'] = Hro::where('id', $value['hardware_device_id'])->first();
                    }
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    if($value['is_business'] == 0){
                        $value['broadband_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }else{
                         $value['broadband_devices'] = BusinessMobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }
                }
                $results[] = $value;
            }
        }   
        $resultsData = [];
        foreach (collect($results)->groupBy('name')->toArray() as $key => $data) {
            $resultsData[] = ['name' => $key , 'data' => $data, 'id' => $key];
        }
        //dd($resultsData[74]);
        return $resultsData;
    }

    public function getAdminDiscountsByEndDate(){
        $discounts = AdminPromotions::where('is_active',1)->get();
        $now = Carbon::parse(Carbon::now())->startOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {

            if($value['is_end_date_enabled'] == 0 || Carbon::parse($value['end_date'])->toDateTimeString() >= $now){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    if($value['device_id'] != 999 && $value['device_id'] != 998){
                        $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                        //dd($value);
                    }

                    if($value['broadband_device_id'] != 0 && $value['broadband_device_id'] != 999 && $value['broadband_device_id'] != 998){
                        $value['mobile_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }

                    if($value['hardware_device_id'] != 0 && $value['hardware_device_id'] != 999 && $value['hardware_device_id'] != 998){
                        $value['mobile_devices'] = Hro::where('id', $value['hardware_device_id'])->first();
                        $value['hro_device'] = Hro::where('id', $value['hardware_device_id'])->first();
                    }
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    $value['broadband_devices'] = MobileBroadband::where('id', $value['device_id'])->first();
                }
                $results[] = $value;
            }
        }   
        $resultsData = [];
        foreach (collect($results)->groupBy(['end_date'])->toArray() as $key => $data) {
            $resultsData[] = ['name' => $key , 'data' => $data, 'id' => $key];
        }
        //dd($resultsData);
        return $resultsData;
    }

    public function getAdminDiscountsNoExpiry(){
        $discounts = AdminPromotions::where('is_active',1)->get();
        $now = Carbon::parse(Carbon::now())->startOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {

            if($value['is_end_date_enabled'] == 0){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    if($value['device_id'] != 999 && $value['device_id'] != 998){
                        $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                        //dd($value);
                    }

                    if($value['broadband_device_id'] != 0 && $value['broadband_device_id'] != 999 && $value['broadband_device_id'] != 998){
                        $value['mobile_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }

                    if($value['hardware_device_id'] != 0 && $value['hardware_device_id'] != 999 && $value['hardware_device_id'] != 998){
                        $value['mobile_devices'] = Hro::where('id', $value['hardware_device_id'])->first();
                        $value['hro_device'] = Hro::where('id', $value['hardware_device_id'])->first();
                    }
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    $value['broadband_devices'] = MobileBroadband::where('id', $value['device_id'])->first();
                }
                $results[] = $value;
            }
        }   
        $resultsData = [];
        foreach (collect($results)->groupBy('name')->toArray() as $key => $data) {
            $resultsData[] = ['name' => $key , 'data' => $data, 'id' => $key];
        }
        //dd($resultsData);
        return $resultsData;
    }

    public function getAdminDiscountsByType(){
        $discounts = AdminPromotions::where('is_active',1)->get();
        $now = Carbon::parse(Carbon::now())->startOfDay()->toDateTimeString();
        $results = [];
        foreach ($discounts as $key => $value) {

            if($value['is_end_date_enabled'] == 0 || Carbon::parse($value['end_date'])->toDateTimeString() >= $now){ 
                if($value['category']['name'] == 'HRO'){
                    $value['hro_device'] = Hro::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Mobile'){
                    $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                }

                if($value['category']['name'] == 'Outright'){
                    if($value['device_id'] != 999 && $value['device_id'] != 998){
                        $value['mobile_devices'] = MobileDevices::where('id', $value['device_id'])->first();
                        //dd($value);
                    }

                    if($value['broadband_device_id'] != 0 && $value['broadband_device_id'] != 999 && $value['broadband_device_id'] != 998){
                        $value['mobile_devices'] = MobileBroadband::where('id', $value['broadband_device_id'])->first();
                    }

                    if($value['hardware_device_id'] != 0 && $value['hardware_device_id'] != 999 && $value['hardware_device_id'] != 998){
                        $value['mobile_devices'] = Hro::where('id', $value['hardware_device_id'])->first();
                        $value['hro_device'] = Hro::where('id', $value['hardware_device_id'])->first();
                    }
                }

                if($value['category']['name'] == 'Mobile Broadband'){
                    $value['broadband_devices'] = MobileBroadband::where('id', $value['device_id'])->first();
                }
                $results[] = $value;
            }
        }   
        $resultsData = [];

        foreach ($results as $key => $data) {
            $resultsData[$data['category']['name']][] = ['name' => $data['category']['name'] , 'data' => collect($data)->toArray(), 'id' => $key, 'type' => $data['category']['name']];
        }
        //dd($resultsData);

        foreach ($resultsData as $key => $value) {
            //dd($value);
            $typeResults[] = ['name' => $key, 'data' => $value,'id' => $key];
            
        }
        return $typeResults;
        dd($typeResults);
        dd(collect($resultsData)->toArray());
        return collect($resultsData)->values()->toArray();
        dd($resultsData);
        foreach (collect($resultsData)->groupBy('name')->toArray() as $key => $data) {
            //dd($data);
            $typeResults[] = $data;
        }
        dd(collect($typeResults)->unique('name')->toArray());
        dd(collect($resultsData)->groupBy('type')->values()->toArray());
        return collect($resultsData)->groupBy('type')->values()->toArray();
        return $resultsData;
    }

    public function addDiscount(Request $request){
        $input = $request->all();
        //dd($input);
        if($input['category']['name'] == 'Outright' && collect($input['aroAccessory'])->isEmpty() && collect($input['mobileDevice'])->isEmpty() && collect($input['broadBandDeviceID'])->isEmpty() && collect($input['hardwareDeviceID'])->isEmpty() && collect($input['id'])->isEmpty()){
            $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();

            $discount = new AdminPromotions;
        
            $discount->name = $input['name'];
            $discount->category = json_encode($input['category']);
            $discount->device_id =  998;
            $discount->vas = json_encode($input['vas']);
            $discount->plan_type = json_encode($input['planOption']);
            $discount->is_automatic =  $input['isAutomatic'];
            $discount->is_business = $input['isBusiness'];
            $discount->month_duration = $input['monthDuration'];
            $discount->is_end_date_enabled = $input['endDateEnabled'];
            $discount->is_credit =  $input['is_credit'];
            $discount->type = json_encode($input['promotionType']);
            $discount->discount_price = $input['discountPrice'];
            $discount->start_date = $effectiveStartDate;
            $discount->end_date = $effectiveUntilDate;
            $discount->save();
        }
        if(isset($input['aroAccessory']) && collect($input['aroAccessory'])->isNotEmpty()){
            if(empty($input['id'])){
                //foreach ($input['planOption'] as $key => $value) {
                    foreach ($input['aroAccessory'] as $key => $data) {
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  999;
                        $discount->aro_device = json_encode($data);
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                    //dd($input['mobileDevice']['id']);
                    
                //}
            }else{
                //dd();
                $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                if(isset($input['id']) && !empty($input['id'])){
                    $discount = AdminPromotions::where('id',$input['id'])->first();
                }else{
                    $discount = new AdminPromotions;
                }
                //$mobile = collect($input['mobileDevice'])->first();
                $discount->name = $input['name'];
                $discount->category = json_encode($input['category']);
                $discount->device_id =  999;
                $discount->aro_device = json_encode($data);
                $discount->vas = json_encode($input['vas']);
                $discount->plan_type = json_encode($input['planOption']);
                $discount->type = json_encode($input['promotionType']);
                $discount->discount_price = $input['discountPrice'];
                $discount->is_automatic =  $input['isAutomatic'];
                $discount->is_business = $input['isBusiness'];
                $discount->month_duration = $input['monthDuration'];
                $discount->is_end_date_enabled = $input['endDateEnabled'];
                $discount->is_credit =  $input['is_credit'];
                $discount->start_date = $effectiveStartDate;
                $discount->end_date = $effectiveUntilDate;
                $discount->save();
                
            }
        }

        if(isset($input['broadBandDeviceID']) && collect($input['broadBandDeviceID'])->isNotEmpty()){
            if(empty($input['id'])){
                //foreach ($input['planOption'] as $key => $value) {
                    foreach ($input['broadBandDeviceID'] as $key => $data) {
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  999;
                        $discount->hardware_device_id = 0;
                        $discount->broadband_device_id = $data['id'];
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                    //dd($input['mobileDevice']['id']);
                    
                //}
            }else{
                //dd();
                $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                if(isset($input['id']) && !empty($input['id'])){
                    $discount = AdminPromotions::where('id',$input['id'])->first();
                }else{
                    $discount = new AdminPromotions;
                }
                //$mobile = collect($input['mobileDevice'])->first();
                $discount->name = $input['name'];
                $discount->category = json_encode($input['category']);
                $discount->device_id =  999;
                $discount->broadband_device_id = $input['broadBandDeviceID'];
                $discount->vas = json_encode($input['vas']);
                $discount->plan_type = json_encode($input['planOption']);
                $discount->type = json_encode($input['promotionType']);
                $discount->discount_price = $input['discountPrice'];
                $discount->is_automatic =  $input['isAutomatic'];
                $discount->is_business = $input['isBusiness'];
                $discount->month_duration = $input['monthDuration'];
                $discount->is_end_date_enabled = $input['endDateEnabled'];
                $discount->is_credit =  $input['is_credit'];
                $discount->start_date = $effectiveStartDate;
                $discount->end_date = $effectiveUntilDate;
                $discount->save();
                
            }
        }

        if(isset($input['hardwareDeviceID']) && collect($input['hardwareDeviceID'])->isNotEmpty()){
            if(empty($input['id'])){
                //foreach ($input['planOption'] as $key => $value) {
                    foreach ($input['hardwareDeviceID'] as $key => $data) {
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  999;
                        $discount->broadband_device_id = 0;
                        $discount->hardware_device_id = $data['id'];
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                    //dd($input['mobileDevice']['id']);
                    
                //}
            }else{
                //dd();
                $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                if(isset($input['id']) && !empty($input['id'])){
                    $discount = AdminPromotions::where('id',$input['id'])->first();
                }else{
                    $discount = new AdminPromotions;
                }
                //$mobile = collect($input['mobileDevice'])->first();
                $discount->name = $input['name'];
                $discount->category = json_encode($input['category']);
                $discount->device_id =  999;
                $discount->hardware_device_id = $input['hardwareDeviceID'];
                $discount->vas = json_encode($input['vas']);
                $discount->plan_type = json_encode($input['planOption']);
                $discount->type = json_encode($input['promotionType']);
                $discount->discount_price = $input['discountPrice'];
                $discount->is_automatic =  $input['isAutomatic'];
                $discount->is_business = $input['isBusiness'];
                $discount->month_duration = $input['monthDuration'];
                $discount->is_end_date_enabled = $input['endDateEnabled'];
                $discount->is_credit =  $input['is_credit'];
                $discount->start_date = $effectiveStartDate;
                $discount->end_date = $effectiveUntilDate;
                $discount->save();
                
            }
        }

        if(collect($input['mobileDevice'])->isNotEmpty()){
            if(empty($input['id'])){
                //foreach ($input['planOption'] as $key => $value) {
                if($input['category']['name'] == 'Mobile Broadband'){
                    foreach ($input['mobileDevice'] as $key => $data) {
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->broadband_device_id =  $data['id'];
                        $discount->device_id =  1000;
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                }else{
                    foreach ($input['mobileDevice'] as $key => $data) {
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  $data['id'];
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                    //dd($input['mobileDevice']['id']);
                }
                //}
            }else{
                //dd();
                $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                if(isset($input['id']) && !empty($input['id'])){
                    $discount = AdminPromotions::where('id',$input['id'])->first();
                }else{
                    $discount = new AdminPromotions;
                }
                //$mobile = collect($input['mobileDevice'])->first();
                $discount->name = $input['name'];
                $discount->category = json_encode($input['category']);
                $discount->device_id =  $input['mobileDevice']['id'];
                $discount->vas = json_encode($input['vas']);
                $discount->plan_type = json_encode($input['planOption']);
                $discount->type = json_encode($input['promotionType']);
                $discount->discount_price = $input['discountPrice'];
                $discount->is_automatic =  $input['isAutomatic'];
                $discount->is_business = $input['isBusiness'];
                $discount->month_duration = $input['monthDuration'];
                $discount->is_end_date_enabled = $input['endDateEnabled'];
                $discount->is_credit =  $input['is_credit'];
                $discount->start_date = $effectiveStartDate;
                $discount->end_date = $effectiveUntilDate;
                $discount->save();
                
            }
        }else{
            if($input['category']['name'] == 'Mobile'){
                if(empty($input['id'])){
                    if(isset($input['planOption'][0]) && is_array($input['planOption'][0])){
                        //dd("with device");
                        // foreach ($input['planOption'] as $key => $value) {
                        //     $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        //     $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
             
                        //     $discount = new AdminPromotions;
                        
                        //     $discount->name = $input['name'];
                        //     $discount->category = json_encode($input['category']);
                        //     $discount->device_id =  0;
                        //     $discount->vas = json_encode($input['vas']);
                        //     $discount->plan_type = json_encode($value);
                        //     $discount->type = json_encode($input['promotionType']);
                        //     $discount->discount_price = $input['discountPrice'];
                        //     $discount->start_date = $effectiveStartDate;
                        //     $discount->end_date = $effectiveUntilDate;
                        //     $discount->save();
                        // }
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
         
                        $discount = new AdminPromotions;
                    
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  0;
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }else{
                        //dd("no device");
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
             
                            if(isset($input['id']) && !empty($input['id'])){
                                $discount = AdminPromotions::where('id',$input['id'])->first();
                            }else{
                                $discount = new AdminPromotions;
                            }
                        
                            $discount->name = $input['name'];
                            $discount->category = json_encode($input['category']);
                            $discount->device_id =  $input['device_id'];
                            $discount->vas = json_encode($input['vas']);
                            $discount->is_automatic =  $input['isAutomatic'];
                            $discount->is_business = $input['isBusiness'];
                            $discount->month_duration = $input['monthDuration'];
                            $discount->is_end_date_enabled = $input['endDateEnabled'];
                            $discount->plan_type = json_encode($input['planOption']);
                            $discount->type = json_encode($input['promotionType']);
                            $discount->is_credit =  $input['is_credit'];
                            $discount->discount_price = $input['discountPrice'];
                            $discount->start_date = $effectiveStartDate;
                            $discount->end_date = $effectiveUntilDate;
                            $discount->save();
                    }
                }else{
                    $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                    $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                    if(isset($input['id']) && !empty($input['id'])){
                        $discount = AdminPromotions::where('id',$input['id'])->first();
                    }else{
                        $discount = new AdminPromotions;
                    }
                    //$mobile = collect($input['mobileDevice'])->first();
                    $discount->name = $input['name'];
                    $discount->category = json_encode($input['category']);
                    $discount->device_id =  0;
                    $discount->is_automatic =  $input['isAutomatic'];
                    $discount->is_business = $input['isBusiness'];
                    $discount->month_duration = $input['monthDuration'];
                    $discount->is_end_date_enabled = $input['endDateEnabled'];
                    $discount->vas = json_encode($input['vas']);
                    $discount->plan_type = json_encode($input['planOption']);
                    $discount->type = json_encode($input['promotionType']);
                    $discount->discount_price = $input['discountPrice'];
                    $discount->is_credit =  $input['is_credit'];
                    $discount->start_date = $effectiveStartDate;
                    $discount->end_date = $effectiveUntilDate;
                    $discount->save();
                }
            }elseif($input['category']['name'] == 'HRO'){
                //dd($input);
                if(collect($input['hroDevice'])->isNotEmpty()){
                    if(empty($input['id'])){
                        foreach ($input['hroDevice'] as $key => $data) {
                            $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
             
                            $discount = new AdminPromotions;
                        
                            $discount->name = $input['name'];
                            $discount->category = json_encode($input['category']);
                            $discount->device_id =  $data['id'];
                            $discount->vas = json_encode($input['vas']);
                            $discount->plan_type = '';
                            $discount->type = json_encode($input['promotionType']);
                            $discount->discount_price = $input['discountPrice'];
                            $discount->is_automatic =  $input['isAutomatic'];
                            $discount->is_business = $input['isBusiness'];
                            $discount->month_duration = $input['monthDuration'];
                            $discount->is_end_date_enabled = $input['endDateEnabled'];
                            $discount->is_credit =  $input['is_credit'];
                            $discount->start_date = $effectiveStartDate;
                            $discount->end_date = $effectiveUntilDate;
                            $discount->save();
                        }
                            
                        
                    }else{
                        $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                        if(isset($input['id']) && !empty($input['id'])){
                            $discount = AdminPromotions::where('id',$input['id'])->first();
                        }else{
                            $discount = new AdminPromotions;
                        }
                        //$mobile = collect($input['mobileDevice'])->first();
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  $input['hroDevice']['id'];
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                    }
                }else{
                    $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                        $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                        if(isset($input['id']) && !empty($input['id'])){
                            $discount = AdminPromotions::where('id',$input['id'])->first();
                        }else{
                            $discount = new AdminPromotions;
                        }
                        //$mobile = collect($input['mobileDevice'])->first();
                        $discount->name = $input['name'];
                        $discount->category = json_encode($input['category']);
                        $discount->device_id =  0;
                        $discount->vas = json_encode($input['vas']);
                        $discount->plan_type = json_encode($input['planOption']);
                        $discount->type = json_encode($input['promotionType']);
                        $discount->discount_price = $input['discountPrice'];
                        $discount->is_automatic =  $input['isAutomatic'];
                        $discount->is_business = $input['isBusiness'];
                        $discount->month_duration = $input['monthDuration'];
                        $discount->is_end_date_enabled = $input['endDateEnabled'];
                        $discount->is_credit =  $input['is_credit'];
                        $discount->start_date = $effectiveStartDate;
                        $discount->end_date = $effectiveUntilDate;
                        $discount->save();
                }
            }elseif($input['category']['name'] == 'Outright'){

            }else{
                $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
                $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();
                if(isset($input['id']) && !empty($input['id'])){
                    $discount = AdminPromotions::where('id',$input['id'])->first();
                }else{
                    $discount = new AdminPromotions;
                }
                //dd($input);
                //dd($input['mobileDevice']['id']);
                $discount->name = $input['name'];
                $discount->category = json_encode($input['category']);
                $discount->device_id = 0;
                $discount->vas = json_encode($input['vas']);
                if(isset($input['fixedList']) && !empty($input['fixedList'])){
                    $discount->plan_type = json_encode($input['fixedList']);
                }else{
                    $discount->plan_type = json_encode($input['planOption']);
                }

                if(isset($input['entertainmentList']) && !empty($input['entertainmentList'])){
                    $discount->plan_type = json_encode($input['entertainmentList']);
                }else{
                    $discount->plan_type = json_encode($input['planOption']);
                }
                
                $discount->type = json_encode($input['promotionType']);
                $discount->discount_price = $input['discountPrice'];
                $discount->is_automatic =  $input['isAutomatic'];
                $discount->is_business = $input['isBusiness'];
                $discount->month_duration = $input['monthDuration'];
                $discount->is_end_date_enabled = $input['endDateEnabled'];
                $discount->is_credit =  $input['is_credit'];
                $discount->start_date = $effectiveStartDate;
                $discount->end_date = $effectiveUntilDate;
                $discount->description = $input['description'];
                $discount->save();
            }
        }

        if($input['category']['name'] == 'Outright' && collect($input['id'])->isNotEmpty() && isset($input['mobileDevice']) && collect($input['mobileDevice'])->isNotEmpty()){
            $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();

            $discount = AdminPromotions::where('id',$input['id'])->first();
        
            $discount->name = $input['name'];
            $discount->device_id =  $input['mobileDevice']['id'];
            $discount->is_credit =  $input['is_credit'];
            $discount->type = json_encode($input['promotionType']);
            $discount->discount_price = $input['discountPrice'];
            $discount->start_date = $effectiveStartDate;
            $discount->end_date = $effectiveUntilDate;
            $discount->save();
        }

        if($input['category']['name'] == 'Outright' && collect($input['id'])->isNotEmpty() && isset($input['hardwareDevice']) && collect($input['hardwareDevice'])->isNotEmpty()){
            $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();

            $discount = AdminPromotions::where('id',$input['id'])->first();
        
            $discount->name = $input['name'];
            $discount->hardware_device_id =  $input['hardwareDevice']['id'];
            $discount->is_credit =  $input['is_credit'];
            $discount->type = json_encode($input['promotionType']);
            $discount->discount_price = $input['discountPrice'];
            $discount->start_date = $effectiveStartDate;
            $discount->end_date = $effectiveUntilDate;
            $discount->save();
        }

        if($input['category']['name'] == 'Outright' && collect($input['id'])->isNotEmpty() && isset($input['broadbandDevice']) && collect($input['broadbandDevice'])->isNotEmpty()){
            $effectiveStartDate = Carbon::parse($input['startDate'])->toDateTimeString();
            $effectiveUntilDate = Carbon::parse($input['effectiveDate'])->toDateTimeString();

            $discount = AdminPromotions::where('id',$input['id'])->first();
        
            $discount->name = $input['name'];
            $discount->broadband_device_id =  $input['broadbandDevice']['id'];
            $discount->is_credit =  $input['is_credit'];
            $discount->type = json_encode($input['promotionType']);
            $discount->discount_price = $input['discountPrice'];
            $discount->start_date = $effectiveStartDate;
            $discount->end_date = $effectiveUntilDate;
            $discount->save();
        }
    }

    public function removeDiscount(Request $request){
        $input = $request->all();
        $discount = AdminPromotions::where('id',$input['id'])->update(['is_active' => 0]);
        return 'success';
    }
}