<?php
namespace App\Http\Controllers;

use App\Orders;
use App\MobileDevices;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use App\CartTemplates;
use App\RedemptionProducts;
use App\AccessorySupplierCodes;
use App\ConsoleData;
use PDF;

class OrderController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        session_start();
        return view('sim_plans.index');  
    }

    public function save(Request $request){
        $input = $request->all();

        $test = json_decode(file_get_contents('js/order.json'), true); 

        //dd($test);
        $results =[];
        foreach ($test as $key => $data) {
            if(($key == 'cart')){
                foreach ($data as $key => $plan) {
                    $results['mobilePlan'][] = ['planDescription' => $plan['planType'], 'planValue' => $plan['planValue'], 'device' => $plan['make'].' '.$plan['model'].' '.$plan['capacity'], 'totalPrice' => $plan['totalPrice'], 'DPC' => ($plan['months'] == 24) ? $plan['mths24Contract'] : $plan['mths36Contract'], 'ARO' => $plan['aroBase'], 'VAS' => $plan['vas'] ];
                }
            }

            if(($key == 'hro')){
                 foreach ($data as $key => $plan) {

                 }
            }
        }
        dd($test);
    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

    public function getIPAddress(Request $request){
        $ip = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));

        dd($this->getIp());
        dd($ip);
    }

    public function sendCartTemplate(Request $request){
        session_start();
        $input = $request->all();
        //$serializeData = (json_encode(['id' => []]));
        $serializeData = (json_encode($input));
        //$cart = new ConsoleData;
        $cart = new CartTemplates;
        $cart->data = $serializeData;
        $cart->sender = $_SESSION['userInfo']['email'];
        $cart->recipient = $input['branchUser'];
        $cart->store = $_SESSION['currentBranch'];
        $cart->save();
        return $cart;
    }

    public function useCartTemplate(Request $request){
        session_start();

        unset($_SESSION['currentCartTemplate']);
        $input = $request->all();
        $cart = CartTemplates::where(['id' => $input['data']])->first();
        $cart->is_used = 1;
        $cart->save();
        $_SESSION['currentCartTemplate'] = collect($cart)->toArray();
        return \Redirect('/cart-items'); 
    }

    public function clearCart(Request $request){
        session_start();
        unset($_SESSION['currentCartTemplate']);
        unset($_SESSION['cartTemplate']);
        return 'success';
    }

    public function verifyTemplateChange(Request $request){
        session_start();
        if(isset($_SESSION['cartTemplate'])){
            $cart = $_SESSION['cartTemplate'];
            CartTemplates::where('id',$cart['id'])->update(['is_modified' => 1]);
            unset($_SESSION['cartTemplate']);
        }
    }

    public function telstraRedemptionPage(){
        session_start();
        return view('redemptions.telstra-redemption');  
    }

    public function getMasterListAccessories(Request $request){
        $accessoriesSupplierCoded = AccessorySupplierCodes::all();
        return collect($accessoriesSupplierCoded)->values()->toArray();
    }

    public function uploadAccessorySupplierCodes(){
        session_start();
        return collect($_SESSION['productList'])->values()->toArray();
    }

    public function getRedemptionProducts(Request $request){
        session_start();
        $redemptionProducts = RedemptionProducts::all();

        $accessoriesSupplierCoded = AccessorySupplierCodes::all();

        // dd(collect($accessoriesSupplierCoded)->values()->toArray());
        $products = [];
        // foreach ($accessoriesSupplierCoded as $key => $value) {
        //     $products[] = collect($_SESSION['productList'])->where('ProductCode', $value['rims_id']);
        // }

        foreach (collect($_SESSION['productList'])->values()->toArray() as $key => $value) {
            $products[] = collect($accessoriesSupplierCoded)->where('rims_id', $value['ProductCode']);
        }

        $results = [];
        foreach ($products as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $results[] = $data;
                }
                
            }
        }
        //dd(collect($results)->toArray());
        $finalResults = [];
        foreach ($results as $key => $value) {
            $finalResults[] = collect($redemptionProducts)->where('sku', $value['sku']);
        }

        $res = [];
        foreach ($finalResults as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $res[] = $data;
                }
            }
        }

        //dd(collect($res)->toArray());
        return collect($res)->toArray();
    }

    public function getRedemptionProductsConsole(Request $request){
        session_start();
        $redemptionProducts = RedemptionProductsConsole::all();

        $accessoriesSupplierCoded = AccessorySupplierCodes::all();

        // dd(collect($accessoriesSupplierCoded)->values()->toArray());
        $products = [];
        // foreach ($accessoriesSupplierCoded as $key => $value) {
        //     $products[] = collect($_SESSION['productList'])->where('ProductCode', $value['rims_id']);
        // }

        foreach (collect($_SESSION['productList'])->values()->toArray() as $key => $value) {
            $products[] = collect($accessoriesSupplierCoded)->where('rims_id', $value['ProductCode']);
        }

        $results = [];
        foreach ($products as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $results[] = $data;
                }
                
            }
        }
        //dd(collect($results)->toArray());
        $finalResults = [];
        foreach ($results as $key => $value) {
            $finalResults[] = collect($redemptionProducts)->where('sku', $value['sku']);
        }

        $res = [];
        foreach ($finalResults as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $res[] = $data;
                }
            }
        }

        //dd(collect($res)->toArray());
        return collect($res)->toArray();
    }

    public function getRedemptionUniqueProducts(Request $request){
        session_start();
        $redemptionProducts = RedemptionProducts::all();

        $accessoriesSupplierCoded = AccessorySupplierCodes::all();

        // dd(collect($accessoriesSupplierCoded)->values()->toArray());
        $products = [];
        // foreach ($accessoriesSupplierCoded as $key => $value) {
        //     $products[] = collect($_SESSION['productList'])->where('ProductCode', $value['rims_id']);
        // }

        foreach (collect($_SESSION['productList'])->values()->toArray() as $key => $value) {
            $products[] = collect($accessoriesSupplierCoded)->where('rims_id', $value['ProductCode']);
        }

        $results = [];
        foreach ($products as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $results[] = $data;
                }
                
            }
        }
        //dd(collect($results)->toArray());
        $finalResults = [];
        foreach ($results as $key => $value) {
            $finalResults[] = collect($redemptionProducts)->where('sku', $value['sku']);
        }

        $res = [];
        foreach ($finalResults as $key => $value) {
            if(collect($value)->isNotEmpty()){
                foreach ($value as $key => $data) {
                    $res[] = $data;
                }
            }
        }

        //dd(collect($res)->toArray());
        return collect($res)->unique('product')->values()->toArray();
    }

    public function getRedemptionProductsCategories(Request $request){
        $products = [];
        $products = RedemptionProducts::all()->pluck('product3');
        return collect($products)->unique()->values()->toArray();
    }

    public function deleteCartTemplate(Request $request){
        session_start();
        CartTemplates::where(['recipient' => $_SESSION['userInfo']['email']])->update(['is_used' => 1]);
        
        return \Redirect('/cart-items'); 
    }
}