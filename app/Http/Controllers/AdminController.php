<?php

namespace App\Http\Controllers;

use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\MobilePlanPage;
use App\MobileDevices;
use App\TempMobileDevices;
use App\DeviceCategories;
use App\MobileDevicesCategories;
use App\BusinessMobileDevices;
use App\BusinessMobilePlanPage;
use App\TempMobileDevicesCategories;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\AccessorySupplierCodes;
use App\Quicklinks;
use App\CartTemplates;
use App\ProposalTemplates;
use App\OfficeUseOnly;
use App\ConsoleData;
use App\AdminPromotions;
use App\MobileBroadbandPlanPage;
use App\BusinessMobileBroadbandPlanPage;

class AdminController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function ssoLogout(){
        session_start();
        session_unset();
        session_destroy();
        return \Redirect('https://login.azenko.com.au/account/logout'); 
    }

    public function admin()
    {
        if(isset($_SESSION['userInfo'])){
            return view('admin.index');
        }else{
            $this->ssoLogout();
        }
    }
    public function cartReports()
    {
        session_start();
        if(isset($_SESSION['userInfo'])){
            return view('cart-reports');
        }else{
            $this->ssoLogout();
        }
    }

    public function quicklinks()
    {
        //session_start();
        if(isset($_SESSION['userInfo'])){
            return view('admin.quicklinks');
        }else{
            $this->ssoLogout();
        }
    }    

    public function uploadPage()
    {
        if(!isset($_SESSION['userInfo'])){
            session_unset();
            session_destroy();
            return redirect('/sso-logout');
        }

        if(isset($_SESSION['userInfo'])){
            return view('admin.uploads.mobile-plans');  
        }else{
            $this->ssoLogout();
        }
    }

    public function mobilePlanPage(){
        if(isset($_SESSION['userInfo'])){
    	   $page = MobilePlanPage::all()->first();
    	   return view('admin.pages.mobile-plan')->with('page',$page);
        }else{
            $this->ssoLogout();
        }
    }

    public function broadbandPlanPage(){
        if(isset($_SESSION['userInfo'])){
            $page = MobileBroadbandPlanPage::all()->first();
            return view('admin.pages.broadband-plan')->with('page',$page);
        }else{
            $this->ssoLogout();
        }
    }


    public function businessBroadbandPlanPage(){
        if(isset($_SESSION['userInfo'])){
            $page = BusinessMobileBroadbandPlanPage::all()->first();
            return view('admin.pages.business-broadband-plan')->with('page',$page);
        }else{
            $this->ssoLogout();
        }
    }

    public function businessMobilePlanPage(){
        if(isset($_SESSION['userInfo'])){
            $page = BusinessMobilePlanPage::all()->first();
            return view('admin.pages.business-mobile-plan')->with('page',$page);
        }else{
            $this->ssoLogout();
        }
    }

    public function getMobilePlanCategory(){
        if(isset($_SESSION['userInfo'])){
    	   return view('admin.categories.mobile');
        }else{
            $this->ssoLogout();
        }
    }

    public function getBusinessMobilePlanCategory(){
        if(isset($_SESSION['userInfo'])){
            return view('admin.categories.business-mobile');
        }else{
            $this->ssoLogout();
        }
    }

    public function reports(){
        if(isset($_SESSION['userInfo'])){
            return view('reports');
        }else{
            $this->ssoLogout();
        }
    }

    public function editMobileBroadbandPlanPage(Request $request){
        $input = $request->all();
        $page = MobileBroadbandPlanPage::all()->first();

        $page->extraSmallPricing = $input['extraSmallPricing'];
        $page->smallPricing = $input['smallPricing'];
        $page->mediumPricing = $input['mediumPricing'];
        $page->largePricing = $input['largePricing'];
        $page->extraLargePricing = $input['extraLargePricing'];
        $page->extraSmallCapacity = $input['extraSmallCapacity'];
        $page->smallCapacity = $input['smallCapacity'];
        $page->mediumCapacity = $input['mediumCapacity'];
        $page->largeCapacity = $input['largeCapacity'];
        $page->extraLargeCapacity = $input['extraLargeCapacity'];
        //$page->content = $input['content'];
        $page->save();
        return redirect('pages/mobile-broadband')->with('status', 'Mobile Broadband Page updated!');
    } 

    public function editBusinessMobileBroadbandPlanPage(Request $request){
        $input = $request->all();
        $page = BusinessMobileBroadbandPlanPage::all()->first();

        $page->extraSmallPricing = $input['extraSmallPricing'];
        $page->smallPricing = $input['smallPricing'];
        $page->mediumPricing = $input['mediumPricing'];
        $page->largePricing = $input['largePricing'];
        $page->extraLargePricing = $input['extraLargePricing'];
        $page->extraSmallCapacity = $input['extraSmallCapacity'];
        $page->smallCapacity = $input['smallCapacity'];
        $page->mediumCapacity = $input['mediumCapacity'];
        $page->largeCapacity = $input['largeCapacity'];
        $page->extraLargeCapacity = $input['extraLargeCapacity'];
        //$page->content = $input['content'];
        $page->save();
        return redirect('pages/business-mobile-broadband')->with('status', 'Mobile Broadband Page updated!');
    } 

    public function getBusinessBroadbandPageContent(){
        $plan = BusinessMobileBroadbandPlanPage::all()->first();
        return $plan;
    }

    public function getBroadbandPageContent(){
        $plan = MobileBroadbandPlanPage::all()->first();
        return $plan;
    }

    public function editMobilePlanPage(Request $request){
    	$input = $request->all();
    	$page = MobilePlanPage::all()->first();

    	$page->extraSmallPricing = $input['extraSmallPricing'];
    	$page->smallPricing = $input['smallPricing'];
    	$page->mediumPricing = $input['mediumPricing'];
    	$page->largePricing = $input['largePricing'];
    	$page->extraLargePricing = $input['extraLargePricing'];
    	$page->extraSmallCapacity = $input['extraSmallCapacity'];
    	$page->smallCapacity = $input['smallCapacity'];
    	$page->mediumCapacity = $input['mediumCapacity'];
    	$page->largeCapacity = $input['largeCapacity'];
    	$page->extraLargeCapacity = $input['extraLargeCapacity'];
        //$page->content = $input['content'];
    	$page->save();
    	return redirect('admin/pages/mobile-plan')->with('status', 'Mobile Page updated!');
    } 

    public function editBusinessMobilePlanPage(Request $request){
        $input = $request->all();
        $page = BusinessMobilePlanPage::all()->first();
        //dd($input);
        $page->extraSmallPricing = $input['extraSmallPricing'];
        $page->smallPricing = $input['smallPricing'];
        $page->mediumPricing = $input['mediumPricing'];
        $page->largePricing = $input['largePricing'];
        $page->extraLargePricing = $input['extraLargePricing'];
        $page->extraSmallCapacity = $input['extraSmallCapacity'];
        $page->smallCapacity = $input['smallCapacity'];
        $page->mediumCapacity = $input['mediumCapacity'];
        $page->largeCapacity = $input['largeCapacity'];
        $page->extraLargeCapacity = $input['extraLargeCapacity'];
        //$page->content = $input['content'];
        $page->save();
        return redirect('admin/pages/business-mobile-plan')->with('status', 'Mobile Page updated!');
    } 

    public function getDeviceCategories($id){
    	$deviceCategories = DeviceCategories::where('category_id',$id)->get();
    	//dd($deviceCategories);
    	if($deviceCategories->isEmpty()){
    		$deviceCategories = DeviceCategories::where('category_id',0)->get();
    	}
    	return $deviceCategories;
    }

    public function updateDeviceCategory(Request $request){
    	$input = $request->all();
    	$device = MobileDevices::where('id', $input['mobileID'])->update(['device_category_id' => $input['categoryID']]);
    	return $device;
    }

    public function updateBusinessMobileDeviceCategory(Request $request){
        $input = $request->all();
        $device = BusinessMobileDevices::where('id', $input['mobileID'])->update(['device_category_id' => $input['categoryID']]);
        return $device;
    }

    public function rejectUpload(){
        TempMobileDevicesCategories::query()->truncate();
        TempMobileDevices::query()->truncate();

        return 'success';
    }

    public function scheduleUpload(Request $request){
        $input = $request->all();
        $categoryArray = [];
        $scheduleDate = Carbon::parse($input['schedule'])->toDateTimeString();
        // Storage::disk('schedules')->put('plans.pdf', file_get_contents($_FILES['link']['tmp_name']));

        // $outputCommand = exec('python3 /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/schedules/uploadSchedules.py');

        $test = \Excel::load(public_path().'/schedules/plans-page-1-tables-1.csv', function($reader)  use ($categoryArray) {
            $file = $reader->get()->toArray();
            foreach($file as $key => $value){
                if(strlen($value['make']) < 30 && !empty($value['make'])){
                    $results[] = $value;
                    $categories[] = ucfirst($value['make']);
                }
            }
            //save devices category
            $categoryResult = collect($categories)->unique()->values()->toArray();
            $i = 1;
            foreach($categoryResult as $k => $value){
                if($value != 'Make'){
                    $category['image_url'] = '/img/mobile_plans/Prod'.$i++.'.png';
                    $category['category_name'] = $value;
                    $categoryArray[] = $category;
                }
            }
            return $categoryArray;
            // foreach ($results as $key => $data) {
            //     if(($data['model']) && $data['make'] != 'Make'){
            //         $collection = collect($deviceCategories);
                    
            //         $category = $collection->firstWhere('category_name', ucfirst($data['make']));
            //         $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
            //         $device = new TempMobileDevices;
            //         $device->category_id = ($category) ? $category->id : 0;
            //         $device->make = $data['make'];
            //         $device->model = $data['model'];
            //         $device->capacity = $data['capacity'];
            //         $device->network = $data['network'];
            //         $device->blue_tick = $data['0'];
            //         $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
            //         $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', $data['12_months'])) ,2);
            //         $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', $data['24_months'])) ,2);
            //         $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', $data['36_months'])) ,2);
            //         $device->product_code = $data['product_code'];
            //     }
            // }
        });

        dd(collect($test)->toArray());
    }

    public function testUpload(Request $request){
        $input = $request->all();
        (Storage::disk('public')->put('consumer-mobiles-plans.pdf', file_get_contents($_FILES['link']['tmp_name'])));

        if(file_exists('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-1.csv')){
            \File::delete('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-1.csv');
        }

        if(file_exists('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-2.csv')){
            \File::delete('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-2.csv');
        }

        if(file_exists('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-3.csv')){
            \File::delete('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-3.csv');
        }

        if(file_exists('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-2-table-1.csv')){
            \File::delete('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-2-table-1.csv');
        }
    

        $process = exec('sudo -u ryan /home/ryan/.local/bin/camelot  --format csv --pages all  --output /var/www/okart/public/consumer-uploads/mobile/consumer-mobile.csv lattice /var/www/okart/public/consumer-mobiles-plans.pdf -scale 40 -shift r -shift b');

        TempMobileDevicesCategories::query()->truncate();
        TempMobileDevices::query()->truncate();


        if(file_exists('/var/www/okart/public/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-3.csv')){
            \Excel::load(public_path().'/consumer-uploads/mobile/consumer-mobile-page-1-table-3.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        }else{
            \Excel::load(public_path().'/consumer-uploads/mobile/consumer-mobile-page-1-table-3.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        }

        \Excel::load(public_path().'/consumer-uploads/mobile/consumer-mobile-page-2-table-1.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        $deviceCategories2 = TempMobileDevicesCategories::all();
        $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        TempMobileDevicesCategories::query()->truncate();
        $i = 1;

        foreach($catResults as $k => $catResult){
            $category = new TempMobileDevicesCategories;
            $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
            $category->category_name = $catResult['category_name'];
            $category->save();
        }

        $mobDevices = TempMobileDevices::all();
        $mobDevicesCategory = TempMobileDevicesCategories::all();
        foreach($mobDevices as $mobDevice){
            $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

            TempMobileDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        }
        return 'success';
    }

    public function uploadMobilePlans(Request $request){
        // MobileDevicesCategories::query()->truncate();
        MobileDevices::query()->truncate();

        
        // \Excel::load(public_path().'/consumer-uploads/mobile/consumer-mobile-plans-page-1-table-3.csv', function($reader) {
        //     $file = $reader->takeColumns(13)->toArray();
        //     foreach($file as $key => $value){
        //         if(strlen($value['make']) < 30 && !empty($value['make'])){
        //             $results[] = $value;
        //             $categories[] = ucfirst($value['make']);
        //         }
        //     }
        //     //save devices category
        //     $categoryResult = collect($categories)->unique()->values()->toArray();
        //     //dd($categoryResult);
        //     $i = 1;
        //     foreach($categoryResult as $k => $value){
        //         if($value != 'Make'){
        //             $category = new MobileDevicesCategories;
        //             $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
        //             $category->category_name = $value;
        //             $category->save();
        //         }
        //     }

        //     $deviceCategories = MobileDevicesCategories::all();
        //     $mobileDeviceCategories = DeviceCategories::all();
        //     foreach ($results as $key => $data) {
        //         if(($data['model']) && $data['make'] != 'Make'){
        //             $collection = collect($deviceCategories);
                    
        //             $category = $collection->firstWhere('category_name', ucfirst($data['make']));
        //             $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
        //             $device = new MobileDevices;
        //             $device->category_id = ($category) ? $category->id : 0;
        //             $device->device_category_id = ($category) ? $category->id : 0;
        //             $device->make = $data['make'];
        //             $device->model = $data['model'];
        //             $device->capacity = $data['capacity'];
        //             $device->network = $data['network'];
        //             $device->blue_tick = $data['0'];
        //             $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
        //             // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
        //             // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
        //             $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
        //             $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
        //             $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
        //             // $device->product_code = $data['product_code'];
        //             $device->save();
        //         }
        //     }
        // });
        // \Excel::load(public_path().'/consumer-uploads/mobile/consumer-mobile-plans-page-2-table-1.csv', function($reader) {
        //     $file = $reader->takeColumns(13)->toArray();
        //     foreach($file as $key => $value){
        //         if(strlen($value['make']) < 30 && !empty($value['make'])){
        //             $results[] = $value;
        //             $categories[] = ucfirst($value['make']);
        //         }
        //     }
        //     //save devices category
        //     $categoryResult = collect($categories)->unique()->values()->toArray();
        //     //dd($categoryResult);
        //     $i = 1;
        //     foreach($categoryResult as $k => $value){
        //         if($value != 'Make'){
        //             $category = new MobileDevicesCategories;
        //             $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
        //             $category->category_name = $value;
        //             $category->save();
        //         }
        //     }

        //     $deviceCategories = MobileDevicesCategories::all();
        //     $mobileDeviceCategories = DeviceCategories::all();
        //     foreach ($results as $key => $data) {
        //         if(($data['model']) && $data['make'] != 'Make'){
        //             $collection = collect($deviceCategories);
                    
        //             $category = $collection->firstWhere('category_name', ucfirst($data['make']));
        //             $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
        //             $device = new MobileDevices;
        //             $device->category_id = ($category) ? $category->id : 0;
        //             $device->device_category_id = ($category) ? $category->id : 0;
        //             $device->make = $data['make'];
        //             $device->model = $data['model'];
        //             $device->capacity = $data['capacity'];
        //             $device->network = $data['network'];
        //             $device->blue_tick = $data['0'];
        //             $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
        //             // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
        //             // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
        //             $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
        //             $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
        //             $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
        //             // $device->product_code = $data['product_code'];
        //             $device->save();
        //         }
        //     }
        // });
        // $deviceCategories2 = MobileDevicesCategories::all();
        // $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        // MobileDevicesCategories::query()->truncate();
        // $i = 1;

        // foreach($catResults as $k => $catResult){
        //     $category = new MobileDevicesCategories;
        //     $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
        //     $category->category_name = $catResult['category_name'];
        //     $category->save();
        // }

        // $mobDevices = MobileDevices::all();
        // $mobDevicesCategory = MobileDevicesCategories::all();
        // foreach($mobDevices as $mobDevice){
        //     $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

        //     MobileDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        // }

        // TempMobileDevicesCategories::query()->truncate();
        // TempMobileDevices::query()->truncate();
        // return 'success';
        $mobileDevices = TempMobileDevices::get();
        foreach ($mobileDevices as $key => $data) {
            $device = new MobileDevices;
            $device->category_id = $data['category_id'];
            $device->device_category_id = $data['device_category_id'];
            $device->make = $data['make'];
            $device->model = $data['model'];
            $device->capacity = $data['capacity'];
            $device->network = $data['network'];
            $device->blue_tick = $data['blue_tick'];
            $device->rrp_incl_gst = $data['rrp_incl_gst'];
            $device->mths_12_device_payment_contract = $data['mths_12_device_payment_contract'];
            $device->mths_24_device_payment_contract = $data['mths_24_device_payment_contract'];
            $device->mths_36_device_payment_contract = $data['mths_36_device_payment_contract'];
            $device->save();
        }

        $mobileDevices = TempMobileDevices::all();
        $promotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '0')->get();

        $businessPromotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '1')->get();
        //dd($promotionDevices);
        $deviceIds = [];
        $bMobileDeviceId = [];
        $mobileBroadbands = [];
        $bMobileBroadbands = [];
        foreach ($promotionDevices as $key => $data) {
            foreach ($mobileDevices as $key => $value) {
                if($data['mobile_devices']['model'].$data['mobile_devices']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['device_id' => $value['id']]);
                    $deviceIds[] = $data['id'];
                }
            }
        }

        return 'success';
    }

    public function getQuicklinks(Request $request){
        $quiclinks = [];
        $quiclinks = Quicklinks::where('is_active',1)->get();
        return $quiclinks;
    }

    public function addQuicklinks(Request $request){
        $input = $request->all();

        if(isset($input['id']) && $input['id'] != 0){
            $quicklink = Quicklinks::where('id', $input['id'])->first();
        }else{
            $quicklink = new Quicklinks;
        }

        $quicklink->name = $input['name'];
        $quicklink->url = $input['url'];
        $quicklink->save();
    }

    public function removeQuicklinks(Request $request){
        $input = $request->all();
        $discount = Quicklinks::where('id',$input['id'])->update(['is_active' => 0]);
        return 'success';
    }

    public function uploadAccessorySupplierCodes(Request $request){
        $input = $request->all();

        //AccessorySupplierCodes::query()->truncate();
        config(['excel.import.startRow' => 1]);
        \Excel::load(public_path().'/rims_id_06042020.xlsx', function($reader) {
            $file = $reader->toArray();
            
            foreach ($file as $key => $data) {
                $product = new AccessorySupplierCodes;
                if(!empty($data['rims'])){
                    $product->sku = $data['sku'];
                    $product->name = rtrim($data['product']);
                    $product->rims_id = $data['rims'];
                    $product->save();
                }
            }
        });
        return 'upload success';
    }

    public function accessoryList(Request $request){
        $masterList = AccessorySupplierCodes::all();
        $clickposStock = collect($_SESSION['productList'])->values()->toArray();

        return ['masterList' => $masterList, 'clickposStock' => $clickposStock];
    }

    public function getReports(Request $request){
        $input = $request->all();
        //dd($input);
        if(isset($input['currentBranch'])){
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->get()->groupBy('store');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->get()->groupBy('store');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->get()->count();
            $receivedCartTemplatesCount = CartTemplates::all()->count();

            $proposal = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->get()->groupBy('store');
            $ouo = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->get()->groupBy('store');
            $ouoCount = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->get()->count();

            $proposalCount = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->get()->count();

            $adjusted = CartTemplates::select('sender','recipient','store','is_modified')->where('is_modified', 1)->where('store', $input['currentBranch'])->get()->groupBy('recipient');
            $adjustedCount = CartTemplates::select('sender','recipient','store','is_modified')->where('is_modified', 1)->where('store', $input['currentBranch'])->get()->count();
        }else{
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->get()->groupBy('sender');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->get()->groupBy('recipient');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->get()->count();
            $receivedCartTemplatesCount = CartTemplates::all()->count();

            $proposal = ProposalTemplates::select('email','store')->get()->groupBy('email');
            $ouo = OfficeUseOnly::select('email','store')->get()->groupBy('email');
            $ouoCount = OfficeUseOnly::select('email','store')->get()->count();

            $proposalCount = ProposalTemplates::select('email','store')->get()->count();

            $adjusted = CartTemplates::select('sender','recipient','store','is_modified')->where('is_modified', 1)->get()->groupBy('recipient');
            $adjustedCount = CartTemplates::select('sender','recipient','store','is_modified')->where('is_modified', 1)->get()->count();
        }

        $sender = [];
        foreach ($sentCartTemplates as $key => $data) {
            $sender[] = ['email' => $key, 'count' => count($data)];
        }

        $receiver = [];
        foreach ($receivedCartTemplates as $key => $data) {
            $receiver[] = ['email' => $key, 'count' => count($data)];
        }

        $ouoResults = [];
        foreach ($ouo as $key => $data) {
            $ouoResults[] = ['email' => $key, 'count' => count($data)];
        }

        $adjustedResults = [];
        foreach ($adjusted as $key => $data) {
            $adjustedResults[] = ['email' => $key, 'count' => count($data)];
        }

        $proposalResults = [];
        foreach ($proposal as $key => $data) {
            $proposalResults[] = ['email' => $key, 'count' => count($data)];
        }

        return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount,'adjusted' => collect($adjustedResults)->sortByDesc('count')->values()->toArray(), 'adjustedTotal' => $adjustedCount];
    }

    public function getFilteredReports(Request $request){
        $input = $request->all();

        $dateFrom = Carbon::parse($input['dateFrom']);
        $dateTo = Carbon::parse($input['dateTo']);
        //dd($input);
        if(isset($input['currentBranch'])){
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->groupBy('store');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->groupBy('store');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->count();
            $receivedCartTemplatesCount = CartTemplates::all()->count();

            $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->groupBy('store');
            $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->groupBy('store');
            $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->count();

            $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->where('store', $input['currentBranch'])->get()->count();

            $adjusted = CartTemplates::select('sender','recipient','store','is_modified')->whereBetween('created_at',[$dateFrom, $dateTo])->where('is_modified', 1)->where('store', $input['currentBranch'])->get()->groupBy('recipient');
            $adjustedCount = CartTemplates::select('sender','recipient','store','is_modified')->whereBetween('created_at',[$dateFrom, $dateTo])->where('is_modified', 1)->where('store', $input['currentBranch'])->get()->count();
        }else{  
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('sender');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('recipient');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            $receivedCartTemplatesCount = CartTemplates::all()->count();

            $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
            $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
            $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $adjusted = CartTemplates::select('sender','recipient','store','is_modified')->whereBetween('created_at',[$dateFrom, $dateTo])->where('is_modified', 1)->get()->groupBy('recipient');
            $adjustedCount = CartTemplates::select('sender','recipient','store','is_modified')->whereBetween('created_at',[$dateFrom, $dateTo])->where('is_modified', 1)->get()->count();
            //dd($adjusted);
        }
          

        $sender = [];
        foreach ($sentCartTemplates as $key => $data) {
            $sender[] = ['email' => $key, 'count' => count($data)];
        }

        $receiver = [];
        foreach ($receivedCartTemplates as $key => $data) {
            $receiver[] = ['email' => $key, 'count' => count($data)];
        }

        $ouoResults = [];
        foreach ($ouo as $key => $data) {
            $ouoResults[] = ['email' => $key, 'count' => count($data)];
        }

        $adjustedResults = [];
        foreach ($adjusted as $key => $data) {
            $adjustedResults[] = ['email' => $key, 'count' => count($data)];
        }

        $proposalResults = [];
        foreach ($proposal as $key => $data) {
            $proposalResults[] = ['email' => $key, 'count' => count($data)];
        }

        return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount,'adjusted' => collect($adjustedResults)->sortByDesc('count')->values()->toArray(), 'adjustedTotal' => $adjustedCount];
    }

    public function getStoreType(Request $request){
        $sentCartTemplates = CartTemplates::select('sender','recipient','store')->get()->groupBy('store');
        $ouo = OfficeUseOnly::select('email','store')->get()->groupBy('email');
        $proposal = ProposalTemplates::select('email','store')->get()->groupBy('email');
    }

    public function exportReports(Request $request){
        $input = $request->all();

        $dateFrom = Carbon::parse($input[0]['dateFrom']);
        $dateTo = Carbon::parse($input[1]['dateTo']);
        $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('sender');
        $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('recipient');

        $sender = [];
        foreach ($sentCartTemplates as $key => $data) {
            $sender[] = ['email' => $key, 'count' => count($data)];
        }

        $receiver = [];
        foreach ($receivedCartTemplates as $key => $data) {
            $receiver[] = ['email' => $key, 'count' => count($data)];
        }

        $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
        $ouoResults = [];
        foreach ($ouo as $key => $data) {
            $ouoResults[] = ['email' => $key, 'count' => count($data)];
        }

        $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
        $proposalResults = [];

        foreach ($proposal as $key => $data) {
            $proposalResults[] = ['email' => $key, 'count' => count($data)];
        }

        \Excel::create('test', function($excel) use($sender,$receiver, $ouoResults, $proposalResults) {

            $excel->sheet('Sent Reports', function($sheet) use($sender) {
                $sheet->fromArray(collect($sender)->sortByDesc('count')->values()->toArray());
            });

            $excel->sheet('Received Reports', function($sheet) use($receiver) {
                $sheet->fromArray(collect($receiver)->sortByDesc('count')->values()->toArray());
            });

            $excel->sheet('Office use only Reports', function($sheet) use($ouoResults) {
                $sheet->fromArray(collect($ouoResults)->sortByDesc('count')->values()->toArray());
            });

            $excel->sheet('Proposal Reports', function($sheet) use($proposalResults) {
                $sheet->fromArray(collect($proposalResults)->sortByDesc('count')->values()->toArray());
            });
            
        })->export();
    }

    public function filterByCategory(Request $request){
        $input = $request->all();
        // $input['category'] = 'Store';
        // $input['dateFrom'] = new Carbon('first day of this month');
        // $input['dateTo'] = new Carbon('last day of this month');
        $dateFrom = Carbon::parse($input['dateFrom']);
        $dateTo = Carbon::parse($input['dateTo']);
        // $dateFrom = Carbon::parse($input['dateFrom']);
        // $dateTo = Carbon::parse($input['dateTo']);

        if($input['category'] == 'Client'){
            if(isset($input['currentBranch'])){
                $sentCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('sender');
                $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('recipient');
                $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $proposal = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
                $proposalCount = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $ouo = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
                $ouoCount = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            }else{
                $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('sender');
                $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('recipient');
                $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
                $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
                $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            }


            $proposalResults = [];

            $sender = [];
            foreach ($sentCartTemplates as $key => $data) {
                $sender[] = ['email' => $key, 'count' => count($data)];
            }

            $receiver = [];
            foreach ($receivedCartTemplates as $key => $data) {
                $receiver[] = ['email' => $key, 'count' => count($data)];
            }

            
            $ouoResults = [];
            foreach ($ouo as $key => $data) {
                $ouoResults[] = ['email' => $key, 'count' => count($data)];
            }

            

            foreach ($proposal as $key => $data) {
                $proposalResults[] = ['email' => $key, 'count' => count($data)];
            }

            return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount];
        }else{
            if(isset($input['currentBranch'])){
                $sentCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

                $ouo = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $ouoCount = OfficeUseOnly::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

                $proposal = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $proposalCount = ProposalTemplates::select('email','store')->where('store', $input['currentBranch'])->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            }else{
                $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
                $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

                $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

                $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
                $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            }

            $sender = [];
            foreach ($sentCartTemplates as $key => $data) {

                $sender[] = ['email' => $key, 'count' => count($data)];
            }

            $receiver = [];
            foreach ($receivedCartTemplates as $key => $data) {
                $receiver[] = ['email' => $key, 'count' => count($data)];
            }

            
            $ouoResults = [];
            foreach ($ouo as $key => $data) {
                $ouoResults[] = ['email' => $key, 'count' => count($data)];
            }
            
            $proposalResults = [];

            foreach ($proposal as $key => $data) {
                $proposalResults[] = ['email' => $key, 'count' => count($data)];
            }
            //dd($proposalResults);
            return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount];
        }
    }

    public function filterByCategoryClient(Request $request){
        $input = $request->all();
        // $input['category'] = 'Store';
        // $input['dateFrom'] = new Carbon('first day of this month');
        // $input['dateTo'] = new Carbon('last day of this month');
        $dateFrom = Carbon::parse($input['dateFrom']);
        $dateTo = Carbon::parse($input['dateTo']);
        // $dateFrom = Carbon::parse($input['dateFrom']);
        // $dateTo = Carbon::parse($input['dateTo']);

        if($input['category'] == 'Client'){
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('sender');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('recipient');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $sender = [];
            foreach ($sentCartTemplates as $key => $data) {
                $sender[] = ['email' => $key, 'count' => count($data)];
            }

            $receiver = [];
            foreach ($receivedCartTemplates as $key => $data) {
                $receiver[] = ['email' => $key, 'count' => count($data)];
            }

            $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
            $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            $ouoResults = [];
            foreach ($ouo as $key => $data) {
                $ouoResults[] = ['email' => $key, 'count' => count($data)];
            }

            $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('email');
            $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            $proposalResults = [];

            foreach ($proposal as $key => $data) {
                $proposalResults[] = ['email' => $key, 'count' => count($data)];
            }

            return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount];
        }else{
            $sentCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
            $receivedCartTemplates = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
            $sentCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();
            $receivedCartTemplatesCount = CartTemplates::select('sender','recipient','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $ouo = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
            $ouoCount = OfficeUseOnly::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $proposal = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->groupBy('store');
            $proposalCount = ProposalTemplates::select('email','store')->whereBetween('created_at',[$dateFrom, $dateTo])->get()->count();

            $sender = [];
            foreach ($sentCartTemplates as $key => $data) {

                $sender[] = ['email' => $key, 'count' => count($data)];
            }

            $receiver = [];
            foreach ($receivedCartTemplates as $key => $data) {
                $receiver[] = ['email' => $key, 'count' => count($data)];
            }

            
            $ouoResults = [];
            foreach ($ouo as $key => $data) {
                $ouoResults[] = ['email' => $key, 'count' => count($data)];
            }
            
            $proposalResults = [];

            foreach ($proposal as $key => $data) {
                $proposalResults[] = ['email' => $key, 'count' => count($data)];
            }
            //dd($proposalResults);
            return ['sender' => collect($sender)->sortByDesc('count')->values()->toArray(), 'receiver' => collect($receiver)->sortByDesc('count')->values()->toArray(), 'ouo' => collect($ouoResults)->sortByDesc('count')->values()->toArray(), 'proposal' => collect($proposalResults)->sortByDesc('count')->values()->toArray(), 'senderTotal' => $sentCartTemplatesCount, 'receiverTotal' => $receivedCartTemplatesCount, 'ouoTotal' => $ouoCount, 'proposalTotal' => $proposalCount];
        }
    }

    public function getConsoles(Request $request){
        $consoles = ConsoleData::all()->sortByDesc('id');
        foreach ($consoles as $key => $data) {
            # code...
            //dd($data['data']);
            $consoleData[] = ['consoles' => $data['data']]; 
        }
        //dd($consoleData);
        return collect($consoleData)->values()->toArray();
    }
}