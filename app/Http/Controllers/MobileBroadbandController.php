<?php
namespace App\Http\Controllers;

use PDF;
use App\Hro;
use App\MobileBroadbandCategories;
use App\MobileBroadband;
use App\BusinessMobileBroadbandCategories;
use App\BusinessMobileBroadband;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MobileBroadbandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        session_start();
        date_default_timezone_set('Asia/Manila');
        return view('mobile_plans.mobile-broadband');  
    }

    public function businessMobileBroadband()
    {
        session_start();
        date_default_timezone_set('Asia/Manila');
        return view('mobile_plans.business-mobile-broadband');  
    }

    public function getMobileBroadbandCategories(Request $request){
        $deviceCategories = MobileBroadbandCategories::all();
        return $deviceCategories;
    }

    public function getBusinessMobileBroadbandCategories(Request $request){
        $deviceCategories = BusinessMobileBroadbandCategories::all();
        return $deviceCategories;
    }

    public function getMobileBroadbands(Request $request){
        $input = $request->all();
        $devices = MobileBroadband::where('category_id', $input['id'])->get();
        $devices = $devices->reverse();
        $collection = collect($devices)->values()->toArray();
        return $collection;
    }

    public function getBusinessMobileBroadbands(Request $request){
        $input = $request->all();
        $devices = BusinessMobileBroadband::where('category_id', $input['id'])->get();
        $devices = $devices->reverse();
        $collection = collect($devices)->values()->toArray();
        return $collection;
    }

    public function getAllBroadbandDevices(Request $request){
        $input = $request->all();
        $devices = MobileBroadband::all();
        $devices = $devices->reverse();
        $collection = collect($devices)->values()->toArray();
        return $collection;
    }

    public function getAllBusinessBroadbandDevices(Request $request){
        $input = $request->all();
        $devices = BusinessMobileBroadband::all();
        $devices = $devices->reverse();
        $collection = collect($devices)->values()->toArray();
        return $collection;
    }

    public function uploadMobileBroadbandPlans(Request $request){
        \Excel::load('mobile_broadbands.xlsx', function($reader) {
            //truncate existing records
            MobileBroadbandCategories::query()->truncate();
            MobileBroadband::query()->truncate();

            // Getting all results
            $file = $reader->get()->toArray();

            foreach($file as $key => $value){
                $results[] = $value;
                $categories[] = 'Mobile Broadband Devices';   
                $categories[] = ucfirst($value['make']);
            }
            
            //save devices category
            $categoryResult = collect($categories)->unique()->values()->toArray();
            //dd($categoryResult);
            foreach($categoryResult as $key => $value){
                $category = new MobileBroadbandCategories;
                $category->category_name = $value;
                $category->image_url = '/img/mobile_plans/default.png';
                $category->save();
            }

            $deviceCategories = MobileBroadbandCategories::all();
           
            foreach ($results as $key => $data) {

                $collection = collect($deviceCategories);
                $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                //dd($category);
                $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp_inc_gst'])) ,2));
                $device = new MobileBroadband;
                //$device->category_id = ($data['capacity'] == 'NA') ? 1 : $category->id;
                $device->category_id = $category->id;
                $device->make = $data['make'];
                $device->model = $data['model'];
                $device->capacity = $data['capacity'];
                $device->network = $data['network'];
                $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp_inc_gst'])) ,2));
                $device->mths_24_incl_gst = number_format( floatval(str_replace('US$', '', $data['24_month_discount_inc_gst'])) ,2);
                $device->mths_36_incl_gst = number_format( floatval(str_replace('US$', '', $data['36_month_discount_inc_gst'])) ,2);
                $device->mths_24_device_payment_contract = number_format( floatval(str_replace('US$', '', $data['dpc_24_months'])) ,2);
                $device->mths_36_device_payment_contract = number_format( floatval(str_replace('US$', '', $data['dpc_36_months'])) ,2);
                $device->save();
            }
            //dd($numbers);
        });
    }

    public function uploadHro(Request $request){
        \Excel::load('hro.xlsx', function($reader) {
            //truncate existing records
            Hro::query()->truncate();

            // Getting all results
            $file = $reader->get()->toArray();

           

            foreach ($file as $key => $data) {
                //dd($data);
                $hro = new Hro;
                $hro->name = $data['product_name'];
                $hro->product_code = $data['product_codes'];
                $hro->dbp_ex_gst = number_format( floatval(str_replace(['US$', ' ',','], '', $data['dbp_ex_gst'])) ,2);
                $hro->rrp = number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp'])) ,2);
                $hro->price_24_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_motnhs'])) ,2);
                $hro->price_36_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['36_months'])) ,2);
                $hro->save();
            }
            //dd($numbers);
        });
    }

    public function checkout(Request $request){
        $input = $request->all();
        //dd($input);
        // $data['data'] = $input;
        // //dd($data);
        // $pdf = PDF::loadView('pdf.cart', $data);
        // return $pdf->download('cart.pdf');
        $data['data'] = $input;
        
        //dd($commissiongstTotal);
        $pdf = PDF::loadView('pdf.cart', $data);
        //dd("123");
        $randStr = sha1(time());
        $randTime = Carbon::now()->timestamp;
        //dd(storage_path());
        //dd($pdf);
        $pdf->save(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'));

        //return response()->stream(public_path('files/pdf/'.$randStr.$randTime.'.pdf'))->deleteFileAfterSend(true);
        //return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
        return response()->download(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'))->deleteFileAfterSend(true);
        return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
    }

    public function addMobileBroadbandItem(Request $request){
        $input = $request->all();
        
        $devices = MobileBroadband::where('category_id', $input['mobileDevice']['category_id'])->get();
        //dd($devices);
        return $devices;
    }

}