<?php
namespace App\Http\Controllers;

use App\Discounts;
use App\RedemptionProducts;
use App\MobileDevices;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TelstraRedemptionController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.redemption-products');  
    }

    public function uploadTelstraRedemption(){
        session_start();
        return view('admin.uploads.telstra-redemption');
    }

    public function getTelstraRedemption(){
        $products = RedemptionProducts::all();
        return collect($products)->toArray();
    }

    public function telstraPlatinum(){
        return view('telstra-platinum');  
    }

    public function getRedemptionProducts(){
        $products = RedemptionProducts::all();
        return collect($products)->toArray();
    }

    public function uploadTelstraRedemptionData(Request $request){
        $input = $request->all();
        //dd($input);
        RedemptionProducts::query()->truncate();
        $fileData = $request->file('link');   
        //dd($file);
            config(['excel.import.startRow' => 1]);
        \Excel::load($fileData, function($reader) {
            $file = $reader->get()->toArray();
            foreach ($file as $key => $data) {
                $product = new RedemptionProducts;
                if(!empty($data['sku'])){
                    //dd($data);
                    $product->sku = $data['sku'];
                    $product->product = rtrim($data['product']);
                    // $product->start = $data['start'];
                    // $product->end = $data['end'];
                    $product->offer = 'Persistent';
                    $product->rrp = $data['rrp'];
                    $product->points = $data['points'];
                    $product->pay = $data['pay'];
                    $product->product2 = $data['product2'];
                    $product->product3 = $data['product3'];
                    $product->url = $data[0];
                    $product->save();
                }
            }
            //dd($numbers);
        });
    }
    
    public function rejectTelstraRedemptionData(Request $request){
        RedemptionProducts::query()->truncate();
        $redemptionProducts = RedemptionProducts::all();
        return $redemptionProducts;
    }

    public function addDiscount(Request $request){
        $input = $request->all();
        $date = Carbon::parse($input['effectiveDate'])->toDateTimeString();
        $discount = new Discounts;
        $discount->mobile_device_id = $input['mobileDevice']['id'];
        $discount->discount_on_small_plan = $input['discountOnSmallPlan'];
        $discount->discount_on_medium_plan = $input['discountOnMediumPlan'];
        $discount->discount_on_large_plan = $input['discountOnLargePlan'];
        $discount->discount_on_extra_large_plan = $input['discountOnExtraLargePlan'];
        $discount->effective_date = $date;

        $discount->save();
    }
}