<?php

namespace App\Http\Controllers;

use App\AccessorySupplierCodes;
use Illuminate\Http\Request;
use App\CartTemplates;
use Jumbojett\OpenIDConnectClient;
use App\ProposalTemplates;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ssoLogin(){
        $oidc = new OpenIDConnectClient('https://login.azenko.com.au',
                'Azenko.Cart.Users',
                null);

        $oidc->setResponseTypes(array('code'));
        $oidc->addScope(array('openid'));
        $oidc->setAllowImplicitFlow(false);
        $oidc->setVerifyHost(false);
        $oidc->setVerifyPeer(false);
        //$oidc->setRedirectURL('http://localhost:8000/_oauth/oidc');
        $oidc->setRedirectURL('https://okart.azenko.com.au/_oauth/oidc');
        $oidc->authenticate();
        $verifiedClaims = $oidc->getVerifiedClaims();
        $tokenID = $oidc->getIdToken();
        return view('home');
    }

    public function ssoLogout(){
        session_start();
        session_unset();
        session_destroy();
        return \Redirect('https://login.azenko.com.au/account/logout'); 
    }

    public function index()
    {
        session_start();
        if(!isset($_SESSION['userInfo'])){
            session_unset();
            session_destroy();
            return redirect('/sso-logout');
        }
        //dd($_SESSION);
        $cart = CartTemplates::where(['recipient' => $_SESSION['userInfo']['email'], 'is_used' => 0])->get();

        $data = [
            'cart' => collect($cart)->values()->toArray()
        ];

        return view('home', $data);
    }
    public function permissionDenied()
    {
        return view('permission-denied');
    }

    public function test(){
        session_start();
        if(isset($_SESSION['userInfo']) && $_SESSION['userInfo']){
            return \Redirect('/home'); 
        }
        $oidc = new OpenIDConnectClient('https://login.azenko.com.au',
                'Azenko.Cart.Users',
                null);

        $oidc->setResponseTypes(array('code'));
        $oidc->addScope(array('openid'));
        $oidc->setAllowImplicitFlow(false);
        $oidc->setVerifyHost(false);
        $oidc->setVerifyPeer(false);
        //$oidc->setRedirectURL('http://localhost:8000/_oauth/oidc');
        $oidc->setRedirectURL('https://okart.azenko.com.au/_oauth/oidc');
        $oidc->authenticate();
        $verifiedClaims = $oidc->getVerifiedClaims();
        $tokenID = $oidc->getIdToken();

        //dd($oidc->requestUserInfo());
        $email = $oidc->requestUserInfo('Email');
        //$email = 'john@thetelstrastore.com.au';

        $handle = curl_init('https://login.azenko.com.au/connect/token');
        $data = array('grant_type' => 'client_credentials', 'client_id' => 'Azenko.Cart', 'client_secret' => 'NdnpUsNJQY9jyky74p35JzZPLNU8vCPYejmNcPD6kMLEkyEftDrf468Q9GmuWxBv4UHcYuHDxjcpdGpMfTTvftEXckAJU9ma', 'scope' => 'Azenko.Management');
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = json_decode(curl_exec($handle), false);
        curl_close($handle);
        if (!is_null($resp) || !empty($resp)) {
            $curl_getSiteList = curl_init();
            curl_setopt_array($curl_getSiteList, array(
                CURLOPT_URL => "https://authorisations.azenko.com.au/authorisation/getauthorisedsites?username=$email&application=Okart",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Authorization: Bearer {$resp->access_token}",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: authorisations.azenko.com.au",
                    "cache-control: no-cache"
                ),
            ));
            $site = curl_exec($curl_getSiteList);
            $siteListResp = json_decode(($site), false);

            $clientCollection = collect(json_decode(json_encode($siteListResp), true))->toArray();
        
            $siteListErr = curl_error($curl_getSiteList);
            curl_close($curl_getSiteList);

            $countSites = count($clientCollection);
            
            if($countSites == 0){
                return \Redirect('/permission-denied');
            }

            //dd($oidc->requestUserInfo());
            if($countSites > 1){
                $_SESSION['userInfo'] = ['email' => $oidc->requestUserInfo('Email'), 'name' => $oidc->requestUserInfo('FullName'), 'firstName' => $oidc->requestUserInfo('GivenName'), 'lastName' => $oidc->requestUserInfo('FamilyName'),'role' => $oidc->requestUserInfo('role'),'UserId' => $oidc->requestUserInfo('UserId'), 'clientAuthorisations' => $oidc->requestUserInfo('clientAuthorisations')];
                    $_SESSION['clientCollection'] = $clientCollection;
            }
            //die;
            if($countSites == 1){
                https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=ryan@bsbo.com.au&application=OKart&client=Lite
                $siteData = collect($clientCollection)->first();
                //dd($siteData);
                $appName = $siteData['name'];
                $curl_getBranchList = curl_init();
                curl_setopt_array($curl_getBranchList, array(
                    CURLOPT_URL => "https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=$email&application=OKart&client=$appName",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Authorization: Bearer {$resp->access_token}",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: authorisations.azenko.com.au",
                        "cache-control: no-cache"
                    ),
                ));
                $branchListResponse = json_decode((curl_exec($curl_getBranchList)), false);
                //dd($branchListResponse);
                $branchCollection = collect(json_decode(json_encode($branchListResponse->branchAuthorisations), true))->toArray();
                
                //$siteListErr = curl_error($curl_getBranchList);
                curl_close($curl_getBranchList);
                
                $countBranch = count($branchCollection);

                if($countBranch == 1){
                    $_SESSION['userInfo'] = ['email' => $oidc->requestUserInfo('Email'), 'name' => $oidc->requestUserInfo('FullName'), 'firstName' => $oidc->requestUserInfo('GivenName'), 'lastName' => $oidc->requestUserInfo('FamilyName'), 'UserId' => $oidc->requestUserInfo('UserId'), 'role' => $oidc->requestUserInfo('role'), 'clientAuthorisations' => $oidc->requestUserInfo('clientAuthorisations')];
                    $_SESSION['clientCollection'] = $clientCollection;
                    $_SESSION['branchCollection'] = $branchCollection;
                    $_SESSION['currentBranch'] = collect($branchCollection)->first()['branchName'];
                    $_SESSION['userInfo']['clickposName'] = $_SESSION['userInfo']['firstName'].' '.substr($_SESSION['userInfo']['lastName'], 0, 1);
                    $_SESSION['clientCollection'] = $clientCollection;
                }else{
                    $_SESSION['userInfo'] = ['email' => $oidc->requestUserInfo('Email'), 'name' => $oidc->requestUserInfo('FullName'), 'firstName' => $oidc->requestUserInfo('GivenName'), 'lastName' => $oidc->requestUserInfo('FamilyName'), 'UserId' => $oidc->requestUserInfo('UserId'), 'role' => $oidc->requestUserInfo('role'), 'clientAuthorisations' => $oidc->requestUserInfo('clientAuthorisations')];
                    $_SESSION['clientCollection'] = $clientCollection;
                    $_SESSION['branchCollection'] = $branchCollection;
                    $_SESSION['clientCollection'] = $clientCollection;
                }
                
            }
        }
        

        //dd($_SESSION);
        return \Redirect('/home'); 
    }

    public function cartItems()
    {
        session_start();
        //dd($_SESSION['currentCartTemplate']['data']);

        if(!isset($_SESSION['userInfo'])){
            session_unset();
            session_destroy();
            return redirect('/sso-logout');
        }
        
        return view('mobile_plans.cart-items');
    }

    public function getBranchUsers(){
        session_start();
        $appName = $_SESSION['currentClient'];
        $currentBranch = $_SESSION['currentBranch'];
        $email = $_SESSION['userInfo']['email'];

        $handle = curl_init('https://login.azenko.com.au/connect/token');
        $data = array('grant_type' => 'client_credentials', 'client_id' => 'Azenko.Cart', 'client_secret' => 'NdnpUsNJQY9jyky74p35JzZPLNU8vCPYejmNcPD6kMLEkyEftDrf468Q9GmuWxBv4UHcYuHDxjcpdGpMfTTvftEXckAJU9ma', 'scope' => 'Azenko.Management');
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = json_decode(curl_exec($handle), false);

        curl_close($handle);

        $client = new \GuzzleHttp\Client();
        
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://authorisations.azenko.com.au/authorisation/getauthorisedusers_branch?application=OKart&client='.$appName.'&branch='.$currentBranch.'', 'verify' => false]);
        $headers = [
            'Authorization' => 'Bearer ' . $resp->access_token,        
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', '', [
                'headers' => $headers
        ]);
        
        return collect(json_decode($response->getBody()->getContents(),true))->where('userName', '!=', $_SESSION['userInfo']['email'])->toArray();
    }

    public function getActiveCartTemplate(){
        session_start();
        $cartTemplate = [];
        if(isset($_SESSION['currentCartTemplate'])){
            $cartTemplate = ($_SESSION['currentCartTemplate']['data']);
        }
        return $cartTemplate;
    }

    public function clearActiveCartTemplate(){
        session_start();
        $_SESSION['cartTemplate'] = $_SESSION['currentCartTemplate'];
        unset($_SESSION['currentCartTemplate']);
    }

    public function getStock(){
        session_start();
        if(isset($_SESSION['productList'])){
            
            return collect($_SESSION['productList'])->unique('BarCode')->values()->toArray();
            $accessoriesSupplierCoded = AccessorySupplierCodes::all();

            // dd(collect($_SESSION['productList'])->unique('BarCode')->values()->toArray());
            // dd(collect($accessoriesSupplierCoded)->values()->toArray());
            $products = [];
            foreach ($accessoriesSupplierCoded as $key => $value) {
                
                $products[] = collect($_SESSION['productList'])->where('ProductCode', $value['rims_id']);
            }

            $results = [];
            foreach ($products as $key => $value) {
                if(collect($value)->isNotEmpty()){
                    foreach ($value as $key => $data) {
                        $results[] = $data;
                    }
                    
                }
            }
            return $results;
            dd($accessoriesSupplierCoded);

            dd(collect($_SESSION['productList'])->unique('BarCode')->values()->toArray());
            //dd(collect($_SESSION['productList'])->values()->toArray());
            //dd(collect($_SESSION['productList'])->unique('BarCode')->values()->toArray());
            return collect($_SESSION['productList'])->unique('BarCode')->values()->toArray();
        }else{
            return [];
        }
    }

    public function saveBranch(Request $request){
        $input = $request->all();
        session_start();
        $_SESSION['PhoneNumber'] = '';
        $_SESSION['DealerCode'] = '';
        foreach ($_SESSION['branchCollection'] as $key => $data) {
            if($data['branchName'] == $input['branch']){
                foreach ($data['entityAttributes'] as $key => $attributes) {
                    if(isset($attributes['attributeType']) && $attributes['attributeType'] == 'PhoneNumber'){
                        $_SESSION['PhoneNumber'] = $attributes['value'];
                    }

                    if(isset($attributes['attributeType']) && $attributes['attributeType'] == 'DealerCode'){
                        $_SESSION['DealerCode'] = $attributes['value'];
                    }
                }
            }
        }

        $pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username='.$_SESSION['userInfo']['email'].'&application=Okart&client='.$_SESSION['currentClient'];
        
        //$pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=keely@thetelstrastore.com.au&application=Okart&client=Artslet';
        $pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username='.$_SESSION['userInfo']['email'].'&application=Okart&client='.$_SESSION['currentClient'];
        
        $appName = $_SESSION['currentClient'];
        $email = $_SESSION['userInfo']['email'];

        $handle = curl_init('https://login.azenko.com.au/connect/token');
        $data = array('grant_type' => 'client_credentials', 'client_id' => 'Azenko.Cart', 'client_secret' => 'NdnpUsNJQY9jyky74p35JzZPLNU8vCPYejmNcPD6kMLEkyEftDrf468Q9GmuWxBv4UHcYuHDxjcpdGpMfTTvftEXckAJU9ma', 'scope' => 'Azenko.Management');
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = json_decode(curl_exec($handle), false);
        curl_close($handle);

        $client = new \GuzzleHttp\Client();
        
        $client = new \GuzzleHttp\Client(['base_uri' => $pNumberURLCheck, 'verify' => false]);
        $headers = [
            'Authorization' => 'Bearer ' . $resp->access_token,        
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', '', [
                'headers' => $headers
        ]);
        $collection = json_decode($response->getBody()->getContents(),true);
        //dd($_SESSION['branchCollection']);
        foreach ($_SESSION['branchCollection'] as $key => $data) {
            if($data['name'] == $input['branch']){
                foreach ($data['entityAttributes'] as $key => $value) {
                    if($value['attributeType'] == 'Clickpos SaleLocation'){
                        $_SESSION['clickPosBranchName'] = $value['value'];
                    }
                }
                
            }
        }

        // if(collect($collection['clientAttributes'])->isEmpty()){
        //     $_SESSION['userInfo']['clickposName'] = $_SESSION['userInfo']['firstName'].' '.substr($_SESSION['userInfo']['lastName'], 0, 1);
        // }else{
        //     foreach ($collection['clientAttributes'] as $key => $data) {
        //         if($data['attributeName'] == 'Clickpos Username'){
        //             $_SESSION['userInfo']['clickposName'] = $data['attributeValue'];
        //         }
        //     }
        // }

        $_SESSION['userInfo']['clickposName'] = $_SESSION['userInfo']['firstName'].' '.substr($_SESSION['userInfo']['lastName'], 0, 1);
        //dd($_SESSION);
        if(collect($collection['branchAttributes'])->isNotEmpty()){
            foreach ($collection['branchAttributes'] as $key => $branchData) {
                if($branchData['branchName'] == $input['branch'] && $branchData['attributeName'] == 'PNumber'){
                    $_SESSION['userInfo']['pNumber'] = $branchData['attributeValue'];
                }
            }
        }
        //dd($_SESSION);

        $client = strtolower(str_replace(' ', '', $_SESSION['currentClient']));
        $token = '';
        if($client == 'artslet' || $client == 'testclient'){
            $token = '346B44F5-F237-47D5-BB7B-9A16B4AFACC6';
        }

        if($client == 'brookside'){
            $token = '16A266B6-192C-4388-B42D-114BCC74D6BF';
        }

        if($client == 'cairns'){
            $token = 'E5362D3D-F728-49EF-B734-E32216D5B8F7';
        }

        if($client == 'bendigogroup'){
            $token = '714887CF-7CB9-493D-86D6-E3F54AB25BC9';
        }

        if($client == 'm4b'){
            $token = 'A4038889-784A-4314-A430-3FEA4A26DC2B';
        }

        if($client == 'nortel'){
            $token = 'A0CB1222-32CB-4E74-ADD0-9508CEF05F22';
        }

        //dd($token);

        // $curl_getStockList = curl_init();
        //     curl_setopt_array($curl_getStockList, array(
        //         CURLOPT_URL => "https://timesheet.clickpos.net/ctimeapi/Stock/ProductSummaryByBranch",
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_ENCODING => "",
        //         CURLOPT_MAXREDIRS => 10,
        //         CURLOPT_TIMEOUT => 30,
        //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //         CURLOPT_CUSTOMREQUEST => "GET",
        //         CURLOPT_HTTPHEADER => array(
        //             "Accept: */*",
        //             "Cache-Control: no-cache",
        //             "Connection: keep-alive",
        //             "cache-control: no-cache",
        //             "Token: $token"
        //         ),
        //     ));
        // $stockResponse = json_decode((curl_exec($curl_getStockList)), false);

        $client = new \GuzzleHttp\Client();
        $url = 'https://tcpsvr121.clickpos.net/apireport/ctimeapi/Stock/ProductSummaryByBranch';
        $client = new \GuzzleHttp\Client(['base_uri' => $url, 'verify' => false]);
        $headers = [
            //'Authorization' => 'Bearer ' . $resp->access_token,  
            'Token' => $token,      
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', '', [
                'headers' => $headers
        ]);

        $collection = json_decode($response->getBody()->getContents(),true);
        
        
        $_SESSION['originalBranchName'] = $input['branch'];
        $branchName = str_replace('TLS ', '', $input['branch']);
        //$branchName = $input['branch'];
        //dd($branchName);
        $stockCollection = collect(json_decode(json_encode($collection), true))->toArray();
        //$stocks = collect($stockCollection)->where('BranchName','like','"%'.$branchName.'%"')->first()['ProductList'];
        //dd('"%'.$branchName.'%"');
        //dd($stocks);
        //dd($collection);
        if($branchName == "Stafford Kiosk"){
            $branchName = "TLS STAFFORD";
        }

        if($branchName == "Victoria Point NEW"){
            $branchName = "Victoria Point";
        }

        if($branchName == "Brookside"){
            $branchName = "Brookside Telstra Store";
        }

        if($branchName == "Brookside"){
            $branchName = "Brookside Telstra Store";
        }

        if($branchName == "Strathpine"){
            $branchName = "Telstra Store Strathpine";
        }

        if($branchName == "Toombul NEW"){
            $branchName = "Telstra Store Toombul";
        }

        if($branchName == "Springfield"){
            $branchName = "TLS Springfield";
        }

        if($branchName == "Jimboomba"){
            $branchName = "TLS Jimboomba";
        }

        if($branchName == "Bendigo Marketplace"){
            $branchName = "Market Place";
        }

        if($branchName == "Bendigo"){
            $branchName = "Mitchell Street";
        }

        if($branchName == "Mildura"){
            $branchName = "Mildura Centro";
        }

        if($branchName == "Bendigo Lansell Plaza"){
            $branchName = "Lansell Plaza";
        }

        if($branchName == "TBC Mildura"){
            $branchName = "Mildura Centro";
        }

        if($branchName == "The Gap"){
            $branchName = "The GAP";
        }

        if($branchName == "Lime Avenue"){
            $branchName = "Mildura Lime";
        }

        if($branchName == "TBTC REGIONAL WA"){
            $branchName = "TBTC Regional WA";
        }

        if($branchName == "Kalgoorlie"){
            $branchName = "Telstra Store Kalgoorlie";
        }

        if($branchName == "Albany"){
            $branchName = "Telstra Store Albany";
        }

        if($branchName == "Broome"){
            $branchName = "Tlife Broome Retail";
        }

        if($branchName == "Subiaco"){
            $branchName = "Telstra Store Subiaco";
        }

        if($branchName == "INNALOO"){
            $branchName = "Telstra Store Innaloo";
        }

        if($branchName == "TBC Great Southern & Bunbury"){
            $branchName = "Telstra Store Floreat";
        }

        if($branchName == "Munno Para"){
            $branchName = "Telstra Store Munno Para";
        }

        if($branchName == "Elizabeth"){
            $branchName = "Telstra Store Elizabeth";
        }

        if($branchName == "Port Lincoln"){
            $branchName = "Telstra Store Port Lincoln";
        }

        if($branchName == "Ingle Farm"){
            $branchName = "Telstra Store Ingle Farm";
        }

        if($branchName == "Salisbury"){
            $branchName = "Telstra Store Salisbury";
        }

        if($branchName == "Whyalla (SLAM)"){
            $branchName = "Telstra Store Whyalla";
        }

        if($branchName == "Port Pirie"){
            $branchName = "Telstra Store Port Pirie";
        }

        if($branchName == "Hollywood Plaza Kiosk"){
            $branchName = "Telstra Hollywood Plaza";
        }

        if($branchName == "FLOREAT"){
            $branchName = "Telstra Store Floreat";
        }

        //dd($stockCollection);
        //dd($branchName);
        if(!empty($token)){
            //$stocks = collect($stockCollection)->where('BranchName','like',$branchName)->first()['ProductList'];
            $stocks = collect($stockCollection)->where('BranchName','like',$branchName)->first()['ProductList'];
            $branch = str_replace('TLS ', '', $input['branch']);
            $finalBranchName = str_replace('NEW', '', $branch);

            $_SESSION['currentBranch'] = $input['branch'];
            $_SESSION['branchName'] = 'Telstra Store '.$finalBranchName;
            $_SESSION['productList'] = (collect($stocks)->isNotEmpty() ? collect($stocks)->where('ProductQty', '>', '1')->toArray() : []);
        }else{
            $branch = str_replace('TLS ', '', $input['branch']);
            $finalBranchName = str_replace('NEW', '', $branch);

            $_SESSION['currentBranch'] = $input['branch'];
            $_SESSION['branchName'] = 'Telstra Store '.$finalBranchName;
            $_SESSION['productList'] = [];
        }

        return \Redirect('/home'); 
    }

    public function saveClient(Request $request){
        session_start();
        $input = $request->all();
        //dd($_SESSION['userInfo']);

        //dd($siteData);
        $appName = $input['client'];
        $email = $_SESSION['userInfo']['email'];

        $handle = curl_init('https://login.azenko.com.au/connect/token');
        $data = array('grant_type' => 'client_credentials', 'client_id' => 'Azenko.Cart', 'client_secret' => 'NdnpUsNJQY9jyky74p35JzZPLNU8vCPYejmNcPD6kMLEkyEftDrf468Q9GmuWxBv4UHcYuHDxjcpdGpMfTTvftEXckAJU9ma', 'scope' => 'Azenko.Management');
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = json_decode(curl_exec($handle), false);
        curl_close($handle);
        //dd($_SESSION);
        //dd($appName);

        // if($_SESSION['userInfo']['role'] == 'Administrator' || $_SESSION['userInfo']['role'] ==  'SuperAdmin'){
        //     $url = "https://authorisations.azenko.com.au/branches/getbranches?client=$appName&application=OKart";
        // }else{
        //     $url = "https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=$email&application=OKart&client=$appName";   
        // }

        if(collect($_SESSION['userInfo']['clientAuthorisations'])->isNotEmpty() || $_SESSION['userInfo']['role'] != "User" || $_SESSION['userInfo']['email'] == 'cassie@azenko.com.au'){
            $url = "https://authorisations.azenko.com.au/branches/getbranches?client=$appName&application=OKart";
        }else{
            $url = "https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=$email&application=OKart&client=$appName";   
        }
        //$url = "https://authorisations.azenko.com.au/branches/getbranches?client=$appName&application=OKart";
        $client = new \GuzzleHttp\Client();
        
        $client = new \GuzzleHttp\Client(['base_uri' => $url, 'verify' => false]);
        $headers = [
            'Authorization' => 'Bearer ' . $resp->access_token,        
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', '', [
                'headers' => $headers
        ]);
        $collection = json_decode($response->getBody()->getContents(),true);
        //dd($collection);
        if(isset($collection['clientAuthorisations'])){
            $url = "https://authorisations.azenko.com.au/branches/getbranches?client=$appName&application=OKart";
            $client = new \GuzzleHttp\Client();
        
            $client = new \GuzzleHttp\Client(['base_uri' => $url, 'verify' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $resp->access_token,        
                'Accept'        => 'application/json',
            ];

            $response = $client->request('GET', '', [
                    'headers' => $headers
            ]);
            $collection = json_decode($response->getBody()->getContents(),true);
            $_SESSION['userInfo']['clientAuthorisations'] = $collection;
        }

        //dd($collection);
        if(collect($_SESSION['userInfo']['clientAuthorisations'])->isNotEmpty() || $_SESSION['userInfo']['role'] != "User"){

            //$collection = json_decode($response->getBody()->getContents(),true);
            foreach ($collection as $key => $value) {
                $value['branchName'] = $value['name'];
                $branchCollection[] = $value;
            }
        }else{
            //$collection = json_decode($response->getBody()->getContents(),true);
            $branchCollection = $collection['branchAuthorisations'];
            // $collection = json_decode($response->getBody()->getContents(),true);
            // foreach ($collection as $key => $value) {
            //     $value['branchName'] = $value['name'];
            //     $branchCollection[] = $value;
            // }
        }

        // $collection = json_decode($response->getBody()->getContents(),true);
        // $branchCollection = $collection['branchAuthorisations'];

        //     dd($collection);
        // die;
        // $curl_getBranchList = curl_init();
        // curl_setopt_array($curl_getBranchList, array(
        //     CURLOPT_URL => "$url",
        //     // CURLOPT_URL => "https://authorisations.azenko.com.au/authorisation/getbranches?client=TestClient&application=OKart",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_HTTPHEADER => array(
        //         "Accept: */*",
        //         "Authorization: Bearer {$resp->access_token}",
        //         "Cache-Control: no-cache",
        //         "Connection: keep-alive",
        //         "Host: authorisations.azenko.com.au",
        //         "cache-control: no-cache"
        //     ),
        // ));
        // $branchListResponse = json_decode((curl_exec($curl_getBranchList)), false);
        
        // $branchCollection = collect(json_decode(json_encode($branchListResponse->branchAuthorisations), true))->toArray();
        // dd($branchCollection);
        // //$siteListErr = curl_error($curl_getBranchList);
        // curl_close($curl_getBranchList);
        //dd($_SESSION);
        $countBranch = count($branchCollection);
        
        if($countBranch == 1){
            //dd("single");
            dd(collect($branchCollection)->first()['branchName']);
            $_SESSION['branchCollection'] = $branchCollection;
            $_SESSION['currentBranch'] = collect($branchCollection)->first()['branchName'];
            $_SESSION['clickPosBranchName'] = collect($branchCollection)->first()['branchName'];
        }else{
            //dd("multi");
            $_SESSION['branchCollection'] = $branchCollection;
        }
        $_SESSION['currentClient'] = $input['client'];
        if(isset($_SESSION['currentBranch'])){
            $_SESSION['PhoneNumber'] = '';
            $_SESSION['DealerCode'] = '';
            foreach ($_SESSION['branchCollection'] as $key => $data) {
                if($data['branchName'] == $_SESSION['currentBranch']){
                    foreach ($data['entityAttributes'] as $key => $attributes) {
                        if(isset($attributes['attributeType']) && $attributes['attributeType'] == 'PhoneNumber'){
                            $_SESSION['PhoneNumber'] = $attributes['value'];
                        }

                        if(isset($attributes['attributeType']) && $attributes['attributeType'] == 'DealerCode'){
                            $_SESSION['DealerCode'] = $attributes['value'];
                        }
                    }
                }
            }

            foreach ($_SESSION['branchCollection'] as $key => $data) {
                if($data['name'] == $_SESSION['currentBranch']){
                    foreach ($data['entityAttributes'] as $key => $value) {
                        if($value['attributeType'] == 'Clickpos SaleLocation'){
                            $_SESSION['clickPosBranchName'] = $value['value'];
                        }
                    }
                    
                }
            }
            //dd($_SESSION);
            $pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username='.$_SESSION['userInfo']['email'].'&application=Okart&client='.$_SESSION['currentClient'];
            
            //$pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username=keely@thetelstrastore.com.au&application=Okart&client=Artslet';
            $pNumberURLCheck = 'https://authorisations.azenko.com.au/authorisation/getappauthorisations?username='.$_SESSION['userInfo']['email'].'&application=Okart&client='.$_SESSION['currentClient'];
            
            $appName = $_SESSION['currentClient'];
            $email = $_SESSION['userInfo']['email'];

            $handle = curl_init('https://login.azenko.com.au/connect/token');
            $data = array('grant_type' => 'client_credentials', 'client_id' => 'Azenko.Cart', 'client_secret' => 'NdnpUsNJQY9jyky74p35JzZPLNU8vCPYejmNcPD6kMLEkyEftDrf468Q9GmuWxBv4UHcYuHDxjcpdGpMfTTvftEXckAJU9ma', 'scope' => 'Azenko.Management');
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
            $resp = json_decode(curl_exec($handle), false);
            curl_close($handle);

            $client = new \GuzzleHttp\Client();
            
            $client = new \GuzzleHttp\Client(['base_uri' => $pNumberURLCheck, 'verify' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $resp->access_token,        
                'Accept'        => 'application/json',
            ];

            $response = $client->request('GET', '', [
                    'headers' => $headers
            ]);
            $collection = json_decode($response->getBody()->getContents(),true);
            
            // if(collect($collection['clientAttributes'])->isEmpty()){
            //     $_SESSION['userInfo']['clickposName'] = $_SESSION['userInfo']['firstName'].' '.substr($_SESSION['userInfo']['lastName'], 0, 1);
            // }else{
            //     foreach ($collection['clientAttributes'] as $key => $data) {
            //         if($data['attributeName'] == 'Clickpos Username'){
            //             $_SESSION['userInfo']['clickposName'] = $data['attributeValue'];
            //         }
            //     }
            // }

            $_SESSION['userInfo']['clickposName'] = $_SESSION['userInfo']['firstName'].' '.substr($_SESSION['userInfo']['lastName'], 0, 1);
            //dd($_SESSION);
            if(collect($collection['branchAttributes'])->isNotEmpty()){
                foreach ($collection['branchAttributes'] as $key => $branchData) {
                    if($branchData['branchName'] == $_SESSION['currentBranch'] && $branchData['attributeName'] == 'PNumber'){
                        $_SESSION['userInfo']['pNumber'] = $branchData['attributeValue'];
                    }
                }
            }
            //dd($_SESSION);

            $client = strtolower(str_replace(' ', '', $_SESSION['currentClient']));
            $token = '';
            if($client == 'artslet' || $client == 'testclient'){
                $token = '346B44F5-F237-47D5-BB7B-9A16B4AFACC6';
            }

            if($client == 'brookside'){
                $token = '16A266B6-192C-4388-B42D-114BCC74D6BF';
            }

            if($client == 'cairns'){
                $token = 'E5362D3D-F728-49EF-B734-E32216D5B8F7';
            }

            if($client == 'bendigogroup'){
                $token = '714887CF-7CB9-493D-86D6-E3F54AB25BC9';
            }

            if($client == 'm4b'){
                $token = 'A4038889-784A-4314-A430-3FEA4A26DC2B';
            }

            if($client == 'nortel'){
                $token = 'A0CB1222-32CB-4E74-ADD0-9508CEF05F22';
            }

            //dd($token);

            // $curl_getStockList = curl_init();
            //     curl_setopt_array($curl_getStockList, array(
            //         CURLOPT_URL => "https://timesheet.clickpos.net/ctimeapi/Stock/ProductSummaryByBranch",
            //         CURLOPT_RETURNTRANSFER => true,
            //         CURLOPT_ENCODING => "",
            //         CURLOPT_MAXREDIRS => 10,
            //         CURLOPT_TIMEOUT => 30,
            //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //         CURLOPT_CUSTOMREQUEST => "GET",
            //         CURLOPT_HTTPHEADER => array(
            //             "Accept: */*",
            //             "Cache-Control: no-cache",
            //             "Connection: keep-alive",
            //             "cache-control: no-cache",
            //             "Token: $token"
            //         ),
            //     ));
            // $stockResponse = json_decode((curl_exec($curl_getStockList)), false);

            $client = new \GuzzleHttp\Client();
            $url = 'https://tcpsvr121.clickpos.net/apireport/ctimeapi/Stock/ProductSummaryByBranch';
            $client = new \GuzzleHttp\Client(['base_uri' => $url, 'verify' => false]);
            $headers = [
                //'Authorization' => 'Bearer ' . $resp->access_token,  
                'Token' => $token,      
                'Accept'        => 'application/json',
            ];

            $response = $client->request('GET', '', [
                    'headers' => $headers
            ]);

            $collection = json_decode($response->getBody()->getContents(),true);
            
            
            $branchName = str_replace('TLS ', '', $_SESSION['currentBranch']);
            //$branchName = $_SESSION['currentBranch'];
            //dd($branchName);
            $stockCollection = collect(json_decode(json_encode($collection), true))->toArray();
            //$stocks = collect($stockCollection)->where('BranchName','like','"%'.$branchName.'%"')->first()['ProductList'];
            //dd('"%'.$branchName.'%"');
            //dd($stocks);
            //dd($collection);
            if($branchName == "Stafford Kiosk"){
                $branchName = "TLS STAFFORD";
            }

            if($branchName == "Victoria Point NEW"){
                $branchName = "Victoria Point";
            }

            if($branchName == "Brookside"){
                $branchName = "Brookside Telstra Store";
            }

            if($branchName == "Brookside"){
                $branchName = "Brookside Telstra Store";
            }

            if($branchName == "Strathpine"){
                $branchName = "Telstra Store Strathpine";
            }

            if($branchName == "Toombul NEW"){
                $branchName = "Telstra Store Toombul";
            }

            if($branchName == "Springfield"){
                $branchName = "TLS Springfield";
            }

            if($branchName == "Jimboomba"){
                $branchName = "TLS Jimboomba";
            }

            if($branchName == "Bendigo Marketplace"){
                $branchName = "Market Place";
            }

            if($branchName == "Bendigo"){
                $branchName = "Mitchell Street";
            }

            if($branchName == "Mildura"){
                $branchName = "Mildura Centro";
            }

            if($branchName == "Bendigo Lansell Plaza"){
                $branchName = "Lansell Plaza";
            }

            if($branchName == "TBC Mildura"){
                $branchName = "Mildura Centro";
            }

            if($branchName == "The Gap"){
                $branchName = "The GAP";
            }

            if($branchName == "Lime Avenue"){
                $branchName = "Mildura Lime";
            }

            if($branchName == "TBTC REGIONAL WA"){
                $branchName = "TBTC Regional WA";
            }

            if($branchName == "Kalgoorlie"){
                $branchName = "Telstra Store Kalgoorlie";
            }

            if($branchName == "Albany"){
                $branchName = "Telstra Store Albany";
            }

            if($branchName == "Broome"){
                $branchName = "Tlife Broome Retail";
            }

            if($branchName == "Subiaco"){
                $branchName = "Telstra Store Subiaco";
            }

            if($branchName == "INNALOO"){
                $branchName = "Telstra Store Innaloo";
            }

            if($branchName == "TBC Great Southern & Bunbury"){
                $branchName = "Telstra Store Floreat";
            }

            if($branchName == "Munno Para"){
                $branchName = "Telstra Store Munno Para";
            }

            if($branchName == "Elizabeth"){
                $branchName = "Telstra Store Elizabeth";
            }

            if($branchName == "Port Lincoln"){
                $branchName = "Telstra Store Port Lincoln";
            }

            if($branchName == "Ingle Farm"){
                $branchName = "Telstra Store Ingle Farm";
            }

            if($branchName == "Salisbury"){
                $branchName = "Telstra Store Salisbury";
            }

            if($branchName == "Whyalla (SLAM)"){
                $branchName = "Telstra Store Whyalla";
            }

            if($branchName == "Port Pirie"){
                $branchName = "Telstra Store Port Pirie";
            }

            if($branchName == "Hollywood Plaza Kiosk"){
                $branchName = "Telstra Hollywood Plaza";
            }

            if($branchName == "FLOREAT"){
                $branchName = "Telstra Store Floreat";
            }

            //dd($stockCollection);
            //dd($branchName);
            if(!empty($token)){
                //$stocks = collect($stockCollection)->where('BranchName','like',$branchName)->first()['ProductList'];
                $stocks = collect($stockCollection)->where('BranchName','like',$branchName)->first()['ProductList'];
                $branch = str_replace('TLS ', '', $_SESSION['currentBranch']);
                $finalBranchName = str_replace('NEW', '', $branch);

                $_SESSION['currentBranch'] = $_SESSION['currentBranch'];
                $_SESSION['branchName'] = 'Telstra Store '.$finalBranchName;
                $_SESSION['productList'] = (collect($stocks)->isNotEmpty() ? collect($stocks)->where('ProductQty', '>', '1')->toArray() : []);
            }else{
                $branch = str_replace('TLS ', '', $_SESSION['currentBranch']);
                $finalBranchName = str_replace('NEW', '', $branch);

                $_SESSION['currentBranch'] = $_SESSION['currentBranch'];
                $_SESSION['branchName'] = 'Telstra Store '.$finalBranchName;
                $_SESSION['productList'] = [];
            }
        }

        return \Redirect('/home'); 
    }

    public function getBranches(Request $request){
        $input = $request->all();
        session_start();
        $_SESSION['currentBranch'] = $input['branch'];

        return \Redirect('/home'); 
    }

    public function changeBranch(){
        session_start();
        //dd($_SESSION);
        unset($_SESSION['branchCollection']);
        unset($_SESSION['currentBranch']);
        unset($_SESSION['productList']);
        unset($_SESSION['currentClient']);

        return \Redirect('/home'); 
    }

    public function getUserDetails(){
        session_start();
        return $_SESSION;
    }
}
