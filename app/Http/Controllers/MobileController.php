<?php
namespace App\Http\Controllers;

use App\Hro;
use App\MobileDevices;
use App\MobileBroadband;
use App\TempMobileDevices;
use App\TempBusinessMobileDevices;
use App\TempMobileBroadBandDevices;
use App\TempBusinessMobileBroadBandDevices;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use PDF;
use App\MobilePlanPage;
use App\BusinessMobilePlanPage;
use App\BusinessMobileDevicesCategories;
use App\BusinessMobileDevices;
use Carbon\Carbon;
use App\ConsoleData;
use App\ConsoleHRO;
use App\ProposalTemplates;
use App\BusinessMobileBroadband;
use App\OfficeUseOnly;

class MobileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        session_start();
        return view('mobile_plans.index');  
    }

    public function businessMobilePlans()
    {
        session_start();
        return view('mobile_plans.business-mobile-plans');  
    }

    public function getMobilePlans(Request $request){
        $deviceCategories = MobileDevicesCategories::with('categories')->get();
        return $deviceCategories;
    }

    public function getBusinessMobilePlans(Request $request){
        $deviceCategories = BusinessMobileDevicesCategories::with('categories')->get();
        return $deviceCategories;
    }

    public function getHro(Request $request){
        $hro = Hro::all();

        $addedHRO = [
            ['id' => 500, 'name' => 'Telstra Smart Wi-Fi Booster with Wi-Fi Guarantee', 'price_12_months' => 24, 'price_24_months' => 12, 'price_36_months' => null, 'product_code' => '', 'rrp' => 288],
            ['id' => 501, 'name' => 'Telstra TV', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
            ['id' => 502, 'name' => 'Telstra Smart Modem', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
        ];
        //dd((collect($hro)->toArray()));
        $collectedHRO = collect($hro)->toArray();
        //dd(());
        $array = array_merge(collect($hro)->toArray(), $addedHRO);
        return $array;
    }

    public function getAllHro(Request $request){
        $hro = Hro::all();

        $addedHRO = [
            ['id' => 500, 'name' => 'Telstra Smart Wi-Fi Booster with Wi-Fi Guarantee', 'price_12_months' => 24, 'price_24_months' => 12, 'price_36_months' => null, 'product_code' => '', 'rrp' => 288],
            ['id' => 501, 'name' => 'Telstra TV', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
            ['id' => 502, 'name' => 'Telstra Smart Modem', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
        ];

        $consoleHRO = ConsoleHRO::all();

        $collectedHRO = collect($hro)->toArray();
        //dd(());
        $array = array_merge(collect($hro)->toArray(),collect($consoleHRO)->toArray(), $addedHRO);
        return (collect($array)->unique('name')->values()->toArray());
        dd(collect($consoleHRO)->where('id', 92));
    }

    public function getHroHomePage(Request $request){
        $hro = Hro::all();

        $addedHRO = [
            ['id' => 500, 'name' => 'Telstra Smart Wi-Fi Booster with Wi-Fi Guarantee', 'price_12_months' => 24, 'price_24_months' => 12, 'price_36_months' => null, 'product_code' => '', 'rrp' => 288],
            ['id' => 501, 'name' => 'Telstra TV', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
            ['id' => 502, 'name' => 'Telstra Smart Modem', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
        ];
        //dd((collect($hro)->toArray()));
        $collectedHRO = collect($hro)->groupBy('category')->toArray();
        //dd(());
        $array = array_merge(collect($hro)->toArray(), $addedHRO);
        
        return ['hro' => $array, 'groupedHRO' => $collectedHRO];
    }

    public function getHroConsole(Request $request){
        $hro = ConsoleHRO::all();

        // $addedHRO = [
        //     ['id' => 500, 'name' => 'Telstra Smart Wi-Fi Booster with Wi-Fi Guarantee', 'price_12_months' => 24, 'price_24_months' => 12, 'price_36_months' => null, 'product_code' => '', 'rrp' => 288],
        //     ['id' => 501, 'name' => 'Telstra TV', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
        //     ['id' => 502, 'name' => 'Telstra Smart Modem', 'price_12_months' => 18, 'price_24_months' => 9, 'price_36_months' => null, 'product_code' => '', 'rrp' => 216],
        // ];
        //dd((collect($hro)->toArray()));

        $collectedHRO = collect($hro)->groupBy('category')->toArray();
        //dd(());
        //$array = array_merge(collect($hro)->toArray(), $addedHRO);
        
        return ['hro' => collect($hro)->values()->toArray(), 'groupedHRO' => $collectedHRO];
    }    

    public function getAllTempMobileDevices(Request $request){
        $input = $request->all();
        //$input['id'] = 1;
        //dd($input);
        $devices = TempMobileDevices::all();
        //$devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->sortByDesc('model')->values()->toArray();
        return $collection;
    }

    public function getAllTempBusinessMobileDevices(Request $request){
        $input = $request->all();
        //$input['id'] = 1;
        //dd($input);
        $devices = TempBusinessMobileDevices::all();
        //$devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->sortByDesc('model')->values()->toArray();
        return $collection;
    }

    public function getAllTempMobileBroadbandDevices(Request $request){
        $input = $request->all();
        //$input['id'] = 1;
        //dd($input);
        $devices = TempMobileBroadBandDevices::all();
        //$devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->sortByDesc('model')->values()->toArray();
        return $collection;
    }

    public function getAllTempBusinessMobileBroadbandDevices(Request $request){
        $input = $request->all();
        //$input['id'] = 1;
        //dd($input);
        $devices = TempBusinessMobileBroadBandDevices::all();
        //$devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->sortByDesc('model')->values()->toArray();
        return $collection;
    }

    public function getAllMobileDevices(Request $request){
        $input = $request->all();
        //$input['id'] = 1;
        //dd($input);
        $devices = MobileDevices::with('discounts')->get();
        //$devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->sortByDesc('model')->values()->toArray();
        foreach ($collection as $key => $data) {
            if(collect($data['discounts'])->isNotEmpty()){
                if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                    $data['discounts'] = $data['discounts'];
                }else{
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                
            }
            if(collect($data['discounts'])->isEmpty()){
                $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
            }

            $results[] = $data;
        }
        return $results;
        //return $collection;
    }

    public function getMobileDevices(Request $request){
        $input = $request->all();
        // $input['id'] = 1;
        $devices = MobileDevices::with('device_category.categories','discounts')->where('category_id', $input['id'])->get();
        $devices = $devices->reverse();
        
        $collection = collect($devices)->values()->toArray();
        foreach ($collection as $key => $data) {
            if(collect($data['discounts'])->isNotEmpty()){
                if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                    $data['discounts'] = $data['discounts'];
                }else{
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                
            }

            if(collect($data['discounts'])->isEmpty()){
                $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
            }
            $results[] = $data;
        }
        return $results;
        //return $collection;
    }

    public function getBusinessMobileDevices(Request $request){
        $input = $request->all();
        // $input['id'] = 1;
        $devices = BusinessMobileDevices::with('device_category.categories')->where('category_id', $input['id'])->get();
        $devices = $devices->reverse();
        //dd($devices);
        $collection = collect($devices)->values()->toArray();
        return $collection;
    }

    public function addCartItem(Request $request){
        $input = $request->all();
        //dd($input);
        //dd($input['deviceItem']['device_category_id']);
        $devices = MobileDevices::with('discounts')->where('device_category_id', $input['mobileDevice']['device_category_id'])->get();
        $devices = $devices->reverse();
        //dd($devices);
        $results = [];
        $collection = collect($devices)->values()->toArray();
        foreach ($collection as $key => $data) {
            if(collect($data['discounts'])->isNotEmpty()){
                if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                    $data['discounts'] = $data['discounts'];
                }else{
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                
            }

            if(collect($data['discounts'])->isEmpty()){
                $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
            }
            $results[] = $data;
        }
        return $results;
    }

    public function addToCartItem(Request $request){
        $input = $request->all();
        $results = [];
        if(isset($input['deviceItem']['device_category_id'])){
            $devices = MobileDevices::with('discounts')->where('device_category_id', $input['deviceItem']['device_category_id'])->get();
            $devices = $devices->reverse();

            $collection = collect($devices)->values()->toArray();
            foreach ($collection as $key => $data) {
                if(collect($data['discounts'])->isNotEmpty()){
                    if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                        $data['discounts'] = $data['discounts'];
                    }else{
                        $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                    }
                    
                }

                if(collect($data['discounts'])->isEmpty()){
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                $results[] = $data;
            }
            return $results;
        }else{
            return $results;
        }
    }

    public function addToCartBroadbandItem(Request $request){
        $input = $request->all();

        $results = [];
        //dd($input['deviceItem']);
        if(isset($input['deviceItem']['category_id'])){
            $devices = MobileBroadband::with('discounts')->where('category_id', $input['deviceItem']['category_id'])->get();
            $devices = $devices->reverse();
            
            $collection = collect($devices)->values()->toArray();
            foreach ($collection as $key => $data) {
                if(collect($data['discounts'])->isNotEmpty()){
                    if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                        $data['discounts'] = $data['discounts'];
                    }else{
                        $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                    }
                    
                }

                if(collect($data['discounts'])->isEmpty()){
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                $results[] = $data;
            }
            return $results;
        }else{
            return $results;
        }
    }

    public function addToCartBusinessBroadbandItem(Request $request){
        $input = $request->all();

        $results = [];
        //dd($input['deviceItem']);
        if(isset($input['deviceItem']['category_id'])){
            $devices = BusinessMobileBroadband::with('discounts')->where('category_id', $input['deviceItem']['category_id'])->get();
            $devices = $devices->reverse();
            
            $collection = collect($devices)->values()->toArray();
            foreach ($collection as $key => $data) {
                if(collect($data['discounts'])->isNotEmpty()){
                    if(Carbon::parse($data['discounts']['start_date'])->toDateTimeString() < Carbon::parse(Carbon::now())->toDateTimeString()){
                        $data['discounts'] = $data['discounts'];
                    }else{
                        $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                    }
                    
                }

                if(collect($data['discounts'])->isEmpty()){
                    $data['discounts'] = ['discount_on_extra_large_plan' => 0, 'discount_on_large_plan' => 0, 'discount_on_medium_plan' => 0, 'discount_on_small_plan' => 0];
                }
                $results[] = $data;
            }
            return $results;
        }else{
            return $results;
        }
    }

    public function uploadMobilePlans(Request $request){
        $file = $request->file('link');   
        dd($file);
        \Excel::load('test4.xlsx', function($reader) {
            //truncate existing records
            MobileDevicesCategories::query()->truncate();
            MobileDevices::query()->truncate();

            // Getting all results
            $file = $reader->get()->toArray();
            
            foreach($file as $key => $value){
                if(strlen($value['make']) < 30 && !empty($value['make'])){
                    $results[] = $value;
                    $categories[] = ucfirst($value['make']);
                }
            }
            //save devices category
            $categoryResult = collect($categories)->unique()->values()->toArray();
            
            foreach($categoryResult as $key => $value){
                $category = new MobileDevicesCategories;
                $category->category_name = $value;
                $category->save();
            }

            $deviceCategories = MobileDevicesCategories::all();

            foreach ($results as $key => $data) {
                
                //dd(number_format( intval(str_replace('US$', '', $data['rrp_inc_gst'])) ,2) );
                $collection = collect($deviceCategories);
                $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                //dd($data);
                //dd(floatval(str_replace('US$', '', $data['rrp_inc_gst'])));
                //dd((number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp_inc_gst'])) ,2)));
                $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                $device = new MobileDevices;
                $device->category_id = $category->id;
                $device->make = $data['make'];
                $device->model = $data['model'];
                $device->capacity = $data['capacity'];
                $device->network = $data['network'];
                $device->image_url = '/img/mobile_device/Prod'.$key.'.png';
                $device->blue_tick = $data['0'];
                $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                $device->mths_24_incl_gst = number_format( floatval(str_replace('US$', '', $data['24_monthdiscountinc_gst'])) ,2);
                $device->mths_36_incl_gst = number_format( floatval(str_replace('US$', '', $data['36_monthdiscountinc_gst'])) ,2);
                $device->mths_24_device_payment_contract = number_format( floatval(str_replace('US$', '', $data['dpc_24_months'])) ,2);
                $device->mths_36_device_payment_contract = number_format( floatval(str_replace('US$', '', $data['dpc_36_months'])) ,2);
                $device->product_code = $data['product_code'];
                $device->save();
            }
            //dd($numbers);
        });
    }

    public function uploadHro(Request $request){
        config(['excel.import.startRow' => 1]);
        \Excel::load('Siebel_HRO_17052021_-_IMAGE_VERSION.xlsx', function($reader) {
            //truncate existing records
            Hro::query()->truncate();

            // Getting all results
            $file = $reader->get()->toArray();
            dd($file);
           

            foreach ($file as $key => $data) {
                dd($data);
                if($data['product_name'] && $data['product_codes']){
                    $hro = new Hro;
                    $hro->name = $data['product_name'];
                    $hro->links = $data['link'];
                    $hro->category = $data['category'];
                    $hro->product_code = $data['product_codes'];
                    $hro->dbp_ex_gst = number_format( floatval(str_replace(['US$', ' ',','], '', $data['dbp_ex_gst'])) ,2);
                    $hro->rrp = number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp'])) ,2);
                    //$hro->price_24_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2);
                    $hro->price_24_months = ($data['24_months'] != 'N/A') ? number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2) : 0;
                    $hro->price_12_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['12_months'])) ,2);
                    $hro->save();
                }
            }
            //dd($numbers);
        });
    }

    public function uploadConsoleHro(Request $request){
        config(['excel.import.startRow' => 1]);
        \Excel::load('Console_HRO_17052021_IMAGE VERSION.xlsx', function($reader) {
            //truncate existing records
            ConsoleHRO::query()->truncate();

            // Getting all results
            $file = $reader->get()->toArray();

           

            foreach ($file as $key => $data) {
                if($data['product'] && $data['siebel_part_number']){
                    $hro = new ConsoleHRO;
                    $hro->links = $data['link'];
                    $hro->category = $data['category'];
                    $hro->name = $data['product'];
                    $hro->product_code = $data['siebel_part_number'];
                    $hro->dbp_ex_gst = number_format( floatval(str_replace(['US$', ' ',','], '', $data['dbp_ex_gst'])) ,2);
                    $hro->rrp = number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrp_in_gst'])) ,2);
                    //$hro->price_24_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2);
                    $hro->price_24_months = ($data['24_months'] != 'N/A') ? number_format( floatval(str_replace(['US$', ' ',','], '', $data['24_months'])) ,2) : 0;
                    $hro->price_12_months = number_format( floatval(str_replace(['US$', ' ',','], '', $data['12_months'])) ,2);
                    $hro->save();
                }
            }
            //dd($numbers);
        });
    }

    public function checkout(Request $request){
        session_start();
        $input = $request->all();
        //dd($input);
        $ouo = new OfficeUseOnly;
        $ouo->email = $_SESSION['userInfo']['email'];
        $ouo->store = $_SESSION['currentBranch'];
        $ouo->data = json_encode($input);
        $ouo->save();
        
        $orderItems = [];
        foreach ($input['quadrantCart'] as $key => $data) {
            $clickPosArray = [];
            $clickPosDevice = '';

            unset($data['isNew']);
            unset($data['isVasNew']);
            unset($data['promotionDiscounts']);
            unset($data['otherDevices']);
            unset($data['smallDiscount']);
            unset($data['mediumDiscount']);
            unset($data['largeDiscount']);
            unset($data['endDate']);
            unset($data['addPromotion']);
            unset($data['xlDiscount']);
            unset($data['xsDiscount']);
            $data['serviceNumber'] = (isset($input['customDescription']) ? $input['customDescription'][$key] : '');
            $data['orderType'] = $input['recon'][$key];
            $data['imei'] = $input['imei'][$key];

            $data['type'] = strtolower($data['mobileOption']['name']);
            //dd($data);
            if($data['itemType'] == 'mobilePlan'){
                //vas
                    if(collect($data['vas'])->isNotEmpty()){
                        foreach ($data['vas'] as $key => $vas) {
                            if($vas['name'] == 'International Calling Pack' && $data['isBusiness'] == 0){
                                $clickPosArray[] = ['clickposName' => '$10 Consumer International Call and SMS Pack'];
                            }

                            if($vas['name'] == 'International Calling Pack' && $data['isBusiness'] == 1){
                                $clickPosArray[] = ['clickposName' => '$10 Business International Call and SMS Pack'];
                            }

                            if($vas['name'] == 'Checkpoint SandBlast' && $data['isBusiness'] == 0){
                                $clickPosArray[] = ['clickposName' => 'Checkpoint Sandblast Agent'];
                            }
                        }
                    }
                //end vas
     
                if($data['months'] == 24){
                    $clickPosArray[] = ['clickposName' => 'Mobile Repayment Option - 24 Months'];
                }

                if($data['recon'] == 'Lease/NPF/Upgrade Protect' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile L Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile L Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile L Plan REC'];
                }

                if($data['recon'] == 'Lease/NPF/Upgrade Protect' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile M Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile M Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile M Plan REC'];
                }

                if($data['recon'] == 'Pre to Post' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile M Plan P2POST'];
                }

                if($data['recon'] == 'Lease/NPF/Upgrade Protect' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile S Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile S Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile S Plan REC'];
                }

                if($data['recon'] == 'Pre to Post' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile S Plan P2POST'];
                }

                if($data['recon'] == 'Lease/NPF/Upgrade Protect' && $data['planType'] == 'XL'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XL Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'XL'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XL Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'XL'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XL Plan REC'];
                }

                if($data['recon'] == 'Lease/NPF/Upgrade Protect' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XS Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XS Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XS Plan REC'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile XS (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile Small Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile Medium Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile Large Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'XL'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Mobile Extra Large Plan (Transition)'];
                }

                if(collect($data['deviceItem'])->isEmpty()){
                    if($data['recon'] == 'New'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (New)'];
                    }

                    if($data['recon'] == 'Recon'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (Recon)'];
                    }

                    if($data['recon'] == 'Transition'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (Transition)'];
                    }

                    if($data['recon'] == 'Lease/NPF/Upgrade Protect'){
                        $clickPosArray[] = ['Contract Only Sim - (Lease/NPF/Upgrade Protect)'];
                    }
                }

                if(collect($data['deviceItem'])->isNotEmpty()){
                    if($data['recon'] == 'New'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Handheld) '.$data['months'].' Mths New'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    if($data['recon'] == 'Recon'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Handheld) '.$data['months'].' Mths Rec'];
                    }

                    if($data['recon'] == 'Transition'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Handheld) '.$data['months'].' Mths Trans'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    if($data['recon'] == 'Lease/NPF/Upgrade Protect'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Handheld) '.$data['months'].' Mths Trans'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    $clickPosArray[] = ['clickposName' => $data['deviceItem']['make'].' '.$data['deviceItem']['model'].' '.$data['deviceItem']['capacity']];
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'outright'){    
                if(collect($data['devices'])->isNotEmpty()){
                    $clickPosArray[] = ['clickposName' => 'Device Payment Contract Consumer (Handheld) (Outright)'];
                }

                if(collect($data['broadBandDevices'])->isNotEmpty()){
                    $clickPosArray[] = ['clickposName' => 'Device Payment Contract Consumer (Tablet) (Outright)'];
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'broadBandPlan'){
                //vas
                    if(collect($data['vas'])->isNotEmpty()){
                        foreach ($data['vas'] as $key => $vas) {
                            if($vas['name'] == 'Checkpoint SandBlast' && $data['isBusiness'] == 0){
                                $clickPosArray[] = ['clickposName' => 'Checkpoint Sandblast Agent'];
                            }
                        }
                    }
                //end vas
              
                if($data['recon'] == 'New' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data L Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data L Plan REC'];
                }

                if($data['recon'] == 'Lease' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data M Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data M Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data M Plan REC'];
                }

                if($data['recon'] == 'Lease' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data S Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data S Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data S Plan REC'];
                }

                if($data['recon'] == 'Lease' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data XS Plan Lease/NPF'];
                }

                if($data['recon'] == 'New' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data XS Plan NEW'];
                }

                if($data['recon'] == 'Recon' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => (($data['isBusiness'] == 1) ? 'Business' : 'Consumer').' Data XS Plan REC'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'XS'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Data XS (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Small'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Data Small Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Medium'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Data Medium Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'Large'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Data Large Plan (Transition)'];
                }

                if($data['recon'] == 'Transition' && $data['planType'] == 'XL'){
                    $clickPosArray[] = ['clickposName' => ($data['isBusiness'] == 1 ? 'Buesinss' : 'Consumer').' Data Extra Large Plan (Transition)'];
                }
                

                if(collect($data['deviceItem'])->isNotEmpty()){
                    if($data['recon'] == 'New'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Tablet) '.$data['months'].' Mths New'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    if($data['recon'] == 'Recon'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Tablet) '.$data['months'].' Mths Rec'];
                    }

                    if($data['recon'] == 'Transition'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Tablet) '.$data['months'].' Mths Trans'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    if($data['recon'] == 'Lease/NPF/Upgrade Protect'){
                        $clickPosArray[] = ['clickposName' => 'Device Payment Contract '.($data['isBusiness'] == 1 ? 'Business' : 'Consumer').' (Tablet) '.$data['months'].' Mths Trans'.(($data['pobo'] == true) ? ' (POBO)' : '')];
                    }

                    $clickPosArray[] = ['clickposName' => $data['deviceItem']['make'].' '.$data['deviceItem']['model'].' '.$data['deviceItem']['capacity']];
                }

                if(collect($data['deviceItem'])->isEmpty()){
                    if($data['recon'] == 'New'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (New)'];
                    }

                    if($data['recon'] == 'Recon'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (Recon)'];
                    }

                    if($data['recon'] == 'Transition'){
                        $clickPosArray[] = ['clickposName' => 'Contract Only Sim - (Transition)'];
                    }

                    if($data['recon'] == 'Lease/NPF/Upgrade Protect'){
                        $clickPosArray[] = ['Contract Only Sim - (Lease/NPF/Upgrade Protect)'];
                    }
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'fixed'){
                //vas
                    if(collect($data['vas'])->isNotEmpty()){
                        foreach ($data['vas'] as $key => $vas) {
                            if($vas['name'] == 'International Ultimate Calling Pack' && $data['isBusiness'] == 0){
                                $clickPosArray[] = ['clickposName' => '$15 International Ultimate Calling Pack', 'okart' => 'Fixed - VAS - International Ultimate Calling Pack '];
                            }

                            if($vas['name'] == 'Telstra Smart Wi-Fi Booster with Wi-Fi Guarantee' || $vas['name'] == 'Telstra Smart Modem SRO'){
                                $clickPosArray[] = ['clickposName' => 'Bundle - Modem In-Store'];
                            }

                            if($vas['name'] == 'Business Calling Pack'){
                                $clickPosArray[] = ['clickposName' => 'Business Calling Pack'];
                            }

                            if($vas['name'] == 'NBN Superfast Speed Boost (For Premium Only)'){
                                $clickPosArray[] = ['clickposName' => 'Fast Speed Boost (Standard) (NBN)'];
                            }
                        }

                        if($data['fixed']['name'] == 'Unlimited Business Internet' && $vas['name'] == 'NBN Speed Boost'){
                            $clickPosArray[] = ['clickposName' => 'Business Broadband NBN Ultimate'];
                        }
                    }
                //end vas
                if($data['fixed']['name'] == 'ADSL Unlimited Data'){
                    $clickPosArray[] = ['clickposName' => 'Business Broadband ADSL Ultimate'];
                }
                if($data['fixed']['name'] == 'Unlimited Business Internet'){
                    $clickPosArray[] = ['clickposName' => 'Business Broadband NBN Standard'];
                }

                if($data['fixed']['name'] == 'Unlimited Business Voice'){
                    $clickPosArray[] = ['clickposName' => 'Business Calling Pack'];
                }

                if($data['fixed']['name'] == 'Core 500GB' && $data['recon'] == 'Recon'){
                    $clickPosArray[] = ['clickposName' => 'Core Internet Plan (Recon)'];
                }
                if($data['fixed']['name'] == 'Core 500GB' && $data['recon'] == 'New nbn connection'){
                    $clickPosArray[] = ['clickposName' => 'Core Internet Bundle (Core NBN Migration)'];
                }
                if($data['fixed']['name'] == 'Core 500GB' && $data['recon'] == 'Transition'){
                    $clickPosArray[] = ['clickposName' => 'Core Internet Bundle (Core Transition)'];
                }

                if($data['fixed']['name'] == 'Core 500GB' && $data['recon'] == 'New'){
                    $clickPosArray[] = ['clickposName' => 'Core Internet Bundle (New)'];
                }

                if($data['fixed']['name'] == 'Core 500GB' && $data['recon'] == 'Recon'){
                    $clickPosArray[] = ['clickposName' => 'Core Internet Bundle (Recontract)'];
                }

                if($data['fixed']['name'] == 'Premium' && $data['recon'] == 'New nbn connection'){
                    $clickPosArray[] = ['clickposName' => 'Premium Internet Bundle (Core NBN Migration)'];
                }
                if($data['fixed']['name'] == 'Premium' && $data['recon'] == 'Transition'){
                    $clickPosArray[] = ['clickposName' => 'Premium Internet Bundle (Core Transition)'];
                }

                if($data['fixed']['name'] == 'Premium' && $data['recon'] == 'New'){
                    $clickPosArray[] = ['clickposName' => 'Premium Internet Bundle (New)'];
                }

                if($data['fixed']['name'] == 'Premium' && $data['recon'] == 'Recon'){
                    $clickPosArray[] = ['clickposName' => 'Premium Internet Bundle (Recontract)'];
                }

                if($data['fixed']['name'] == 'Ultimate Voice' && $data['recon'] == 'New nbn connection'){
                    $clickPosArray[] = ['clickposName' => 'Ultimate Voice Plan (Core NBN Migration)'];
                }
                if($data['fixed']['name'] == 'Ultimate Voice' && $data['recon'] == 'Transition'){
                    $clickPosArray[] = ['clickposName' => 'Ultimate Voice Plan (Core Transition)'];
                }

                if($data['fixed']['name'] == 'Ultimate Voice' && $data['recon'] == 'New'){
                    $clickPosArray[] = ['clickposName' => 'Ultimate Voice Plan (New)'];
                }

                if($data['fixed']['name'] == 'Ultimate Voice' && $data['recon'] == 'Recon'){
                    $clickPosArray[] = ['clickposName' => 'Ultimate Voice Plan (Recontract)'];
                }

                if($data['fixed']['name'] == 'Unlimited' && $data['recon'] == 'New nbn connection'){
                    $clickPosArray[] = ['clickposName' => 'Unlimited Internet'.(($data['isBusiness'] == 1) ? ' Business Internet Plan ' : ' Bundle ').'(Core NBN Migration)'];
                }
                if($data['fixed']['name'] == 'Unlimited' && $data['recon'] == 'Transition'){
                    $clickPosArray[] = ['clickposName' => 'Unlimited Internet'.(($data['isBusiness'] == 1) ? ' Business Internet Plan ' : ' Bundle ').'(Core Transition)'];
                }

                if($data['fixed']['name'] == 'Unlimited' && $data['recon'] == 'New'){
                    $clickPosArray[] = ['clickposName' => 'Unlimited Internet'.(($data['isBusiness'] == 1) ? ' Business Internet Plan ' : ' Bundle ').'(New)'];
                }

                if($data['fixed']['name'] == 'Unlimited' && $data['recon'] == 'Recon'){
                    $clickPosArray[] = ['clickposName' => 'Unlimited Internet'.(($data['isBusiness'] == 1) ? ' Business Internet Plan ' : ' Bundle ').'(Recontract)'];
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'platinum'){
                if($data['platinumPlanType'] == 'Basic'){
                    $clickPosArray[] = ['clickposName' => 'Platinum Plus'];
                }

                if($data['platinumPlanType'] == 'Subscription - Monthly'){
                    $clickPosArray[] = ['clickposName' => 'Platinum Service Subscription'];
                }

                if($data['platinumPlanType'] == 'Set it up & optimise - Outright' || $data['platinumPlanType'] == 'Set it up & optimise - Monthly'){
                    $clickPosArray[] = ['clickposName' => 'Set IT Up - In Home'];
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'entertainment'){
                foreach ($data['entertainment'] as $key => $value) {
                    if(collect($data['vas'])->isEmpty() && collect($data['selectedPromotions'])->isEmpty()){
                        if($value['name'] == 'Binge Standard' || $value['name'] == 'Binge Premium'){
                            $clickPosArray[] = ['clickposName' => 'Binge 3M Offer'];
                        }

                        if($value['name'] == 'Kayo Basic' || $value['name'] == 'Kayo Premium'){
                            $clickPosArray[] = ['clickposName' => $value['name']];
                        }

                        if($value['name'] == 'FOXTEL NOW'){
                            $clickPosArray[] = ['clickposName' => 'Foxtel Now (1 Month Trial)'];
                        }

                        if($value['name'] == 'Foxtel Plus with Netflix'){
                            $clickPosArray[] = ['clickposName' => 'Foxtel Plus + Netflix'];
                        }

                        if($value['name'] == 'Movies HD + Foxtel Plus'){
                            $clickPosArray[] = ['clickposName' => 'Movies HD + Foxtel Plus'];
                        }

                        if($value['name'] == 'Movies HD + Foxtel Plus with Netflix'){
                            $clickPosArray[] = ['clickposName' => 'Movies HD + Foxtel Plus + Netflix'];
                        }

                        if($value['name'] == 'Sports HD + Foxtel Plus'){
                            $clickPosArray[] = ['clickposName' => 'Sports HD + Foxtel Plus'];
                        }

                        if($value['name'] == 'Sports HD + Foxtel Plus with Netflix'){
                            $clickPosArray[] = ['clickposName' => 'Sports HD + Foxtel Plus + Netflix'];
                        }

                        if($value['name'] == 'Foxtel Premium'){
                            $clickPosArray[] = ['clickposName' => 'Premium'];
                        }

                        if($value['name'] == 'Foxtel Premium with Netflix'){
                            $clickPosArray[] = ['clickposName' => 'Premium + Netflix'];
                        }
                    }

                    if(collect($data['selectedPromotions'])->isNotEmpty()){
                        if($value['name'] == 'Kayo Basic'){
                            $clickPosArray[] = ['clickposName' => 'Kayo ($15 P/M for 12 months)'];
                        }
                    }

                    if(collect($data['vas'])->isNotEmpty() && collect($data['selectedPromotions'])->isEmpty()){
                        foreach ($data['vas'] as $key => $vas) {
                            if($value['name'] == 'Multiroom box'){
                                $clickPosArray[] = ['clickposName' => 'Foxtel Multiroom iQ / iQ2 / iQ3 / iQ4 Set-top Box'];
                            }
                        }
                    }
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'miscellaneous'){
                foreach ($data['selectedPromotions'] as $key => $value) {
                    if($value['name'] == '5G Home Internet Plan - 500GB'){
                        $clickPosArray[] = ['clickposName' => '5G Home Internet Plan - NEW (BTL)'];
                    }

                    if($value['name'] == 'Telstra Home Phone Essential'){
                        $clickPosArray[] = ['clickposName' => 'Telstra Home Phone Essential Plan('.$input['reconMisc'][$key].')'];
                    }

                    if($value['name'] == 'Starter Internet Plan - 25GB'){
                        $clickPosArray[] = ['clickposName' => 'Starter Internet Bundle('.$input['reconMisc'][$key].')'];
                    }
                }
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'hro'){
                foreach ($data['hro'] as $key => $val) {
                    $clickPosArray[] = ['clickposName' => $val['name']];
                }
                
                   
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            if($data['itemType'] == 'redemption'){
                $clickPosArray = ['clickposName' => $data['product']];
                
                   
                $data['clickpos'] = $clickPosArray;
                $orderItems['orderItems'][] = ['items' => $data];
            }

            // $results = [];
            
            // $testArray[] = $clickPosArray;

            //$data['clickpos'][] = $clickPosArray;
            //$data['clickpos_device'] = $clickPosDevice;
            // $data['customerAdvisor'] = $_SESSION['userInfo']['name'];
            // $data['branch'] = $_SESSION['currentBranch'];
            // $data['client'] = $_SESSION['currentClient'];
            // $data['rescan'] = $input['rescan'];
            // $data['invoiceNumber'] = $input['invoiceNumber'];
            // $data['customerName'] = $input['customerName'];
            // $data['ban'] = $input['ban'];
            // $data['accountType'] = $input['accountType'];
            // $data['customerType'] = $input['isBusiness'];
            // $data['outright'] = $input['scantek'];
            // $data['dl'] = $input['dl'];
            // $data['abn'] = $input['abn'];
            // $data['crat'] = $input['crat'];
            //$resultData[] = ['items' => ['okart' => $data, 'clickpos' => ]];
            // $data['service_number'] = $data['customDescription'];
            // unset($data['customDescription']);
            // $data['type'] = $data['mobileOption']['name'];
            // unset($data['mobileOption']);
            // $data['items'] = $data;
            // $resultData['orderDetails'][] = ['items' => $data['items']];
        }
        //dd($orderItems);
        // foreach ($input['quadrantCart'] as $key => $data) {
        //     $type = strtolower($data['mobileOption']['name']);
        //     $results = [];
        //     $results['customerAdvisor'] = $_SESSION['userInfo']['name'];
        //     $results['branch'] = $_SESSION['currentBranch'];
        //     $results['client'] = $_SESSION['currentClient'];
        //     //$results['serviceNumber'] = $input['customDescription'];
        //     $results['rescan'] = $input['rescan'];
        //     $results['invoiceNumber'] = $input['invoiceNumber'];
        //     $results['customerName'] = $input['customerName'];
        //     $results['ban'] = $input['ban'];
        //     $results['accountType'] = $input['accountType'];
        //     $results['customerType'] = $input['isBusiness'];
        //     $results['outright'] = $input['scantek'];
        //     $results['dl'] = $input['dl'];
        //     $results['abn'] = $input['abn'];
        //     $results['crat'] = $input['crat'];
        //     foreach ($input['quadrantCart'] as $key => $data) {
        //         unset($data['isNew']);
        //         unset($data['isVasNew']);
        //         unset($data['promotionDiscounts']);
        //         unset($data['otherDevices']);
        //         unset($data['smallDiscount']);
        //         unset($data['mediumDiscount']);
        //         unset($data['largeDiscount']);
        //         unset($data['xlDiscount']);
        //         unset($data['xsDiscount']);
        //         $data['serviceNumber'] = $input['customDescription'][$key];
        //         $data['orderType'] = $input['recon'][$key];
        //         $data['imei'] = $input['imei'][$key];
        //         $results['items'][] = $data;
        //     }
        //     unset($results['quadrantCart']);
        //     $resultData[] = $data;
        // }

        $console = new ConsoleData;
        $orderItems['customerDetails'] = ['name' => $input['customerName'], 'contact_number' => isset($input['contact']) ? $input['contact'] : '', 'invoice_number' => $input['invoiceNumber']];
        $orderItems['customerAdvisor'] = ['name' => $_SESSION['userInfo']['name'],'email' => $_SESSION['userInfo']['email'], 'branch' => $_SESSION['clickPosBranchName']];

        //$orderItems['customerAdvisor'] = ['name' => $_SESSION['userInfo']['name'],'email' => $_SESSION['userInfo']['email']];
        $console->data = json_encode($orderItems);
        //$console->type = $type;
        //$console->clickpos_plan = json_encode($clickPosArray);
        $console->save();
            
        $data['data'] = $input;

        //$data['data'] = json_decode(file_get_contents('js/checkout.json'), true); 
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('pdf.checkout', $data);
        //dd("123");
        $randStr = sha1(time());
        $randTime = Carbon::now()->timestamp;
        //dd(storage_path());
        //dd($pdf);
        //$pdf->save(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'));
        return $pdf->stream();
        return $pdf->download('voucher.pdf');

        //return response()->stream(public_path('files/pdf/'.$randStr.$randTime.'.pdf'))->deleteFileAfterSend(true);
        //return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
        return response()->download(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'))->deleteFileAfterSend(true);
        return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
    }

    public function sendProposal(Request $request){
        session_start();
        $input = $request->all();

        //$cart = new ConsoleData;
        $cart = new ProposalTemplates;
        $cart->data = json_encode($input);
        $cart->email = $_SESSION['userInfo']['email'];
        $cart->store = $_SESSION['currentBranch'];
        $cart->save();
        // $data['data'] = $input;
        // //dd($data);
        // $pdf = PDF::loadView('pdf.cart', $data);
        // return $pdf->download('cart.pdf');
        $data['data'] = $input;

        //$data = json_decode(file_get_contents('js/proposal.json'), true); 
        //dd($data);
        //dd($commissiongstTotal);
        $pdf = PDF::loadView('pdf.proposal', $data);
        //dd("123");
        $randStr = sha1(time());
        $randTime = Carbon::now()->timestamp;
        //dd(storage_path());
        //dd($pdf);
        //$pdf->save(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'));
        return $pdf->stream();
        return $pdf->download('voucher.pdf');

        //return response()->stream(public_path('files/pdf/'.$randStr.$randTime.'.pdf'))->deleteFileAfterSend(true);
        //return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
        return response()->download(public_path('files/pdf/'.$input['randString'].$input['timestamp'].'.pdf'))->deleteFileAfterSend(true);
        return 'files/pdf/'.$input['randString'].$input['timestamp'].'.pdf';
    }

    public function aroForm(){
        session_start();
        return view('aro-form');  
    }

    public function printAROForm(Request $request){
        $input = $request->all();
        $upfrontPayments = [];
        $aroBases = [];
        $monthlyCommitments = [];
        $orderNumbers = [];
        $aroProducts = [];
        $repaymentTerm = [];
        //dd($input);
        foreach ($input['aro']['upfrontPayment'] as $key => $upfrontPayment) {
            $upfrontPayments[] = ['upfrontPayment' => $upfrontPayment];
        }

        foreach ($input['aro']['itemType'] as $key => $value) {
            $itemType[] = ['itemType' => $value];
        }

        foreach ($input['aro']['months'] as $key => $value) {
            $repaymentTerm[] = ['repaymentTerm' => $value];
        }

        foreach ($input['aro']['aroBase'] as $key => $aroBase) {
            $aroBases[] = ['aroBase' => $aroBase];
        }

        foreach ($input['aro']['monthlyCommitment'] as $key => $monthlyCommitment) {
            $monthlyCommitments[] = ['monthlyCommitment' => $monthlyCommitment];
        }

        foreach ($input['aro']['orderNumber'] as $key => $orderNumber) {
            $orderNumbers[] = ['orderNumber' => $orderNumber];
        }

        foreach ($input['aro']['aroProducts'] as $key => $aroProduct) {
            $aroProducts[] = ['aroProducts' => $aroProduct];
        }
        //dd($data['aro']);
        $data['data'] = $input;
        $data['data']['aro'] = array_replace_recursive($aroBases,$upfrontPayments,$itemType,$repaymentTerm,$monthlyCommitments,$orderNumbers,$aroProducts);
        //$data['data'] = json_decode(file_get_contents('js/aro-form.json'), true); 
        $pdf = PDF::loadView('pdf.aro', $data);
        $randStr = sha1(time());
        $randTime = Carbon::now()->timestamp;
        return $pdf->stream();
        return ($data);
    }

    public function officeUseOnly(){
        session_start();
        return view('office-use-only');  
    }

    public function printOfficeUseOnly(Request $request){
        $input = $request->all();
        dd($input);
    }

    public function aroAcknowledgement(Request $request){
        $input = $request->all();
        $data['data'] = $input;
        //$data['data'] = json_decode(file_get_contents('js/aro.json'), true); 
        //dd($data);
        $pdf = PDF::loadView('pdf.aro', $data);
        $randStr = sha1(time());
        $randTime = Carbon::now()->timestamp;
        return $pdf->stream();
        return (json_encode($input));
    }

    public function getMobilePageContent(){
        $plan = MobilePlanPage::all()->first();
        return $plan;
    }
    public function getBusinessMobilePageContent(){
        $plan = BusinessMobilePlanPage::all()->first();
        return $plan;
    }

    public function getGroupedMobileDevices(Request $request){
        $input = $request->all();

        // $results = collect($input)->groupBy('customDescription')->toArray();

        // $newCollection = collect([]);
        // $newCollection2 = collect([]);
        // collect($results)->each(function($item, $key) use ($newCollection) {
        //     $newCollection->put($key-1, $item);
        // });

        // collect($newCollection)->each(function($item, $key) use ($newCollection2) {
        //         $newCollection2->put($key-1, $item);
        //     });  

        // return $newCollection2;

        $results = [];
        foreach ($input as $key => $data) {
            //dd($data);
            if($data['itemType'] == 'mobilePlan'){
                $results[$data['customDescription']][$key + 0] = $data;
            }
        }
        return $results;
        dd($input);
    }

    public function getProposalHistory(Request $request){
        session_start();
        $proposals = ProposalTemplates::where('email', 'ryan.david@azenko.com.au')->orderBy('created_at','desc')->take(10)->get();

        $mobile = 0;
        $businessMobile = 0;
        $businessmobileBroadband = 0;
        $mobileBroadband = 0;
        $businessFixed = 0;
        $fixed = 0;
        $businessHRO = 0;
        $hro = 0;
        $entertainment = 0;
        $miscellaneous = 0;
        $outright = 0;
        $proposalData = [];
        foreach ($proposals as $key => $proposal) {
            //dd($proposal['data']);
            // $proposal['mobile'] = 0;
            // $proposal['businessMobile'] = 0;
            // $proposal['broadband'] = 0;
            // $proposal['businessmobileBroadband'] = 0;
            // $proposal['businessFixed'] = 0;
            // $proposal['fixed'] = 0;
            // $proposal['businessHRO'] = 0;
            // $proposal['hro'] = 0;
            // $proposal['miscellaneous'] = 0;
            // $proposal['entertainment'] = 0;
            // $proposal['outright'] = 0;

            foreach ($proposal['data']['quadrantCart'] as $key => $data) {
                
                if($data['itemType'] == 'mobilePlan' && $data['isBusiness'] == 0){
                    $proposal['mobile'] = ($mobile + 1);
                }

                if($data['itemType'] == 'mobilePlan' && $data['isBusiness'] == 1){
                    $proposal['businessMobile'] = $businessMobile + 1;
                }

                if($data['itemType'] == 'broadBandPlan' && $data['isBusiness'] == 0){
                    $proposal['broadband'] = $mobileBroadband + 1;
                }

                if($data['itemType'] == 'broadBandPlan' && $data['isBusiness'] == 1){
                    $proposal['businessmobileBroadband'] = $businessmobileBroadband + 1;
                }

                if($data['itemType'] == 'fixed' && $data['isBusiness'] == 1){
                    $proposal['businessFixed'] = $businessFixed + 1;
                }

                if($data['itemType'] == 'fixed' && $data['isBusiness'] == 0){
                    $proposal['fixed'] = $fixed + 1;
                }

                if($data['itemType'] == 'hro' && $data['isBusiness'] == 1){
                    $proposal['businessHRO'] = $businessHRO + 1;
                }

                if($data['itemType'] == 'hro' && $data['isBusiness'] == 0){
                    $proposal['hro'] = $hro + 1;
                }

                if($data['itemType'] == 'miscellaneous'){
                    $proposal['miscellaneous'] = $miscellaneous + 1;
                }

                if($data['itemType'] == 'entertainment'){
                    $proposal['entertainment'] = $entertainment + 1;
                }

                if($data['itemType'] == 'outright'){
                    $proposal['outright'] = $outright + 1;
                }
            }
            $proposalData[] = $proposal;
        }
        //dd($proposalData);
        return $proposalData;
    }

    public function getProposalHistoryBreakdown(Request $request){
        session_start();
        $proposals = ProposalTemplates::where('email', 'ryan.david@azenko.com.au')->orderBy('created_at','desc')->take(10)->get();
        $mobile = 0;
        $businessMobile = 0;
        $businessmobileBroadband = 0;
        $mobileBroadband = 0;
        $businessFixed = 0;
        $fixed = 0;
        $businessHRO = 0;
        $hro = 0;
        $entertainment = 0;
        $miscellaneous = 0;
        $outright = 0;
        foreach ($proposals as $key => $proposal) {
            //dd($proposal['data']);
            foreach ($proposal['data']['quadrantCart'] as $key => $data) {
                
                if($data['itemType'] == 'mobilePlan' && $data['isBusiness'] == 0){
                    ($mobile++);
                }

                if($data['itemType'] == 'mobilePlan' && $data['isBusiness'] == 1){
                    ($businessMobile++);
                }

                if($data['itemType'] == 'broadBandPlan' && $data['isBusiness'] == 0){
                    ($mobileBroadband++);
                }

                if($data['itemType'] == 'broadBandPlan' && $data['isBusiness'] == 1){
                    ($businessmobileBroadband++);
                }

                if($data['itemType'] == 'fixed' && $data['isBusiness'] == 1){
                    ($businessFixed++);
                }

                if($data['itemType'] == 'fixed' && $data['isBusiness'] == 0){
                    ($fixed++);
                }

                if($data['itemType'] == 'hro' && $data['isBusiness'] == 1){
                    ($businessHRO++);
                }

                if($data['itemType'] == 'hro' && $data['isBusiness'] == 0){
                    ($hro++);
                }

                if($data['itemType'] == 'miscellaneous'){
                    ($miscellaneous++);
                }

                if($data['itemType'] == 'entertainment'){
                    ($entertainment++);
                }

                if($data['itemType'] == 'outright'){
                    ($outright++);
                }
            }
        }

        return ['mobile' => $mobile, 
            'businessMobile' => $businessMobile, 
            'mobileBroadband' => $mobileBroadband, 
            'businessmobileBroadband' => $businessmobileBroadband, 
            'fixed' => $fixed, 
            'businessFixed' => $businessFixed,
            'businessHRO' => $businessHRO, 
            'hro' => $hro,
            'miscellaneous' => $miscellaneous, 
            'entertainment' => $entertainment,
            'outright' => $outright,
        ];
    }

    public function cartHistory(Request $request){
        session_start();
        return view('cart-history');  
    }

    public function useProposalHistory(Request $request){
        $input = $request->all();
        // dd($input);
        unset($_SESSION['currentCartTemplate']);
        // $input = $request->all();
        // $cart = CartTemplates::where(['id' => $input['data']])->first();
        $_SESSION['currentCartTemplate'] = collect($input)->toArray();
        return 'success';
    }
}