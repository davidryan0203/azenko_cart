<?php
namespace App\Http\Controllers;

use App\Hro;
use App\MobileDevices;
use App\MobileDevicesCategories;
use Illuminate\Http\Request;
use PDF;

class SimController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	/**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        session_start();
        return view('sim_plans.index');  
    }


}