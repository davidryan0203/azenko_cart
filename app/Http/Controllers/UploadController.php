<?php

namespace App\Http\Controllers;

use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\MobilePlanPage;
use App\BusinessMobileDevices;
use App\TempBusinessMobileDevices;
use App\DeviceCategories;
use App\BusinessMobileDevicesCategories;
use App\BusinessMobilePlanPage;
use App\TempBusinessMobileDevicesCategories;
use App\TempMobileBroadBandDevices;
use App\TempMobileBroadBandCategories;
use App\MobileBroadbandCategories;
use App\MobileBroadband;
use App\TempBusinessMobileBroadBandCategories;
use App\TempBusinessMobileBroadBandDevices;
use App\BusinessMobileBroadband;
use App\BusinessMobileBroadbandCategories;
use App\RedemptionProducts;
use App\TempHRO;
use App\Hro;
use App\TelstraAccessories;
use App\AdminPromotions;
use App\MobileDevices;
use App\TempMobileDevices;
use Illuminate\Support\Facades\File; 

use Symfony\Component\Process\Process;

class UploadController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function businessMobileDeviceUpload()
    {
        return view('admin.uploads.business-mobile-plans');  
    }

    public function hroUpload()
    {
        return view('admin.uploads.hro');  
    }

    public function mobileBroadbandPlanUpload()
    {
        return view('admin.uploads.mobile-broadband-upload');  
    }

    public function businessMobileBroadbandUpload()
    {
        return view('admin.uploads.business-mobile-broadband-upload');  
    }

    public function uploader(Request $request){
        $input = $request->all();
        (Storage::disk('public')->put('business-mobiles-plans.pdf', file_get_contents($_FILES['link']['tmp_name'])));   
        die;
        $removeCommand = escapeshellcmd('rm -rf /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/business-uploads/mobile/business-mobiles-plans-page-1-table-2.csv /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/business-uploads/mobile/business-mobiles-plans-page-2-table-1.csv');

        // $removeOutput = exec($removeCommand);
        
        $command = escapeshellcmd("camelot  --format csv --pages all  --output /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/business-uploads/mobile/business-mobile.csv lattice business-mobiles-plans.pdf -scale 40 -shift r -shift b");
        
        //$command = exec('python3 /var/www/azenko_cart/public/test.py');
        // $outputCommand = exec('python3 /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/business-uploads/mobile/uploadMobilePhones.py');

        //$outputCommand = exec('python3 /var/www/azenko_cart/public/uploadMobilePhones.py');
        $outputCommand = exec($command);
        //dd($outputCommand);
        //die;
        TempBusinessMobileDevicesCategories::query()->truncate();
        TempBusinessMobileDevices::query()->truncate();
        //die;
        
        //for ($x = 1; $x <= 10; $x++) {
            \Excel::load(public_path().'/business-uploads/mobile/business-mobile-page-1-table-2.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        //     if($x == 1){
        //         break;
        //     }
        // }
        \Excel::load(public_path().'/business-uploads/mobile/business-mobile-page-2-table-1.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        $deviceCategories2 = TempBusinessMobileDevicesCategories::all();
        $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        TempBusinessMobileDevicesCategories::query()->truncate();
        $i = 1;

        foreach($catResults as $k => $catResult){
            $category = new TempBusinessMobileDevicesCategories;
            $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
            $category->category_name = $catResult['category_name'];
            $category->save();
        }

        $mobDevices = TempBusinessMobileDevices::all();
        $mobDevicesCategory = TempBusinessMobileDevicesCategories::all();
        foreach($mobDevices as $mobDevice){
            $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

            TempBusinessMobileDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        }
        return 'success';

        $promotionDevices = AdminPromotions::with('business_mobile_devices')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->get();

        $tempMobileDevices = TempBusinessMobileDevices::all();
        $promotions = [];
        foreach ($promotionDevices as $key => $data) {
            foreach ($tempMobileDevices as $key => $value) {
                if($data['business_mobile_devices']['model'].$data['business_mobile_devices']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['device_id' => $value['id']]);
                    
                }
            }
        }
        return 'success';
    }

    public function testMobileUploadPromotions(Request $request){
        $mobileDevices = TempMobileDevices::all();
        $promotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '0')->get();

        $businessPromotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '1')->get();
        //dd($promotionDevices);
        $deviceIds = [];
        $bMobileDeviceId = [];
        $mobileBroadbands = [];
        $bMobileBroadbands = [];
        foreach ($promotionDevices as $key => $data) {
            foreach ($mobileDevices as $key => $value) {
                if($data['mobile_devices']['model'].$data['mobile_devices']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['device_id' => $value['id']]);
                    $deviceIds[] = $data['id'];
                }
            }
        }
        //dd($deviceIds);
        
        $broadBandDevices = TempMobileBroadBandDevices::all();
        //dd($broadBandDevices);
        $broadbanddeviceIds = [];
        foreach ($promotionDevices as $key => $data) {
            foreach ($broadBandDevices as $key => $value) {
                if($data['mobile_broadband']['model'].$data['mobile_broadband']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['broadband_device_id' => $value['id']]);
                    $broadbanddeviceIds[] = $data['id'];
                }
            }
        }

        

        $businssMobileDevices = TempBusinessMobileDevices::all();
        //dd($businessPromotionDevices);
        foreach ($businessPromotionDevices as $key => $data) {
            foreach ($businssMobileDevices as $key => $value) {
                if($data['mobile_devices']['model'].$data['mobile_devices']['capacity'] == $value['model'].$value['capacity']){
                    //dd($data['id']);
                    AdminPromotions::where('id', $data['id'])->update(['device_id' => $value['id']]);
                    $bMobileDeviceId[] = $data['id'];
                    
                }
            }
        }

        $businessBroadBandDevices = TempBusinessMobileBroadBandDevices::all();
        $businessBroadbanddeviceIds = [];
        foreach ($businessPromotionDevices as $key => $data) {
            foreach ($businessBroadBandDevices as $key => $value) {
                if($data['mobile_broadband']['model'].$data['mobile_broadband']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['broadband_device_id' => $value['id']]);
                    $bMobileBroadbands[] = $data['id'];
                }
            }
        }
        //dd($bMobileBroadbands);
        return 'success';
    }

    public function uploadBusinessMobilePlans(Request $request){
        $input = $request->all();
        \File::delete('/var/www/okart/public/business-mobiles-plans.pdf');
        (Storage::disk('public')->put('business-mobiles-plans.pdf', file_get_contents($_FILES['link']['tmp_name'])));
        //die;

        if(file_exists(public_path() . '/business-uploads/mobile/business-mobile-page-1-table-1.csv')){
            File::delete(public_path() .'/business-uploads/mobile/business-mobile-page-1-table-1.csv');
        }

        if(file_exists(public_path() . '/business-uploads/mobile/business-mobile-page-1-table-2.csv')){
            \File::delete(public_path() . '/business-uploads/mobile/business-mobile-page-1-table-2.csv');
        }

        if(file_exists(public_path() . '/business-uploads/mobile/business-mobile-page-1-table-3.csv')){
            \File::delete('/var/www/okart/public/business-uploads/mobile/business-mobile-page-1-table-3.csv');
        }

        if(file_exists(public_path() . '/business-uploads/mobile/business-mobile-page-2-table-1.csv')){
            \File::delete(public_path() . '/business-uploads/mobile/business-mobile-page-2-table-1.csv');
        }
       // dd("!23");
        //die;
        $process = exec('sudo -u ryan /home/ryan/.local/bin/camelot  --format csv --pages all  --output /var/www/okart/public/business-uploads/mobile/business-mobile.csv lattice /var/www/okart/public/business-mobiles-plans.pdf -scale 40 -shift r -shift b');
        
        TempBusinessMobileDevicesCategories::query()->truncate();
        TempBusinessMobileDevices::query()->truncate();
        
        if(file_exists('/var/www/okart/public/business-uploads/mobile/business-mobile-page-1-table-3.csv')){
            \Excel::load(public_path().'/business-uploads/mobile/business-mobile-page-1-table-3.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        //dd($data);
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        }else{
            \Excel::load(public_path().'/business-uploads/mobile/business-mobile-page-1-table-3.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        //dd($data);
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        }

        \Excel::load(public_path().'/business-uploads/mobile/business-mobile-page-2-table-1.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileDevicesCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileDevicesCategories::all();
                $mobileDeviceCategories = DeviceCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['US$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->device_category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->blue_tick = $data['0'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        $deviceCategories2 = TempBusinessMobileDevicesCategories::all();
        $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        TempBusinessMobileDevicesCategories::query()->truncate();
        $i = 1;

        foreach($catResults as $k => $catResult){
            $category = new TempBusinessMobileDevicesCategories;
            $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
            $category->category_name = $catResult['category_name'];
            $category->save();
        }

        $mobDevices = TempBusinessMobileDevices::all();
        $mobDevicesCategory = TempBusinessMobileDevicesCategories::all();
        foreach($mobDevices as $mobDevice){
            $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

            TempBusinessMobileDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        }
        return 'success';
    }

    public function uploadHro(Request $request){
        TempHRO::query()->truncate();
        $input = $request->all();
        $file = $request->file('link');   
        //dd($input);
        config(['excel.import.startRow' => 10]);
        \Excel::load($file, function($reader) {
            $file = $reader->takeColumns(6)->toArray();
            dd($file);
        });
    }

    public function acceptUploadBusinessMobilePlans(Request $request){
        BusinessMobileDevices::query()->truncate();
        
        $mobileDevices = TempBusinessMobileDevices::get();
        foreach ($mobileDevices as $key => $data) {
            //dd($data);
            $device = new BusinessMobileDevices;
            $device->category_id = $data['category_id'];
            $device->device_category_id = $data['device_category_id'];
            $device->make = $data['make'];
            $device->model = $data['model'];
            $device->capacity = $data['capacity'];
            $device->network = $data['network'];
            $device->blue_tick = $data['blue_tick'];
            $device->rrp_incl_gst = $data['rrp_incl_gst'];
            $device->mths_12_device_payment_contract = $data['mths_12_device_payment_contract'];
            $device->mths_24_device_payment_contract = $data['mths_24_device_payment_contract'];
            $device->mths_36_device_payment_contract = $data['mths_36_device_payment_contract'];
            $device->save();
        }

        $mobileDevices = TempMobileDevices::all();
        $promotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '0')->get();

        $businessPromotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '1')->get();
        //dd($promotionDevices);
        $deviceIds = [];
        $bMobileDeviceId = [];
        $mobileBroadbands = [];
        $bMobileBroadbands = [];

        $businssMobileDevices = TempBusinessMobileDevices::all();
        //dd($businessPromotionDevices);
        foreach ($businessPromotionDevices as $key => $data) {
            foreach ($businssMobileDevices as $key => $value) {
                if($data['mobile_devices']['model'].$data['mobile_devices']['capacity'] == $value['model'].$value['capacity']){
                    //dd($data['id']);
                    AdminPromotions::where('id', $data['id'])->update(['device_id' => $value['id']]);
                    $bMobileDeviceId[] = $data['id'];
                    
                }
            }
        }
        return 'success';
    }

    public function uploadMobileBroadbandPlan(Request $request){
        $input = $request->all();

        \File::delete('/var/www/okart/public/consumer-mobiles-broadband-plans.pdf');
        (Storage::disk('public')->put('consumer-mobiles-broadband-plans.pdf', file_get_contents($_FILES['link']['tmp_name'])));
        //die;
        // $removeCommand = escapeshellcmd('rm -rf /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-1-table-1.csv /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-1-table-2.csv /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-2-table-1.csv');

        \File::delete('/var/www/okart/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-1-table-1.csv');
        \File::delete('/var/www/okart/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-1-table-2.csv');

        //$removeOutput = exec($removeCommand);
        
        // $command = escapeshellcmd("camelot  --format csv --pages all  --output /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans.csv lattice consumer-mobiles-broadband-plans.pdf -scale 40 -shift r -shift b");

        $process = exec('sudo -u ryan /home/ryan/.local/bin/camelot  --format csv --pages all  --output /var/www/okart/public/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans.csv lattice /var/www/okart/public/consumer-mobiles-broadband-plans.pdf -scale 40 -shift r -shift b');

        //$command = exec('python3 /var/www/azenko_cart/public/test.py');
        // $outputCommand = exec('python3 /Applications/XAMPP/xamppfiles/htdocs/shopping_cart_v2/public/consumer-uploads/mobile-broadband/uploadMobileBroadband.py');

        //$outputCommand = exec('python3 /var/www/azenko_cart/public/uploadMobilePhones.py');
        //$outputCommand = exec($command);
        //dd($outputCommand);

        TempMobileBroadBandCategories::query()->truncate();
        TempMobileBroadBandDevices::query()->truncate();
        //return;
        config(['excel.import.startRow' => 2]);
        \Excel::load(public_path().'/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-1-table-2.csv', function($reader) {
                $file = $reader->takeColumns(13)->toArray();
                //dd($file);
                foreach($file as $key => $value){
                    //dd($value);
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempMobileBroadBandCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempMobileBroadBandCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        //dd($data);
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempMobileBroadBandDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        $device->save();
                    }
                }
        });

        \Excel::load(public_path().'/consumer-uploads/mobile-broadband/consumer-mobile-broadband-plans-page-2-table-1.csv', function($reader) {
            $file = $reader->takeColumns(14)->toArray();
            $collection = collect($file)->values()->toArray();
            $file = array_map('array_values', $file);
            foreach($file as $key => $value){
                if(strlen($value[0]) < 30 && !empty($value[0])){
                    $results[] = $value;
                    $categories[] = ucfirst($value[0]);
                }
            }
            //save devices category
            $categoryResult = collect($categories)->unique()->values()->toArray();
            //dd($categoryResult);
            $i = 1;
            foreach($categoryResult as $k => $value){
                if($value != 'Make'){
                    $category = new TempMobileBroadBandCategories;
                    $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                    $category->category_name = $value;
                    $category->save();
                }
            }
                //dd($results);

            $deviceCategories = TempMobileBroadBandCategories::all();
            foreach ($results as $key => $data) {
                if(($data[0]) && $data[0] != 'Make'){
                    $collection = collect($deviceCategories);
                    
                    $category = $collection->firstWhere('category_name', ucfirst($data[0]));
                    $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data[4])) ,2));
                    $device = new TempMobileBroadBandDevices;
                    $device->category_id = ($category) ? $category->id : 0;
                    $device->make = $data[0];
                    $device->model = $data[1];
                    $device->capacity = $data[2];
                    $device->network = $data[3];
                    $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data[4])) ,2));
                    // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                    // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                    $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data[6]) ? $data[6] : $data[6] )) ,2);
                    $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data[7]) ? $data[7] : $data[7] )) ,2);
                    $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data[8]) ? $data[8] : $data[8])) ,2);
                    // $device->product_code = $data['product_code'];
                    $device->save();
                }
            }
        });

        $deviceCategories2 = TempMobileBroadBandCategories::all();
        $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        TempMobileBroadBandCategories::query()->truncate();
        $i = 1;

        foreach($catResults as $k => $catResult){
            $category = new TempMobileBroadBandCategories;
            $category->image_url = '/img/mobile_broadband/Prod'.$i++.'.png';
            $category->category_name = $catResult['category_name'];
            $category->save();
        }

        $mobDevices = TempMobileBroadBandDevices::all();
        $mobDevicesCategory = TempMobileBroadBandCategories::all();
        foreach($mobDevices as $mobDevice){
            $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

            TempMobileBroadBandDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        }
        return 'success';
    }

    public function acceptUploadMobileBroadbandPlans(Request $request){
        //MobileBroadbandCategories::query()->truncate();
        MobileBroadband::query()->truncate();

        // $categories = TempMobileBroadBandCategories::all();
        // foreach (collect($categories)->values()->toArray() as $key => $data) {
        //     $category = new MobileBroadbandCategories;
        //     $category->image_url = $data['image_url'];
        //     $category->category_name = $data['category_name'];
        //     $category->save();
        // }
        
        $mobileBroadbands = TempMobileBroadBandDevices::all();

        foreach (collect($mobileBroadbands)->values()->toArray() as $key => $data) {
            $device = new MobileBroadband;
            $device->category_id = ($data['category_id']) ? $data['category_id'] : 0;
            $device->make = $data['make'];
            $device->model = $data['model'];
            $device->capacity = $data['capacity'];
            $device->network = $data['network'];
            $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrp_incl_gst'])) ,2));
            // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
            // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
            $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_12_device_payment_contract']) ? $data['mths_12_device_payment_contract'] : $data['mths_12_device_payment_contract'] )) ,2);
            $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_24_device_payment_contract']) ? $data['mths_24_device_payment_contract'] : $data['mths_24_device_payment_contract'] )) ,2);
            $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_36_device_payment_contract']) ? $data['mths_36_device_payment_contract'] : $data['mths_36_device_payment_contract'])) ,2);
            // $device->product_code = $data['product_code'];
            $device->save();
        }

        $mobileDevices = TempMobileDevices::all();
        $promotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '0')->get();

        $businessPromotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '1')->get();
        //dd($promotionDevices);
        $deviceIds = [];
        $bMobileDeviceId = [];
        $mobileBroadbands = [];
        $bMobileBroadbands = [];
        //dd($deviceIds);
        
        $broadBandDevices = TempMobileBroadBandDevices::all();
        //dd($broadBandDevices);
        $broadbanddeviceIds = [];
        foreach ($promotionDevices as $key => $data) {
            foreach ($broadBandDevices as $key => $value) {
                if($data['mobile_broadband']['model'].$data['mobile_broadband']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['broadband_device_id' => $value['id']]);
                    $broadbanddeviceIds[] = $data['id'];
                }
            }
        }

        return 'success';
    }

    public function uploadBusinessMobileBroadbandPlan(Request $request){
        $input = $request->all();
        \File::delete('/var/www/okart/public/business-mobiles-broadband-plans.pdf');

        
        \File::delete('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-1-table-1.csv.csv');

        if(file_exists('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-1-table-2.csv')){
            \File::delete('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-1-table-2.csv');
        }

        if(file_exists('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-2-table-1.csv')){
            \File::delete('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-2-table-1.csv');
        }

        (Storage::disk('public')->put('business-mobiles-broadband-plans.pdf', file_get_contents($_FILES['link']['tmp_name'])));

        $process = exec('sudo -u ryan /home/ryan/.local/bin/camelot  --format csv --pages all  --output /var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans.csv lattice /var/www/okart/public/business-mobiles-broadband-plans.pdf -scale 40 -shift r -shift b');
      
        
        TempBusinessMobileBroadbandCategories::query()->truncate();
        TempBusinessMobileBroadbandDevices::query()->truncate();
        
        \Excel::load(public_path().'/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-1-table-2.csv', function($reader) {
                $file = $reader->takeColumns(13)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileBroadbandCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileBroadbandCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileBroadbandDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });

        if(file_exists('/var/www/okart/public/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-2-table-1.csv')){
            \Excel::load(public_path().'/business-uploads/mobile-broadband/business-mobile-broadband-plans-page-2-table-1.csv', function($reader) {
                $file = $reader->takeColumns(14)->toArray();
                foreach($file as $key => $value){
                    if(strlen($value['make']) < 30 && !empty($value['make'])){
                        $results[] = $value;
                        $categories[] = ucfirst($value['make']);
                    }
                }
                //save devices category
                $categoryResult = collect($categories)->unique()->values()->toArray();
                //dd($categoryResult);
                $i = 1;
                foreach($categoryResult as $k => $value){
                    if($value != 'Make'){
                        $category = new TempBusinessMobileBroadbandCategories;
                        $category->image_url = '/img/mobile_plans/Prod'.$i++.'.png';
                        $category->category_name = $value;
                        $category->save();
                    }
                }

                $deviceCategories = TempBusinessMobileBroadbandCategories::all();
                foreach ($results as $key => $data) {
                    if(($data['model']) && $data['make'] != 'Make'){
                        $collection = collect($deviceCategories);
                        
                        $category = $collection->firstWhere('category_name', ucfirst($data['make']));
                        $numbers[] = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        $device = new TempBusinessMobileBroadbandDevices;
                        $device->category_id = ($category) ? $category->id : 0;
                        $device->make = $data['make'];
                        $device->model = $data['model'];
                        $device->capacity = $data['capacity'];
                        $device->network = $data['network'];
                        $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrpinc_gst'])) ,2));
                        // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
                        // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
                        $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['12_months']) ? $data['12_months'] : $data['12_month'] )) ,2);
                        $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['24_months']) ? $data['24_months'] : $data['24_month'] )) ,2);
                        $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['36_months']) ? $data['36_months'] : $data['36_month'])) ,2);
                        // $device->product_code = $data['product_code'];
                        $device->save();
                    }
                }
            });
        }

        $deviceCategories2 = TempBusinessMobileBroadbandCategories::all();
        $catResults = collect($deviceCategories2)->unique('category_name')->values()->toArray();
        TempBusinessMobileBroadbandCategories::query()->truncate();
        $i = 1;

        foreach($catResults as $k => $catResult){
            $category = new TempBusinessMobileBroadbandCategories;
            $category->image_url = '/img/mobile_broadband/Prod'.$i++.'.png';
            $category->category_name = $catResult['category_name'];
            $category->save();
        }

        $mobDevices = TempBusinessMobileBroadbandDevices::all();
        $mobDevicesCategory = TempBusinessMobileBroadbandCategories::all();
        foreach($mobDevices as $mobDevice){
            $category = $mobDevicesCategory->firstWhere('category_name', ucfirst($mobDevice['make']));

            TempBusinessMobileBroadbandDevices::where('id',$mobDevice['id'])->update(['category_id' => $category->id]);
        }
        return 'success';
    }

    public function acceptUploadBusinessMobileBroadbandPlans(Request $request){
        BusinessMobileBroadband::query()->truncate();
        $mobileBroadbands = TempBusinessMobileBroadBandDevices::all();

        foreach (collect($mobileBroadbands)->values()->toArray() as $key => $data) {
            $device = new BusinessMobileBroadband;
            $device->category_id = ($data['category_id']) ? $data['category_id'] : 0;
            $device->make = $data['make'];
            $device->model = $data['model'];
            $device->capacity = $data['capacity'];
            $device->network = $data['network'];
            $device->rrp_incl_gst = str_replace(',','',number_format( floatval(str_replace(['$', ' ',','], '', $data['rrp_incl_gst'])) ,2));
            // $device->mths_24_incl_gst = number_format( floatval(str_replace('$', '', $data['24_monthdiscountinc_gst'])) ,2);
            // $device->mths_36_incl_gst = number_format( floatval(str_replace('$', '', $data['36_monthdiscountinc_gst'])) ,2);
            $device->mths_12_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_12_device_payment_contract']) ? $data['mths_12_device_payment_contract'] : $data['mths_12_device_payment_contract'] )) ,2);
            $device->mths_24_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_24_device_payment_contract']) ? $data['mths_24_device_payment_contract'] : $data['mths_24_device_payment_contract'] )) ,2);
            $device->mths_36_device_payment_contract = number_format( floatval(str_replace('$', '', isset($data['mths_36_device_payment_contract']) ? $data['mths_36_device_payment_contract'] : $data['mths_36_device_payment_contract'])) ,2);
            // $device->product_code = $data['product_code'];
            $device->save();
        }

        $mobileDevices = TempMobileDevices::all();
        $promotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '0')->get();

        $businessPromotionDevices = AdminPromotions::with('mobile_devices','mobile_broadband')->where('device_id' , '!=' , '0')->where('device_id', '<', '300')->where('is_business' , '1')->get();
        //dd($promotionDevices);
        $deviceIds = [];
        $bMobileDeviceId = [];
        $mobileBroadbands = [];
        $bMobileBroadbands = [];

        $businessBroadBandDevices = TempBusinessMobileBroadBandDevices::all();
        $businessBroadbanddeviceIds = [];
        foreach ($businessPromotionDevices as $key => $data) {
            foreach ($businessBroadBandDevices as $key => $value) {
                if($data['mobile_broadband']['model'].$data['mobile_broadband']['capacity'] == $value['model'].$value['capacity']){
                    AdminPromotions::where('id', $data['id'])->update(['broadband_device_id' => $value['id']]);
                    $bMobileBroadbands[] = $data['id'];
                }
            }
        }
        return 'successess';
    }

    public function rejectBusinessMobileUpload(Request $request){
        TempBusinessMobileDevicesCategories::query()->truncate();
        TempBusinessMobileDevices::query()->truncate();

        return 'success';
    }

    public function rejectMobileBroadBandUpload(){
        TempMobileBroadBandCategories::query()->truncate();
        TempMobileBroadBandDevices::query()->truncate();

        return 'success';
    }

    public function rejectBusinessMobileBroadBandUpload(){
        TempBusinessMobileBroadbandCategories::query()->truncate();
        TempBusinessMobileBroadbandDevices::query()->truncate();

        return 'success';
    }

    public function uploadRedemptionsProducts(request $request){
        RedemptionProducts::query()->truncate();
        config(['excel.import.startRow' => 1]);
        \Excel::load(public_path().'/19052021_tplus.xlsx', function($reader) {
            $file = $reader->toArray();
            
            foreach ($file as $key => $data) {
                $product = new RedemptionProducts;
                if(!empty($data['sku'])){
                    //dd($data);
                    $product->sku = $data['sku'];
                    $product->product = rtrim($data['product']);
                    // $product->start = $data['start'];
                    // $product->end = $data['end'];
                    $product->offer = 'Persistent';
                    $product->rrp = $data['rrp'];
                    $product->points = $data['points'];
                    $product->pay = $data['pay'];
                    $product->product2 = $data['product2'];
                    $product->product3 = $data['product3'];
                    $product->url = $data[0];
                    $product->save();
                }
            }
        });
        $emptyImages = RedemptionProducts::where('url','=', null)->update(['url' => "/img/product-coming-soon.png"]);
        return 'upload success';
    }

    public function updateRedemptionProductImage(){
        config(['excel.import.startRow' => 1]);
        \Excel::load(public_path().'/redemption_product_images.xlsx', function($reader) {
            $file = $reader->toArray();
            //dd($file);
            foreach ($file as $key => $data) {
                $image = ($data['image_link']) ;
                $product = RedemptionProducts::where('product',rtrim($data['product']))->update(['url' => $image]);
            }
        });
        $emptyImages = RedemptionProducts::where('url','not like', "%https%")->update(['url' => "/img/product-coming-soon.png"]);
        return 'upload success';
    }

    public function uploadTLSAccessories(){
        TelstraAccessories::query()->truncate();
        config(['excel.import.startRow' => 1]);
        \Excel::load(public_path().'/TLS accessories range - simplified.xlsx', function($reader) {
            $file = $reader->toArray();
            
            foreach ($file as $key => $data) {
                $product = new TelstraAccessories;
                dd($data);
                if(!empty($data['item'])){
                    // dd($data);
                    // dd(intval($data['item']));
                    $product->sku = intval($data['item']);
                    $product->rims_id = intval($data['rims_id']);
                    $product->name = rtrim($data['item_description']);
                    $product->save();
                }
            }
        });
        return 'upload success';
    }

    public function compareAccessories(){
        $accessories1 = RedemptionProducts::all()->pluck('sku');
        $uploadTLSAccessories2 = TelstraAccessories::whereNotIn('sku' , collect($accessories1)->unique()->toArray())->get();
        //dd($uploadTLSAccessories1);
        
        $accessories2 = TelstraAccessories::all()->pluck('sku');
        $uploadTLSAccessories1 = RedemptionProducts::whereNotIn('sku' , collect($accessories2)->unique()->toArray())->get();
        // \Excel::create('Filename', function($excel) {
        //     dd($uploadTLSAccessories1);
        // })->download('xlsx');

        $arr = json_encode((collect($uploadTLSAccessories2)->pluck('sku')->toArray()));
        dd($arr);
        $myArray = explode(',', $arr);
        dd($myArray);
    }

    public function uploadBatching(Request $request){
        $input = $request->all();
        $file = $request->file('link');   

        $redemptionProducts = RedemptionProducts::all();
        $queryResults = collect($redemptionProducts)->toArray();
        $results = [];
        foreach ($queryResults as $key => $data) {
            $results[$data['sku']] = $data;
        }
        //dd($results);
        $redemptionProductsResults = [];

        config(['excel.import.startRow' => 1]);

        //$excelResults = \Excel::load(public_path().'/29122020_telstra_plus.xlsx', function($reader) {
        $excelResults = \Excel::load($file, function($reader) {

        })->toArray();

        foreach ($excelResults as $key => $data) {
            $diffArray[$data['sku']][] = $data;
        }
        //dd($diffArray);
        //dd($diffArray);
        $diffArrayResult = array_diff_key(collect($results)->toArray(),collect($diffArray)->toArray());
        return collect($diffArrayResult)->values()->toArray();
    }
}