<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\MobilePlanPage;
use App\MobileDevices;
use App\DeviceCategories;
use App\MobileDevicesCategories;

class FixedController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        session_start();
        return view('fixed.index');
    }

    public function businessFixedPlan(){
        session_start();
    	return view('fixed.business-fixed');
    }
}