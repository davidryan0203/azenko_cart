<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempBusinessMobileBroadBandDevices extends Model
    {
        protected $table = 'temp_business_mobile_broadband';

        public function device_category()
	    {
	        return $this->belongsTo('App\TempBusinessMobileBroadBandCategories', 'category_id');
	    }
    }
?>