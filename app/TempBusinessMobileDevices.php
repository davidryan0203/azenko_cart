<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempBusinessMobileDevices extends Model
    {
        protected $table = 'temp_business_mobile_devices';

        public function device_category()
	    {
	        return $this->belongsTo('App\TempBusinessMobileDevicesCategories', 'category_id');
	    }
    }
?>