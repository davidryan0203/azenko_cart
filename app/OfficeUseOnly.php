<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class OfficeUseOnly extends Model
    {
        protected $table = 'office_use_only';

        public function getDataAttribute($value){
	        return json_decode($value, true);
	    }
    }
?>