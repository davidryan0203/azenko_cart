<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class Discounts extends Model
    {
        protected $table = 'discounts';

        public function mobile_devices()
	    {
	        return $this->belongsTo('App\MobileDevices', 'mobile_device_id');
	    }

        public function getAroDeviceAttribute($value){
            return json_decode($value, true);
        }
    }
?>