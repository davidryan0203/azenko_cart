<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class BusinessMobileDevices extends Model
    {
        protected $table = 'business_mobile_devices';

        public function device_category()
	    {
	        return $this->belongsTo('App\BusinessMobileDevicesCategories', 'category_id');
	    }
    }
?>