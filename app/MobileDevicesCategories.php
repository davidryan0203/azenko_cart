<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class MobileDevicesCategories extends Model
    {
        protected $table = 'mobile_devices_category';

        public function categories()
	    {
	        return $this->hasMany('App\DeviceCategories', 'category_id');
	    }
    }
?>