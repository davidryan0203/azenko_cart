<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class ProposalTemplates extends Model
    {
        protected $table = 'proposal_templates';

        public function getDataAttribute($value){
	        return json_decode($value, true);
	    }
    }
?>