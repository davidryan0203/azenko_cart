<?php 
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class TempMobileDevicesCategories extends Model
    {
        protected $table = 'temp_mobile_devices_category';

        public function categories()
	    {
	        return $this->hasMany('App\DeviceCategories', 'category_id');
	    }
    }
?>