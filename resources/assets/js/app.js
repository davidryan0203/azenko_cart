
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('mobile-plan', require('./components/MobilePlan.vue').default);
Vue.component('cart-dropdown', require('./components/Cart.vue').default);
Vue.component('plans', require('./components/Plans.vue').default);
Vue.component('business-mobile-plan', require('./components/BusinessMobilePlans.vue').default);
Vue.component('cart-items', require('./components/CartItems.vue').default);

Vue.component('sim-plans', require('./components/SimPlans.vue').default);
Vue.component('mobile-broadband', require('./components/MobileBroadband.vue').default);
Vue.component('mobile-plan-cart', require('./components/MobilePlanCart.vue').default);
Vue.component('mobile-broadband-cart', require('./components/MobileBroadbandCart.vue').default);
Vue.component('hro-cart', require('./components/HroCart.vue').default);
Vue.component('slider', require('./components/Slider.vue').default);

Vue.component('hro', require('./components/Hro.vue').default);

Vue.component('upload-mobile-plans', require('./components/UploadMobilePlans.vue').default);
Vue.component('business-mobile-broadband-upload', require('./components/BusinessMobileBroadbandUpload.vue').default);
Vue.component('mobile-broadband-upload', require('./components/MobileBroadBandUpload.vue').default);
Vue.component('business-mobile-plans', require('./components/BusinessMobilePlan.vue').default);

Vue.component('mobile-categories', require('./components/MobileCategories.vue').default);
Vue.component('fixed', require('./components/Fixed.vue').default);
Vue.component('fixed-cart', require('./components/FixedCart.vue').default);
Vue.component('entertainment', require('./components/Entertainment.vue').default);
Vue.component('entertainment-cart', require('./components/EntertainmentCart.vue').default);

Vue.component('business-mobile-plan-cart', require('./components/BusinessMobilePlanCart.vue').default);
Vue.component('business-hro', require('./components/BusinessHRO.vue').default);
Vue.component('business-hro-cart', require('./components/BusinessHROCart.vue').default);
Vue.component('business-mobile-broadband', require('./components/BusinessMobileBroadband.vue').default);
Vue.component('business-mobile-broadband-cart', require('./components/BusinessMobileBroadbandCart.vue').default);
Vue.component('business-fixed', require('./components/BusinessFixed.vue').default);
Vue.component('business-fixed-cart', require('./components/BusinessFixedCart.vue').default);
Vue.component('existing-services', require('./components/ExistingServices.vue').default);
Vue.component('promotions', require('./components/Promotions.vue').default);
Vue.component('available-promotions', require('./components/AvailablePromotions.vue').default);
Vue.component('admin-mobile-category-selection', require('./components/AdminMobilePlanCategorySelection.vue').default);
Vue.component('admin-business-mobile-category-selection', require('./components/AdminBusinessMobilePlanCategorySelection.vue').default);
Vue.component('quadrant', require('./components/Quadrant.vue').default);
Vue.component('telstra-redemption', require('./components/TelstraRedemption.vue').default);
Vue.component('redemptions', require('./components/Redemptions.vue').default);

Vue.component('aro-form', require('./components/AROForm.vue').default);
Vue.component('hro-upload', require('./components/HroUpload.vue').default);
Vue.component('discounts', require('./components/Discounts.vue').default);
Vue.component('redemption-products', require('./components/RedemptionProducts.vue').default);
Vue.component('admin-promotions', require('./components/AdminPromotions.vue').default);

Vue.component('outright-mobile', require('./components/Outright/Mobile.vue').default);
Vue.component('outright-mobile-broadband', require('./components/Outright/MobileBroadband.vue').default);
Vue.component('outright-accessories', require('./components/Outright/Accessories.vue').default);
Vue.component('office-use-only', require('./components/OfficeUseOnly.vue').default);
Vue.component('telstra-platinum', require('./components/TelstraPlatinum.vue').default);
Vue.component('quicklinks', require('./components/Quicklinks.vue').default);
Vue.component('available-quicklinks', require('./components/AvailableQuicklinks.vue').default);
Vue.component('cart-history', require('./components/CartHistory.vue').default);
Vue.component('cart-reports', require('./components/CartReports.vue').default);
Vue.component('reports', require('./components/Reports.vue').default);
Vue.component('consumer-hro', require('./components/ConsumerHro.vue').default);
Vue.component('console-hro', require('./components/ConsoleHro.vue').default);
Vue.component('telstra-redemption-upload', require('./components/UploadTelstraRedemption.vue').default);

import store from './store.js';
import VueToastr from "vue-toastr";
Vue.use(VueToastr, {
  progressbar: false,
});
new Vue({
    el: '#app',

    store: new Vuex.Store(store)
});