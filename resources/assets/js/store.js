let cart = window.localStorage.getItem('cart');
let simPlan = window.localStorage.getItem('simPlan');
let mobileBroadbands = window.localStorage.getItem('mobileBroadbands');
let businessMobileBroadBand = window.localStorage.getItem('businessMobileBroadBand');
let hro = window.localStorage.getItem('hro');
let fixed = window.localStorage.getItem('fixed');
let entertainment = window.localStorage.getItem('entertainment');
let businessMobilePlan = window.localStorage.getItem('businessMobilePlan');
let businessHRO = window.localStorage.getItem('businessHRO');
let businessFixed = window.localStorage.getItem('businessFixed');
let existingServices = window.localStorage.getItem('existingServices');
let otherServices = window.localStorage.getItem('otherServices');
let promotions = window.localStorage.getItem('promotions');
let cartCount = window.localStorage.getItem('cartCount');
let quadrantCart = window.localStorage.getItem('quadrantCart');
let quadrantCartCount = window.localStorage.getItem('quadrantCartCount');
let productRedemption = window.localStorage.getItem('productRedemption');

let store = {
    state: {
	    cart: cart ? JSON.parse(cart) : [],
	    simPlan: simPlan ? JSON.parse(simPlan) : [],
	    mobileBroadbands: mobileBroadbands ? JSON.parse(mobileBroadbands) : [],
	    businessMobileBroadBand: businessMobileBroadBand ? JSON.parse(businessMobileBroadBand) : [],
	    hro: hro ? JSON.parse(hro) : [],
	    fixed: fixed ? JSON.parse(fixed) : [],
	    entertainment: entertainment ? JSON.parse(entertainment) : [],
	    businessMobilePlan : businessMobilePlan ? JSON.parse(businessMobilePlan) : [],
	    businessHRO : businessHRO ? JSON.parse(businessHRO) : [],
	    businessFixed : businessFixed ? JSON.parse(businessFixed) : [],
	    existingServices : existingServices ? JSON.parse(existingServices) : [],
	    otherServices : otherServices ? JSON.parse(otherServices) : [],
	    promotions : promotions ? JSON.parse(promotions) : [],
	    cartCount: cartCount ? parseInt(cartCount) : 0,
	    quadrantCart : quadrantCart ? JSON.parse(quadrantCart) : [],
	    productRedemption : productRedemption ? JSON.parse(productRedemption) : [],
	    quadrantCartCount: quadrantCartCount ? parseInt(quadrantCartCount) : 0,
	},
    mutations: {
    	addProductRedemption(state,item){
		  	// console.log(item)
    	// 	return
    		var crypto = require("crypto");

    		state.productRedemption.push({
    			'points' : item.points,
    			'pay' : item.pay,
    			'product' : item.product,
        		'id' : crypto.randomBytes(20).toString('hex'),
        		'itemId' : item.id,
        		'selectedItem' : item.selectedItem,
	    		'customDescription' : '',
	    		'quantity' : 1
    		})
    		state.cartCount++;
    		window.localStorage.setItem('productRedemption', JSON.stringify(state.productRedemption));
		    window.localStorage.setItem('cartCount', state.cartCount);
    	},
    	updateProductRedemption(state,plan){
    		var itemObj = []
			var crypto = require("crypto");
			//console.log(plan);return
			for (let item of state.productRedemption) {
				if(item.id == plan.id){
					itemObj.push({
			    		'points' : (plan.points),
			    		'pay': plan.pay,
			    		'id': plan.id,
			    		'product' : plan.product,
        				'itemId' : plan.id,
			    		'selectedItem' : plan,
			    		'customDescription' : plan.customDescription
			    	});
				}else{
					itemObj.push({
			    		'points' : (item.points),
			    		'pay': item.pay,
			    		'id': item.id,
			    		'product' : item.product,
        				'itemId' : item.id,
			    		'selectedItem' : item.selectedItem,
			    		'customDescription' : item.customDescription
			    	});
				}

			}
			// console.log(itemObj)
			// return
			state.productRedemption = []
			for (let items of itemObj) {
	         	state.productRedemption.push({
	        		'points' : (items.points),
		    		'pay': items.pay,
		    		'id': items.id,
		    		'product' : items.product,
    				'itemId' : items.id,
		    		'selectedItem' : items.selectedItem,
		    		'customDescription' : items.customDescription
	        	})
            }
		    window.localStorage.setItem('productRedemption', JSON.stringify(state.productRedemption));
		    window.localStorage.setItem('cartCount', state.cartCount);
    	},
    	addSimToCart(state, item){
    		let vasIdArray = []
		  	// console.log(vasIdArray)
    		// console.log(Number(item.grandTotal).toFixed(2))
    		// return
    		var crypto = require("crypto");
	  		for (let val of item.valueAddedService) {
		      	vasIdArray.push(val.id) // the value of the current key.
		  	}

    		state.simPlan.push({
    			'vasIdArray' : vasIdArray,
    			'paymentDuration' : item.paymentDuration,
    			'paymentFrequency' : item.paymentFrequency,
    			'paymentFrequencyVal' : item.paymentFrequencyVal,
        		'frequency' : item.frequency,
    			'planType' : item.planType,
    			'selectedPlan' : item.selectedPlan,
    			'planValue' : item.selectedPlan,
    			'description' : item.selectedPlanType,
    			'vas' : item.valueAddedService,
        		'vasTotalAmount' : Number(item.valueAddedService.totalAmount).toFixed(2),
        		'totalPrice' : Number(item.grandTotal).toFixed(2),
        		'id' : crypto.randomBytes(20).toString('hex'),
    		})
    		state.cartCount++;
    		window.localStorage.setItem('simPlan', JSON.stringify(state.simPlan));
		    window.localStorage.setItem('cartCount', state.cartCount);
    	},
        addToCart(state, item) {
	        //let found = state.cart.find(product => product.id == item.mobileDevice.id);
	        let vasIdArray = []
	        let mobileDevice = []
	        let planOption = ''
	        let deviceDiscounts = 0
	        let discountPrice = 0
	        // console.log(item)
	        // return
	        var crypto = require("crypto");
	  		for (let val of item.valueAddedService) {
		      	vasIdArray.push(val.id) // the value of the current key.
		  	}
		  	mobileDevice.push(item.mobileDevice)

		  	if(store.state.quadrantCartCount < 4){
		  		planOption = state.quadrantCart
		  	}else{

			  	if(item.itemType == 'mobilePlan'){
			  		planOption = state.cart
			  	}

			  	if(item.itemType == 'broadBandPlan'){
			  		planOption = state.mobileBroadbands
			  	}

			  	if(item.itemType == 'businessMobilePlan'){
			  		planOption = state.businessMobilePlan
			  	}

			  	if(item.itemType == 'businessMobileBroadBand'){
			  		planOption = state.businessMobileBroadBand
			  	}
			}

		  	if(item.itemType != 'simPlan-mobile' && item.itemType != 'simPlan-broadband'){

	        	if(item.selectedPromotions){
					for(let promo of item.selectedPromotions){
						if(promo.type.name === 'Reoccuring Discount'){
							discountPrice += Number(promo.discount_price)
						}
					}
				}

	        	planOption.push({
	        		'make' :  item.mobileDevice.make,
	        		'model' : item.mobileDevice.model,
	        		'capacity' : item.mobileDevice.capacity,
	        		'totalPrice' : (((Number(item.price) + Number(item.aroPayment) + Number(item.valueAddedService.totalAmount) + Number(item.planValue) - Number(discountPrice) ).toFixed(2) * 12) / item.paymentFrequency).toFixed(2),
	        		'paymentFrequency' : item.paymentFrequency,
	        		'paymentFrequencyVal' : item.paymentFrequencyVal,
	        		'frequency' : item.frequency,
	        		'planValue' : item.planValue,
	        		'planType' : item.planType,
	        		'quantity' : 1,
	        		'aroAmount' : (Number(item.aro.base) / Number(item.aro.monthly)),
	        		'aroBase' : Number(item.aro.base).toFixed(2),
	        		'aroMonths' : Number(item.aro.monthly),
	        		'vas' : item.valueAddedService,
	        		'vasTotalAmount' : Number(item.valueAddedService.totalAmount).toFixed(2),
	        		'vasIdArray' : vasIdArray,
	        		'price' : Number(item.price).toFixed(2),
	        		'mths24Contract' : item.mobileDevice.mths_24_device_payment_contract,
	        		'mths12Contract' : item.mobileDevice.mths_12_device_payment_contract,
	        		'mths36Contract' : item.mobileDevice.mths_36_device_payment_contract,
	        		'months' : item.months,
	        		'id' : crypto.randomBytes(20).toString('hex'),
	        		'deviceCategory' : item.mobileDevice.device_category_id,
	        		'otherDevices' : item.otherDevices,
	        		'deviceItem' : item.mobileDevice,
	        		'itemType' : item.itemType,
	        		'selectedARO' : item.selectedARO,
	        		'selectedAROMonth' : item.selectedAROMonth,
	        		'planOption' : item.planOption,
	        		'mobilePlanMonths' : item.mobilePlanMonths,
	        		'discounts' : 0,
	        		'deviceDiscounts' : 0,
	        		'promotionsPrice' : 0,
	        		'promotionsDescription' : '',
	        		'customDescription' : '',
        			'aroAccessories': [{'aroID':crypto.randomBytes(20).toString('hex'),'BarCode': "",'ProductType': "",'ProductUnitRRPCost' : 0,'aroPrice': 0,'isAROBaseEqual': false}],
        			'aroAccessoriesTotalPrice' : 0,
        			'aroTotalMatched' : true,
		    		'addPromotion': {'id':0,'name':'no'},
        			'promotionSets' : [],
        			'selectedPromotion': {'id' : 0, 'name' : ''},
        			'smallDiscount' : item.smallDiscount,
					'mediumDiscount' : item.mediumDiscount,
					'largeDiscount' : item.largeDiscount,
					'xlDiscount' : item.xlDiscount,
					'xsDiscount' : item.xsDiscount,
					'promotionDiscounts': [],
					'isBusiness' : item.isBusiness,
					'selectedPromotions' : item.selectedPromotions,
					'mobileOption' : item.mobileOption
	        	});	

	        	//if(store.state.quadrantCartCount < 4){
				    store.state.quadrantCartCount++
        			state.cartCount++;
			    	window.localStorage.setItem('cartCount', state.cartCount);
			    	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
    				window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
				   
			    // }else{

		    	//     	state.cartCount++;
					  //   if(item.itemType == 'mobilePlan'){
					  //   	window.localStorage.setItem('cart', JSON.stringify(state.cart));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType == 'businessMobilePlan'){
					  //   	window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'broadBandPlan'){
					  //   	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'businessMobileBroadBand'){
					  //   	window.localStorage.setItem('businessMobileBroadBand', JSON.stringify(state.businessMobileBroadBand));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }
			    // }
	        }

	        if(item.itemType == 'simPlan-mobile'){
	        	if(item.selectedPromotions){
					for(let promo of item.selectedPromotions){
						if(promo.type.name === 'Reoccuring Discount'){
							discountPrice += Number(promo.discount_price)
						}
					}
				}

	        	planOption.push({
					'make' :  '',
					'model' : '',
					'capacity' : '',
					'totalPrice' : (Number(item.planValue) + Number(item.valueAddedService.totalAmount) - Number(discountPrice)).toFixed(2),
					'paymentFrequency' : item.paymentFrequency,
					'paymentFrequencyVal' : item.paymentFrequencyVal,
					'frequency' : item.frequency,
					'planValue' : item.planValue,
					'planType' : item.planType,
					'quantity' : 1,
	        		'aroAmount' : (Number(item.aroAmount)),
	        		'aroBase' : Number(item.aroBase).toFixed(2),
	        		'aroMonths' : Number(item.aroMonths),
					'vas' : item.valueAddedService,
					'vasTotalAmount' : Number(item.valueAddedService.totalAmount).toFixed(2),
					'vasIdArray' : vasIdArray,
					'price' : Number(0).toFixed(2),
					'mths24Contract' : 0,
					'mths12Contract' : 0,
					'mths36Contract' : 0,
					'months' : item.months,
					'id' : crypto.randomBytes(20).toString('hex'),
					'deviceCategory' : '',
					'otherDevices' : [],
					'deviceItem' : [],
					'itemType' : 'mobilePlan',
					'selectedARO' : item.selectedARO,
					'selectedAROMonth' : item.selectedAROMonth,
					'planOption' : item.planOption,
					'mobilePlanMonths' : item.mobilePlanMonths,
					'discounts' : 0,
	        		'deviceDiscounts' : 0,
					'promotionsPrice' : 0,
					'promotionsDescription' : '',
					'customDescription' : '',
					'aroAccessories': [{'aroID':crypto.randomBytes(20).toString('hex'),'BarCode': "",'ProductType': "",'ProductUnitRRPCost' : 0,'aroPrice': 0,'isAROBaseEqual': false}],
					'aroAccessoriesTotalPrice' : 0,
					'aroTotalMatched' : true,
					'addPromotion': {'id':0,'name':'no'},
        			'promotionSets' : [],
        			'selectedPromotion': {'id' : 0, 'name' : ''},
        			'smallDiscount' : item.smallDiscount,
					'mediumDiscount' : item.mediumDiscount,
					'largeDiscount' : item.largeDiscount,
					'xlDiscount' : item.xlDiscount,
					'xsDiscount' : item.xsDiscount,
					'isBusiness' : item.isBusiness,
					'promotionDiscounts': [],
					'selectedPromotions' : item.selectedPromotions,
	        		'planOption' : item.planOption,
					'mobileOption' : item.mobileOption
				});

	        	//if(store.state.quadrantCartCount < 4){
				    store.state.quadrantCartCount++
        			state.cartCount++;
			    	window.localStorage.setItem('cartCount', state.cartCount);
			    	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
    				window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
				   
			    // }else{

		    	//     	state.cartCount++;
					  //   if(item.itemType == 'mobilePlan'){
					  //   	window.localStorage.setItem('cart', JSON.stringify(state.cart));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType == 'businessMobilePlan'){
					  //   	window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'broadBandPlan'){
					  //   	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'businessMobileBroadBand'){
					  //   	window.localStorage.setItem('businessMobileBroadBand', JSON.stringify(state.businessMobileBroadBand));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }
			    // }
	        }

	        if(item.itemType == 'simPlan-broadband'){

	        	planOption.push({
					'make' :  '',
					'model' : '',
					'capacity' : '',
					'totalPrice' : (Number(item.planValue) + Number(item.valueAddedService.totalAmount)).toFixed(2),
					'paymentFrequency' : item.paymentFrequency,
					'paymentFrequencyVal' : item.paymentFrequencyVal,
					'frequency' : item.frequency,
					'planValue' : item.planValue,
					'planType' : item.planType,
					'quantity' : 1,	        		
	        		'aroAmount' : (Number(item.aroAmount)),
	        		'aroBase' : Number(item.aroBase).toFixed(2),
	        		'aroMonths' : Number(item.aroMonths),
					'vas' : item.valueAddedService,
					'vasTotalAmount' : Number(item.valueAddedService.totalAmount).toFixed(2),
					'vasIdArray' : vasIdArray,
					'price' : Number(0).toFixed(2),
					'mths24Contract' : 0,
					'mths12Contract' : 0,
					'mths36Contract' : 0,
					'months' : item.months,
					'id' : crypto.randomBytes(20).toString('hex'),
					'deviceCategory' : '',
					'otherDevices' : [],
					'deviceItem' : [],
					'itemType' : 'broadBandPlan',
					'selectedARO' : item.selectedARO,
					'selectedAROMonth' : item.selectedAROMonth,
					'planOption' : item.planOption,
					'mobilePlanMonths' : item.mobilePlanMonths,
					'discounts' : 0,
	        		'deviceDiscounts' : 0,
					'promotionsPrice' : 0,
					'promotionsDescription' : '',
					'customDescription' : '',
					'aroAccessories': [{'aroID':crypto.randomBytes(20).toString('hex'),'BarCode': "",'ProductType': "",'ProductUnitRRPCost' : 0,'aroPrice': 0,'isAROBaseEqual': false}],
					'aroAccessoriesTotalPrice' : 0,
					'aroTotalMatched' : true,
					'addPromotion': {'id':0,'name':'no'},
        			'promotionSets' : [],
        			'selectedPromotion': {'id' : 0, 'name' : ''},
        			'smallDiscount' : 0,
					'mediumDiscount' : 0,
					'largeDiscount' : 0,
					'xlDiscount' : 0,
					'xsDiscount' : 0,
					'isBusiness' : item.isBusiness,
					'selectedPromotions' : [],
	        		'planOption' : item.planOption,
					'mobileOption' : item.mobileOption
				});

	        	//if(store.state.quadrantCartCount < 4){
				    store.state.quadrantCartCount++
        			state.cartCount++;
			    	window.localStorage.setItem('cartCount', state.cartCount);
			    	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
    				window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
				   
			    // }else{

		    	//     	state.cartCount++;
					  //   if(item.itemType == 'mobilePlan'){
					  //   	window.localStorage.setItem('cart', JSON.stringify(state.cart));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType == 'businessMobilePlan'){
					  //   	window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'broadBandPlan'){
					  //   	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }

					  //   if(item.itemType === 'businessMobileBroadBand'){
					  //   	window.localStorage.setItem('businessMobileBroadBand', JSON.stringify(state.businessMobileBroadBand));
					  //   	window.localStorage.setItem('cartCount', state.cartCount);
					  //   }
			    // }
	        }
	    
        },
        removeFromCart(state, item) {
        	let planOption = ''
			if(item.itemType == 'mobilePlan' || item.itemType == 'simPlan'){
				planOption = state.cart
			}

			if(item.itemType == 'businessMobilePlan'){
				planOption = state.businessMobilePlan
			}
		    let index = planOption.indexOf(item);
		    if (index > -1) {
		        let product = planOption[index];
		        state.cartCount -= product.quantity;

		        planOption.splice(index, 1);
		    }

		    if(item.itemType == 'mobilePlan'){
		    	window.localStorage.setItem('cart', JSON.stringify(state.cart));
		    }

		    if(item.itemType == 'businessMobilePlan'){
		    	window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
		    }

		    window.localStorage.setItem('cartCount', state.cartCount);
		},
        removeFromQuadrantCart(state, item) {
        	let index = state.quadrantCart.indexOf(item);
		    if (index > -1) {
		        let product = state.quadrantCart[index];
		        state.quadrantCartCount -= product.quantity;
		        state.cartCount -= product.quantity;

		        state.quadrantCart.splice(index, 1);
		    }
		    window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		    window.localStorage.setItem('cartCount', JSON.stringify(state.cartCount));
		    window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
		},
		removeHroFromCart(state, item) {
		    let planOption = ''
			if(item.itemType == 'hro'){
				planOption = state.hro
			}

			if(item.itemType == 'businessHRO'){
				planOption = state.businessHRO
			}
		    let index = planOption.indexOf(item);
		    if (index > -1) {
		        let product = planOption[index];
		        state.cartCount -= product.quantity;

		        planOption.splice(index, 1);
		    }

		    if(item.itemType == 'hro'){
		    	window.localStorage.setItem('hro', JSON.stringify(state.hro));
		    }

		    if(item.itemType == 'businessHRO'){
		    	window.localStorage.setItem('businessHRO', JSON.stringify(state.businessHRO));
		    }

		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removeFixedFromCart(state, item) {
			let planOption = ''
			if(item.itemType == 'fixed'){
				planOption = state.fixed
			}

			if(item.itemType == 'businessFixed'){
				planOption = state.businessFixed
			}
		    let index = planOption.indexOf(item);
		    if (index > -1) {
		        let product = planOption[index];
		        state.cartCount -= product.quantity;

		        planOption.splice(index, 1);
		    }
		    if(item.itemType == 'fixed'){
		    	window.localStorage.setItem('fixed', JSON.stringify(state.fixed));
		    }

		    if(item.itemType == 'businessFixed'){
		    	window.localStorage.setItem('businessFixed', JSON.stringify(state.businessFixed));
		    }
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removeEntertainmentFromCart(state,item){
			let index = state.entertainment.indexOf(item);
		    if (index > -1) {
		        let product = state.entertainment[index];
		        state.cartCount -= product.quantity;

		        state.entertainment.splice(index, 1);
		    }
		    window.localStorage.setItem('entertainment', JSON.stringify(state.entertainment));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removePromotions(state,item){
			let index = state.promotions.indexOf(item);
		    if (index > -1) {
		        let product = state.promotions[index];
		        state.cartCount -= product.quantity;

		        state.promotions.splice(index, 1);
		    }
		    window.localStorage.setItem('promotions', JSON.stringify(state.promotions));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removeExistingServicesFromCart(state,item){
			let index = state.existingServices.indexOf(item);
		    if (index > -1) {
		        let product = state.existingServices[index];
		        state.cartCount -= product.quantity;

		        state.existingServices.splice(index, 1);
		    }
		    window.localStorage.setItem('existingServices', JSON.stringify(state.existingServices));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removeRedemption(state,item){
			let index = state.productRedemption.indexOf(item);
		    if (index > -1) {
		        let product = state.productRedemption[index];
		        state.cartCount -= product.quantity;

		        state.productRedemption.splice(index, 1);
		    }
		    window.localStorage.setItem('productRedemption', JSON.stringify(state.productRedemption));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		removeSimPlanFromCart(state, item) {
		    let index = state.simPlan.indexOf(item);

		    if (index > -1) {
		        let product = state.simPlan[index];
		        state.cartCount -= product.quantity;

		        state.simPlan.splice(index, 1);
		    }
		    window.localStorage.setItem('simPlan', JSON.stringify(state.simPlan));
		    window.localStorage.setItem('cartCount', state.cartCount);
		  
		},
		removebroadBandPlanFromCart(state, item) {
		    let planOption = ''
			if(item.itemType == 'broadBandPlan'){
				planOption = state.mobileBroadbands
			}

			if(item.itemType == 'businessMobileBroadBand'){
				planOption = state.businessMobileBroadBand
			}
		    let index = planOption.indexOf(item);
		    if (index > -1) {
		        let product = planOption[index];
		        state.cartCount -= product.quantity;

		        planOption.splice(index, 1);
		    }

		    if(item.itemType == 'broadBandPlan'){
		    	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
		    }

		    if(item.itemType == 'businessMobileBroadBand'){
		    	window.localStorage.setItem('businessMobileBroadBand', JSON.stringify(state.businessMobileBroadBand));
		    }

		    window.localStorage.setItem('cartCount', state.cartCount);
		  
		},
		saveCart(state) {
		    window.localStorage.setItem('cart', JSON.stringify(state.cart));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},

		changeSimPlanType(state, plan){
			var itemObj = []
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			var itemPrice = 0;
			for (let val of plan.vas) {
		      	vasArray.push(val) // the value of the current key.
		      	vasIdArray.push(val.id)
		      	vasTotalAmount += val.amount
		  	}

			for (let item of state.simPlan) {
				if(item.id == plan.item.id){
					itemObj.push({
		        		'vasIdArray' : vasIdArray,
		    			'paymentDuration' : item.paymentDuration,
		    			'paymentFrequency' : item.paymentFrequency,
		    			'paymentFrequencyVal' : item.paymentFrequencyVal,
        				'frequency' : plan.item.frequency,
		    			'planType' : plan.description,
		    			'selectedPlan' : plan.amount,
		    			'description' : plan.description,
		    			'vas' : plan.vas,
	    				'planValue' : plan.amount,
		        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
		        		'totalPrice' : (Number(plan.amount) + Number(vasTotalAmount)).toFixed(2),
		        		'id' : item.id,
		        	})
				}else{
					itemObj.push({
		        		'vasIdArray' : item.vasIdArray,
		    			'paymentDuration' : item.paymentDuration,
		    			'paymentFrequency' : item.paymentFrequency,
		    			'paymentFrequencyVal' : item.paymentFrequencyVal,
        				'frequency' : item.frequency,
		    			'planType' : item.description,
		    			'selectedPlan' : item.selectedPlan,
		    			'description' : item.description,
		    			'vas' : item.vas,
	    				'planValue' : item.planValue,
		        		'vasTotalAmount' : Number(item.vasTotalAmount).toFixed(2),
		        		'totalPrice' : Number(item.totalPrice).toFixed(2),
		        		'id' : item.id,
		        	})
				}
			}
			state.simPlan = []
            for (let items of itemObj) {
	         	state.simPlan.push({
	        		'vasIdArray' : items.vasIdArray,
	    			'paymentDuration' : items.paymentDuration,
	    			'paymentFrequency' : items.paymentFrequency,
	    			'paymentFrequencyVal' : items.paymentFrequencyVal,
        			'frequency' : items.frequency,
	    			'planType' : items.planType,
	    			'selectedPlan' : items.selectedPlan,
	    			'description' : items.planValue,
	    			'vas' : items.vas,
	    			'planValue' : items.description,
	        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
	        		'totalPrice' : Number(items.totalPrice).toFixed(2),
	        		'id' : items.id,
	        	})
            }
            window.localStorage.setItem('simPlan', JSON.stringify(state.simPlan));
		},
		changePlanType(state, plan){
			var itemObj = []
			var hroMonthlyPrice = 0;
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			var itemPrice = 0;
			var discountPrice = 0;
			var aroAccessories = []
			var aroAccessoriesTotalPrice = 0
			var aroAccessoriesTotal = 0
			var aroTotalMatched = false
			var deviceDiscounts = 0;

			for (let val of plan.vas) {
		      	vasArray.push(val) // the value of the current key.
		      	vasIdArray.push(val.id)
		      	vasTotalAmount += val.amount
		  	}

		  	if(plan.aroAccessories){
				for (let aro of plan.aroAccessories) {
					if((plan.aroAccessory) && aro.aroID == plan.aroAccessory.aroID){
				      	aroAccessories.push(plan.aroAccessory) // the value of the current key.

				    }else{
				    	aroAccessories.push(aro)
				    }
			  	}

			  	for (let aro of aroAccessories) {
				    aroAccessoriesTotalPrice += Number(aro.aroPrice)
			  	}

			  	if(plan.aroBase == aroAccessoriesTotalPrice){
			  		aroTotalMatched = true
			  	}
			}

			if(plan.aroAccessory && plan.aroAccessory.aroAdd == true){
				aroAccessories.push(plan.aroAccessory)
			}

			if(plan.aroAccessoryForDelete){
				let index = plan.aroAccessories.indexOf(plan.aroAccessoryForDelete);
				if (index > -1) {
					plan.aroAccessories.splice(index,1);
				}
				aroAccessories = plan.aroAccessories

				for (let aro of aroAccessories) {
				    aroAccessoriesTotal += Number(aro.aroPrice)
			  	}

			  	aroAccessoriesTotalPrice = aroAccessoriesTotal
			}

	        let planOption = ''

	        if(plan.item.itemType === 'mobilePlan'){
				planOption = state.cart
			}

			if(plan.item.itemType === 'businessMobilePlan'){
				planOption = state.businessMobilePlan
			}

			if(plan.item.itemType === 'broadBandPlan'){
				planOption = state.mobileBroadbands
			}

			if(plan.deviceDiscounts){
				deviceDiscounts = plan.deviceDiscounts
			}else{
				deviceDiscounts = 0
			}

			for (let item of planOption) {
				if(item.id == plan.item.id){
					
					if(plan.deviceItem != 0 && plan.months == "24"){
						itemPrice = plan.deviceItem.mths_24_device_payment_contract
					}

					if(plan.deviceItem != 0 && plan.months == "36"){
						itemPrice = plan.deviceItem.mths_36_device_payment_contract
					}

					if(plan.deviceItem != 0 && plan.months == "12"){
						itemPrice = plan.deviceItem.mths_12_device_payment_contract
					}

					if(plan.aroBase != 0){
						var aroAmount = (Number(plan.aroBase) / Number(plan.aroMonths)) 
					}else{
						var aroAmount = 0.00
					}

					if(plan.discounts && plan.promotionsPrice && plan.discounts.name === 'Reoccuring Discount'){
						discountPrice = Number(plan.promotionsPrice).toFixed(2)
					}else{
						discountPrice = 0
					}
					itemObj.push({
		        		'id': item.id,
		        		'customDescription' : plan.customDescription,
		        		'make' :  (plan.deviceItem != 0) ? plan.deviceItem.make : '',
		        		'model' : (plan.deviceItem != 0) ? plan.deviceItem.model : '',
		        		'capacity' : (plan.deviceItem != 0) ? plan.deviceItem.capacity : '',
		        		'totalPrice' : ((((Number(itemPrice) + Number(aroAmount)  + Number(vasTotalAmount) + Number(plan.planValue) ).toFixed(2) * 12) / item.paymentFrequency - deviceDiscounts) ).toFixed(2),
		        		'paymentFrequency' : item.paymentFrequency,
		        		'paymentFrequencyVal' : item.paymentFrequency,
		        		'frequency' : item.frequency,
		        		'planValue' : plan.planValue,
		        		'planType' : plan.planType,
		        		'planOption' : plan.planOption,
		        		'quantity' : item.quantity,
		        		'aroMonths' : plan.aroMonths,
		        		'aroAmount' : Number(aroAmount).toFixed(2),
		        		'aroBase' : Number(plan.aroBase).toFixed(2),
		        		'vas' : vasArray,
		        		'vasIdArray' : vasIdArray,
		        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
		        		'price' : Number(itemPrice).toFixed(2),
		        		'mths12Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_12_device_payment_contract : '',
		        		'mths24Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_24_device_payment_contract : '',
		        		'mths36Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_36_device_payment_contract : '',
		        		'months' : plan.months,
		        		'deviceCategory' : (plan.deviceItem != 0) ? plan.deviceItem.deviceCategory : '',
		        		'otherDevices' : plan.otherDevices,
		        		'deviceItem' : (plan.deviceItem != 0) ? plan.deviceItem : '',
		        		'itemType' : plan.itemType,
	        			'mobilePlanMonths' : plan.mobilePlanMonths,
	        			'selectedARO' : plan.selectedARO,
	        			'selectedAROMonth' : plan.selectedAROMonth,
	        			'promotionsDescription' : plan.promotionsDescription,
	        			'discounts' : plan.discounts,
	        			'deviceDiscounts' : plan.deviceDiscounts,
	        			'promotionsPrice' : plan.promotionsPrice,
	        			'aroAccessories' : aroAccessories,
	        			'aroAccessoriesTotalPrice' : aroAccessoriesTotalPrice,
	        			'aroTotalMatched' : aroTotalMatched,
	        			'addPromotion' : plan.addPromotion
		        	})


				}else{
					itemObj.push({
		        		'id': item.id,
		        		'customDescription' : item.customDescription,
		        		'make' :  item.make,
		        		'model' : item.model,
		        		'capacity' : item.capacity,
		        		'totalPrice' : Number(item.totalPrice).toFixed(2),
		        		'paymentFrequency' : item.paymentFrequency,
		        		'paymentFrequencyVal' : item.paymentFrequency,
		        		'frequency' : item.frequency,
		        		'planValue' : item.planValue,
		        		'planType' : item.planType,
		        		'planOption' : item.planOption,
		        		'quantity' : item.quantity,
		        		'aroAmount' : Number(item.aroAmount).toFixed(2),
		        		'aroBase' : Number(item.aroBase).toFixed(2),
		        		'aroMonths' : Number(item.aroMonths),
		        		'vas' : item.vas,
		        		'vasIdArray' : item.vasIdArray,
		        		'vasTotalAmount' : Number(item.vasTotalAmount).toFixed(2),
		        		'price' : Number(item.price).toFixed(2),
		        		'mths12Contract' : item.mths12Contract,
		        		'mths24Contract' : item.mths24Contract,
		        		'mths36Contract' : item.mths36Contract,
		        		'months' : item.months,
		        		'deviceCategory' : item.deviceCategory,
		        		'otherDevices' : item.otherDevices,
		        		'deviceItem' : item.deviceItem,
		        		'itemType' : plan.itemType,
	        			'mobilePlanMonths' : item.mobilePlanMonths,
	        			'selectedARO' : item.selectedARO,
	        			'selectedAROMonth' : item.selectedAROMonth,
	        			'promotionsDescription' : item.promotionsDescription,
	        			'discounts' : item.discounts,
	        			'deviceDiscounts' : item.deviceDiscounts,
	        			'promotionsPrice' : item.promotionsPrice,
	        			'aroAccessories' : item.aroAccessories,
	        			'aroAccessoriesTotalPrice' : item.aroAccessoriesTotalPrice,
	        			'aroTotalMatched' : item.aroTotalMatched,
	        			'addPromotion' : item.addPromotion
		        	})
				}
			}

			if(plan.item.itemType === 'mobilePlan'){
				state.cart = []
				planOption = state.cart
			}

			if(plan.item.itemType === 'businessMobilePlan'){
				state.businessMobilePlan = []
				planOption = state.businessMobilePlan
			}

			if(plan.item.itemType === 'broadBandPlan'){
				state.mobileBroadbands = []
				planOption = state.mobileBroadbands
			}

            for (let items of itemObj) {
	         	planOption.push({
	        		'id': items.id,
	        		'customDescription' : items.customDescription,
	        		'make' :  items.make,
	        		'model' : items.model,
	        		'capacity' : items.capacity,
	        		'totalPrice' : Number(items.totalPrice).toFixed(2),
	        		'paymentFrequency' : items.paymentFrequencyVal,
	        		'paymentFrequencyVal' : items.paymentFrequencyVal,
	        		'frequency' : items.paymentFrequency,
	        		'planValue' : items.planValue,
	        		'planType' : items.planType,
	        		'planOption' : items.planOption,
	        		'quantity' : items.quantity,
	        		'aroAmount' : Number(items.aroAmount).toFixed(2),
	        		'aroBase' : Number(items.aroBase).toFixed(2),
	        		'aroMonths' : Number(items.aroMonths),
	        		'vas' : items.vas,
	        		'vasIdArray' : items.vasIdArray,
	        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
	        		'price' : Number(items.price).toFixed(2),
	        		'mths24Contract' : items.mths24Contract,
	        		'mths12Contract' : items.mths12Contract,
	        		'mths36Contract' : items.mths36Contract,
	        		'months' : items.months,
	        		'deviceCategory' : items.deviceCategory,
	        		'otherDevices' : items.otherDevices,
	        		'deviceItem' : items.deviceItem,
	        		'itemType' : items.itemType,
	        		'mobilePlanMonths' : items.mobilePlanMonths,
        			'selectedARO' : items.selectedARO,
        			'selectedAROMonth' : items.selectedAROMonth,
        			'promotionsDescription' : items.promotionsDescription,
        			'discounts' : items.discounts,
        			'deviceDiscounts' : items.deviceDiscounts,
        			'promotionsPrice' : items.promotionsPrice,
        			'aroAccessories' : items.aroAccessories,
        			'aroAccessoriesTotalPrice' : items.aroAccessoriesTotalPrice,
        			'aroTotalMatched' : items.aroTotalMatched,
        			'addPromotion' : items.addPromotion
	        	})
            }
            
            if(plan.item.itemType === 'mobilePlan'){
            	window.localStorage.setItem('cart', JSON.stringify(state.cart));
		    }

		    if(plan.item.itemType === 'businessMobilePlan'){
				window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
			}

		    if(plan.item.itemType === 'broadBandPlan'){
		    	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
		    }

		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		updateQuadrantItem(state, plan){
			var crypto = require("crypto");
			var itemObj = []
			var hroMonthlyPrice = 0;
			var vasArray = [];
			var miscSelectedPromotions = [];
			var miscTotalAmount = 0;
			var vasIdArray = [];
			var vasTotalAmount = 0;
			var itemPrice = 0;
			var discountPrice = 0;
			let hroPlanOption = ''
			var hroMonthlyPrice = 0;
			var aroAccessories = []
			var aroAccessoriesTotalPrice = 0
			var aroAccessoriesTotal = 0
			var aroTotalMatched = false
			var deviceDiscounts = 0
			var chargeDiscount = 0
			var outrightBroadbandPrice = 0
			var outrightAccessoryPrice = 0
			var outrightDevicePrice = 0
			var outrightHardwarePrice = 0
			var hroDevices = []
			var hroDevicesTotalAmount = 0
			var entertainmentDevicesTotalAmount = 0
			var entertainmentDevices = []

			//console.log(plan)
			// return

			if(plan.vas){
				for (let val of plan.vas) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}
			if(plan.hro){
				for(let hro of plan.hro){
					if(hro){
						hroDevices.push(hro)

						if(plan.hroMonthly == 12){
							hroDevicesTotalAmount += Number(hro.price_12_months)
						}

						if(plan.hroMonthly == 24){
							hroDevicesTotalAmount += Number(hro.price_24_months)
						}
					}
				}
			}
			// console.log(hroDevices)
			// return
			if(plan.entertainment){
				for(let entertainment of plan.entertainment){
					if(entertainment){
						entertainmentDevices.push(entertainment)

						entertainmentDevicesTotalAmount += Number(entertainment.price)
					}
				}
			}

			if(plan.aroAccessories){
				for (let aro of plan.aroAccessories) {
					if((plan.aroAccessory) && aro.aroID == plan.aroAccessory.aroID){
				      	aroAccessories.push(plan.aroAccessory) // the value of the current key.

				    }else{
				    	aroAccessories.push(aro)
				    }
			  	}

			  	for (let aro of aroAccessories) {
				    aroAccessoriesTotalPrice += Number(aro.aroPrice)
			  	}

			  	if(plan.aroBase == aroAccessoriesTotalPrice || plan.aroBase == "0.00" || plan.aroBase == 0){
			  		aroTotalMatched = true
			  	}
			}

			if(plan.aroAccessory && plan.aroAccessory.aroAdd == true){
				aroAccessories.push(plan.aroAccessory)
			}

			if(plan.aroAccessoryForDelete){
				let index = plan.aroAccessories.indexOf(plan.aroAccessoryForDelete);
				if (index > -1) {
					plan.aroAccessories.splice(index,1);
				}
				aroAccessories = plan.aroAccessories

				for (let aro of aroAccessories) {
				    aroAccessoriesTotal += Number(aro.aroPrice)
			  	}

			  	aroAccessoriesTotalPrice = aroAccessoriesTotal
			}

	        let planOption = ''

			planOption = state.quadrantCart
			
			for (let item of planOption) {
				if(item.itemType == 'miscellaneous'){
					if(plan.selectedPromotions){
						for (let val of plan.selectedPromotions) {
					      	miscSelectedPromotions.push(val) // the value of the current key.
					      	miscTotalAmount += Number(val.discount_price)
					  	}
					}

					if(item.id == plan.id){
						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 1){
									discountPrice += Number(promo.discount_price)
								}

								if(promo.type.name === 'Once Off Discount' && promo.is_credit == 0){
									chargeDiscount += Number(promo.discount_price)
								}
							}
						}

						itemObj.push({
				    		'id': crypto.randomBytes(20).toString('hex'),
				    		'customDescription' : plan.customDescription,
				    		'itemType' : 'miscellaneous',
							'quantity': 1,
		        			'totalPriceMiscellaneous' : 0,
		        			'selectedPromotions' : miscSelectedPromotions,
		        			'promotionsPrice' : discountPrice,
		        			'chargeDiscount' : Number(miscTotalAmount),
		        			'mobileOption' : plan.mobileOption
				    	});
					}else{
						itemObj.push({
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : 'miscellaneous',
							'quantity': 1,
		        			'totalPriceMiscellaneous' : 0,
		        			'selectedPromotions' : item.selectedPromotions,
		        			'promotionsPrice' : item.promotionsPrice,
		        			'chargeDiscount' : item.chargeDiscount,
		        			'mobileOption' : item.mobileOption
				    	});
					}
				}

				if(item.itemType == 'outright'){
					if(item.id == plan.id){

						if(plan.mobileBroadbandOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.broadBandDevices
							if(item.broadBandDevices){
								var outrightMobileDevices = []
								outrightMobileDevices = plan.broadBandDevices

								plan.broadBandDevices = []
								for(let device of outrightMobileDevices){
									if(device.id == plan.mobileOutrightDevice.id){
										device.rrp_incl_gst = Number(plan.mobileOutrightDevicePrice)

									}
									plan.broadBandDevices.push(device)
									outrightBroadbandPrice += Number(device.rrp_incl_gst)
								}
							}
							
						}else{
							for(let device of plan.broadBandDevices){
								outrightBroadbandPrice += Number(device.rrp_incl_gst)
							}
						}

						if(plan.mobileOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.devices

							plan.devices = []
							for(let device of outrightMobileDevices){
								if(device.id == plan.mobileOutrightDevice.id){
									device.rrp_incl_gst = Number(plan.mobileOutrightDevicePrice)

								}
								plan.devices.push(device)
								outrightDevicePrice += Number(device.rrp_incl_gst)
							}
							
						}else{
							for(let device of plan.devices){
								outrightDevicePrice += Number(device.rrp_incl_gst)
							}
						}

						if(plan.hroOutrightPricing){
							var outrightHRODevices = []
							outrightHRODevices = plan.hardwareDevices

							plan.hardwareDevices = []
							for(let device of outrightHRODevices){
								if(device.id == plan.hroOutrightDevice.id){
									device.rrp = Number(plan.hroOutrightDevicePrice)
								}
								plan.hardwareDevices.push(device)
								outrightHardwarePrice += Number(device.rrp)
							}							
						}else{
							for(let device of plan.hardwareDevices){
								outrightHardwarePrice += Number(device.rrp)
							}
						}

						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 1){
									discountPrice += Number(promo.discount_price)
								}

								if(promo.type.name === 'Once Off Discount' && promo.is_credit == 0){
									chargeDiscount += Number(promo.discount_price)
								}
							}
						}

						if(plan.accessoryOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.aroDevices

							plan.aroDevices = []
							for(let device of outrightMobileDevices){
								if(device.BarCode == plan.accessoryOutrightDevice.BarCode){
									device.ProductUnitRRPCost = Number(plan.accessoryOutrightDevicePrice)
								}
								plan.aroDevices.push(device)
								outrightAccessoryPrice += Number(device.ProductUnitRRPCost)
							}							
						}else{
							for(let device of plan.aroDevices){
								outrightAccessoryPrice += Number(device.ProductUnitRRPCost)
							}
						}

						// if(plan.hardwareOutrightPricing){
						// 	console.log(plan)
						// 	return
						// 	var outrightMobileDevices = []
						// 	outrightMobileDevices = plan.aroDevices

						// 	plan.aroDevices = []
						// 	for(let device of outrightMobileDevices){
						// 		if(device.BarCode == plan.accessoryOutrightDevice.BarCode){
						// 			device.rrp = Number(plan.accessoryOutrightDevicePrice)
						// 		}
						// 		plan.aroDevices.push(device)
						// 		outrightHardwarePrice += Number(device.rrp)
						// 	}							
						// }else{
						// 	for(let device of plan.aroDevices){
						// 		outrightHardwarePrice += Number(device.rrp)
						// 	}
						// }

						// for(let device of plan.hardwareDevices){
						// 	outrightHardwarePrice += Number(device.rrp)
						// }

						itemObj.push({
							'price': Number(outrightDevicePrice),
							'broadbandPrice': Number(outrightBroadbandPrice),
							'hardwarePrice' : Number(outrightHardwarePrice),
							'outrightDiscountedPrice' : (outrightDevicePrice - discountPrice),
							'aroPrice': Number(outrightAccessoryPrice),
				    		'id': crypto.randomBytes(20).toString('hex'),
				    		'customDescription' : plan.customDescription,
				    		'itemType' : 'outright',
							'quantity': 1,
							'devices' : plan.devices,
							'broadBandDevices' : plan.broadBandDevices,
							'hardwareDevices' : plan.hardwareDevices,
							'aroDevices' : plan.aroDevices,
							'addPromotion': plan.addPromotion,
		        			'promotionSets' : plan.promotionSets,
		        			'selectedPromotion': plan.selectedPromotion,
		        			'selectedPromotions' : plan.selectedPromotions,
		        			'promotionsPrice' : discountPrice,
		        			'chargeDiscount' : chargeDiscount,
		        			'mobileOption' : plan.mobileOption
				    	});
					}else{
						itemObj.push({
				    		'price' : (item.price),
							'broadbandPrice': item.broadbandPrice,
							'hardwarePrice' : item.hardwarePrice,
				    		'outrightDiscountedPrice' : item.outrightDiscountedPrice,
							'aroPrice': item.aroPrice,
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : 'outright',
							'quantity': 1,
							'devices' : item.devices,
							'broadBandDevices' : item.broadBandDevices,
							'hardwareDevices' : item.hardwareDevices,
							'aroDevices' : item.aroDevices,
							'addPromotion': item.addPromotion,
		        			'promotionSets' : item.promotionSets,
		        			'selectedPromotion': item.selectedPromotion,
		        			'selectedPromotions' : item.selectedPromotions,
		        			'promotionsPrice' : item.promotionsPrice,
		        			'chargeDiscount' : item.chargeDiscount,
		        			'mobileOption' : item.mobileOption
				    	});
					}
				}

				if(item.itemType == 'bnpl'){
					if(item.id == plan.id){

						if(plan.mobileBroadbandOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.broadBandDevices
							if(item.broadBandDevices){
								var outrightMobileDevices = []
								outrightMobileDevices = plan.broadBandDevices

								plan.broadBandDevices = []
								for(let device of outrightMobileDevices){
									if(device.id == plan.mobileOutrightDevice.id){
										device.rrp_incl_gst = Number(plan.mobileOutrightDevicePrice)

									}
									plan.broadBandDevices.push(device)
									outrightBroadbandPrice += Number(device.rrp_incl_gst)
								}
							}
							
						}else{
							for(let device of plan.broadBandDevices){
								outrightBroadbandPrice += Number(device.rrp_incl_gst)
							}
						}

						if(plan.mobileOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.devices

							plan.devices = []
							for(let device of outrightMobileDevices){
								if(device.id == plan.mobileOutrightDevice.id){
									device.rrp_incl_gst = Number(plan.mobileOutrightDevicePrice)

								}
								plan.devices.push(device)
								outrightDevicePrice += Number(device.rrp_incl_gst)
							}
							
						}else{
							for(let device of plan.devices){
								outrightDevicePrice += Number(device.rrp_incl_gst)
							}
						}

						if(plan.hroOutrightPricing){
							var outrightHRODevices = []
							outrightHRODevices = plan.hardwareDevices

							plan.hardwareDevices = []
							for(let device of outrightHRODevices){
								if(device.id == plan.hroOutrightDevice.id){
									device.rrp = Number(plan.hroOutrightDevicePrice)
								}
								plan.hardwareDevices.push(device)
								outrightHardwarePrice += Number(device.rrp)
							}							
						}else{
							for(let device of plan.hardwareDevices){
								outrightHardwarePrice += Number(device.rrp)
							}
						}

						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 1){
									discountPrice += Number(promo.discount_price)
								}

								if(promo.type.name === 'Once Off Discount' && promo.is_credit == 0){
									chargeDiscount += Number(promo.discount_price)
								}
							}
						}

						if(plan.accessoryOutrightPricing){
							var outrightMobileDevices = []
							outrightMobileDevices = plan.aroDevices

							plan.aroDevices = []
							for(let device of outrightMobileDevices){
								if(device.BarCode == plan.accessoryOutrightDevice.BarCode){
									device.ProductUnitRRPCost = Number(plan.accessoryOutrightDevicePrice)
								}
								plan.aroDevices.push(device)
								outrightAccessoryPrice += Number(device.ProductUnitRRPCost)
							}							
						}else{
							for(let device of plan.aroDevices){
								outrightAccessoryPrice += Number(device.ProductUnitRRPCost)
							}
						}

						// if(plan.hardwareOutrightPricing){
						// 	console.log(plan)
						// 	return
						// 	var outrightMobileDevices = []
						// 	outrightMobileDevices = plan.aroDevices

						// 	plan.aroDevices = []
						// 	for(let device of outrightMobileDevices){
						// 		if(device.BarCode == plan.accessoryOutrightDevice.BarCode){
						// 			device.rrp = Number(plan.accessoryOutrightDevicePrice)
						// 		}
						// 		plan.aroDevices.push(device)
						// 		outrightHardwarePrice += Number(device.rrp)
						// 	}							
						// }else{
						// 	for(let device of plan.aroDevices){
						// 		outrightHardwarePrice += Number(device.rrp)
						// 	}
						// }

						// for(let device of plan.hardwareDevices){
						// 	outrightHardwarePrice += Number(device.rrp)
						// }

						itemObj.push({
							'price': Number(outrightDevicePrice),
							'broadbandPrice': Number(outrightBroadbandPrice),
							'hardwarePrice' : Number(outrightHardwarePrice),
							'outrightDiscountedPrice' : (outrightDevicePrice - discountPrice),
							'aroPrice': Number(outrightAccessoryPrice),
				    		'id': crypto.randomBytes(20).toString('hex'),
				    		'customDescription' : plan.customDescription,
				    		'itemType' : 'bnpl',
							'quantity': 1,
							'devices' : plan.devices,
							'broadBandDevices' : plan.broadBandDevices,
							'hardwareDevices' : plan.hardwareDevices,
							'aroDevices' : plan.aroDevices,
							'addPromotion': plan.addPromotion,
		        			'promotionSets' : plan.promotionSets,
		        			'selectedPromotion': plan.selectedPromotion,
		        			'selectedPromotions' : plan.selectedPromotions,
		        			'promotionsPrice' : discountPrice,
		        			'chargeDiscount' : chargeDiscount,
		        			'mobileOption' : plan.mobileOption,
		        			'amount' : plan.amount,
		        			'term' : plan.term,
		        			'company' : plan.company
				    	});
					}else{
						itemObj.push({
				    		'price' : (item.price),
							'broadbandPrice': item.broadbandPrice,
							'hardwarePrice' : item.hardwarePrice,
				    		'outrightDiscountedPrice' : item.outrightDiscountedPrice,
							'aroPrice': item.aroPrice,
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : 'bnpl',
							'quantity': 1,
							'devices' : item.devices,
							'broadBandDevices' : item.broadBandDevices,
							'hardwareDevices' : item.hardwareDevices,
							'aroDevices' : item.aroDevices,
							'addPromotion': item.addPromotion,
		        			'promotionSets' : item.promotionSets,
		        			'selectedPromotion': item.selectedPromotion,
		        			'selectedPromotions' : item.selectedPromotions,
		        			'promotionsPrice' : item.promotionsPrice,
		        			'chargeDiscount' : item.chargeDiscount,
		        			'mobileOption' : item.mobileOption,
		        			'amount' : item.amount,
		        			'term' : item.term,
		        			'company' : item.company
				    	});
					}
				}

				if(item.itemType == 'mobilePlan' || item.itemType == 'broadBandPlan'){
					if(item.id == plan.item.id){
						if(plan.deviceItem != 0 && plan.months == "36"){
							itemPrice = plan.deviceItem.mths_36_device_payment_contract
						}
						
						if(plan.deviceItem != 0 && plan.months == "24"){
							itemPrice = plan.deviceItem.mths_24_device_payment_contract
						}

						if(plan.deviceItem != 0 && plan.months == "12"){
							itemPrice = plan.deviceItem.mths_12_device_payment_contract
						}

						if(plan.aroBase != 0){
							var aroAmount = (Number(plan.aroBase) / Number(plan.aroMonths)) 
						}else{
							var aroAmount = 0.00
						}

						// if(plan.discounts && plan.promotionsPrice && plan.discounts.type.name === 'Reoccuring Discount'){
						// 	discountPrice = Number(plan.promotionsPrice).toFixed(2)
						// }else{
						// 	discountPrice = 0
						// }
						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount'){
									discountPrice += Number(promo.discount_price)
								}
							}
						}

						if(plan.deviceDiscounts){
							deviceDiscounts = plan.deviceDiscounts
						}else{
							deviceDiscounts = 0
						}
						let discountPriceTypes = 0

						if(plan.planOption.name == 'Small'){
							discountPriceTypes = plan.smallDiscount
						}

						if(plan.planOption.name == 'Medium'){
							discountPriceTypes = plan.mediumDiscount
						}

						if(plan.planOption.name == 'Large'){
							discountPriceTypes = plan.largeDiscount
						}

						if(plan.planOption.name == 'Extra Large'){
							discountPriceTypes = plan.xlDiscount
						}

						if(plan.planOption.name == 'Extra Small'){
							discountPriceTypes = plan.xsDiscount
						}
						// console.log(plan.planOption)
						// console.log(discountPriceTypes)
						// return
						itemObj.push({
			        		'id': item.id,
			        		'customDescription' : plan.customDescription,
			        		'make' :  (plan.deviceItem != 0) ? plan.deviceItem.make : '',
			        		'model' : (plan.deviceItem != 0) ? plan.deviceItem.model : '',
			        		'capacity' : (plan.deviceItem != 0) ? plan.deviceItem.capacity : '',
			        		//'totalPrice' : ((((Number(itemPrice) - Number(discountPriceTypes) + Number(aroAmount)  + Number(vasTotalAmount) + Number(plan.planValue) ).toFixed(2) * 12) / item.paymentFrequency - discountPrice - Number(deviceDiscounts)) ).toFixed(2),
			        		'totalPrice' : ((((Number(itemPrice) + Number(aroAmount)  + Number(vasTotalAmount) + Number(plan.planValue) ).toFixed(2) * 12) / item.paymentFrequency - discountPrice - Number(deviceDiscounts)) ).toFixed(2),
			        		'paymentFrequency' : item.paymentFrequency,
			        		'paymentFrequencyVal' : item.paymentFrequency,
			        		'frequency' : item.frequency,
			        		'planValue' : Number(plan.planValue),
			        		'planType' : plan.planType,
			        		'planOption' : plan.planOption,
			        		'quantity' : item.quantity,
			        		'aroMonths' : plan.aroMonths,
			        		'aroAmount' : Number(aroAmount).toFixed(2),
			        		'aroBase' : Number(plan.aroBase).toFixed(2),
			        		'vas' : vasArray,
			        		'vasIdArray' : vasIdArray,
			        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
			        		'price' : Number(itemPrice).toFixed(2),
			        		'mths12Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_12_device_payment_contract : '',
			        		'mths24Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_24_device_payment_contract : '',
			        		'mths36Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_36_device_payment_contract : '',
			        		'months' : plan.months,
			        		'deviceCategory' : (plan.deviceItem != 0) ? plan.deviceItem.deviceCategory : '',
			        		'otherDevices' : plan.otherDevices,
			        		'deviceItem' : (plan.deviceItem != 0) ? plan.deviceItem : '',
			        		'itemType' : item.itemType,
		        			'mobilePlanMonths' : plan.mobilePlanMonths,
		        			'selectedARO' : plan.selectedARO,
		        			'selectedAROMonth' : plan.selectedAROMonth,
		        			'promotionsDescription' : plan.promotionsDescription,
		        			'discounts' : plan.discounts,
		        			'deviceDiscounts' : plan.deviceDiscounts,
		        			'promotionsPrice' : plan.promotionsPrice,
		        			'aroAccessories' : aroAccessories,
		        			'aroAccessoriesTotalPrice' : aroAccessoriesTotalPrice,
		        			'aroTotalMatched' : aroTotalMatched,
		        			'addPromotion' : plan.addPromotion,
		        			'promotionSets' : plan.promotionSets,
		        			'selectedPromotion' : plan.selectedPromotion,
		        			'smallDiscount' : Number(plan.smallDiscount),
							'mediumDiscount' : Number(plan.mediumDiscount),
							'largeDiscount' : Number(plan.largeDiscount),
							'xlDiscount' : Number(plan.xlDiscount),
							'xsDiscount' : Number(plan.xsDiscount),
							'promotionDiscounts' : plan.promotionDiscounts,
							'selectedPromotions' : plan.selectedPromotions,
							'isBusiness' : plan.isBusiness,
							'mobileOption' : plan.mobileOption
			        	})


					}else{
						itemObj.push({
			        		'id': item.id,
			        		'customDescription' : item.customDescription,
			        		'make' :  item.make,
			        		'model' : item.model,
			        		'capacity' : item.capacity,
			        		'totalPrice' : Number(item.totalPrice).toFixed(2),
			        		'paymentFrequency' : item.paymentFrequency,
			        		'paymentFrequencyVal' : item.paymentFrequency,
			        		'frequency' : item.frequency,
			        		'planValue' : item.planValue,
			        		'planType' : item.planType,
			        		'planOption' : item.planOption,
			        		'quantity' : item.quantity,
			        		'aroAmount' : Number(item.aroAmount).toFixed(2),
			        		'aroBase' : Number(item.aroBase).toFixed(2),
			        		'aroMonths' : Number(item.aroMonths),
			        		'vas' : item.vas,
			        		'vasIdArray' : item.vasIdArray,
			        		'vasTotalAmount' : Number(item.vasTotalAmount).toFixed(2),
			        		'price' : Number(item.price).toFixed(2),
			        		'mths12Contract' : item.mths12Contract,
			        		'mths24Contract' : item.mths24Contract,
			        		'mths36Contract' : item.mths36Contract,
			        		'months' : item.months,
			        		'deviceCategory' : item.deviceCategory,
			        		'otherDevices' : item.otherDevices,
			        		'deviceItem' : item.deviceItem,
			        		'itemType' : item.itemType,
		        			'mobilePlanMonths' : item.mobilePlanMonths,
		        			'selectedARO' : item.selectedARO,
		        			'selectedAROMonth' : item.selectedAROMonth,
		        			'promotionsDescription' : item.promotionsDescription,
		        			'discounts' : item.discounts,
		        			'deviceDiscounts' : item.deviceDiscounts,
		        			'promotionsPrice' : item.promotionsPrice,
		        			'aroAccessories' : item.aroAccessories,
		        			'aroAccessoriesTotalPrice' : item.aroAccessoriesTotalPrice,
		        			'aroTotalMatched' : item.aroTotalMatched,
		        			'addPromotion' : item.addPromotion,
		        			'promotionSets' : item.promotionSets,
		        			'selectedPromotion' : item.selectedPromotion,
		        			'smallDiscount' : item.smallDiscount,
							'mediumDiscount' : item.mediumDiscount,
							'largeDiscount' : item.largeDiscount,
							'xlDiscount' : item.xlDiscount,
							'xsDiscount' : item.xsDiscount,
							'promotionDiscounts' : item.promotionDiscounts,
							'selectedPromotions' : item.selectedPromotions,
							'isBusiness' : item.isBusiness,
							'mobileOption' : item.mobileOption
			        	})
					}
				}

				if(item.itemType == 'redemption'){
					if(item.id == plan.id){
						itemObj.push({
							'product' : plan.product,
							'category' : plan.product3,
							'points' : plan.points,
							'pay' : plan.pay,
							'rrp' : plan.rrp,
							'sku' : plan.sku,
							'id': plan.id,
							'quantity' : 1,
							'itemType' : 'redemption'
				    	});
					}else{
						itemObj.push({
							'product' : item.product,
							'category' : item.product3,
							'points' : item.points,
							'pay' : item.pay,
							'rrp' : item.rrp,
							'sku' : item.sku,
							'id': item.id,
							'quantity' : 1,
							'itemType' : 'redemption'
				    	});
					}
				}

				if(item.itemType == 'hro'){

					if(item.id == plan.item.id){
						if(plan.hro && plan.hroMonthly == 24){
							hroMonthlyPrice = plan.hro.price_24_months
						}

						if(plan.hro && plan.hroMonthly == 12){
							hroMonthlyPrice = plan.hro.price_12_months
						}

						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount'){
									discountPrice += Number(promo.discount_price)
								}
							}
						}

						itemObj.push({
				    		'hro' : hroDevices,
				    		'hroMonthlyPrice': Number(Number(hroDevicesTotalAmount) - Number(discountPrice)),
				    		'hroNetTotal' : Number(hroDevicesTotalAmount),
				    		'hroMonthly': plan.hroMonthly,
				    		'id': item.id,
				    		'customDescription' : plan.customDescription,
				    		'itemType' : plan.itemType,
		        			'promotionSets' : plan.promotionSets,
		        			'selectedPromotion' : plan.selectedPromotion,
	        				'addPromotion' : plan.addPromotion,
	        				'promotionsPrice' : discountPrice,
		        			'selectedPromotions' : plan.selectedPromotions,
		        			'isBusiness' : plan.isBusiness,
		        			'mobileOption' :plan.mobileOption
				    	});
					}else{
						itemObj.push({
				    		'hro' : item.hro,
				    		'hroMonthlyPrice': item.hroMonthlyPrice,
				    		'hroMonthly': item.hroMonthly,
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : item.itemType,
		        			'promotionSets' : item.promotionSets,
		        			'selectedPromotion' : item.selectedPromotion,
	        				'addPromotion' : item.addPromotion,
	        				'promotionsPrice' : item.promotionsPrice,
		        			'selectedPromotions' : item.selectedPromotions,
		        			'isBusiness' : item.isBusiness,
		        			'mobileOption' :item.mobileOption
				    	});
					}
				}

				if(item.itemType == 'fixed'){					
					if(item.id == plan.id){
						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 1){
									discountPrice += Number(promo.discount_price)
								}

								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 0){
									chargeDiscount += Number(promo.discount_price)
								}
							}
						}
						
						itemObj.push({
	        				'addPromotion' : plan.addPromotion,
				    		'fixedPlanPrice' : (Number(plan.fixed.amount) + Number(vasTotalAmount) +  Number(chargeDiscount) -  Number(discountPrice)),
				    		'fixedPrice' : Number(plan.fixed.amount),
				    		'fixedPlanType': plan.fixedPlan,
				    		'fixed' : plan.fixed,
				    		'id': item.id,
				    		'customDescription' : plan.customDescription,
				    		'itemType': plan.itemType,
				    		'valueAddedService': plan.valueAddedService,
		        			'promotionSets' : plan.promotionSets,
		        			'selectedPromotion' : plan.selectedPromotion,
		        			'selectedPromotions' : plan.selectedPromotions,
	        				'addPromotion' : plan.addPromotion,
	        				'promotionsPrice' : discountPrice,
	        				'chargeDiscount' : chargeDiscount,
	        				'planDescription' : plan.planDescription,
	        				'isBusiness' : plan.isBusiness,
			        		'vas' : vasArray,
			        		'vasTotalAmount' : Number(vasTotalAmount),
			        		'vasIdArray' : vasIdArray,
							'mobileOption' : plan.mobileOption
				    	});
					}else{
						itemObj.push({
	        				'addPromotion' : item.addPromotion,
				    		'fixedPlanPrice' :item.fixedPlanPrice,
				    		'fixedPrice' : item.fixedPrice,
				    		'fixedPlanType': item.fixedPlanType,
				    		'id' : item.id,
				    		'fixed': item.fixed,
				    		'quantity' : 1,
				    		'customDescription' : item.customDescription,
				    		'itemType': item.itemType,
				    		'valueAddedService': item.valueAddedService,
		        			'promotionSets' : item.promotionSets,
		        			'selectedPromotion' : item.selectedPromotion,
		        			'selectedPromotions' : item.selectedPromotions,
	        				'addPromotion' : item.addPromotion,
	        				'promotionsPrice' : item.promotionsPrice,
	        				'chargeDiscount' : item.chargeDiscount,
	        				'planDescription' : item.planDescription,
	        				'isBusiness' : item.isBusiness,
	        				'vas' : item.vas,
			        		'vasIdArray' : item.vasIdArray,
			        		'vasTotalAmount' : Number(item.vasTotalAmount),
							'mobileOption' : item.mobileOption
				    	});
					}
				}

				if(item.itemType == 'platinum'){
					if(item.id == plan.id){
						itemObj.push({
							'addPromotion': {'id':0,'name':'no'},
			        		'platinumPlanPrice' : Number(plan.platinumPlanPrice).toFixed(2),
				    		'platinumPlanType': plan.platinumPlanType,
				    		'platinumPlanDescription': plan.platinumPlanDescription,
				    		'promotionSets' : [],
				    		'selectedEntertainmentPlan' : plan.selectedEntertainmentPlan,
				    		'id': item.id,
				    		'itemType' : 'platinum',
				    		'quantity' : 1,
				    		'customDescription' : plan.customDescription,
				    		'planDescription' : plan.selectedEntertainmentPlan.planDescription,
				    		'chargeDiscount' : 0,
				    		'promotionsPrice' : 0,
				    		'totalPlatinumPrice' : Number(plan.platinumPlanPrice).toFixed(2),
			        		'selectedPromotions' : []
			        	});	
					}else{
						itemObj.push({
				    		'addPromotion': {'id':0,'name':'no'},
			        		'platinumPlanPrice' : Number(item.platinumPlanPrice).toFixed(2),
				    		'platinumPlanType': item.platinumPlanType,
				    		'platinumPlanDescription': item.platinumPlanDescription,
				    		'promotionSets' : [],
				    		'selectedEntertainmentPlan' : item.selectedEntertainmentPlan,
				    		'id': item.id,
				    		'itemType' : 'platinum',
				    		'quantity' : 1,
				    		'customDescription' : item.customDescription,
				    		'planDescription' : item.selectedEntertainmentPlan.planDescription,
				    		'chargeDiscount' : 0,
				    		'promotionsPrice' : 0,
				    		'totalPlatinumPrice' : Number(item.platinumPlanPrice).toFixed(2),
			        		'selectedPromotions' : []
				    	});
					}	
				}

				if(item.itemType == 'entertainment'){
					if(item.id == plan.id){
						console.log('test')
						if(plan.selectedPromotions){
							for(let promo of plan.selectedPromotions){
								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 1){
									discountPrice += Number(promo.discount_price)
								}

								if(promo.type.name === 'Reoccuring Discount' && promo.is_credit == 0){
									chargeDiscount += Number(promo.discount_price)
								}
							}
						}
						itemObj.push({
							// 'addPromotion' : plan.addPromotion,
				   //  		'entertainmentPlanPrice' : Number(plan.entertainmentPlanPrice),
				   //  		'totalEntertainmentPrice' : (Number(plan.entertainmentPlanPrice) + Number(vasTotalAmount) +  Number(chargeDiscount) -  Number(discountPrice)),
				   //  		'entertainmentPlanType': plan.selectedEntertainmentPlan.name,
				   //  		'entertainmentPlanDescription': plan.entertainmentPlanDescription,
				   //  		'entertainment' : plan.entertainment,
				   //  		'selectedEntertainmentPlan' : plan.selectedEntertainmentPlan,
				   //  		'id': crypto.randomBytes(20).toString('hex'),
				   //  		'promotionSets' : plan.promotionSets,
				   //  		'itemType' : 'entertainment',
				   //  		'customDescription' : plan.customDescription,
				   //  		'planDescription' : plan.planDescription,
				   //  		'selectedPromotions' : plan.selectedPromotions,
	      //   				'promotionsPrice' : discountPrice,
	      //   				'chargeDiscount' : chargeDiscount,
			    //     		'vas' : vasArray,
			    //     		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
			    //     		'vasIdArray' : vasIdArray,
		    				'addPromotion' : plan.addPromotion,
	        				'chargeDiscount' : chargeDiscount,
				    		'customDescription' : plan.customDescription,
				    		'entertainment' : plan.entertainment,
				    		'vas' : vasArray,
			        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
			        		'vasIdArray' : vasIdArray,
			        		'id': crypto.randomBytes(20).toString('hex'),
			        		'itemType' : 'entertainment',
			        		'promotionSets' : plan.promotionSets,
			        		'selectedPromotions' : plan.selectedPromotions,
			        		'promotionsPrice' : discountPrice,
			        		'totalEntertainmentPrice' : (Number(entertainmentDevicesTotalAmount) + Number(vasTotalAmount) +  Number(chargeDiscount) -  Number(discountPrice))

				    		// 'entertainmentPlanPrice' : Number(plan.entertainmentPlanPrice),
				    		// 'totalEntertainmentPrice' : (Number(plan.entertainmentPlanPrice) + Number(vasTotalAmount) +  Number(chargeDiscount) -  Number(discountPrice)),
				    		// 'entertainmentPlanType': plan.selectedEntertainmentPlan.name,
				    		// 'entertainmentPlanDescription': plan.entertainmentPlanDescription,
				    		
				    		// 'selectedEntertainmentPlan' : plan.selectedEntertainmentPlan,
				    		
				    		
				    		
				    		// 'planDescription' : plan.planDescription,			    		
	        				
			        		
				    	});
					}else{
						itemObj.push({
							'addPromotion' : item.addPromotion,
	        				'chargeDiscount' : item.chargeDiscount,
				    		'customDescription' : item.customDescription,
				    		'entertainment' : item.entertainment,
				    		'vas' : item.vas,
			        		'vasTotalAmount' : item.vasTotalAmount,
			        		'vasIdArray' : item.vasIdArray,
			        		'id': item.id,
			        		'itemType' : 'entertainment',
			        		'promotionSets' : item.promotionSets,
			        		'selectedPromotions' : item.selectedPromotions,
			        		'promotionsPrice' : item.promotionsPrice,
			        		'totalEntertainmentPrice' : item.totalEntertainmentPrice
				    	});
					}
				}

				if(item.itemType == 'existingServices'){
					if(item.id == plan.id){
						itemObj.push({
				    		'existingServicesPrice' : (plan.existingServicesPrice),
				    		'existingServicesDescription': plan.existingServicesDescription,
				    		'id': crypto.randomBytes(20).toString('hex'),
				    		'customDescription' : plan.customDescription,
				    		'itemType' : 'existingServices'
				    	});
					}else{
						itemObj.push({
				    		'existingServicesPrice' : (item.existingServicesPrice),
				    		'existingServicesDescription': item.existingServicesDescription,
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : 'existingServices'
				    	});
					}
				}

				if(item.itemType == 'otherServices'){
					if(item.id == plan.id){
						itemObj.push({
				    		'otherServicesPrice' : (plan.otherServicesPrice),
				    		'otherServicesDescription': plan.otherServicesDescription,
				    		'id': crypto.randomBytes(20).toString('hex'),
				    		'customDescription' : plan.customDescription,
				    		'itemType' : 'otherServices'
				    	});
					}else{
						itemObj.push({
				    		'otherServicesPrice' : (item.otherServicesPrice),
				    		'otherServicesDescription': item.otherServicesDescription,
				    		'id': item.id,
				    		'customDescription' : item.customDescription,
				    		'itemType' : 'otherServices'
				    	});
					}
				}
			}
			// console.log(itemObj)
			// return
			state.quadrantCart = []
			planOption = state.quadrantCart

            for (let items of itemObj) {
            	if(items.itemType == 'mobilePlan' || items.itemType == 'broadBandPlan'){
		         	planOption.push({
		        		'id': items.id,
		        		'customDescription' : items.customDescription,
		        		'make' :  items.make,
		        		'model' : items.model,
		        		'capacity' : items.capacity,
		        		'totalPrice' : Number(items.totalPrice).toFixed(2),
		        		'paymentFrequency' : items.paymentFrequencyVal,
		        		'paymentFrequencyVal' : items.paymentFrequencyVal,
		        		'frequency' : items.paymentFrequency,
		        		'planValue' : items.planValue,
		        		'planType' : items.planType,
		        		'planOption' : items.planOption,
		        		'quantity' : items.quantity,
		        		'aroAmount' : Number(items.aroAmount).toFixed(2),
		        		'aroBase' : Number(items.aroBase).toFixed(2),
		        		'aroMonths' : Number(items.aroMonths),
		        		'vas' : items.vas,
		        		'vasIdArray' : items.vasIdArray,
		        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
		        		'price' : Number(items.price).toFixed(2),
		        		'mths24Contract' : items.mths24Contract,
		        		'mths12Contract' : items.mths12Contract,
		        		'mths36Contract' : items.mths36Contract,
		        		'months' : items.months,
		        		'deviceCategory' : items.deviceCategory,
		        		'otherDevices' : items.otherDevices,
		        		'deviceItem' : items.deviceItem,
		        		'itemType' : items.itemType,
		        		'mobilePlanMonths' : items.mobilePlanMonths,
	        			'selectedARO' : items.selectedARO,
	        			'selectedAROMonth' : items.selectedAROMonth,
	        			'promotionsDescription' : items.promotionsDescription,
	        			'discounts' : items.discounts,
	        			'deviceDiscounts' : items.deviceDiscounts,
	        			'promotionsPrice' : items.promotionsPrice,
	        			'aroAccessories' : items.aroAccessories,
	        			'aroAccessoriesTotalPrice' : items.aroAccessoriesTotalPrice,
	        			'aroTotalMatched' : items.aroTotalMatched,
	        			'addPromotion' : items.addPromotion,
	        			'promotionSets' : items.promotionSets,
	        			'selectedPromotion' : items.selectedPromotion,
	        			'smallDiscount' : items.smallDiscount,
						'mediumDiscount' : items.mediumDiscount,
						'largeDiscount' : items.largeDiscount,
						'xlDiscount' : items.xlDiscount,
						'xsDiscount' : items.xsDiscount,
						'promotionDiscounts' : items.promotionDiscounts,
						'selectedPromotions' : items.selectedPromotions,
						'isBusiness' : items.isBusiness,
						'mobileOption' : items.mobileOption
		        	})
		        }

		        if(items.itemType == 'hro'){
		         	planOption.push({
		        		'hro' : items.hro,
			    		'hroMonthlyPrice': Number(items.hroMonthlyPrice).toFixed(2),
			    		'hroMonthly': items.hroMonthly,
			    		'id' : items.id,
			    		'quantity' : 1,
			    		'customDescription' : items.customDescription,
			    		'itemType' : items.itemType,
	        			'addPromotion' : items.addPromotion,
	        			'promotionSets' : items.promotionSets,
	        			'selectedPromotion' : items.selectedPromotion,
	        			'promotionsPrice' : items.promotionsPrice,
	        			'selectedPromotions' : items.selectedPromotions,
	        			'isBusiness' : items.isBusiness,
	        			'mobileOption' :items.mobileOption
		        	})
		        }

		        if(items.itemType == 'platinum'){
		        	state.quadrantCart.push({
			        	'addPromotion': {'id':0,'name':'no'},
		        		'platinumPlanPrice' : Number(items.platinumPlanPrice).toFixed(2),
			    		'platinumPlanType': items.platinumPlanType,
			    		'platinumPlanDescription': items.platinumPlanDescription,
			    		'promotionSets' : [],
			    		'selectedEntertainmentPlan' : items.selectedEntertainmentPlan,
			    		'id': items.id,
			    		'itemType' : 'platinum',
			    		'quantity' : 1,
			    		'customDescription' : items.customDescription,
			    		'planDescription' : items.planDescription,
			    		'chargeDiscount' : 0,
			    		'promotionsPrice' : 0,
			    		'totalPlatinumPrice' : Number(items.platinumPlanPrice).toFixed(2),
		        		'selectedPromotions' : [],
						'mobileOption' : {'id':0,'name':'Siebel'}
		        	})
		        }
		        
		        if(items.itemType == 'redemption'){
		        	state.quadrantCart.push({
						'product' : items.product,
						'category' : items.product3,
						'points' : items.points,
						'itemType' : 'redemption',
						'pay' : items.pay,
						'quantity' : 1,
						'id' : items.id,
						'rrp' : items.rrp,
						'sku' : items.sku,
						'mobileOption' : {'id':0,'name':'Siebel'}
			    	});
		        }

		        if(items.itemType == 'existingServices'){
		         	planOption.push({
		        		'existingServicesPrice' : (items.existingServicesPrice),
			    		'existingServicesDescription': items.existingServicesDescription,
			    		'id': items.id,
			    		'customDescription' : items.customDescription,
			    		'quantity' : 1,
			    		'itemType' : 'existingServices',
						'mobileOption' : {'id':0,'name':'Siebel'}
		        	})
            	}

            	if(items.itemType == 'otherServices'){
		         	planOption.push({
		        		'otherServicesPrice' : (items.otherServicesPrice),
			    		'otherServicesDescription': items.otherServicesDescription,
			    		'id': items.id,
			    		'customDescription' : items.customDescription,
			    		'quantity' : 1,
			    		'itemType' : 'otherServices',
						'mobileOption' : {'id':0,'name':'Siebel'}
		        	})
            	}

		        if(items.itemType == 'fixed'){
		        	planOption.push({
		        		'fixedPlanPrice' : items.fixedPlanPrice,
		        		'fixedPrice' : items.fixedPrice,
			    		'fixedPlanType': items.fixedPlanType,
			    		'id' : items.id,
			    		'fixed': items.fixed,
			    		'quantity' : 1,
			    		'customDescription' : items.customDescription,
			    		'itemType': items.itemType,
			    		'valueAddedService': items.valueAddedService,
	        			'promotionSets' : items.promotionSets,
	        			'chargeDiscount' : items.chargeDiscount,
	        			'selectedPromotion' : items.selectedPromotion,
	        			'selectedPromotions' : items.selectedPromotions,
        				'addPromotion' : items.addPromotion,
        				'promotionsPrice' : items.promotionsPrice,
        				'planDescription' : items.planDescription,
        				'isBusiness' : items.isBusiness,
        				'vas' : items.vas,
		        		'vasIdArray' : items.vasIdArray,
		        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
						'mobileOption' : items.mobileOption
		        	})
		        }

		        if(items.itemType == 'entertainment'){
		        	planOption.push({
        	// 			'addPromotion' : items.addPromotion,
		       //  		'entertainmentPlanPrice' : Number(items.entertainmentPlanPrice).toFixed(2),
			    		// 'entertainmentPlanType': items.entertainmentPlanType,
			    		// 'entertainmentPlanDescription': items.entertainmentPlanDescription,
			    		// 'id' : items.id,
			    		// 'entertainment': items.entertainment,
			    		// 'selectedEntertainmentPlan' : items.selectedEntertainmentPlan,
			    		// 'quantity' : 1,
			    		// 'promotionSets' : items.promotionSets,
			    		// 'itemType' : 'entertainment',
			    		// 'customDescription' : items.customDescription,
        	// 			'planDescription' : items.planDescription,
        	// 			'selectedPromotions' : items.selectedPromotions,
        	// 			'promotionsPrice' : items.promotionsPrice,
        	// 			'chargeDiscount' : items.chargeDiscount,
        	// 			'totalEntertainmentPrice' : Number(items.totalEntertainmentPrice).toFixed(2),
		       //  		'vas' : items.vas,
		       //  		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
		       //  		'vasIdArray' : items.vasIdArray,
		       			'addPromotion' : items.addPromotion,
        				'chargeDiscount' : items.chargeDiscount,
			    		'customDescription' : items.customDescription,
			    		'entertainment' : items.entertainment,
			    		'vas' : items.vas,
		        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
		        		'vasIdArray' : items.vasIdArray,
		        		'id': items.id,
		        		'itemType' : 'entertainment',
		        		'promotionSets' : items.promotionSets,
		        		'selectedPromotions' : items.selectedPromotions,
		        		'promotionsPrice' : items.promotionsPrice,
		        		'quantity' : 1,
		        		'totalEntertainmentPrice' : items.totalEntertainmentPrice,
						'mobileOption' : {'id':0,'name':'Siebel'}
		        	})
		        }

		        if(items.itemType == 'outright'){
		         	planOption.push({
		        		'price' : Number(items.price),
		        		'broadbandPrice' : Number(items.broadbandPrice),
		        		'hardwarePrice' : Number(items.hardwarePrice),
		        		'outrightDiscountedPrice' : items.outrightDiscountedPrice,
						'aroPrice': Number(items.aroPrice).toFixed(2),
			    		'id': items.id,
			    		'customDescription' : items.customDescription,
			    		'itemType' : 'outright',
						'quantity': 1,
						'devices' : items.devices,
						'broadBandDevices' : items.broadBandDevices,
						'hardwareDevices' : items.hardwareDevices,
						'aroDevices' : items.aroDevices,
						'totalPriceOutright' : (Number(items.price) + Number(items.aroPrice) + Number(items.hardwarePrice) + Number(items.broadbandPrice)),
						'addPromotion': items.addPromotion,
	        			'promotionSets' : items.promotionSets,
	        			'selectedPromotion': items.selectedPromotion,
	        			'selectedPromotions' : items.selectedPromotions,
	        			'promotionsPrice' : items.promotionsPrice,
	        			'chargeDiscount' : items.chargeDiscount,
						'mobileOption' : items.mobileOption
		        	})
            	}

            	if(items.itemType == 'bnpl'){
		         	planOption.push({
		        		'price' : Number(items.price),
		        		'broadbandPrice' : Number(items.broadbandPrice),
		        		'hardwarePrice' : Number(items.hardwarePrice),
		        		'outrightDiscountedPrice' : items.outrightDiscountedPrice,
						'aroPrice': Number(items.aroPrice).toFixed(2),
			    		'id': items.id,
			    		'customDescription' : items.customDescription,
			    		'itemType' : 'bnpl',
						'quantity': 1,
						'devices' : items.devices,
						'broadBandDevices' : items.broadBandDevices,
						'hardwareDevices' : items.hardwareDevices,
						'aroDevices' : items.aroDevices,
						'totalPriceOutright' : (Number(items.price) + Number(items.aroPrice) + Number(items.hardwarePrice) + Number(items.broadbandPrice)),
						'addPromotion': items.addPromotion,
	        			'promotionSets' : items.promotionSets,
	        			'selectedPromotion': items.selectedPromotion,
	        			'selectedPromotions' : items.selectedPromotions,
	        			'promotionsPrice' : items.promotionsPrice,
	        			'chargeDiscount' : items.chargeDiscount,
						'mobileOption' : items.mobileOption,
						'amount' : items.amount,
						'term' : items.term,
						'company' : items.company
		        	})
            	}

            	if(items.itemType == 'miscellaneous'){
		         	planOption.push({
		        		'id': items.id,
			    		'customDescription' : items.customDescription,
			    		'itemType' : 'miscellaneous',
						'quantity': 1,
	        			'totalPriceMiscellaneous' : 0,
	        			'selectedPromotions' : items.selectedPromotions,
	        			'promotionsPrice' : items.promotionsPrice,
	        			'chargeDiscount' : Number(items.chargeDiscount),
						'mobileOption' : items.mobileOption
		        	})
            	}
            }
        	//if(state.quadrantCartCount < 5){
        		window.localStorage.setItem('cartCount', JSON.stringify(state.cartCount));
	    		window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
        		window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
        	//}

		 //    if(plan.item.itemType === 'businessMobilePlan'){
			// 	window.localStorage.setItem('businessMobilePlan', JSON.stringify(state.businessMobilePlan));
			// }

		 //    if(plan.item.itemType === 'broadBandPlan'){
		 //    	window.localStorage.setItem('mobileBroadbands', JSON.stringify(state.mobileBroadbands));
		 //    }

		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		changeCartPaymentFrequency(state, frequency){

			var itemObj = []
			for (let item of state.cart) {
	         	itemObj.push({
	        		'id': item.id,
	        		'make' :  item.make,
	        		'model' : item.model,
	        		'capacity' : item.capacity,
	        		'totalPrice' : (((Number(item.price) + Number(item.aroAmount) + Number(item.vasTotalAmount) + Number(item.planValue) ).toFixed(2) * 12) / frequency.paymentFrequencyVal).toFixed(2),
	        		'paymentFrequency' : frequency.paymentFrequency,
	        		'paymentFrequencyVal' : frequency.paymentFrequencyVal,
	        		'frequency' : frequency.paymentFrequencyVal,
	        		'planValue' : item.planValue,
	        		'planType' : item.planType,
	        		'quantity' : item.quantity,
	        		'aroAmount' : Number(item.aroAmount).toFixed(2),
	        		'aroBase' : Number(item.aroBase).toFixed(2),
	        		'aroMonths' : Number(item.aroMonths),
	        		'vas' : item.vas,
	        		'vasIdArray' : item.vasIdArray,
	        		'vasTotalAmount' : Number(item.vasTotalAmount).toFixed(2),
	        		'price' : Number(item.price).toFixed(2),
	        		'mths12Contract' : item.mths12Contract,
	        		'mths24Contract' : item.mths24Contract,
	        		'months' : item.months,
	        		'deviceCategory' : item.deviceCategory,
	        		'otherDevices' : item.otherDevices,
	        		'deviceItem' : item.deviceItem
	        	})
            }

            state.cart = []
            for (let items of itemObj) {
	         	state.cart.push({
	        		'id': items.id,
	        		'make' :  items.make,
	        		'model' : items.model,
	        		'capacity' : items.capacity,
	        		'totalPrice' : (((Number(items.price) + Number(items.aroAmount) + Number(items.vasTotalAmount) + Number(items.planValue) ).toFixed(2) * 12) / frequency.paymentFrequencyVal).toFixed(2),
	        		'paymentFrequency' : frequency.paymentFrequency,
	        		'paymentFrequencyVal' : frequency.paymentFrequencyVal,
	        		'frequency' : frequency.paymentFrequencyVal,
	        		'planValue' : items.planValue,
	        		'planType' : items.planType,
	        		'quantity' : items.quantity,
	        		'aroAmount' : Number(items.aroAmount).toFixed(2),
	        		'aroBase' : Number(items.aroBase).toFixed(2),
	        		'aroMonths' : Number(items.aroMonths),
	        		'vas' : items.vas,
	        		'vasIdArray' : items.vasIdArray,
	        		'vasTotalAmount' : Number(items.vasTotalAmount).toFixed(2),
	        		'price' : Number(items.price).toFixed(2),
	        		'mths12Contract' : items.mths12Contract,
	        		'mths24Contract' : items.mths24Contract,
	        		'months' : items.months,
	        		'deviceCategory' : items.deviceCategory,
	        		'otherDevices' : items.otherDevices,
	        		'deviceItem' : items.deviceItem
	        	})
            }
            window.localStorage.setItem('cart', JSON.stringify(state.cart));
		},
		addHro(state, hro){
			var itemObj = []
			var crypto = require("crypto");
			var hroMonthlyPrice = 0;
			var hroNetTotal = 0;
			var hroDevicesTotalAmount = 0
			var hroDevices  = []

			//if(state.quadrantCartCount < 4){
				if(hro.hro && hro.hroMonthly == 24){
					hroMonthlyPrice = hro.hro.price_24_months
				}

				if(hro.hro && hro.hroMonthly == 12){
					hroMonthlyPrice = (hro.hro.price_24_months * 2).toFixed(2)
				}

				if(hro.hro){
					for(let item of hro.hro){
						hroDevices.push(item)
						if(hro.hroMonthly == 12){
							hroDevicesTotalAmount += Number(item.price_12_months)
						}

						if(hro.hroMonthly == 24){
							hroDevicesTotalAmount += Number(item.price_24_months)
						}
					}
				}

				state.quadrantCart.push({
	        		'hro' : hroDevices,
		    		'hroMonthlyPrice': hroDevicesTotalAmount,
		    		'hroNetTotal': hroDevicesTotalAmount,
		    		'hroMonthly': hro.hroMonthly,
		    		'id': crypto.randomBytes(20).toString('hex'),
		    		'customDescription' : hro.customDescription,
		    		'itemType' : hro.itemType,
		    		'quantity' : 1,
		    		'addPromotion': {'id':0,'name':'no'},
        			'promotionSets' : [],
        			'selectedPromotion': {'id' : 0, 'name' : ''},
        			'promotionsPrice' : 0,
        			'selectedPromotions' : [],
        			'isBusiness' : hro.isBusiness,
        			'mobileOption' : hro.mobileOption
	        	});	

				state.quadrantCartCount++
	        	window.localStorage.setItem('cartCount', state.cartCount);
	        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
				window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
			// }else{
			// 	let hroPlanOption = ''

			// 	if(hro.itemType === 'hro'){
			// 		hroPlanOption = state.hro
			// 	}

			// 	if(hro.itemType === 'businessHRO'){
			// 		hroPlanOption = state.businessHRO
			// 	}

			// 	for (let item of hroPlanOption) {
			// 		if(item.id == hro.item.id){
			// 			if(hro.hro && hro.hroMonthly == 24){
			// 				hroMonthlyPrice = hro.hro.price_24_months
			// 			}

			// 			if(hro.hro && hro.hroMonthly == 12){
			// 				hroMonthlyPrice = (hro.hro.price_24_months * 2).toFixed(2)
			// 			}
			// 			itemObj.push({
			// 	    		'hro' : hro.hro,
			// 	    		'hroMonthlyPrice': hroMonthlyPrice,
			// 	    		'hroMonthly': hro.hroMonthly,
			// 	    		'id': crypto.randomBytes(20).toString('hex'),
			// 	    		'customDescription' : hro.customDescription,
			// 	    		'itemType' : hro.itemType,
			// 	    		'quantity' : 1,
			// 	    		'addPromotion': {'id':0,'name':'no'},
		 //        			'promotionSets' : [],
		 //        			'selectedPromotion': {'id' : 0, 'name' : ''},
		 //        			'promotionsPrice' : 0
			// 	    	});
			// 		}else{
			// 			itemObj.push({
			// 	    		'hro' : item.hro,
			// 	    		'hroMonthlyPrice': item.hroMonthlyPrice,
			// 	    		'hroMonthly': item.hroMonthly,
			// 	    		'id': item.id,
			// 	    		'customDescription' : item.customDescription,
			// 	    		'itemType' : item.itemType,
			// 	    		'quantity' : 1,
			// 	    		'addPromotion': {'id':0,'name':'no'},
		 //        			'promotionSets' : [],
		 //        			'selectedPromotion': {'id' : 0, 'name' : ''},
		 //        			'promotionsPrice' : 0
			// 	    	});
			// 		}

			// 	}
				
			// 	if(hro.itemType === 'hro'){
			// 		state.hro = []
			// 		hroPlanOption = state.hro
			// 	}

			// 	if(hro.itemType === 'businessHRO'){
			// 		state.businessHRO = []
			// 		hroPlanOption = state.businessHRO
			// 	}

			// 	for (let items of itemObj) {
		 //         	hroPlanOption.push({
		 //        		'hro' : items.hro,
			//     		'hroMonthlyPrice': items.hroMonthlyPrice,
			//     		'hroMonthly': items.hroMonthly,
			//     		'id' : items.id,
			//     		'quantity' : 1,
			//     		'customDescription' : items.customDescription,
			//     		'itemType' : items.itemType,
			//     		'addPromotion': {'id':0,'name':'no'},
	  //       			'promotionSets' : [],
	  //       			'selectedPromotion': {'id' : 0, 'name' : ''},
	  //       			'promotionsPrice' : 0,
	  //       			'selectedPromotions' : []
		 //        	})
	  //           }

	  //           if(hro.itemType === 'hro'){
			//     	window.localStorage.setItem('cartCount', state.cartCount);
			// 		window.localStorage.setItem('hro', JSON.stringify(state.hro));
			// 	}

			// 	if(hro.itemType === 'businessHRO'){
			//     	window.localStorage.setItem('cartCount', state.cartCount);
			// 		window.localStorage.setItem('businessHRO', JSON.stringify(state.businessHRO));
			// 	}
			// }

		},
		addFixedPlan(state, fixed){
			var itemObj = []
			var crypto = require("crypto");
			var hroMonthlyPrice = 0;
			let fixedPlanOption = ''
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;

			if(fixed.valueAddedService){
				for (let val of fixed.valueAddedService) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}

			state.quadrantCart.push({
        		'fixedPlanPrice' : (Number(fixed.fixedPlanPrice) + Number(vasTotalAmount)),
        		'fixedPrice' : (Number(fixed.fixedPlanPrice)),
	    		'fixedPlanType': fixed.fixedPlan,
	    		'fixed' : fixed.fixed,
	    		'id': crypto.randomBytes(20).toString('hex'),
	    		'customDescription' : fixed.customDescription,
	    		'itemType': 'fixed',
	    		'valueAddedService': fixed.valueAddedService,
	    		'quantity' : 1,
	    		'addPromotion': {'id': 0, 'name': "no"},
	    		'promotionSets': [],
				'promotionsPrice': 0,
	    		'selectedPromotion': {'id': 0, 'name': ""},
	    		'selectedPromotions' : [],
	    		'planDescription' : fixed.fixed.planDescription,
	    		'isBusiness' : fixed.isBusiness,
        		'vas' : vasArray,
        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
        		'vasIdArray' : vasIdArray,
        		'mobileOption' : fixed.mobileOption
        	});	

        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
			
		    
		},
		addExistingServices(state, existingServices){
			var itemObj = []
			var crypto = require("crypto");
			for (let item of state.existingServices) {
				if(item.id == existingServices.id){
					itemObj.push({
			    		'existingServicesPrice' : (existingServices.existingServicesPrice),
			    		'existingServicesDescription': existingServices.existingServicesDescription,
			    		'id': crypto.randomBytes(20).toString('hex'),
			    		'customDescription' : existingServices.customDescription,
			    		'itemType' : 'existingServices'
			    	});
				}else{
					itemObj.push({
			    		'existingServicesPrice' : (item.existingServicesPrice),
			    		'existingServicesDescription': item.existingServicesDescription,
			    		'id': item.id,
			    		'customDescription' : item.customDescription,
			    		'itemType' : 'existingServices'
			    	});
				}

			}
			state.existingServices = []
			for (let items of itemObj) {
	         	state.existingServices.push({
	        		'existingServicesPrice' : (items.existingServicesPrice),
		    		'existingServicesDescription': items.existingServicesDescription,
		    		'id': items.id,
		    		'customDescription' : items.customDescription,
		    		'quantity' : 1,
		    		'itemType' : 'existingServices'
	        	})
            }
		    window.localStorage.setItem('existingServices', JSON.stringify(state.existingServices));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		addPromotions(state, promotions){
			var itemObj = []
			var crypto = require("crypto");
			for (let item of state.promotions) {
				if(item.id == promotions.id){
					itemObj.push({
						'discounts' : promotions.discounts,
			    		'promotionsPrice' : (promotions.promotionsPrice),
			    		'promotionsDescription': promotions.promotionsDescription,
			    		'id': crypto.randomBytes(20).toString('hex'),
			    		'customDescription' : promotions.customDescription
			    	});
				}else{
					itemObj.push({
						'discounts' : item.discounts,
			    		'promotionsPrice' : (item.promotionsPrice),
			    		'promotionsDescription': item.promotionsDescription,
			    		'id': item.id,
			    		'customDescription' : item.customDescription
			    	});
				}

			}
			state.promotions = []
			for (let items of itemObj) {
	         	state.promotions.push({
	         		'discounts' : items.discounts,
	        		'promotionsPrice' : (items.promotionsPrice),
		    		'promotionsDescription': items.promotionsDescription,
		    		'id': items.id,
		    		'customDescription' : items.customDescription,
		    		'quantity' : 1
	        	})
            }
		    window.localStorage.setItem('promotions', JSON.stringify(state.promotions));
		    window.localStorage.setItem('cartCount', state.cartCount);
		},
		addEntertainmentPlan(state, entertainment){
			var itemObj = []
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;

			if(entertainment.valueAddedService){
				for (let val of entertainment.valueAddedService) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}
			var crypto = require("crypto");
			var entertainmentPlans = []
		 	entertainmentPlans.push(entertainment.selectedEntertainmentPlan)
		 	
			// console.log(entertainmentPlans)
			// return

			state.quadrantCart.push({
				'addPromotion': {'id':0,'name':'no'},
	    		'chargeDiscount' : 0,
	    		'customDescription' : '',
	    		'entertainment' : entertainmentPlans,
        		'vas' : vasArray,
        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
        		'vasIdArray' : vasIdArray,
	    		'id': crypto.randomBytes(20).toString('hex'),
				'quantity': 1,
				'itemType': 'entertainment',
				'promotionSets' : [],
        		'selectedPromotions' : [],
	    		'promotionsPrice' : 0,
				'selectedPromotion': {'id' : 0, 'name' : ''},
	    		'totalEntertainmentPrice' : Number(entertainment.entertainmentPlanPrice + vasTotalAmount).toFixed(2),
        	});	

			state.cartCount++;
        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		
		},
		addTelstraPlatinum(state, plan){
			var itemObj = []
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			// console.log(plan)
			// return
			var crypto = require("crypto");
			state.quadrantCart.push({
				'addPromotion': {'id':0,'name':'no'},
        		'platinumPlanPrice' : Number(plan.entertainmentPlanPrice).toFixed(2),
	    		'platinumPlanType': plan.selectedEntertainmentPlan.name,
	    		'platinumPlanDescription': plan.entertainmentPlanDescription,
	    		'platinum' : plan.entertainment,
	    		'promotionSets' : [],
	    		'selectedEntertainmentPlan' : plan.selectedEntertainmentPlan,
	    		'id': crypto.randomBytes(20).toString('hex'),
	    		'itemType' : 'platinum',
	    		'quantity' : 1,
	    		'customDescription' : plan.customDescription,
	    		'planDescription' : plan.selectedEntertainmentPlan.planDescription,
	    		'chargeDiscount' : 0,
	    		'promotionsPrice' : 0,
	    		'totalPlatinumPrice' : Number(plan.entertainmentPlanPrice).toFixed(2),
        		'selectedPromotions' : []
        	});	

			state.cartCount++;
        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		
		},
		duplicateEntertainment(state, plan){
			var itemObj = []
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			var entertainmentDevicesTotalAmount = 0;
			var entertainmentDevices = []

			if(plan.vas){
				for (let val of plan.vas) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}
			var crypto = require("crypto");
				

			if(plan.entertainment){
				for(let entertainment of plan.entertainment){
					if(entertainment){
						entertainmentDevices.push(entertainment)

						entertainmentDevicesTotalAmount += Number(entertainment.price)
					}
				}
			}

			state.quadrantCart.push({
				'addPromotion' : plan.addPromotion,
				'chargeDiscount' : plan.chargeDiscount,
	    		'customDescription' : plan.customDescription,
	    		'entertainment' : plan.entertainment,
	    		'vas' : plan.vas,
        		'vasTotalAmount' : Number(plan.vasTotalAmount).toFixed(2),
        		'vasIdArray' : vasIdArray,
        		'id': crypto.randomBytes(20).toString('hex'),
        		'itemType' : 'entertainment',
        		'promotionSets' : plan.promotionSets,
        		'selectedPromotions' : plan.selectedPromotions,
        		'promotionsPrice' : plan.promotionsPrice,
        		'totalEntertainmentPrice' : plan.totalEntertainmentPrice
	    	});
			// console.log(state.quadrantCart)
			// return
			// state.quadrantCart.push({
			// 	'addPromotion': entertainment.addPromotion,
   //      		'entertainmentPlanPrice' : Number(entertainment.entertainmentPlanPrice + vasTotalAmount).toFixed(2),
	  //   		'entertainmentPlanType': entertainment.selectedEntertainmentPlan.name,
	  //   		'entertainmentPlanDescription': entertainment.entertainmentPlanDescription,
	  //   		'entertainment' : entertainment.entertainment,
	  //   		'promotionSets' : entertainment.promotionSets,
	  //   		'selectedEntertainmentPlan' : entertainment.selectedEntertainmentPlan,
	  //   		'id': crypto.randomBytes(20).toString('hex'),
	  //   		'itemType' : 'entertainment',
	  //   		'quantity' : 1,
	  //   		'customDescription' : entertainment.customDescription,
	  //   		'planDescription' : entertainment.selectedEntertainmentPlan.planDescription,
	  //   		'chargeDiscount' : 0,
	  //   		'promotionsPrice' : 0,
	  //   		'totalEntertainmentPrice' : Number(entertainment.entertainmentPlanPrice + vasTotalAmount).toFixed(2),
   //      		'vas' : vasArray,
   //      		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
   //      		'vasIdArray' : vasIdArray,
   //      		'selectedPromotions' : entertainment.selectedPromotions
   //      	});	
			state.cartCount++;
        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		},
		duplicatePlatinum(state, plan){
			var itemObj = []
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			// console.log(plan)
			// return
			var crypto = require("crypto");
			state.quadrantCart.push({
				'addPromotion': {'id':0,'name':'no'},
        		'platinumPlanPrice' : Number(plan.platinumPlanPrice).toFixed(2),
	    		'platinumPlanType': plan.platinumPlanType,
	    		'platinumPlanDescription': plan.platinumPlanDescription,
	    		'platinum' : plan.platinum,
	    		'promotionSets' : [],
	    		'selectedEntertainmentPlan' : plan.selectedEntertainmentPlan,
	    		'id': crypto.randomBytes(20).toString('hex'),
	    		'itemType' : 'platinum',
	    		'quantity' : 1,
	    		'customDescription' : plan.customDescription,
	    		'planDescription' : plan.planDescription,
	    		'chargeDiscount' : 0,
	    		'promotionsPrice' : 0,
	    		'totalPlatinumPrice' : Number(plan.totalPlatinumPrice).toFixed(2),
        		'selectedPromotions' : []
        	});	

			state.cartCount++;
        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		},
		duplicateFixed(state, fixed){
			var itemObj = []
			var crypto = require("crypto");
			var hroMonthlyPrice = 0;
			let fixedPlanOption = ''
			let vasAmount = 0
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;

			if(fixed.vas){
				for (let val of fixed.vas) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}

			state.quadrantCart.push({
        		'fixedPlanPrice' : (Number(fixed.fixedPlanPrice) + Number(vasTotalAmount)),
        		'fixedPrice' : (Number(fixed.fixedPlanPrice)),
	    		'fixedPlanType': fixed.fixedPlan,
	    		'fixed' : fixed.fixed,
	    		'id': crypto.randomBytes(20).toString('hex'),
	    		'customDescription' : fixed.customDescription,
	    		'itemType': 'fixed',
	    		'valueAddedService': fixed.valueAddedService,
	    		'quantity' : 1,
	    		'addPromotion' : fixed.addPromotion,
	    		'promotionSets': fixed.promotionSets,
				'promotionsPrice': 0,
	    		'selectedPromotion': fixed.selectedPromotion,
	    		'selectedPromotions' : fixed.selectedPromotions,
	    		'planDescription' : fixed.fixed.planDescription,
	    		'isBusiness' : fixed.isBusiness,
        		'vas' : vasArray,
        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
        		'vasIdArray' : vasIdArray,
				'mobileOption' : fixed.mobileOption
        	});	
			state.cartCount++;
        	window.localStorage.setItem('cartCount', state.cartCount);
			state.quadrantCartCount++
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
			
		    
		},
		duplicateHRO(state, hro,quantity){
			var itemObj = []
			var crypto = require("crypto");
			var hroMonthlyPrice = 0;
			var hroNetTotal = 0;
			var hroDevicesTotalAmount = 0
			var hroDevices  = []
			if(hro.hro && hro.hroMonthly == 24){
				hroMonthlyPrice = hro.hro.price_24_months
			}

			if(hro.hro && hro.hroMonthly == 12){
				hroMonthlyPrice = (hro.hro.price_24_months * 2).toFixed(2)
			}

			if(hro.hro){
				for(let item of hro.hro){
					hroDevices.push(item)
					if(hro.hroMonthly == 12){
						hroDevicesTotalAmount += Number(item.price_12_months)
					}

					if(hro.hroMonthly == 24){
						hroDevicesTotalAmount += Number(item.price_24_months)
					}
				}
			}

			state.quadrantCart.push({
        		'hro' : hroDevices,
	    		'hroMonthlyPrice': hroDevicesTotalAmount,
	    		'hroNetTotal': hroDevicesTotalAmount,
	    		'hroMonthly': hro.hroMonthly,
	    		'id': crypto.randomBytes(20).toString('hex'),
	    		'customDescription' : hro.customDescription,
	    		'itemType' : hro.itemType,
	    		'quantity' : 1,
	    		'addPromotion': hro.addPromotion,
    			'promotionSets' : [],
    			'selectedPromotion': hro.selectedPromotion,
    			'promotionsPrice' : 0,
    			'selectedPromotions' : hro.selectedPromotions,
    			'isBusiness' : hro.isBusiness
        	});	
			state.cartCount++;
			state.quadrantCartCount++
        	window.localStorage.setItem('cartCount', state.cartCount);
        	window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
			window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		},
		duplicateMobilePlan(state, plan,quantity){
			var crypto = require("crypto");
			var itemObj = []
			var hroMonthlyPrice = 0;
			var vasArray = [];
			var vasIdArray = [];
			var vasTotalAmount = 0;
			var itemPrice = 0;
			var discountPrice = 0;
			let hroPlanOption = ''
			var hroMonthlyPrice = 0;
			var aroAccessories = []
			var aroAccessoriesTotalPrice = 0
			var aroAccessoriesTotal = 0
			var aroTotalMatched = false
			var deviceDiscounts = 0
			var chargeDiscount = 0
			// console.log(quantity)
			// console.log(state)
			// console.log(plan)
			// return
			if(plan.vas){
				for (let val of plan.vas) {
			      	vasArray.push(val) // the value of the current key.
			      	vasIdArray.push(val.id)
			      	vasTotalAmount += val.amount
			  	}
			}

			if(plan.aroAccessories){
				for (let aro of plan.aroAccessories) {
					if((plan.aroAccessory) && aro.aroID == plan.aroAccessory.aroID){
				      	aroAccessories.push(plan.aroAccessory) // the value of the current key.

				    }else{
				    	aroAccessories.push(aro)
				    }
			  	}

			  	for (let aro of aroAccessories) {
				    aroAccessoriesTotalPrice += Number(aro.aroPrice)
			  	}

			  	if(plan.aroBase == aroAccessoriesTotalPrice){
			  		aroTotalMatched = true
			  	}
			}

			if(plan.aroAccessory && plan.aroAccessory.aroAdd == true){
				aroAccessories.push(plan.aroAccessory)
			}

			if(plan.aroAccessoryForDelete){
				let index = plan.aroAccessories.indexOf(plan.aroAccessoryForDelete);
				if (index > -1) {
					plan.aroAccessories.splice(index,1);
				}
				aroAccessories = plan.aroAccessories

				for (let aro of aroAccessories) {
				    aroAccessoriesTotal += Number(aro.aroPrice)
			  	}

			  	aroAccessoriesTotalPrice = aroAccessoriesTotal
			}

		  		let planOption = ''

				planOption = state.cart

				if(plan.deviceItem != 0 && plan.months == "24"){
					itemPrice = plan.deviceItem.mths_24_device_payment_contract
				}

				if(plan.deviceItem != 0 && plan.months == "12"){
					itemPrice = plan.deviceItem.mths_12_device_payment_contract
				}

				if(plan.deviceItem != 0 && plan.months == "36"){
					itemPrice = plan.deviceItem.mths_36_device_payment_contract
				}

				if(plan.aroBase != 0){
					var aroAmount = (Number(plan.aroBase) / Number(plan.aroMonths)) 
				}else{
					var aroAmount = 0.00
				}

				// if(plan.discounts && plan.promotionsPrice && plan.discounts.type.name === 'Reoccuring Discount'){
				// 	discountPrice = Number(plan.promotionsPrice).toFixed(2)
				// }else{
				// 	discountPrice = 0
				// }
				if(plan.selectedPromotions){
					for(let promo of plan.selectedPromotions){
						if(promo.type.name === 'Reoccuring Discount'){
							discountPrice += Number(promo.discount_price)
						}
					}
				}

				if(plan.deviceDiscounts){
					deviceDiscounts = plan.deviceDiscounts
				}else{
					deviceDiscounts = 0
				}
				let discountPriceTypes = 0

				if(plan.planOption.name == 'Small'){
					discountPriceTypes = plan.smallDiscount
				}

				if(plan.planOption.name == 'Medium'){
					discountPriceTypes = plan.mediumDiscount
				}

				if(plan.planOption.name == 'Large'){
					discountPriceTypes = plan.largeDiscount
				}

				if(plan.planOption.name == 'Extra Large'){
					discountPriceTypes = plan.xlDiscount
				}

				if(plan.planOption.name == 'Extra Small'){
					discountPriceTypes = plan.xsDiscount
				}

				// if(plan.aroAccessories){
				// 	for (let aro of plan.aroAccessories) {
				// 		//delete(aro.aroID)
				// 		// var crypto = require("crypto");
				// 		// aro.aroID = crypto.randomBytes(20).toString('hex')
				// 	    aroAccessories.push(aro) // the value of the current key.
				//   	}
				// }
				state.quadrantCart.push({
	        		'id': crypto.randomBytes(20).toString('hex'),
	        		'customDescription' : plan.customDescription,
	        		'make' :  (plan.deviceItem != 0) ? plan.deviceItem.make : '',
	        		'model' : (plan.deviceItem != 0) ? plan.deviceItem.model : '',
	        		'capacity' : (plan.deviceItem != 0) ? plan.deviceItem.capacity : '',
	        		//'totalPrice' : ((((Number(itemPrice) - Number(discountPriceTypes) + Number(aroAmount)  + Number(vasTotalAmount) + Number(plan.planValue) ).toFixed(2) * 12) / item.paymentFrequency - discountPrice - Number(deviceDiscounts)) ).toFixed(2),
	        		'totalPrice' : ((((Number(itemPrice) + Number(aroAmount)  + Number(vasTotalAmount) + Number(plan.planValue) ).toFixed(2) * 12) / plan.paymentFrequency - discountPrice - Number(deviceDiscounts)) ).toFixed(2),
	        		'paymentFrequency' : plan.paymentFrequency,
	        		'paymentFrequencyVal' : plan.paymentFrequency,
	        		'frequency' : plan.frequency,
	        		'planValue' : Number(plan.planValue),
	        		'planType' : plan.planType,
	        		'planOption' : plan.planOption,
	        		'quantity' : plan.quantity,
	        		'aroMonths' : plan.aroMonths,
	        		'aroAmount' : Number(aroAmount).toFixed(2),
	        		'aroBase' : Number(plan.aroBase).toFixed(2),
	        		'vas' : vasArray,
	        		'vasIdArray' : vasIdArray,
	        		'vasTotalAmount' : Number(vasTotalAmount).toFixed(2),
	        		'price' : Number(itemPrice).toFixed(2),
	        		'mths12Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_12_device_payment_contract : '',
	        		'mths24Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_24_device_payment_contract : '',
	        		'mths36Contract' : (plan.deviceItem != 0) ? plan.deviceItem.mths_36_device_payment_contract : '',
	        		'months' : plan.months,
	        		'deviceCategory' : (plan.deviceItem != 0) ? plan.deviceItem.deviceCategory : '',
	        		'otherDevices' : plan.otherDevices,
	        		'deviceItem' : (plan.deviceItem != 0) ? plan.deviceItem : '',
	        		'itemType' : plan.itemType,
        			'mobilePlanMonths' : plan.mobilePlanMonths,
        			'selectedARO' : plan.selectedARO,
        			'selectedAROMonth' : plan.selectedAROMonth,
        			'promotionsDescription' : plan.promotionsDescription,
        			'discounts' : plan.discounts,
        			'deviceDiscounts' : plan.deviceDiscounts,
        			'promotionsPrice' : plan.promotionsPrice,
        			'aroAccessories' : aroAccessories,
        			'aroAccessoriesTotalPrice' : aroAccessoriesTotalPrice,
        			'aroTotalMatched' : aroTotalMatched,
        			'addPromotion' : plan.addPromotion,
        			'promotionSets' : plan.promotionSets,
        			'selectedPromotion' : plan.selectedPromotion,
        			'smallDiscount' : Number(plan.smallDiscount),
					'mediumDiscount' : Number(plan.mediumDiscount),
					'largeDiscount' : Number(plan.largeDiscount),
					'xlDiscount' : Number(plan.xlDiscount),
					'xsDiscount' : Number(plan.xsDiscount),
					'promotionDiscounts' : plan.promotionDiscounts,
					'selectedPromotions' : plan.selectedPromotions,
					'isBusiness' : plan.isBusiness,
					'mobileOption' : plan.mobileOption
	        	})

	        	state.cartCount++;
        		window.localStorage.setItem('cartCount', (state.cartCount));
        		if(state.quadrantCartCount < 4){
		        	state.quadrantCartCount++
		    		window.localStorage.setItem('quadrantCartCount', state.quadrantCartCount);
		    	}
        		window.localStorage.setItem('quadrantCart', JSON.stringify(state.quadrantCart));
		},
    }
};

export default store;