<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://matomo.bsbo.com.au/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '3']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="/css/font.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plan-styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<style type="text/css">
    html {
      position: relative;
      min-height: 100%;
      font-family: 'Open Sans', sans-serif !important;
    }
    body {
      margin-bottom: 60px; /* Margin bottom by footer height */
    }
    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 60px; /* Set the fixed height of the footer here */
      line-height: 60px; /* Vertically center the text there */
      background-color: #f5f5f5;
    }

    @media (min-width: 1200px) {
      .container {
        max-width: 1340px;
      }
    }

    a:hover{
        text-decoration: none;
    }

    .endDate{
        font-size:17px !important;
    }

    .canvas{
        border:2px solid black !important;
        border-radius: 10px !important;
    }
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- config('app.name', 'Laravel') --}}
                    <img src="/img/Okart_LogoWhite.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>



                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{-- Auth::user()->name --}} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div> -->
                            <cart-dropdown></cart-dropdown>
                        </li>
                        <li class="nav-item">
                            <!-- <span id="navbarDropdown3" role="button" class="nav-link text-white" style="font-size: 20px;"><span>{{$_SESSION['userInfo']['name']}}</span> <span style="font-size: 26px;">&nbsp;</span>
                            </span> -->

                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" style="font-size: 20px;padding-top:15px;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{$_SESSION['userInfo']['name']}} <span class="caret"></span>
                            </a>

                            <!--select current site location -->
                            @if(!isset($_SESSION['currentClient']))
                            <div class="modal fade" id="currentClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Client</h5>
                                  </div>
                                    <form action="save-client" method="POST">
                                      <div class="modal-body">
                                            <div class="form-group">
                                                {{ csrf_field() }}
                                                <label for="exampleFormControlSelect1">Select Client</label>
                                                <select name="client" class="form-control" id="branchVal">
                                                    @foreach($_SESSION['clientCollection'] as $data)
                                                    <option>{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="">Save</button>
                                      </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            @endif
                            <!--end select current site location-->

                            <!--select current branch -->
                            @if(isset($_SESSION['currentClient']))
                                <div class="modal fade" id="currentBranch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Branch</h5>
                                      </div>
                                        <form action="save-branch" method="POST">
                                          <div class="modal-body">
                                                <div class="form-group">
                                                    {{ csrf_field() }}
                                                    <label for="exampleFormControlSelect1">Select Branch</label>
                                                    <select name="branch" class="form-control" id="branchVal">
                                                        @foreach($_SESSION['branchCollection'] as $data)
                                                        <option>{{$data['branchName']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" id="">Save</button>
                                          </div>
                                        </form>
                                    </div>
                                  </div>
                                </div>
                            @endif
                            <!--end select current branch-->

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(isset($_SESSION['currentBranch']))
                                    <p class="dropdown-item" >Branch: {{$_SESSION['currentBranch']}}</p>
                                    <div class="text-center" style="width:100%">
                                        <a href="/change-branch"><button class="btn btn-primary">Change Location</button></a>
                                    </div>
                                @endif
                                <hr/>
                                
                                @if(isset($_SESSION['userInfo']) && $_SESSION['userInfo']['role'] == 'Administrator' || $_SESSION['userInfo']['email'] == 'cassie@azenko.com.au'|| $_SESSION['userInfo']['email'] == 'trent@azenko.com.au' || $_SESSION['userInfo']['email'] == 'KymB@tlsbendigo.com.au' || $_SESSION['userInfo']['email'] == 'rachel@azenko.com.au' || $_SESSION['userInfo']['email'] == 'Repairs@tlsbendigo.com.au' || $_SESSION['userInfo']['email'] == 'jasonj@tlsbendigo.com.au')
                                    <div class="text-center" style="width:100%">
                                        <a href="/admin"><button class="btn btn-primary">Go to Admin</button></a>
                                    </div>
                                @endif
                                <hr/>
                                <div class="text-center" style="width:100%">
                                    <a href="/promotions"><button class="btn btn-primary">Promotions</button></a>
                                </div>
                                <div class="text-center" style="width:100%;padding-top:20px;">
                                    <a href="/cart-history"><button class="btn btn-primary">Cart History</button></a>
                                </div>
                                <div class="text-center" style="width:100%;padding-top:20px;">
                                    <a href="/cart-reports"><button class="btn btn-primary">Cart Reports</button></a>
                                </div>
                                <hr/>
                                <a class="dropdown-item" href="sso-logout" onclick='return logout()'>
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="footer">
          <div class="container">
            <div class="row">
                <span class="col-6 pull-left text-muted">© {{date('Y')}} Azenko Pty Ltd</span>
                <span class="col-6 pull-right text-right text-muted"><a href="https://azenko.com.au/wp-content/uploads/2020/01/Azenko_Privacy_Policy_Jan_20_Updated-1.pdf">Privacy</a> | <a target="_blank" href="https://forms.gle/HeMTZYkdvCBSzAzS7">Support</a> | <a data-toggle="modal" data-target="#quiclinkModal"> Quicklinks</a></span>
            </div>
          </div>
        </footer>

        <!-- Modal -->
        <div class="modal fade" id="quiclinkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quicklinks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <available-quicklinks></available-quicklinks>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </div>
<script src="/js/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/bootstrap-3.3.7.min.js"></script>
    <script type="text/javascript">
        var currentClient = "<?php echo isset($_SESSION['currentClient']) ? 'true' : 'false' ?>"; 
        var currentBranch = "<?php echo isset($_SESSION['currentBranch']) ? 'true' : 'false' ?>"; 
   
        if(currentClient != 'false' && currentBranch === 'false'){
            console.log(currentBranch)
            $(window).on('load',function(){
                $('#currentBranch').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }

        if(currentClient === 'false'){
            console.log(currentClient)
            $(window).on('load',function(){
                $('#currentClient').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }

        var cartTemplate = "<?php echo isset($cart) ? 'true' : 'false' ?>"; 

        if(currentClient != 'false' && currentBranch != 'false' && cartTemplate != 'false'){
            $(window).on('load',function(){
                $('#cartTemplate').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }

        function logout()
        {
            localStorage.clear();
        }
    </script>
    @yield('scripts')
    <!-- Matomo -->
    <!-- End Matomo Code -->
</body>
</html>
