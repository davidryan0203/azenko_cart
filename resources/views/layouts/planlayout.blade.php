<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plan-styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<style type="text/css">
    html {
      position: relative;
      min-height: 100%;
      font-family: 'Open Sans', sans-serif !important;
    }
    body {
      margin-bottom: 60px; /* Margin bottom by footer height */
    }
    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 60px; /* Set the fixed height of the footer here */
      line-height: 60px; /* Vertically center the text there */
      background-color: #f5f5f5;
    }

    .endDate{
        font-size:17px !important;
    }

    .canvas{
        border:2px solid black !important;
        border-radius: 10px !important;
    }
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- config('app.name', 'Laravel') --}}
                    <img src="/img/Okart_LogoWhite.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        {{--@guest
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li> -->
                            @if (Route::has('register'))
                            <!--     <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> -->
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div> -->
                                <cart-dropdown></cart-dropdown>
                            </li>
                        @endguest--}}
                        <li class="nav-item dropdown">
                            <cart-dropdown></cart-dropdown>
                        </li>

                        <li class="nav-item">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" style="font-size: 20px;padding-top:15px;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{$_SESSION['userInfo']['name']}} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(isset($_SESSION['currentBranch']))
                                    <p class="dropdown-item" >Branch: {{$_SESSION['currentBranch']}}</p>
                                    <div class="text-center" style="width:100%">
                                        <a href="/change-branch"><button class="btn btn-primary">Change Location</button></a>
                                    </div>
                                @endif
                                <hr/>

                                @if(isset($_SESSION['userInfo']) && $_SESSION['userInfo']['role'] == 'Administrator')
                                    <div class="text-center" style="width:100%">
                                        <a href="/admin"><button class="btn btn-primary">Go to Admin</button></a>
                                    </div>
                                @endif
                                <hr/>
                                <div class="text-center" style="width:100%">
                                    <a href="/promotions"><button class="btn btn-primary">Promotions</button></a>
                                </div>
                                <div class="text-center" style="width:100%;padding-top:20px;">
                                    <a href="/cart-history"><button class="btn btn-primary">Cart History</button></a>
                                </div>
                                <div class="text-center" style="width:100%;padding-top:20px;">
                                    <a href="/cart-reports"><button class="btn btn-primary">Cart Reports</button></a>
                                </div>
                                <hr/>
                                <a class="dropdown-item" href="sso-logout" onclick='return logout()'>
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="footer">
          <div class="container">
            <div class="row">
                <span class="col-6 pull-left text-muted">© {{date('Y')}} Azenko Pty Ltd</span>
                <span class="col-6 pull-right text-right text-muted"><a href="https://azenko.com.au/wp-content/uploads/2020/01/Azenko_Privacy_Policy_Jan_20_Updated-1.pdf
">Privacy</a> | <a target="_blank" href="https://forms.gle/HeMTZYkdvCBSzAzS7">Support</a> | <a data-toggle="modal" data-target="#quiclinkModal"> Quicklinks</a></span>
            </div>
          </div>
          </div>
        </footer>

        
        <!-- Modal -->
        <div class="modal fade" id="quiclinkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quicklinks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <available-quicklinks></available-quicklinks>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    <script type="text/javascript">
        function logout()
        {
            localStorage.clear();
        }
    </script>
    @yield('scripts')
</body>
</html>
