<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>{{ config('app.name', 'Xero Project') }}</title>
        <link href="/css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <style type="text/css">
        @media (min-width: 1200px){
            .container-xl, .container-lg, .container-md, .container-sm, .container {
                max-width: 1200px;
            }
        }

        .no-padding{
            padding-left:0px !important;
            padding-right:5px !important;
        }

        .VueTables__child-row-toggler--closed::before {
          content: "+" !important;
          text-align: center !important;
        }

        .VueTables__child-row-toggler--open::before {
          content: "-" !important;
          text-align: center !important;
        }
    </style>
    <body class="nav-fixed">
        <span id="app">
            <nav class="topnav navbar navbar-expand shadow navbar-light bg-white" id="sidenavAccordion">
                <a class="navbar-brand d-none d-sm-block" href="/">
                    <img src="/img/Okart_Logo.png" style="height: 2.8rem">
                </a>

                <ul class="navbar-nav align-items-center ml-auto">
                    <li class="nav-item dropdown no-caret mr-3 dropdown-user">
                        <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- <img class="img-fluid" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"/> -->
                            <i class="fa fa-user"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                            <h6 class="dropdown-header d-flex align-items-center">
                                <!-- <img class="dropdown-user-img" src="https://source.unsplash.com/QAB-WJcbgJk/60x60" /> -->
                                <div class="dropdown-user-details">
                                    <div class="dropdown-user-details-name">{{$_SESSION['userInfo']['name']}}</div>
                                    <div class="dropdown-user-details-email">{{$_SESSION['userInfo']['email']}}</div>
                                </div>
                            </h6>
                            <div class="dropdown-divider"></div>
                            <!-- <a class="dropdown-item" href="#!">
                                <div class="dropdown-item-icon"><i data-feather="settings"></i></div>
                                Account
                            </a> -->
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="dropdown-item-icon">
                                    <i data-feather="log-out"></i>
                                </div>
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <nav class="sidenav shadow-right sidenav-light">
                        <div class="sidenav-menu">
                            <div class="nav accordion" id="accordionSidenav">
                                 <div class="sidenav-menu-heading"></div>
                                <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#plans" aria-expanded="false" aria-controls="plans"
                                    ><div class="nav-link-icon"><i data-feather="upload"></i></div>
                                    Upload Plans
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="plans" data-parent="#accordionSidenav">
                                    <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavLayout">
                                        
                                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayoutsPageHeaders" aria-expanded="false" aria-controls="collapseLayoutsPageHeaders">
                                            Cosumer
                                            <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                        </a>
                                        <div class="collapse" id="collapseLayoutsPageHeaders" data-parent="#accordionSidenavLayout">
                                            <nav class="sidenav-menu-nested nav">
                                                <a class="nav-link" href="/mobile-plan/upload">Mobile Plan</a>
                                                <a class="nav-link" href="/mobile-broadband/upload">Mobile Broadband</a>
                                            </nav>
                                        </div>

                                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#businessPlans" aria-expanded="false" aria-controls="businessPlans">
                                            Business
                                            <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                        </a>
                                        <div class="collapse" id="businessPlans" data-parent="#accordionSidenavLayout">
                                            <nav class="sidenav-menu-nested nav">
                                                <a class="nav-link" href="/business-mobile-plan/upload">Mobile Plan</a>
                                                <a class="nav-link" href="/business-mobile-broadband/upload">Mobile Broadband</a>
                                            </nav>
                                        </div>
                                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#hro" aria-expanded="false" aria-controls="hro">
                                            HRO
                                            <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                        </a>
                                        <div class="collapse" id="hro" data-parent="#accordionSidenavLayout">
                                            <nav class="sidenav-menu-nested nav">
                                                <a class="nav-link" href="/upload-consumer-hro">Consumer HRO</a>
                                                <a class="nav-link" href="/upload-console-hro">Console HRO</a>
                                            </nav>
                                        </div>
                                        <a class="nav-link" href="/upload-telstra-plus">Telstra Plus Redemption</a>
                                    </nav>
                                </div>

                                <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#pages" aria-expanded="false" aria-controls="pages"
                                    ><div class="nav-link-icon"><i data-feather="layout"></i></div>
                                    Pages
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="pages" data-parent="#accordionSidenav">
                                    <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavLayout">
                                        <a class="nav-link" href="/admin/pages/mobile-plan">Mobile Plan</a>
                                        <a class="nav-link" href="/admin/pages/business-mobile-plan">Business Mobile Plan Page</a>
                                        <a class="nav-link" href="/pages/mobile-broadband">Mobile Broadband</a>
                                        <a class="nav-link" href="/pages/business-mobile-broadband">Business Mobile Broadband</a>
                                    </nav>
                                </div>

                                <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#categories" aria-expanded="false" aria-controls="categories"
                                    ><div class="nav-link-icon"><i data-feather="layout"></i></div>
                                    Categories
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="categories" data-parent="#accordionSidenav">
                                    <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavLayout">
                                        <a class="nav-link" href="/category/mobile-devices">Consumer Mobile Categories</a>
                                        <a class="nav-link" href="/category/business/mobile-devices">Business Mobile Categories</a>
                                        <a class="nav-link" href="/category/mobile-broadband">Mobile Broadband Categories</a>
                                    </nav>
                                </div>

                                 <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#outright" aria-expanded="false" aria-controls="outright"
                                    ><div class="nav-link-icon"><i data-feather="layout"></i></div>
                                    Pricing
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="outright" data-parent="#accordionSidenav">
                                    <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavLayout">
                                        <a class="nav-link" href="/outright-mobile">Mobile</a>
                                        <a class="nav-link" href="/outright-mobile-broadband">Mobile Broadband</a>
                                        <a class="nav-link" href="/outright-accessories">Accssories</a>
                                    </nav>
                                </div>
                                <!-- <a class="nav-link" href="/discounts">
                                    <div class="nav-link-icon"><i data-feather="bar-chart"></i></div>
                                    Handset Discounts
                                </a> -->
                                <a class="nav-link" href="/admin-promotions">
                                    <div class="nav-link-icon"><i data-feather="bar-chart"></i></div>
                                    Promotions
                                </a>
                                <a class="nav-link" href="/redemption-products">
                                    <div class="nav-link-icon"><i data-feather="bar-chart"></i></div>
                                    Telstra Plus
                                </a>

                                <a class="nav-link" href="/quicklinks">
                                    <div class="nav-link-icon"><i data-feather="file-text"></i></div>
                                    Quicklinks
                                </a>

                                <a class="nav-link" href="/reports">
                                    <div class="nav-link-icon"><i data-feather="file-text"></i></div>
                                    Reports
                                </a>
                            </div>
                        </div>
                        <div class="sidenav-footer">
                            <div class="sidenav-footer-content">
                                <div class="sidenav-footer-subtitle">Logged in as:</div>
                                <div class="sidenav-footer-title">Admin</div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div id="layoutSidenav_content" style="padding-top:2rem;">
                    <main>
                        @yield('content')
                    </main>
                    <footer class="footer mt-auto footer-light">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 small">Copyright &copy; oKart {{date('Y')}}</div>
                                <div class="col-md-6 text-md-right small">
                                    <a href="#!">Privacy Policy</a>
                                    &middot;
                                    <a href="#!">Terms &amp; Conditions</a>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </span>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>

        <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="/js/script.js" defer></script>
        <!-- <script type="text/javascript"> 
            setTimeout(function() {
                var quill = new Quill('#editor', {
                    theme: 'snow'
                });
            }, 0);
            console.log($('#editor').text());
        </script> -->
        <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script> -->

    </body>
</html>
