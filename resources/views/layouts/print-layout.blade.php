<!DOCTYPE html>
<html>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
  <title></title>

    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<style type="text/css" media="all">
    @page {
                margin: 0cm 0cm;
            }
  body{
    font-family: "Nunito", sans-serif;
    font-size:12px !important;
    margin-top: .3cm;
    margin-left: 1cm;
    margin-right: 1cm;
    margin-bottom: 0cm;
  }
  .grayBG{
    /*background-color:#B8B8B8;*/
    color:orange;
    font-weight: bold;
    text-align: center;
    width: 20%;
    font-size:18px;
  }

  h2{
    font-size:28px !important;
    color:#2D87C8;
  }

  h5{
    font-size:16px !important;
    font-weight: normal;
    margin:0px;
    padding:0px;
  }



footer {
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: .8cm;

    /** Extra personal styles **/
    color: white;
    text-align: left;
    line-height: 1.5cm;
}

#burb{
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: 2cm;

    /** Extra personal styles **/
    color: #000;
    text-align: center;
    line-height: .5cm;
    margin-left: 1cm;
    margin-right: 1cm;
}

.borderItems{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    text-align: right;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
}

.borderItemsPrice{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

table th, td {
  padding: .5em 0px;
  border-bottom: 2px solid white; 
}

/*tr:last-child  td:last-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}

tr:last-child  td:first-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}*/

.borders{
    border:2px solid #2D87C8 !important;
    padding:10px;background: #2D87C8;
    border-radius: 10px !important;
    color:#fff;
    /*font-size:16px !important;*/
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

.borderGreen{
    padding:10px;background: #30C42C;
    color:#fff !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

/*table tr{
    border-radius: 10px !important
}*/

table {
  border-collapse: collapse;
  /*border-radius: 1em;*/
  overflow: hidden;
  /*border-radius: 10px !important*/
}

/*input[type=text]{
        border-top-style: hidden;
        border-right-style: hidden;
        border-left-style: hidden;
        border-bottom-style: hidden;
      }
      
      .no-outline:focus {
        outline: none;
      }*/
table { page-break-inside:auto }
tr.page-break  { page-break-before:always!important; }
   /*tr    { page-break-inside:avoid; page-break-after:auto }*/
tr    { page-break-inside:avoid; page-break-after:auto }
   .page-break {
    page-break-after: always;
}

ul{
    list-style:none;
    padding:0;
    margin:0;
}

.noSpacing{
    margin:0px;
    padding:0px;
}

.endDate{
    font-size:17px !important;
}

.canvas{
        border:2px solid gray !important;
        border-radius: 10px !important;
    }

    .endDate{
        height: 35px !important;
        width: 80% !important;
    }
</style>
<body>
    <!-- <p id="burb"><i>Your proposal is a current estimate as of {{date('d-M-Y')}} and is subject to change. Other fees and charges may apply, these include but are not limited to usage outside of your plan inclusions, installation and other once off charges. All prices include GST.</i></p> -->
    <!-- <div style="width:300px;position: absolute;top:0px;left:0px;">
        <h2 style="padding:10px;margin:0px;padding-left:0px;clear:both;">Your Proposal</h2>
        <h5 style="margin:0px;padding:0px;padding-left:0px;padding-bottom:0cm;">Valid at {{ (isset($_SESSION['branchName'])) ? $_SESSION['branchName'] : 'TLS '.str_replace('TLS ', '', $_SESSION['currentBranch']) }}</h5>
    </div> -->

    <div class="reps" style="width:250px;position: absolute;top:0px;right:0px;">
        <p style="padding-bottom:5px;margin-bottom:5px">Sales Representative: {{$_SESSION['userInfo']['clickposName']}}</p>
        <div class="section-1" style="float: left;">
            <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">Date: {{date('d/m/Y')}}</p>
            <!-- <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">{{$_SESSION['userInfo']['name']}}</p> -->
        </div>
        <div class="section-2">
            
            <!-- <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">{{date('d-M-Y')}}</p> -->
        </div>
    </div>
    <div id="app" style="padding:20px;clear:both;margin-top:1cm">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>