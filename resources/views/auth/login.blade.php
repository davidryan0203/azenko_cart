@extends('layouts.app')

@section('content')
<div class="container" style="padding-top:10rem">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="border-radius: 10px;background-color:#f6f6f6">
               

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group" style="display: none">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="ryan.david@azenko.com.au" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label style="font-weight: bolder;color:#028cde;font-size:18px" for="password" class="col-md-3 col-form-label text-md-right">{{ __('Access Code:') }}</label>

                            <div class="col-md-9">
                                <input id="password" style="font-style: italic;border-radius: 10px;color:#bababa" type="password" class="text-muted form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Input credentials here." required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-11 offset-md-3">
                                <div class="col-7">
                                    <button type="submit" class="btn btn-primary" style="width: 100%">
                                        {{ __('Log in') }}
                                    </button>
                                </div>
                          
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
