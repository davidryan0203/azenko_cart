@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <span class="pull-left col-lg-6">Edit Mobile Plan Page</span>
                            
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form method="POST" action="/pages/mobile-plan/edit">
                        {{ csrf_field() }}

                        <table id="planTable" class="table text-center" style="border:none !important;background: #fff">
                        <tbody>
                            <tr>
                                <td style="border-top-left-radius: 10px;" class="tabPlan text-white"><h1>XS</h1></td>
                                <td class="tabPlan text-white"><h1>S</h1></td>
                                <td class="tabPlan text-white"><h1>M</h1></td>
                                <td class="tabPlan text-white"><h1>L</h1></td>
                                <td class="tabPlan text-white"><h1>XL</h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-1 col-form-label text-md-right">$</label>

                                        <div class="col-md-6">
                                            <input id="contact_number" type="number" class="form-control" name="extraSmallPricing" value="{{$page['extraSmallPricing']}}" autofocus>
                                        </div>
                                        <label for="description" class="col-md-4 no-padding col-form-label text-md-right">per month</label>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-5 col-form-label text-md-right">Capacity</label>

                                        <div class="col-md-7">
                                            <input id="extraSmallCapacity" type="text" class="form-control" name="extraSmallCapacity" value="{{$page['extraSmallCapacity']}}" autofocus>
                                        </div>
                                    </div>
                                    <h6>with Peace of Mind Data</h6>
                                </td>
                                <td>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-1 col-form-label text-md-right">$</label>

                                        <div class="col-md-6">
                                            <input id="smallPricing" type="number" class="form-control" name="smallPricing" value="{{$page['smallPricing']}}" autofocus>
                                        </div>
                                        <label for="description" class="col-md-4 no-padding col-form-label text-md-right">per month</label>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-5 col-form-label text-md-right">Capacity</label>

                                        <div class="col-md-7">
                                            <input id="smallCapacity" type="text" class="form-control" name="smallCapacity" value="{{$page['smallCapacity']}}" autofocus>
                                        </div>
                                    </div>
                                    <h6>with Peace of Mind Data</h6>
                                </td>
                                <td>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-1 col-form-label text-md-right">$</label>

                                        <div class="col-md-6">
                                            <input id="mediumPricing" type="number" class="form-control" name="mediumPricing" value="{{$page['mediumPricing']}}" autofocus>
                                        </div>
                                        <label for="description" class="col-md-4 no-padding col-form-label text-md-right">per month</label>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-5 col-form-label text-md-right">Capacity</label>

                                        <div class="col-md-7">
                                            <input id="mediumCapacity" type="text" class="form-control" name="mediumCapacity" value="{{$page['mediumCapacity']}}" autofocus>
                                        </div>
                                    </div>
                                    <h6>with Peace of Mind Data</h6>
                                </td>
                                <td>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-1 col-form-label text-md-right">$</label>

                                        <div class="col-md-6">
                                            <input id="largePricing" type="number" class="form-control" name="largePricing" value="{{$page['largePricing']}}" autofocus>
                                        </div>
                                        <label for="description" class="col-md-4 no-padding col-form-label text-md-right">per month</label>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-5 col-form-label text-md-right">Capacity</label>

                                        <div class="col-md-7">
                                            <input id="largeCapacity" type="text" class="form-control" name="largeCapacity" value="{{$page['largeCapacity']}}" autofocus>
                                        </div>
                                    </div>
                                    <h6>with Peace of Mind Data</h6>    
                                </td>
                                <td>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-1 col-form-label text-md-right">$</label>

                                        <div class="col-md-6">
                                            <input id="extraLargePricing" type="number" class="form-control" name="extraLargePricing" value="{{$page['extraLargePricing']}}" autofocus>
                                        </div>
                                        <label for="description" class="col-md-4 no-padding col-form-label text-md-right">per month</label>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-md-5 col-form-label text-md-right">Capacity</label>

                                        <div class="col-md-7">
                                            <input id="extraLargeCapacity" type="text" class="form-control" name="extraLargeCapacity" value="{{$page['extraLargeCapacity']}}" autofocus>
                                        </div>
                                    </div>
                                    <h6>with Peace of Mind Data</h6>    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group row col-lg-6 pull-right">
                                <div class="col-md-6 offset-4">
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        
                    </form>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
