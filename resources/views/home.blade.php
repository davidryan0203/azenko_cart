@extends('layouts.app')

@section('content')
<!-- <div class="container-fluid no-padding">
  <div class="row">
    <div class="col-md-12">
      <img src="img/Banner_1.png" alt="placeholder 960" class="img-fluid" />
    </div>
  </div>
</div> -->

<slider></slider>
<div class="container">
    <div class="row justify-content-center">
        <!-- <img src="img/Banner_1.png" class="img-fluid"> -->
        <div class="col-md-10" style="padding-top:1rem">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="background: 000">
                    <a style="border-top-left-radius: 5px;border:1px solid #e2e2e2" class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Personal</a>
                    <a style="border-top-right-radius: 5px;border:1px solid #e2e2e2" class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Business</a>
                </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0 text-left" id="nav-tabContent" style="background: 000;border:none;">
                    <div class="tab-pane fade show active plan-tab whiteBg" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" style="">
                        <div class="row planContainer">
                            <a href="/mobile-plans" class="col-4 plans">
                                <span class="col-md-3"><img src="/img/welcome_page_icons/Mobile_Icon.svg" height="40"></span> <span class="col-md-9">Mobile</span>
                            </a>
                            <a href="/mobile-broadband"  class="col-4 plans">
                                <span class="col-md-3"><img src="/img/MobileBroadband.png" height="50"></span>
                                <span class="col-md-9" style="padding-left:5px;">Mobile Broadband</span>
                            </a>
                            <a class="col-4 plans" href="/fixed-plan">
                                <span class="col-md-3"><img src="/img/Fixed.png" height="50"></span>
                                <span class="col-md-9">Fixed</span>
                            </a>
                        </div>

                        <div class="row planContainer">
                            <a  class="col-4 plans" style="border-bottom-left-radius:5px" href="/hro">
                                <span class="col-md-3"><img src="/img/welcome_page_icons/HRO_Icon.svg" height="40"></span>
                                <span class="col-md-9" style="padding-left:0px;">Hardware Repayment Option</span>
                            </a>
                            <a class="col-4 plans" href="/entertainment">
                                <span class="col-md-3"><img src="/img/welcome_page_icons/Entertainmetn_Icon.svg" height="40"></span>
                                <span class="col-md-9">Entertainment</span>
                            </a>
                            <a href="/telstra-redemptions-products" class="col-4 plans" style="border-bottom-right-radius:5px">
                                <span class="col-md-3" style="padding-right: 0px;"><img src="/img/TelstraPlus_Icon.svg" height="40"></span>
                                <span class="col-md-9" style="padding-left:32px">Telstra Plus</span>
                            </a>
                        </div>

                    </div>
                    <div class="tab-pane fade plan-tab whiteBg" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row planContainer">
                            <a href="/business-mobile-plan" class="col-4 plans">
                                <span class="col-md-3"><img src="/img/welcome_page_icons/Mobile_Icon.svg" height="40"></span> <span class="col-md-9">Mobile </span>
                            </a>
                            <a href="/business-mobile-broadband"  class="col-4 plans">
                                <span class="col-md-2"><img src="/img/MobileBroadband.png" height="50"></span>
                                <span class="col-md-9" style="padding-left:0px;">Mobile Broadband</span>
                            </a>
                            <a class="col-4 plans" href="/business-fixed-plan">
                                <span class="col-md-3"><img src="/img/Fixed.png" height="50"></span>
                                <span class="col-md-9">Fixed</span>
                            </a>
                        </div>

                        <div class="row planContainer">
                            <a  class="col-4 plans" style="border-bottom-left-radius:5px" href="/business-hro">
                                <span class="col-md-3"><img src="/img/welcome_page_icons/HRO_Icon.svg" height="40"></span>
                                <span class="col-md-9" style="padding-left:0px;">Hardware Repayment Option</span>
                            </a>
                            <a  class="col-4 plans" style="border-bottom-left-radius:5px" href="/telstra-platinum">
                               <span class="col-md-3" style="padding-left:6px;"><img src="/img/welcome_page_icons/Telstra_Business_Platinum_Icon.svg" height="40"></span>
                                <span class="col-md-9">Telstra Platinum</span>
                            </a>
                            <div class="col-4 plans">
                               
                            </div>
                        </div>
                    </div>
                
                </div>
        </div>
    </div>
</div>
@if($cart)
    <div class="modal fade" id="cartTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">&nbsp;</h5>
            <a href="/delete-cart-template" type="button" class="close">
              <span aria-hidden="true">&times;</span>
            </a>
          </div>
            <form action="use-cart-template" method="POST">
              <div class="modal-body">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <h4 class="text-center">A Cart template has been sent to you by:</h4>
                        <br/>
                        <label for="exampleFormControlSelect1">Sender</label>
                        <select name="data" class="form-control" id="branchVal">
                            @foreach($cart as $data)
                            <option value='<?php echo $data["id"] ?>'>{{$data['sender']}}</option>
                            @endforeach
                        </select>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="">use</button>
              </div>
            </form>
        </div>
      </div>
    </div>
@endif

@endsection
