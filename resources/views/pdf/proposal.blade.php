<style type="text/css" media="all">
    @page {
                margin: 0cm 0cm;
            }
  body{
    font-family: "Nunito", sans-serif;
    font-size:12px !important;
    margin-top: .3cm;
    margin-left: 1cm;
    margin-right: 1cm;
    margin-bottom: 1cm;
  }
  .grayBG{
    /*background-color:#B8B8B8;*/
    color:orange;
    font-weight: bold;
    text-align: center;
    width: 20%;
    font-size:18px;
  }

  h2{
    font-size:28px !important;
    color:#2D87C8;
  }

  h5{
    font-size:16px !important;
    font-weight: normal;
    margin:0px;
    padding:0px;
  }

  header {
    position: fixed;
    top: -.5cm;
    left: 0cm;
    right: 0cm;
    height: 4.8cm;

    /** Extra personal styles **/
    color: white;
    text-align: center;
    line-height: 1.5cm;
    /*background-image:url('/img/Telstra-Accessory-Purchase_Banner-01.png'); 
    background-size: 200px 100px; */
}

footer {
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: .8cm;

    /** Extra personal styles **/
    color: white;
    text-align: left;
    line-height: 1.5cm;
}

#burb{
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: 2cm;

    /** Extra personal styles **/
    color: #000;
    text-align: center;
    line-height: .5cm;
    margin-left: 1cm;
    margin-right: 1cm;
}

.borderItems{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    text-align: right;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
}

.borderItemsPrice{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

table th, td {
  padding: .5em 0px;
  border-bottom: 2px solid white; 
}

/*tr:last-child  td:last-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}

tr:last-child  td:first-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}*/

.borders{
    border:2px solid #2D87C8 !important;
    padding:10px;background: #2D87C8;
    border-radius: 10px !important;
    color:#fff;
    /*font-size:16px !important;*/
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

.borderGreen{
    padding:10px;background: #30C42C;
    color:#fff !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

/*table tr{
    border-radius: 10px !important
}*/

table {
  border-collapse: collapse;
  /*border-radius: 1em;*/
  overflow: hidden;
  /*border-radius: 10px !important*/
}

/*input[type=text]{
        border-top-style: hidden;
        border-right-style: hidden;
        border-left-style: hidden;
        border-bottom-style: hidden;
      }
      
      .no-outline:focus {
        outline: none;
      }*/
table { page-break-inside:auto }
tr.page-break  { page-break-before:always!important; }
   /*tr    { page-break-inside:avoid; page-break-after:auto }*/
tr    { page-break-inside:avoid; page-break-after:auto }
   .page-break {
    page-break-after: always;
}

ul{
    list-style:none;
    padding:0;
    margin:0;
}

.noSpacing{
    margin:0px;
    padding:0px;
}
</style>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
    <p id="burb"><i>Your proposal is a current estimate as of {{date('d-M-Y')}} and is subject to change. Other fees and charges may apply, these include but are not limited to usage outside of your plan inclusions, installation and other once off charges. All prices include GST.</i></p>
    <main>
        <div style="width:300px;position: absolute;top:0px;left:0px;">
            <h2 style="padding:10px;margin:0px;padding-left:0px;clear:both;">Your Proposal</h2>
            <h5 style="margin:0px;padding:0px;padding-left:0px;padding-bottom:0cm;">Valid at {{ (isset($_SESSION['branchName'])) ? $_SESSION['branchName'] : 'TLS '.str_replace('TLS ', '', $_SESSION['currentBranch']) }}</h5>
        </div>

        <div class="reps" style="width:250px;position: absolute;top:0px;right:0px;">
            <p style="padding-bottom:5px;margin-bottom:5px">Sales Representative: {{$_SESSION['userInfo']['clickposName']}}</p>
            <div class="section-1" style="float: left;">
                <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">Date: {{date('d/m/Y')}}</p>
                <!-- <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">{{$_SESSION['userInfo']['name']}}</p> -->
            </div>
            <div class="section-2">
                
                <!-- <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">{{date('d-M-Y')}}</p> -->
            </div>
        </div>

        <?php $broadbandCredit = 0;?>
        <?php $broadbandCharge = 0;?>
        <?php $mobileAROAmount = 0;?>
        <?php $broadbandAROAmount = 0;?>
        <?php $planPrice = 0; ?>
        <?php $planPriceGrandTotal = 0;?>
        <?php $entertainmentPlanPrice = 0;?>
        <?php $platinumPlanPrice = 0;?>
        <?php $hroPrice = 0;?>
        <?php $fixedTotalPrice = 0;?>
        <?php $fixedGrandTotalPrice = 0;?>
        <?php $fixedTotalDiscountPrice = 0;?>
        <?php $miscTotalPrice = 0;?>
        <?php $vasTotalAmount = 0;?>
        <?php $totalMonthlyCharge = 0;?>
        <?php $totalMobileAndBroadband = 0;?>
        <?php $totalPriceOutright = 0;?>
        <?php $existingServicesPrice = 0;?>
        <?php $aroAmount = 0;?>
        <?php $broadbandAROamount = 0;?>
        <?php $totalMiscPrice = 0;?>
        @if($data['quadrantCart'])
            <div style="padding:20px;clear:both;margin-top:1cm">
                <table class="table table-bordered" style="width:100% !important;border:none !important;background: #fff">
                    <tbody>
                        @foreach($data['quadrantCart'] as $key => $item)
                            @if($item['itemType'] == 'mobilePlan')
                                <?php $aroAmount += $item['aroAmount'];?>
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">
                                        <img height="25" src="img/welcome_page_icons/Mobile_Icon.png" style="position: absolute;top: 0px;z-index: -99999;">
                                    </td>
                                    <td class="noSpacing" style="width:80%;">
                                            <p style="margin-bottom:2px;padding-bottom:2px;">
                                            <p style="padding:0px;margin:0px;"><strong>
                                                {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} {{($item['mobileOption']['name'] == 'Console') ? 'Telstra Upfront' : ''}} Mobile Plan @if($item['planType'] == 'XS')
                                                    {{ 'Extra Small' }}
                                                @elseif($item['planType'] == 'XL')
                                                    {{ 'Extra Large' }}
                                                @else
                                                    {{ $item['planType'] }} 
                                                @endif
                                                 @if($item['planType'] == 'Small')
                                                    40GB
                                                @elseif($item['planType'] == 'XS')
                                                    2GB
                                                @elseif($item['planType'] == 'Medium')
                                                    80GB
                                                @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                    120GB
                                                @else
                                                    180GB
                                                @endif
                                            </strong></p>
                                            </p>
                                    </td>

                                    <td class="noSpacing grayBG" style="width:10%;">
                                       ${{ $data['duration'] === "fortnightly" ? number_format(($item['planValue'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['planValue'] * 12 / 52), 2, '.', '') : number_format($item['planValue'], 2, '.', '')) }}
                                    </td>
                                </tr>

                                @if($item['make']) 
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                @if($item['make']) 
                                                    for {{$item['months']}} Months | <strong>${{$item['deviceItem']['rrp_incl_gst']}}</strong>
                                                @endif
                                                @if(!$item['make']) 
                                                    BYO
                                                @endif
                                            </strong>
                                        </td>
                                        <td class="noSpacing grayBG">${{ $data['duration'] === "fortnightly" ? number_format(($item['price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['price'] * 12 / 52), 2, '.', '') : number_format($item['price'], 2, '.', '')) }}</td>
                                    </tr>
                                @endif
                                @if($item['aroAmount'] != "0.00")
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            Accessory Repayment Option for {{$item['aroMonths']}} Months (${{$item['aroBase']}})
                                        </td>
                                        <td class="noSpacing grayBG">
                                            ${{ $data['duration'] === "fortnightly" ? number_format(($item['aroAmount'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['aroAmount'] * 12 / 52), 2, '.', '') : number_format($item['aroAmount'], 2, '.', '')) }}
                                        </td>
                                    </tr>
                                    <?php $mobileAROAmount += $item['aroAmount']; ?>
                                @endif

                                @if(count($item['aroAccessories']) != 0 && $item['aroAccessories'][0]['ProductType'] != '')
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <ul style="padding-left:10px;padding-top:0px;padding-bottom: 0px;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li>{{$aro['ProductType']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <ul style="text-decoration: none !important;text-align: center !important;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li style="text-decoration: none !important;text-align: center !important;">${{$aro['aroPrice']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            {{$vas['name']}}
                                            @if($vas['name'] == 'International Calling Pack')
                                                <br/><p style="font-size:8px !important;padding-top:0px;margin-top:0px;">
                                                    Unlimited voice calls and SMS from Australia to standard numbers in 23 international countries: Canada, China, Denmark, Germany, Guam, Hong Kong, India, Ireland, Indonesia, Japan, Malaysia, New Zealand, Norway, Puerto Rico, Romania, Singapore, South Korea, Vietnam, USA and UK.
                                                </span>
                                            @endif
                                        </td>
                                        <td class="noSpacing grayBG">
                                            ${{ $data['duration'] === "fortnightly" ? number_format(($vas['amount'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($vas['amount'] * 12 / 52), 2, '.', '') : number_format($vas['amount'], 2, '.', '')) }}
                                        </td>
                                    </tr>
                                @endforeach

                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td class="noSpacing">
                                                    <i>{{$discount['name']}}</i>
                                                    @if($discount['name'] == 'International Call & SMS Pack to LOTE customers')
                                                        <br/><p style="font-size:8px !important;padding-top:0px;margin-top:0px;">
                                                            Unlimited voice calls and SMS from Australia to standard numbers in 23 international countries: Canada, China, Denmark, Germany, Guam, Hong Kong, India, Ireland, Indonesia, Japan, Malaysia, New Zealand, Norway, Puerto Rico, Romania, Singapore, South Korea, Vietnam, USA and UK.
                                                        </p>
                                                    @endif
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount')
                                                        -${{($discount['discount_price']) ? ($data['duration'] === "fortnightly" ? number_format(($discount['discount_price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($discount['discount_price'] * 12 / 52), 2, '.', '') : number_format($discount['discount_price'], 2, '.', ''))) : 0.00}}
                                                    @else
                                                        -${{($discount['discount_price']) ? ($data['duration'] === "fortnightly" ? number_format(($discount['discount_price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($discount['discount_price'] * 12 / 52), 2, '.', '') : number_format($discount['discount_price'], 2, '.', ''))) : 0.00}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif

                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <span style="font-size:8px !important;">
                                        Includes @if($item['planType'] == 'Small')
                                                40GB
                                            @elseif($item['planType'] == 'XS')
                                                2GB
                                            @elseif($item['planType'] == 'Medium')
                                                80GB
                                            @elseif($item['planType'] == 'Large')
                                                120GB
                                            @else
                                                180GB
                                            @endif 
                                            @if(isset($item['mobileOption']) && $item['mobileOption']['name'] == 'Console')
                                                of data with unlimited standard national calls & texts with 4G/5G network access. No lock-in plan. Plan includes data sharing plus 30 minutes of International Calling to all destinations and unlimited texts to all destination.
                                            @else
                                                of data with unlimited standard national calls & texts with 4G/5G network access. Data-free Apple Music, live sport, and free Telstra Air. No excess data charges in Australia, no lock-in plan, international pay as you go or purchase as an international Call & SMS Pack.
                                            @endif
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>

                                <tr class="borders">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td>
                                        ${{--$item['totalPrice']--}}
                                        <?php $mobileTotalPrice = 0;?>
                                        <?php $mobileTotalPrice = number_format( $data['duration'] === "fortnightly" ? ($item['totalPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['totalPrice'] * 12 / 52) : $item['totalPrice']) ,2); ?>
                                        {{ $mobileTotalPrice }}
                                    </td>
                                </tr>

                                @if($item['planType'] != 'XS' && $item['promotionsPrice'] != 0)
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.5cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            <i>Minimum cost after 12 months:</i>
                                        </td>
                                        <td style="text-align:center;margin-bottom:.5cm !important;">
                                            <?php $mobileCredit = 0; $mobileCharge = 0; ?>
                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    {{--@if($discount['month_duration'] == 24)--}}
                                                        @if($discount['is_credit'] == 0)
                                                            <?php $mobileCharge += $discount['discount_price'] ?>
                                                       
                                                        @endif
                                                    {{--@endif--}}
                                                @endforeach
                                                <?php $promotionsPrice = $data['duration'] === "fortnightly" ? ($item['promotionsPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['promotionsPrice'] * 12 / 52) : $item['promotionsPrice'])?>
                                            @endif
                                            @if($item['months'] == 24)
                                                <i>${{ number_format(($mobileTotalPrice + $promotionsPrice + $mobileCredit + $mobileCharge),2) }}</i>
                                            @endif

                                            @if($item['months'] == 12)


                                                <i>${{ number_format(($item['planValue'] + $mobileCredit + $mobileCharge),2) }}</i>
                                            @endif
                                            <!-- <i>${{ number_format(($item['totalPrice'] - $mobileCredit + $mobileCharge + $aroAmount ),2) }}</i> -->
                                            <!-- <i>${{ number_format(($item['price'] + $item['vasTotalAmount'] + $item['planValue']),2) }}</i> -->
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.2cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            &nbsp;
                                        </td>
                                        <td style="text-align:center;margin-bottom:.2cm !important;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                @endif
                            @endif

                            @if($item['itemType'] == 'broadBandPlan')
                                <?php $broadbandAROamount += $item['aroAmount'];?>
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">
                                        <img height="25" src="img/welcome_page_icons/MobileBroadband_Icon.png" style="position: absolute;top: 0px;z-index: -99999;">
                                    </td>
                                    <td class="noSpacing" style="width:80%;">
                                            <p style="margin-bottom:2px;padding-bottom:2px;">
                                            <p style="padding:0px;margin:0px;"><strong>
                                                {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} {{($item['mobileOption']['name'] == 'Console') ? 'Telstra Upfront' : ''}} Data Plan @if($item['planType'] == 'XS')
                                                    {{ 'Extra Small' }}
                                                @elseif($item['planType'] == 'XL')
                                                    {{ 'Extra Large' }}
                                                @else
                                                    {{ $item['planType'] }} 
                                                @endif
                                                 @if($item['planType'] == 'Small')
                                                    {{($item['mobileOption']['name'] == 'Console' || $item['mobileOption']['name'] == 'Legacy') ? '30GB' : '20GB'}}
                                                @elseif($item['planType'] == 'XS')
                                                    5GB
                                                @elseif($item['planType'] == 'Medium')
                                                    {{($item['mobileOption']['name'] == 'Console' || $item['mobileOption']['name'] == 'Legacy') ? '75GB' : '60GB'}}
                                                @elseif($item['planType'] == 'Large')
                                                    {{($item['mobileOption']['name'] == 'Console' || $item['mobileOption']['name'] == 'Legacy') ? '400GB' : '200GB'}}
                                                @else
                                                    180GB
                                                @endif
                                            </strong></p>
                                            </p>
                                    </td>

                                    <td class="noSpacing grayBG" style="width:10%;">
                                       ${{ $data['duration'] === "fortnightly" ? number_format(($item['planValue'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['planValue'] * 12 / 52), 2, '.', '') : number_format($item['planValue'], 2, '.', '')) }}
                                    </td>
                                </tr>
                                @if($item['make']) 
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                @if($item['make']) 
                                                    for {{$item['months']}} Months | <strong>${{$item['deviceItem']['rrp_incl_gst']}}</strong>
                                                @endif
                                                @if(!$item['make']) 
                                                    BYO
                                                @endif
                                            </strong>
                                        </td>
                                        <td class="noSpacing grayBG">${{ $data['duration'] === "fortnightly" ? number_format(($item['price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['price'] * 12 / 52), 2, '.', '') : number_format($item['price'], 2, '.', '')) }}</td>
                                    </tr>
                                @endif
                                @if($item['aroAmount'] != "0.00")
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            Accessory Repayment Option for {{$item['aroMonths']}} Months (${{$item['aroBase']}})
                                        </td>
                                        <td class="noSpacing grayBG">
                                            ${{ $data['duration'] === "fortnightly" ? number_format(($item['aroAmount'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['aroAmount'] * 12 / 52), 2, '.', '') : number_format($item['aroAmount'], 2, '.', '')) }}
                                        </td>
                                    </tr>
                                    <?php $broadbandAROAmount += $item['aroAmount']; ?>
                                @endif
                                @if(count($item['aroAccessories']) != 0 && $item['aroAccessories'][0]['ProductType'] != '')
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <ul style="padding-left:10px;padding-top:0px;padding-bottom: 0px;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li>{{$aro['ProductType']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <ul style="text-decoration: none !important;text-align: center !important;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li style="text-decoration: none !important;text-align: center !important;">${{$aro['aroPrice']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            {{$vas['name']}}
                                            @if($vas['name'] == 'International Calling Pack')
                                                <br/><p style="font-size:8px !important;padding-top:0px;margin-top:0px;">
                                                    Unlimited voice calls and SMS from Australia to standard numbers in 23 international countries: Canada, China, Denmark, Germany, Guam, Hong Kong, India, Ireland, Indonesia, Japan, Malaysia, New Zealand, Norway, Puerto Rico, Romania, Singapore, South Korea, Vietnam, USA and UK.
                                                </span>
                                            @endif
                                        </td>
                                        <td class="noSpacing grayBG">
                                            ${{ $data['duration'] === "fortnightly" ? number_format(($vas['amount'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($vas['amount'] * 12 / 52), 2, '.', '') : number_format($vas['amount'], 2, '.', '')) }}
                                        </td>
                                    </tr>
                                @endforeach
                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td class="noSpacing">
                                                    <i>{{$discount['name']}}</i>
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount')
                                                        -${{($discount['discount_price']) ? ($data['duration'] === "fortnightly" ? number_format(($discount['discount_price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($discount['discount_price'] * 12 / 52), 2, '.', '') : number_format($discount['discount_price'], 2, '.', ''))) : 0.00}}
                                                    @else
                                                        -${{($discount['discount_price']) ? ($data['duration'] === "fortnightly" ? number_format(($discount['discount_price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($discount['discount_price'] * 12 / 52), 2, '.', '') : number_format($discount['discount_price'], 2, '.', ''))) : 0.00}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <span style="font-size:10px !important;">
                                        @if($item['planType'] == 'XS')
                                                    {{ 'Extra Small' }}
                                                @elseif($item['planType'] == 'XL')
                                                    {{ 'Extra Large' }}
                                                @else
                                                    {{ $item['planType'] }} 
                                                @endif Data Plan includes @if($item['planType'] == 'Small')
                                                    {{($item['mobileOption']['name'] != 'Console') ? '20GB' : '30GB'}}
                                                @elseif($item['planType'] == 'XS')
                                                    5GB
                                                @elseif($item['planType'] == 'Medium')
                                                    {{($item['mobileOption']['name'] != 'Console') ? '60GB' : '75GB'}}
                                                @elseif($item['planType'] == 'Large')
                                                    {{($item['mobileOption']['name'] != 'Console') ? '200GB' : '400GB'}}
                                                @else
                                                    180GB
                                                @endif 
                                                @if(isset($item['mobileOption']) && $item['mobileOption']['name'] == 'Console')
                                                    of data/month with 4G network access, and no lock-in plan.
                                                @else
                                                    of data/month with 4G network access, no excess data Australia, and no lock-in plan. Free Telstra Air Wi-Fi, and data-free sports and Apple Music streaming included.
                                                @endif
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class="borders">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="">
                                        ${{--$item['totalPrice']--}}
                                        <?php $broadbandTotalPrice = 0; $broadbandPromotionsPrice = 0;?>
                                        <?php $broadbandPromotionsPrice = $data['duration'] === "fortnightly" ? ($item['promotionsPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['promotionsPrice'] * 12 / 52) : $item['promotionsPrice'])?>

                                        <?php $broadbandTotalPrice = number_format( $data['duration'] === "fortnightly" ? ($item['totalPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['totalPrice'] * 12 / 52) : $item['totalPrice']) ,2); ?>
                                        {{ $broadbandTotalPrice }}
                                    </td>
                                </tr>

                                @if($item['promotionsPrice'] != 0)
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.5cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            <i>Minimum cost after 12 months:</i>
                                        </td>
                                        <td style="text-align:center;margin-bottom:.5cm !important;">
                                            <?php $broadbandCredit = 0; $broadbandCharge = 0; ?>
                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['month_duration'] == 24)
                                                        @if($discount['is_credit'] == 0)
                                                            <?php $broadbandCharge += $discount['discount_price']?>
                                                        @else
                                                            <?php $broadbandCredit += $discount['discount_price'] ?>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                            @if($item['months'] == 24)
                                                <i>${{ number_format(($broadbandTotalPrice + $broadbandPromotionsPrice - $broadbandCredit + $broadbandCharge),2) }}</i>
                                            @endif

                                            @if($item['months'] == 12)
                                                 <i>${{ number_format(($broadbandTotalPrice + $broadbandPromotionsPrice - $broadbandCredit + $broadbandCharge),2) }}</i>
                                            @endif
                                            <!-- <i>${{($item['totalPrice'] + $broadbandCharge + $broadbandCredit) }}</i> -->
                                            <!-- <i>${{ number_format(($item['price'] + $item['vasTotalAmount'] + $item['planValue']),2) }}</i> -->
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.2cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            &nbsp;
                                        </td>
                                        <td style="text-align:center;margin-bottom:.2cm !important;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                @endif
                            @endif      

                            @if($item['itemType'] == 'fixed')
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="20" src="img/welcome_page_icons/Fixed_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>{{($item['isBusiness'] == '1') ? 'Business' : ''}} Fixed</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <span><strong>{{ ucfirst($item['fixed']['name']) }} Plan</strong></span><br/>
                                        <span style="font-size:10px !important;">{{$item['planDescription']}}</span>
                                    </td>
                                    <td class="grayBG" style="text-align:center;">${{ $data['duration'] === "fortnightly" ? number_format(($item['fixedPrice'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['fixedPrice'] * 12 / 52), 2, '.', '') : number_format($item['fixedPrice'], 2, '.', '')) }}</td>
                                </tr>

                                @if($item['vas'])
                                    @foreach($item['vas'] as $key => $vas)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td class="noSpacing">
                                                {{$vas['name']}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                ${{number_format($vas['amount'],2)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td class="noSpacing">
                                                    {{$discount['name']}}
                                                </td>
                                                <td class="grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 1)
                                                        -${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                        <?php $fixedTotalDiscountPrice += $discount['discount_price'];?>
                                                    @endif

                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                        ${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="text-align:center">
                                        ${{ $data['duration'] === "fortnightly" ? number_format(($item['fixedPlanPrice'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($item['fixedPlanPrice'] * 12 / 52), 2, '.', '') : number_format($item['fixedPlanPrice'], 2, '.', '')) }}
                                    </td>
                                </tr>

                                @if($item['promotionsPrice'] != 0)
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.5cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            <i>Minimum cost after 12 months:</i>
                                        </td>
                                        <td style="text-align:center;margin-bottom:.5cm !important;">
                                            <i>${{--number_format($item['fixedPlanPrice'] + $item['promotionsPrice'], 2)--}}</i>
                                            <?php $fixedTotalPrice = 0;?>
                                            <?php $fixedTotalPrice = number_format( $data['duration'] === "fortnightly" ? ($$item['fixedPlanPrice'] + $item['promotionsPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($$item['fixedPlanPrice'] + $item['promotionsPrice'] * 12 / 52) : $item['fixedPlanPrice'] + $item['promotionsPrice']) ,2); ?>
                                            {{ $fixedTotalPrice }}
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.2cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            &nbsp;
                                        </td>
                                        <td style="text-align:center;margin-bottom:.2cm !important;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                @endif
                            @endif                      
                            
                        @endforeach                                    
                    </tbody>
                </table>
                @foreach($data['quadrantCart'] as $key => $item)
                    @if($item['itemType'] == 'mobilePlan' || $item['itemType'] == 'broadBandPlan' || $item['itemType'] == 'fixed')
                        <?php $totalMobileAndBroadband++; ?>
                    @endif
                @endforeach
                {{--@if($totalMobileAndBroadband > 3)--}}
                    <!-- <div class="page-break"></div> -->
                {{--@endif--}}

                <table class="table table-bordered" style="width:100% !important;border:none !important;background: #fff">
                    <tbody>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'entertainment')
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="20" src="img/welcome_page_icons/Entertainmetn_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Entertainment</strong>
                                    </td>
                                </tr>

                                @foreach($item['entertainment'] as $key => $entertainment)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <span><strong>{{ ucfirst($entertainment['name']) }}</strong></span><br/>
                                            <span style="font-size:10px !important;">{{$entertainment['planDescription']}}</span>
                                        </td>
                                        <td class="grayBG" style="text-align:center;">${{ $data['duration'] === "fortnightly" ? number_format(($entertainment['price'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($entertainment['price'] * 12 / 52), 2, '.', '') : number_format($entertainment['price'], 2, '.', '')) }}</td>
                                    
                                    </tr>
                                @endforeach
                                @if($item['vas'])
                                    @foreach($item['vas'] as $key => $vas)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td class="noSpacing">
                                                {{$vas['name']}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                ${{number_format($vas['amount'],2)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td class="noSpacing">
                                                    {{$discount['name']}}
                                                </td>
                                                <td class="grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 1)
                                                        -${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                    @endif

                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                        ${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif

                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="margin-bottom:text-align:center">
                                        ${{number_format( $data['duration'] === "fortnightly" ? ($item['totalEntertainmentPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['totalEntertainmentPrice'] * 12 / 52) : $item['totalEntertainmentPrice']) ,2)}}
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'platinum')
                                @if($item['selectedEntertainmentPlan']['type'] != 'once-off')
                                    <tr>
                                        <td colspan="3" style="padding:0px;margin:0px;">
                                            <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                            </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" style="width:5%;">
                                                <img height="20" src="img/welcome_page_icons/Entertainmetn_Icon.png" style="position: absolute;top:0px">
                                            </td>
                                        <td class="noSpacing" colspan="2">
                                            <strong>Telstra Platinum</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <span><strong>{{ ucfirst($item['platinumPlanType']) }}</strong></span><br/>
                                            <span style="font-size:10px !important;">{{$item['planDescription']}}</span>
                                        </td>
                                        <td class="grayBG" style="text-align:center;">${{$item['platinumPlanPrice']}}</td>
                                    </tr>

                                    {{--
                                    @if($item['vas'])
                                        @foreach($item['vas'] as $key => $vas)
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td class="noSpacing">
                                                    {{$vas['name']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    ${{number_format($vas['amount'],2)}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif--}}
                                    {{--
                                    @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                        @foreach($item['selectedPromotions'] as $discount)
                                            @if($discount['type']['name'] == 'Reoccuring Discount')
                                                <tr>
                                                    <td class="noSpacing">&nbsp;</td>
                                                    <td class="noSpacing">
                                                        {{$discount['name']}}
                                                    </td>
                                                    <td class="grayBG">
                                                        @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 1)
                                                            -${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                        @endif

                                                        @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                            ${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif--}}

                                    <tr class="borders">
                                        <td style="">&nbsp;</td>
                                        <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                        </td>
                                        <td style="margin-bottom:text-align:center">
                                            ${{$item['totalPlatinumPrice']}}
                                        </td>
                                    </tr>
                                @endif
                            @endif

                            @if($item['itemType'] == 'existingServices')
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Existing Services</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td style="margin-left:2% !important;" class="noSpacing">{{$item['existingServicesDescription']}}</td>
                                    <td class="grayBG" style="text-align:center;">${{number_format($item['existingServicesPrice'],2)}}</td>
                                </tr>
                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="text-align:center">
                                        ${{number_format($item['existingServicesPrice'],2)}}
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'otherServices')
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Other Services</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td style="margin-left:2% !important;" class="noSpacing">{{$item['otherServicesDescription']}}</td>
                                    <td class="grayBG" style="text-align:center;">${{number_format($item['otherServicesPrice'],2)}}</td>
                                </tr>
                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="text-align:center">
                                        ${{number_format($item['otherServicesPrice'],2)}}
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                                                    
                            @if($item['itemType'] == 'miscellaneous')
                                <tr>
                                    <td class="">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Miscellaneous</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'miscellaneous')
                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">
                                                    {{$discount['name']}}

                                                    
                                                </td>
                                                <td class="grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                        ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0}}
                                                        <?php $totalMiscPrice += $discount['discount_price']?>
                                                    @endif
                                                </td>
                                            </tr>

                                            @if(!empty($discount['description']))
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">
                                                    <span style="font-size:10px !important;">
                                                        {{$discount['description']}}
                                                    </span>                                                    
                                                </td>
                                                <td class="grayBG">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach


                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'miscellaneous')
                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="text-align:center">
                                        ${{number_format(($totalMiscPrice),2)}}
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'hro')
                                <tr>
                                    <td class="">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>{{($item['isBusiness'] == '1') ? 'Business' : ''}} Hardware Repayment Options</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach

                        <?php $totalHRO = 0; $totalHRODiscount = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'hro')
                                @foreach($item['hro'] as $hro)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">

                                            {{$hro['name']}} for {{$item['hroMonthly']}} Months | <strong>${{$hro['rrp']}}</strong>
                                        </td>
                                        @if($item['hroMonthly'] == 12)
                                            <td class="grayBG" style="text-align:center;">${{ $data['duration'] === "fortnightly" ? number_format(($hro['price_12_months'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($hro['price_12_months'] * 12 / 52), 2, '.', '') : number_format($hro['price_12_months'], 2, '.', '')) }}</td>
                                        @else
                                            <td class="grayBG" style="text-align:center;">${{ $data['duration'] === "fortnightly" ? number_format(($hro['price_24_months'] *12 / 26), 2, '.', '') : ($data['duration'] === "weekly" ? number_format(($hro['price_24_months'] * 12 / 52), 2, '.', '') : number_format($hro['price_24_months'], 2, '.', '')) }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                    @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                        @foreach($item['selectedPromotions'] as $discount)
                                            @if($discount['type']['name'] == 'Reoccuring Discount')
                                                <tr>
                                                    <td class="noSpacing">&nbsp;</td>
                                                    <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">
                                                        {{$discount['name']}}
                                                    </td>
                                                    <td class="grayBG">
                                                        @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 1)
                                                            -${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                        @endif

                                                        @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                            ${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    <?php $totalHRO += $item['hroMonthlyPrice']; $totalHRODiscount += $item['promotionsPrice'];?>
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'hro')
                                <tr class="borders">
                                    <td style="">&nbsp;</td>
                                    <td style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        {{ $data['duration'] === "fortnightly" ? 'Fortnightly' : ($data['duration'] === "weekly" ? 'Weekly' : 'Monthly') }} Cost:
                                    </td>
                                    <td style="text-align:center">
                                        ${{--number_format($totalHRO,2)--}}
                                        <?php $hroTotalPrice = 0;?>
                                        <?php $hroTotalPrice = number_format( $data['duration'] === "fortnightly" ? ($totalHRO / 2) : ($data['duration'] === "weekly" ? ($totalHRO * 12 / 52) : $totalHRO) ,2); ?>
                                        {{ $hroTotalPrice }}
                                    </td>
                                </tr>

                                @if($totalHRODiscount != 0)
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.5cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            <i>Minimum cost after 12 months:</i>
                                        </td>
                                        <td style="text-align:center;margin-bottom:.5cm !important;">
                                            <?php $hroCredit = 0; $hroCharge = 0; ?>
                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['is_credit'] == 0)
                                                        <?php $hroCharge += $discount['discount_price']?>
                                                    @else
                                                        <?php $hroCredit += $discount['discount_price'] ?>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <i>${{($hroTotalPrice + $hroCharge + $hroCredit) }}</i>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2" style="margin-bottom:.2cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                            &nbsp;
                                        </td>
                                        <td style="text-align:center;margin-bottom:.2cm !important;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                @endif
                                @break;
                            @endif
                        @endforeach

                        <!--minimum monthly commitment section-->
                        @if($data['grandTotal'] != 0.00)
                            <tr class="borderGreen">
                                <td colspan="2" style="margin-top:.2cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">Minimum {{ $data['duration'] === "fortnightly" ? 'Fortnightly Commitment:' : ($data['duration'] === "weekly" ? 'Weekly Commitment:' : 'Monthly Commitment:') }} </td>
                                <td class="grayBG" style="margin-top:.2cm !important;text-align:center;">${{ $data['duration'] === "fortnightly" ? number_format(($data['grandTotal'] *12 / 26),2) : ($data['duration'] === "weekly" ? number_format(($data['grandTotal'] * 12 / 52),2) : number_format($data['grandTotal'],2)) }}</td>
                            </tr>

                            <!-- <tr>
                                <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                    <i>Minimum cost after 12 months:</i>
                                </td>
                                <td style="text-align:center;">
                                    <?php $totalMonthlyCredit = 0; $totalMonthlyCharge = 0; ?>
                                    @foreach($data['quadrantCart'] as $key => $item)
                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'] && $item['itemType'] != 'miscellaneous')
                                            @foreach($item['selectedPromotions'] as $discount)
                                                @if($discount['is_credit'] == 0)
                                                    <?php $totalMonthlyCharge += $discount['discount_price']?>
                                                @endif
                                                @if($discount['is_credit'] == 1)
                                                    <?php $totalMonthlyCharge += $discount['discount_price']?>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    <i>${{number_format(($data['grandTotal'] + $totalMonthlyCredit + $totalMonthlyCharge),2)}}</i>
                                </td>
                            </tr> -->
                                
                            <tr>
                                <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                    <i>Minimum cost after 12 months:</i>
                                </td>
                                <td style="text-align:center;">
                                    <?php $totalMonthlyCredit = 0; $totalMonthlyCharge = 0; ?>
                                    @foreach($data['quadrantCart'] as $key => $item)
                                        @if($item['itemType'] == 'entertainment')
                                            @foreach($item['entertainment'] as $key => $entertainment)
                                                <?php $entertainmentPlanPrice += $data['duration'] === "fortnightly" ? ($entertainment['price'] *12 / 26) : ($data['duration'] === "weekly" ? ($entertainment['price'] * 12 / 52) : $entertainment['price']) ?>
                                            @endforeach
                                        @endif

                                        @if((isset($item['hroMonthlyPrice']) && $item['hroMonthly'] == 24))
                                           <!--  <?php $hroPrice += $item['hroMonthlyPrice'];?> -->
                                            @if($item['itemType'] == 'entertainment')
                                                @foreach($item['hro'] as $key => $hro)
                                                    <?php $hroPrice += $data['duration'] === "fortnightly" ? ($hro['hroMonthlyPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($hro['hroMonthlyPrice'] * 12 / 52) : $hro['hroMonthlyPrice']) ?>
                                                @endforeach
                                            @endif
                                        @endif

                                        @if((isset($item['fixedPlanPrice'])))
                                            <?php $fixedGrandTotalPrice += $data['duration'] === "fortnightly" ? ($item['fixedPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['fixedPrice'] * 12 / 52) : $item['fixedPrice']) ?>
                                        @endif

                                        @if((isset($item['chargeDiscount'])))
                                            <!--to be edited if 24 month on misc is required-->
                                            {{--$miscTotalPrice += $item['chargeDiscount'];?> --}}
                                            <?php $miscTotalPrice += 0;?>
                                        @endif                                        

                                        @if(isset($item['planValue']))
                                            @if($item['months'] == '12')
                                                <?php $planPriceGrandTotal += $data['duration'] === "fortnightly" ? (($item['planValue']  ) *12 / 26) : ($data['duration'] === "weekly" ? (($item['planValue']  ) * 12 / 52) : ($item['planValue'] )) ?>
                                            @else
                                                <?php $planPriceGrandTotal += $data['duration'] === "fortnightly" ? (($item['planValue'] + $item['price'] ) *12 / 26) : ($data['duration'] === "weekly" ? (($item['planValue'] + $item['price'] ) * 12 / 52) : ($item['planValue'] + $item['price'] )) ?>
                                            @endif
                                        @endif

                                        @if((isset($item['existingServicesPrice'])))
                                            <?php $existingServicesPrice += $data['duration'] === "fortnightly" ? ($item['existingServicesPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['existingServicesPrice'] * 12 / 52) : $item['existingServicesPrice']) ?>
                                        @endif

                                        @if((isset($item['otherServicesPrice'])))
                                            <?php $existingServicesPrice += $data['duration'] === "fortnightly" ? ($item['otherServicesPrice'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['otherServicesPrice'] * 12 / 52) : $item['otherServicesPrice']) ?>
                                        @endif

                                        @if(isset($item['vasTotalAmount']))
                                            <?php $vasTotalAmount += $data['duration'] === "fortnightly" ? ($item['vasTotalAmount'] *12 / 26) : ($data['duration'] === "weekly" ? ($item['vasTotalAmount'] * 12 / 52) : $item['vasTotalAmount']) ?>
                                        @endif

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'] && $item['itemType'] != 'miscellaneous')
                                            @foreach($item['selectedPromotions'] as $discount)
                                                @if($discount['is_credit'] == 0 && $discount['month_duration'] == 24)
                                                    <?php $totalMonthlyCharge += $discount['discount_price']?>
                                                @endif

                                                @if($discount['is_credit'] == 1 && $discount['month_duration'] == 24)
                                                    <?php $totalMonthlyCredit += $discount['discount_price']?>
                                                @endif
                                                <!--special request-->
                                                @if($discount['name'] == 'BTL Centrelink XS Mobile offer' && $item['planType'] == 'XS')
                                                    <?php $planPriceGrandTotal += $item['planValue'] + $item['price'] - $discount['discount_price']?>
                                                @endif
                                                <!--end special request-->
                                            @endforeach
                                        @endif
                                    @endforeach
                                    <i>${{number_format((($planPriceGrandTotal + $entertainmentPlanPrice + $hroPrice + $fixedGrandTotalPrice + $miscTotalPrice + $vasTotalAmount + $existingServicesPrice  + $totalMonthlyCharge)),2)}}</i>
                                </td>
                            </tr>
                        @endif
                        <!--minimum monthly commitment section-->
                           

                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'] || $item['itemType'] == 'platinum' && $item['selectedEntertainmentPlan']['type'] == 'once-off')
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')

                                        <tr>
                                            <td class="noSpacing" colspan="3" style="padding-top:.5cm">
                                                <strong>Once-Off Credits and Charges</strong>
                                            </td>
                                        </tr>
                                        
                                        @break
                                    @endif
                                @endforeach
                                @if($item['itemType'] == 'platinum' && $item['selectedEntertainmentPlan']['type'] == 'once-off')
                                    <tr>
                                        <td class="noSpacing" colspan="3" style="padding-top:.5cm">
                                            <strong>Once-Off Credits and Charges</strong>
                                        </td>
                                    </tr>
                                @endif
                                @break
                            @endif
                        @endforeach

                        <?php $totalCharge = 0; $totalCredit = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')
                                    <tr>
                                        <td style="margin-left:1.1cm !important;" class="noSpacing" colspan="2">
                                            {{$discount['name']}}
                                        </td>
                                        <td class="noSpacing grayBG">
                                            @if($discount['is_credit'] == 1)
                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0}}
                                            @else
                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0}}
                                            @endif
                                        </td>
                                    </tr>
                                    @if($discount['is_credit'] == 0)
                                            <?php $totalCharge += number_format($discount['discount_price'],2) ?>
                                        @endif
                                        @if($discount['is_credit'] == 1)
                                            <?php $totalCredit += number_format($discount['discount_price'],2) ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif


                            @if($item['itemType'] == 'platinum' && $item['selectedEntertainmentPlan']['type'] == 'once-off')
                                <tr>
                                    <td style="" class="noSpacing" colspan="2">
                                        Telstra Platinum - {{$item['platinumPlanType']}}<br/>
                                        <span style="font-size:10px !important;padding-left:5px;">-{{$item['planDescription']}}</span>
                                    </td>
                                    <td class="noSpacing grayBG">
                                        ${{($item['selectedEntertainmentPlan']['price']) ? number_format($item['selectedEntertainmentPlan']['price'],2) : 0}}
                                    </td>
                                </tr>
                                <?php $totalCharge += number_format($item['selectedEntertainmentPlan']['price'],2) ?>
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')
                                        <tr class="borders" style="background-color:rgb(240, 200, 0)">
                                            <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                                Once-off Total:
                                            </td>
                                            <td>
                                                ${{number_format(($totalCharge - $totalCredit),2)}}
                                            </td>
                                        </tr>
                                        @break
                                    @endif
                                @endforeach
                                @break
                            @endif
                            @if($item['itemType'] == 'platinum' && $item['selectedEntertainmentPlan']['type'] == 'once-off')
                                <tr class="borders" style="background-color:rgb(240, 200, 0)">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        Once-off Total:
                                    </td>
                                    <td>
                                        ${{number_format(($totalCharge - $totalCredit),2)}}
                                    </td>
                                </tr>
                                @break
                            @endif
                        @endforeach


                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                <tr>
                                    <td class="noSpacing" style="padding-top:.5cm">
                                            <img height="25" src="img/welcome_page_icons/mobile-watch_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2" style="padding-top:.5cm">
                                        <strong style="margin-left:1%;padding-top:.5cm">Outright Purchases</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                        <?php $totalChargeOutright = 0; $totalCreditOutright = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                @if(count($item['devices']) != 0 || count($item['hardwareDevices']) != 0 || count($item['broadBandDevices']) != 0)
                                    @foreach($item['devices'] as $key => $device)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$device['make']}} {{$device['model']}} {{($device['capacity']) ? $device['capacity'] : ""}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                <?php $totalPriceOutright += $device['rrp_incl_gst'];?>
                                                {{number_format($device['rrp_incl_gst'],2)}}
                                            </td>
                                        </tr>
                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['device_id'] == $device['id'])
                                                    <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                <!--mobile device-->
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['device_id'] != 0 && $discount['device_id'] != 999 && $discount['device_id'] != 998 && $discount['device_id'] == $device['id'] && $discount['broadband_device_id'] == 999 && $discount['hardware_device_id'] == 999)
                                                   <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                               
                                            @endforeach
                                        @endif

                                    @endforeach

                                    @if($item['hardwareDevices'])
                                        @foreach($item['hardwareDevices'] as $key => $device)
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['name']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{number_format($device['rrp'],2)}}
                                                    <?php $totalPriceOutright += $device['rrp'];?>
                                                </td>
                                            </tr>

                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['hardware_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    @if($item['broadBandDevices'])
                                        @foreach($item['broadBandDevices'] as $key => $device)
                                            <?php $totalPriceOutright += $device['rrp_incl_gst'];?>
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['make']}} {{$device['model']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{number_format($device['rrp_incl_gst'],2)}}
                                                </td>
                                            </tr>

                                             @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['broadband_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    
                                @endif
                                @if(count($item['aroDevices']) != 0)
                                    @foreach($item['aroDevices'] as $key => $aro)
                                        <?php $totalPriceOutright += $aro['ProductUnitRRPCost'];?>
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$aro['ProductType']}} {{$aro['BarCode']}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                ${{number_format($aro['ProductUnitRRPCost'],2)}}
                                            </td>
                                        </tr>

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && !empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                <tr class="borders" style="background-color:rgb(240, 117, 0)">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        Outright Cost:
                                    </td>
                                    <td>
                                        ${{number_format($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright,2)}}
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'bnpl')
                                <tr>
                                    <td class="noSpacing" style="padding-top:.5cm">
                                            <img height="25" src="img/welcome_page_icons/mobile-watch_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2" style="padding-top:.5cm">
                                        <strong style="margin-left:1%;padding-top:.5cm">BNPL Purchases</strong>
                                        <br>
                                        <span style="width:33.3333333%"><b style="margin-left: 5px;">Company:</b> {{$item['company']['name']}}</span>
                                        <span style="width:33.3333333%"><b style="margin-left: 5px;">Term:</b> {{$item['term']['name']}}</span>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                        <?php $totalChargeOutright = 0; $totalCreditOutright = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'bnpl')
                                @if(count($item['devices']) != 0 || count($item['hardwareDevices']) != 0 || count($item['broadBandDevices']) != 0)
                                    @foreach($item['devices'] as $key => $device)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$device['make']}} {{$device['model']}} {{($device['capacity']) ? $device['capacity'] : ""}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                <?php $totalPriceOutright += $device['rrp_incl_gst'];?>
                                                {{number_format($device['rrp_incl_gst'],2)}}
                                            </td>
                                        </tr>
                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['hardware_device_id'] == 999 && $discount['device_id'] == 998 && $discount['broadband_device_id'] == 999)
                                                    <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                <!--mobile device-->
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['device_id'] != 0 && $discount['device_id'] != 999 && $discount['device_id'] != 998 && $discount['device_id'] == $device['id'] && $discount['broadband_device_id'] == 999 && $discount['hardware_device_id'] == 999)
                                                   <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                               
                                            @endforeach
                                        @endif

                                    @endforeach

                                    @if($item['hardwareDevices'])
                                        @foreach($item['hardwareDevices'] as $key => $device)
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['name']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{number_format($device['rrp'],2)}}
                                                    <?php $totalPriceOutright += $device['rrp'];?>
                                                </td>
                                            </tr>

                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['hardware_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    @if($item['broadBandDevices'])
                                        @foreach($item['broadBandDevices'] as $key => $device)
                                            <?php $totalPriceOutright += $device['rrp_incl_gst'];?>
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['make']}} {{$device['model']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{number_format($device['rrp_incl_gst'],2)}}
                                                </td>
                                            </tr>

                                             @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['broadband_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    
                                @endif
                                @if(count($item['aroDevices']) != 0)
                                    @foreach($item['aroDevices'] as $key => $aro)
                                        <?php $totalPriceOutright += $aro['ProductUnitRRPCost'];?>
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$aro['ProductType']}} {{$aro['BarCode']}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                ${{number_format($aro['ProductUnitRRPCost'],2)}}
                                            </td>
                                        </tr>

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && !empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'bnpl')
                                <tr class="borders" style="background-color:green">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        Amount:
                                    </td>
                                    <td>
                                        ${{number_format($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright,2)}}
                                    </td>
                                </tr>
                                <tr class="borders" style="background-color:rgb(240, 117, 0)">
                                    <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                        Term Cost:
                                    </td>
                                    <td>
                                        ${{intval($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright) / 4}}
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="3" style="margin-bottom:.5cm !important;text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                    <!-- <i>Payable in:</i> -->
                                    @if($item['company']['name'] == 'Afterpay')
                                        Pay ${{number_format($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright,2)}} over 4 instalments at ${{$item['amount']}} due every 2 weeks
                                    @else
                                        Pay ${{number_format($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright,2)}} over 4 instalments at ${{$item['amount']}} due every {{$item['term']['name']}}
                                    @endif
                                </td>
                               <!--  <td style="text-align:center;margin-bottom:.5cm !important;">
                                    <strong><?php echo intval(($totalChargeOutright + $totalPriceOutright - $item['promotionsPrice'] - $totalCreditOutright) / $item['amount']) ?> Terms</strong>
                                </td> -->
                            </tr>
                                @break;
                            @endif
                        @endforeach
                    </tbody>
                </table>
                <table class="table table-bordered" style="width:100% !important;border:none !important;background: #fff">
                    <tbody>
                        @foreach($data['quadrantCart'] as $key => $item)
                                    
                            @if($item['itemType'] == 'redemption')
                                <tr>
                                    <td class="">
                                        <img height="25" src="img/welcome_page_icons/TelstraPlus_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                    </td>
                                    <td class="noSpacing">
                                        <strong>Redemption</strong>
                                    </td>

                                    <td class="noSpacing">
                                        <strong>Outright Cost</strong>
                                    </td>
                                    <td class="noSpacing">
                                        <strong>Points</strong>
                                    </td>                                    
                                    <td class="noSpacing">
                                        <strong>You Pay</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach

                        <?php $totalRRPTelstraRedemption = 0; ?>
                        <?php $totalPointsTelstraRedemption = 0; ?>
                        <?php $totalPayTelstraRedemption = 0; ?>
                        <?php $totalProductsTelstraRedemption = 0; ?>

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'redemption')
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        {{$item['product']}} 
                                    </td>
                                    <td class="noSpacing">
                                        ${{$item['rrp']}} 
                                    </td>
                                    <td class="noSpacing">
                                        {{$item['points']}} points
                                    </td>
                                    <td class="noSpacing">
                                        ${{$item['pay']}} 
                                    </td>
                                </tr>
                                <?php $totalRRPTelstraRedemption += $item['rrp'];?>
                                <?php $totalPointsTelstraRedemption += $item['points'];?>
                                <?php $totalPayTelstraRedemption += $item['pay'];?>
                                <?php $totalProductsTelstraRedemption++;?>
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                                    
                            @if($item['itemType'] == 'redemption')
                                <tr>
                                    <td colspan="5">
                                        <hr/>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing"><strong>{{$totalProductsTelstraRedemption}} Products</strong></td>
                                    <td class="noSpacing"></td>
                                    <td class="noSpacing">{{$totalPointsTelstraRedemption}} Points</td>
                                    <td class="noSpacing">${{$totalPayTelstraRedemption}}</td>
                                </tr>
                            @break;
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        </div>
        @if(count($data['quadrantCart']) > 3)
            <div class="page-break"></div>
        @endif

        <strong><h5>Customer Acknowledgement</h5></strong>
        <div style="width:100%">
            @if($data)
            <table>
                <tbody>
                    <tr>
                        <td colspan="2">
                            I have been advised and understand that the individual products and services listed above and their associated cost will be charged to me through monthly billing and/or upfront/outright charges when applicable.
                        </td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            <form>
                                <p>Name</p>
                                <input type="text" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%">
                            <form>
                                <p>Date</p>
                                <input type="text" value="{{date('d-M-Y')}}" style="width:100%;height:20px">
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <form>
                            <p>Signature</p>
                            <input type="text" style="width:100%;height:80px">
                        </form>
                    </tr>
                </tbody>
            </table>
            @endif

        </div>
    </main>
</body>
</html>
