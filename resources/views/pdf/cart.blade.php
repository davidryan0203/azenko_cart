<style type="text/css" media="all">
    @page {
                margin: 0cm 0cm;
            }
  body{
    font-family: "Nunito", sans-serif;
    font-size:12px !important;
    margin-top: .3cm;
    margin-left: 1cm;
    margin-right: 1cm;
    margin-bottom: 1cm;
  }
  .grayBG{
    /*background-color:#B8B8B8;*/
    color:orange;
    font-weight: bold;
    text-align: center;
    width: 20%;
    font-size:18px;
  }

  h2{
    font-size:28px !important;
    color:#2D87C8;
  }

  h5{
    font-size:16px !important;
    font-weight: normal;
    margin:0px;
    padding:0px;
  }

  header {
    position: fixed;
    top: -.5cm;
    left: 0cm;
    right: 0cm;
    height: 4.8cm;

    /** Extra personal styles **/
    color: white;
    text-align: center;
    line-height: 1.5cm;
    /*background-image:url('/img/Telstra-Accessory-Purchase_Banner-01.png'); 
    background-size: 200px 100px; */
}

footer {
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: .8cm;

    /** Extra personal styles **/
    color: white;
    text-align: left;
    line-height: 1.5cm;
}

#burb{
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: 2cm;

    /** Extra personal styles **/
    color: #000;
    text-align: center;
    line-height: .5cm;
    margin-left: 1cm;
    margin-right: 1cm;
}

.borderItems{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    text-align: right;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
}

.borderItemsPrice{
    padding:10px;background: #2D87C8;border-radius: 10px !important;
    color:#fff;
    font-size:16px !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

table th, td {
  padding: .5em 0px;
  border-bottom: 2px solid white; 
}

/*tr:last-child  td:last-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}

tr:last-child  td:first-child {
    padding:10px;background: #2D87C8;border-radius: 10px !important;
}*/

.borders{
    border:2px solid #2D87C8 !important;
    padding:10px;background: #2D87C8;
    border-radius: 10px !important;
    color:#fff;
    /*font-size:16px !important;*/
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

.borderGreen{
    padding:10px;background: #30C42C;
    color:#fff !important;
    font-weight: bold;
    width:90%;padding-left: 1cm;text-align: center;
}

/*table tr{
    border-radius: 10px !important
}*/

table {
  border-collapse: collapse;
  /*border-radius: 1em;*/
  overflow: hidden;
  /*border-radius: 10px !important*/
}

/*input[type=text]{
        border-top-style: hidden;
        border-right-style: hidden;
        border-left-style: hidden;
        border-bottom-style: hidden;
      }
      
      .no-outline:focus {
        outline: none;
      }*/
table { page-break-inside:auto }
tr.page-break  { page-break-before:always!important; }
   /*tr    { page-break-inside:avoid; page-break-after:auto }*/
tr    { page-break-inside:avoid; page-break-after:auto }
   .page-break {
    page-break-after: always;
}

ul{
    list-style:none;
    padding:0;
    margin:0;
}

.noSpacing{
    margin:0px;
    padding:0px;
}

input[type=checkbox]
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(2); /* IE */
  -moz-transform: scale(2); /* FF */
  -webkit-transform: scale(2); /* Safari and Chrome */
  -o-transform: scale(2); /* Opera */
  transform: scale(2);
  padding: 10px;
}
</style>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
    <main>

        <div class="reps" style="width:250px;position: absolute;top:0px;right:0px;">
            <p style="padding-bottom:5px;margin-bottom:5px">Sales Representative: {{$_SESSION['userInfo']['clickposName']}}</p>
            <div class="section-1" style="float: left;">
                <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">Date: {{date('d/m/Y')}}</p>
                <!-- <p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;">{{$_SESSION['userInfo']['name']}}</p> -->
            </div>            
        </div>
        <div class="reps" style="position: absolute;top:50px;left:0px;">
            <table class="table" style="width:100%">
                <tbody>
                    <tr>
                        <td style="width:20%">
                            <div class="form-group col-2">
                                <label for="exampleFormControlInput1">&nbsp;</label>
                                <div class="form-check">
                                    <label class="form-check-label" for="defaultCheck1" style="font-size:14px !important;">
                                        Rescan 
                                    </label>
                                    @if($data['rescan'] == 1)
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" checked>
                                    @else
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    @endif
                                    
                                </div>
                            </div>
                        </td>
                        
                        <td style="width:20%">
                            <div class="form-group col-2">
                                <label for="exampleFormControlInput1">&nbsp;</label>
                                <div class="form-check">
                                    <label class="form-check-label" for="defaultCheck1" style="font-size:14px !important;">
                                        Direct Debit 
                                    </label>

                                    @if($data['debit'] == 1)
                                        <input class="form-check-input" type="checkbox" value="" id="" checked>
                                    @else
                                        <input class="form-check-input" type="checkbox" value="" id="">
                                    @endif
                                    
                                </div>
                            </div>
                        </td>

                        <td style="width:60%">
                             <div class="form-group col-2">
                                <label for="exampleFormControlInput1">&nbsp;</label>
                                <div class="form-check" style="padding-top:10px;">
                                    <input type="text" value="{{$data['isBusiness']}}">
                                </div>
                            </div>
                        </td>
                    <tr>
                </tbody>
            </table>
        </div>
        <?php $mobileAROAmount = 0;?>
        <?php $broadbandAROAmount = 0;?>
        <?php $planPrice = 0; ?>
        <?php $entertainmentPlanPrice = 0;?>
        <?php $hroPrice = 0;?>
        <?php $fixedTotalPrice = 0;?>
        <?php $fixedTotalDiscountPrice = 0;?>
        <?php $miscTotalPrice = 0;?>
        <?php $vasTotalAmount = 0;?>
        <?php $totalMonthlyCharge = 0;?>
        <?php $totalMobileAndBroadband = 0;?>
        <?php $totalPriceOutright = 0;?>
        <?php $existingServicesPrice = 0;?>
        <?php $aroAmount = 0;?>
        <?php $broadbandAROamount = 0;?>
        @if($data['quadrantCart'])
            <div style="padding:20px;clear:both;margin-top:2.5cm">
                <table class="table table-bordered" style="width:100% !important;border:none !important;background: #fff">
                    <tbody>
                        @foreach($data['quadrantCart'] as $key => $item)
                            @if($item['itemType'] == 'mobilePlan')
                                <?php $aroAmount += $item['aroAmount'];?>
                                <tr>
                                    <td class="noSpacing">
                                        <img height="25" src="img/welcome_page_icons/Mobile_Icon.png" style="position: relative;top: 0px;z-index: -99999;">
                                    </td>
                                    <td style="width:80%;">
                                        
                                            <p style="margin-bottom:2px;padding-bottom:2px;">
                                            <p style="padding:0px;margin:0px;"><strong>
                                                <input type="text" value="{{$item['recon']}}">&nbsp;
                                                {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} Mobile Plan @if($item['planType'] == 'XS')
                                                    {{ 'Extra Small' }}
                                                @elseif($item['planType'] == 'XL')
                                                    {{ 'Extra Large' }}
                                                @else
                                                    {{ $item['planType'] }} 
                                                @endif
                                                 @if($item['planType'] == 'Small')
                                                    40GB
                                                @elseif($item['planType'] == 'XS')
                                                    2GB
                                                @elseif($item['planType'] == 'Medium')
                                                    80GB
                                                @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                    120GB
                                                @else
                                                    180GB
                                                @endif
                                            </strong> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}</p>
                                            </p>
                                    </td>
                                </tr>

                                @if($item['make']) 
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                @if($item['make']) 
                                                    for {{$item['months']}} Months
                                                @endif
                                                @if(!$item['make']) 
                                                    BYO
                                                @endif | <span style="color:orange;font-weight: bold">{{ $item['imei'] }}</span>
                                            </strong>
                                        </td>
                                        <td class="noSpacing grayBG">
                                            
                                        </td>
                                    </tr>
                                @endif
                                @if($item['aroAmount'] != "0.00")
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            Accessory Repayment Option for {{$item['aroMonths']}} Months (${{$item['aroBase']}})
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- ${{$item['aroAmount']}} -->
                                        </td>
                                    </tr>
                                    <?php $mobileAROAmount += $item['aroAmount']; ?>
                                @endif

                                @if(count($item['aroAccessories']) != 0 && $item['aroAccessories'][0]['ProductType'] != '')
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing" colspan="2">
                                            <ul style="padding-left:10px;padding-top:0px;padding-bottom: 0px;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li><span style="color:orange;font-weight: bold">{{$aro['BarCode']}} &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; </span> {{$aro['ProductType']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing" colspan="2">
                                            @if($vas['name'] == 'Stay Connected Advanced')
                                                 <span style="color:orange;font-weight:bold">{{ucfirst($item['isVasNew'])}} &nbsp;|&nbsp;</span>
                                            @endif{{$vas['name']}}
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td class="noSpacing">
                                        &nbsp;
                                    </td>
                                    <td colspan="2" class="noSpacing" style="border-bottom:1px solid black;">@if($item['recon'] == 'Recon' || $item['recon'] == 'Transition' || $item['recon'] == 'Lease')
                                            <p>End date:</p>
                                            <input type="text" value="{{$item['endDate']}}">
                                            <br/>
                                        @endif</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <br/><br/>
                                    </td>
                                </tr>
                                
                            @endif

                            @if($item['itemType'] == 'broadBandPlan')
                                <tr>
                                    <td class="noSpacing">
                                        <img height="25" src="img/welcome_page_icons/MobileBroadband_Icon.png" style="position: relative;top: 0px;z-index: -99999;">
                                    </td>
                                    <td class="noSpacing" style="width:80%;">
                                            <p style="margin-bottom:2px;padding-bottom:2px;">
                                            <p style="padding:0px;margin:0px;"><strong>
                                                <input type="text" value="{{$item['recon']}}">
                                                {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} Data Plan @if($item['planType'] == 'XS')
                                                    {{ 'Extra Small' }}
                                                @elseif($item['planType'] == 'XL')
                                                    {{ 'Extra Large' }}
                                                @else
                                                    {{ $item['planType'] }} 
                                                @endif
                                                 @if($item['planType'] == 'Small')
                                                    40GB
                                                @elseif($item['planType'] == 'XS')
                                                    2GB
                                                @elseif($item['planType'] == 'Medium')
                                                    80GB
                                                @elseif($item['planType'] == 'Large')
                                                    120GB
                                                @else
                                                    180GB
                                                @endif
                                            </strong> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}} </p>
                                            </p>
                                    </td>

                                    <td class="noSpacing grayBG" style="width:10%;">
                                       <!-- ${{number_format($item['planValue'], 2, '.', '')}} -->
                                    </td>
                                </tr>

                                @if($item['make']) 
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                @if($item['make']) 
                                                    for {{$item['months']}} Months
                                                @endif
                                                @if(!$item['make']) 
                                                    BYO
                                                @endif | <span style="color:orange;font-weight: bold">{{$item['imei']}}</span>
                                            </strong>
                                        </td>
                                        <td class="noSpacing grayBG">
                                            
                                        </td>
                                    </tr>
                                @endif
                                @if($item['aroAmount'] != "0.00")
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            Accessory Repayment Option for {{$item['aroMonths']}} Months (${{$item['aroBase']}})
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- ${{$item['aroAmount']}} -->
                                        </td>
                                    </tr>
                                    <?php $mobileAROAmount += $item['aroAmount']; ?>
                                @endif

                                @if(count($item['aroAccessories']) != 0 && $item['aroAccessories'][0]['ProductType'] != '')
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            <ul style="padding-left:10px;padding-top:0px;padding-bottom: 0px;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li><span style="color:orange;font-weight: bold">{{$aro['BarCode']}}&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;</span>{{$aro['ProductType']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- <ul style="text-decoration: none !important;text-align: center !important;">
                                                @foreach($item['aroAccessories'] as $aro)
                                                    @if($aro['ProductType'] != '')
                                                        <li style="text-decoration: none !important;text-align: center !important;">{{$aro['BarCode']}}</li>
                                                    @endif
                                                @endforeach
                                            </ul> -->
                                        </td>
                                    </tr>
                                @endif
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            @if($vas['name'] == 'Stay Connected Advanced')
                                                <span style="color:orange;font-weight:bold">{{ucfirst($item['isVasNew'])}}&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;</span>
                                            @endif{{$vas['name']}} 
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- ${{number_format($vas['amount'],2)}} -->
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td class="noSpacing">
                                        &nbsp;
                                    </td>
                                    <td colspan="2" class="noSpacing" style="border-bottom:1px solid black;">@if($item['recon'] == 'Recon' || $item['recon'] == 'Transition' || $item['recon'] == 'Lease')
                                            <p>End date:</p>
                                            <input type="text" value="{{$item['endDate']}}">
                                            <br/>
                                        @endif</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <br/><br/>
                                    </td>
                                </tr>
                            @endif                         
                            
                        @endforeach                                    
                    </tbody>
                </table>
                @foreach($data['quadrantCart'] as $key => $item)
                    @if($item['itemType'] == 'mobilePlan' || $item['itemType'] == 'broadBandPlan')
                        <?php $totalMobileAndBroadband++; ?>
                    @endif
                    @if($totalMobileAndBroadband > 3)
                    <div class="page-break"></div>
                    @endif
                @endforeach

                <table class="table table-bordered" style="width:100% !important;border:none !important;background: #fff">
                    <tbody>
                        @foreach($data['quadrantCart'] as $key => $item)
                            @if($item['itemType'] == 'fixed')
                                <tr>
                                    <td colspan="3" class="noSpacing">
                                        <input type="text" value="{{$item['type']}}">
                                        
                                    </td>
                                    <!-- 
                                    <td class="noSpacing">{{$item['isBusiness']}}</td>
                                    <td class="noSpacing"></td> -->
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">
                                        <img height="25" src="img/welcome_page_icons/Fixed_Icon.png" style="position: relative;top: 0px;z-index: -99999;">
                                    </td>
                                    <td class="noSpacing" style="width:80%;">
                                            <p style="margin-bottom:2px;padding-bottom:2px;">
                                            <p style="padding:0px;margin:0px;"><strong>
                                                {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} Fixed Plan 
                                            </strong></p>
                                            </p>
                                    </td>

                                    <td class="noSpacing grayBG" style="width:10%;">
                                    </td>
                                </tr>

                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <strong>{{ $item['fixed']['name'] }} 
                                        </strong>
                                    </td>
                                    <td class="noSpacing grayBG">
                                        {{-- $item['imei'] --}}
                                    </td>
                                </tr>
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            {{$vas['name']}} @if($vas['name'] == 'Stay Connected Advanced')
                                                 |  <span style="color:orange;font-weight:bold">{{ucfirst($item['isVasNew'])}}</span>
                                            @endif
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- ${{number_format($vas['amount'],2)}} -->
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="3" style="border-bottom:2px solid gray;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <br/><br/>
                                    </td>
                                </tr>
                            @endif
                            
                            @if($item['itemType'] == 'entertainment')
                                <tr>
                                    <td colspan="3" style="padding:0px;margin:0px;">
                                        <h4 style="font-size:15px !important;padding:0px;margin:0px;">{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="20" src="img/welcome_page_icons/Entertainmetn_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Entertainment</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <span>{{ ucfirst($item['entertainmentPlanType']) }}</span><br/>
                                    </td>
                                    <td class="grayBG" style="text-align:center;"></td>
                                </tr>
                                @foreach($item['vas'] as $key => $vas)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td class="noSpacing">
                                            {{$vas['name']}} @if($vas['name'] == 'Stay Connected Advanced')
                                                 |  <span style="color:orange;font-weight:bold">{{ucfirst($item['isVasNew'])}}</span>
                                            @endif
                                        </td>
                                        <td class="noSpacing grayBG">
                                            <!-- ${{number_format($vas['amount'],2)}} -->
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="3" style="border-bottom:2px solid gray;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <br/><br/>
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'redemption')
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="20" src="img/welcome_page_icons/TelstraPlus_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Redemption</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td class="noSpacing">
                                        <span>{{ ucfirst($item['product']) }}</span><br/>
                                    </td>
                                    <td class="grayBG" style="text-align:center;">{{$item['points']}} points | ${{$item['pay']}}</td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'existingServices')
                                <tr>
                                    <td class="" style="width:5%;">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Existing Services</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noSpacing">&nbsp;</td>
                                    <td style="margin-left:2% !important;" class="noSpacing">{{$item['existingServicesDescription']}}</td>
                                    <td class="grayBG" style="text-align:center;">${{number_format($item['existingServicesPrice'],2)}}</td>
                                </tr>
                            @endif
                        @endforeach


                        @foreach($data['quadrantCart'] as $key => $item)
                                                    
                            @if($item['itemType'] == 'miscellaneous')
                                <tr>
                                    <td class="">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>Miscellaneous</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'miscellaneous')
                                @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                    @foreach($item['selectedPromotions'] as $discount)
                                        @if($discount['type']['name'] == 'Reoccuring Discount')
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">
                                                    {{$discount['name']}}

                                                    
                                                </td>
                                                <td class="grayBG">
                                                    @if($discount['type']['name'] == 'Reoccuring Discount' && $discount['is_credit'] == 0)
                                                        ${{($discount['discount_price']) ? $discount['discount_price'] : 0}}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">
                                                    @if(!empty($discount['description']))
                                                        <span style="font-size:10px !important;">
                                                            {{$discount['description']}}
                                                        </span>
                                                    @endif
                                                </td>
                                                <td class="grayBG">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        @endif
                                       <!--  <tr class="borders">
                                            <td colspan="3" style="height:1px !important"></td>
                                        </tr> -->
                                    @endforeach
                                @endif
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'hro')
                                <tr>
                                    <td class="">
                                            <img height="25" src="img/welcome_page_icons/HRO_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2">
                                        <strong>{{($item['isBusiness'] == '1') ? 'Business' : ''}} Hardware Repayment Options</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach

                        <?php $totalHRO = 0; $totalHRODiscount = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'hro')
                                @foreach($item['hro'] as $hro)
                                    <tr>
                                        <td class="noSpacing">&nbsp;</td>
                                        <td style="width:80%;text-align:left;margin-left:2% !important;" class="noSpacing">

                                            <span style="color:orange;font-weight: bold">{{ $hro['product_code'] }} &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>{{$hro['name']}} for {{$item['hroMonthly']}} Months 
                                        </td>
                                        <td class="grayBG" style="text-align:center;"></td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach                          

                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')

                                        <tr>
                                            <td class="noSpacing" colspan="3" style="margin-left:.8cm !important;padding-top:.5cm">
                                                <strong>Once-Off Credits and Charges</strong>
                                            </td>
                                        </tr>
                                        
                                        @break
                                    @endif
                                @endforeach
                                @break
                            @endif
                        @endforeach

                        <?php $totalCharge = 0; $totalCredit = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')
                                    <tr>
                                        <td style="margin-left:1.1cm !important;" class="noSpacing" colspan="2">
                                            {{$discount['name']}}
                                        </td>
                                        <td class="noSpacing grayBG">
                                            @if($discount['is_credit'] == 1)
                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0}}
                                            @else
                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0}}
                                            @endif
                                        </td>
                                    </tr>
                                    @if($discount['is_credit'] == 0)
                                            <?php $totalCharge += number_format($discount['discount_price'],2) ?>
                                        @endif
                                        @if($discount['is_credit'] == 1)
                                            <?php $totalCredit += number_format($discount['discount_price'],2) ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                @foreach($item['selectedPromotions'] as $discount)
                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['category']['name'] != 'Outright' || $discount['type']['name'] == 'Once Off Discount' && $discount['name'] == 'Super Saver credit applied to Telstra Account' && $discount['category']['name'] == 'Outright')
                                        <tr class="borders" style="background-color:rgb(240, 200, 0)">
                                            <td colspan="2" style="text-align: right;margin-right:10px !important;padding-right:10px !important;">
                                                Once-off Total:
                                            </td>
                                            <td>
                                                ${{number_format(($totalCharge - $totalCredit),2)}}
                                            </td>
                                        </tr>
                                        @break
                                    @endif
                                @endforeach
                                @break
                            @endif
                        @endforeach


                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                <tr>
                                    <td class="noSpacing" style="padding-top:.5cm">
                                            <img height="25" src="img/welcome_page_icons/mobile-watch_Icon.png" style="position: absolute;top:0px;z-index: 1;">
                                        </td>
                                    <td class="noSpacing" colspan="2" style="padding-top:.5cm">
                                        <strong style="margin-left:1%;padding-top:.5cm">Outright Purchases</strong>
                                    </td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                        <?php $totalChargeOutright = 0; $totalCreditOutright = 0;?>
                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                @if(count($item['devices']) != 0 || count($item['hardwareDevices']) != 0 || count($item['broadBandDevices']) != 0)
                                    @foreach($item['devices'] as $key => $device)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$device['make']}} {{$device['model']}} {{($device['capacity']) ? $device['capacity'] : ""}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                {{$device['rrp_incl_gst']}}
                                            </td>
                                        </tr>
                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['hardware_device_id'] == 999 && $discount['device_id'] == 998 && $discount['broadband_device_id'] == 999)
                                                    <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                            @foreach($item['selectedPromotions'] as $discount)
                                                <!--mobile device-->
                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['device_id'] != 0 && $discount['device_id'] != 999 && $discount['device_id'] != 998 && $discount['device_id'] == $device['id'] && $discount['broadband_device_id'] == 999 && $discount['hardware_device_id'] == 999)
                                                   <tr>
                                                        <td class="noSpacing">&nbsp;</td>
                                                        <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                            {{$discount['name']}}
                                                       </td>
                                                            <td class="noSpacing grayBG">
                                                            @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @else
                                                                ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($discount['is_credit'] == 1)
                                                        <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                    @else
                                                        <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                    @endif
                                                @endif
                                               
                                            @endforeach
                                        @endif

                                    @endforeach

                                    @if($item['hardwareDevices'])
                                        @foreach($item['hardwareDevices'] as $key => $device)
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['name']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{$device['rrp']}}
                                                </td>
                                            </tr>

                                            @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['hardware_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    @if($item['broadBandDevices'])
                                        @foreach($item['broadBandDevices'] as $key => $device)
                                            <tr>
                                                <td class="noSpacing">&nbsp;</td>
                                                <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                    {{$device['make']}} {{$device['model']}}
                                                </td>
                                                <td class="noSpacing grayBG">
                                                    {{$device['rrp_incl_gst']}}
                                                </td>
                                            </tr>

                                             @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && $discount['broadband_device_id'] != 0 && $discount['device_id'] == 999 && empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif

                                        @endforeach
                                    @endif

                                    
                                @endif
                                @if(count($item['aroDevices']) != 0)
                                    @foreach($item['aroDevices'] as $key => $aro)
                                        <tr>
                                            <td class="noSpacing">&nbsp;</td>
                                            <td style="margin-left:2% !important;width: 80%" class="noSpacing">
                                                {{$aro['ProductType']}} {{$aro['BarCode']}}
                                            </td>
                                            <td class="noSpacing grayBG">
                                                ${{number_format($aro['ProductUnitRRPCost'],2)}}
                                            </td>
                                        </tr>

                                        @if(isset($item['selectedPromotions']) && $item['selectedPromotions'])
                                                @foreach($item['selectedPromotions'] as $discount)
                                                    @if($discount['type']['name'] == 'Once Off Discount' && !empty($discount['aro_device']))
                                                       <tr>
                                                            <td class="noSpacing">&nbsp;</td>
                                                            <td style="margin-left:3% !important;width: 80%" class="noSpacing">
                                                                {{$discount['name']}}
                                                           </td>
                                                                <td class="noSpacing grayBG">
                                                                @if($discount['type']['name'] == 'Once Off Discount' && $discount['is_credit'] == 1)
                                                                    -${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @else
                                                                    ${{($discount['discount_price']) ? number_format($discount['discount_price'],2) : 0.00}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @if($discount['is_credit'] == 1)
                                                            <?php $totalCreditOutright += number_format($discount['discount_price'],2) ?>
                                                        @else
                                                            <?php $totalChargeOutright += number_format($discount['discount_price'],2) ?>
                                                        @endif
                                                    @endif
                                                   
                                                @endforeach
                                            @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach

                        @foreach($data['quadrantCart'] as $key => $item)
                            
                            @if($item['itemType'] == 'outright')
                                <tr class="borders">
                                    <td colspan="3" style="height:1px !important"></td>
                                </tr>
                                @break;
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        </div>
        <div style="width:100%">
            @if($data['notes'])
                <div class="form-group col-6">
                    <label for="exampleFormControlInput1">Notes</label>
                    <textarea class="form-control" style="width:100%" rows="5">{{$data['notes']}}</textarea>
                </div>
            @endif
            @if($data)
           <!--  <table>
                <tbody>
                    <tr>
                        <td >
                            <form>
                                <p>Name</p>
                                <input type="text" style="height:20px" class="no-outline" value="{{$_SESSION['userInfo']['name']}}" style="border:none !important;">
                            </form>
                        </td>
                        <td>
                            <form>
                                <p>Date</p>
                                <input type="text" value="{{date('d-M-Y')}}" class="no-outline" style="height:20px" style="border:none !important;">
                            </form>
                        </td>
                        <td>
                            <form>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <form>
                            <p>Signature</p>
                            <textarea class="form-control" style="height:80px;"></textarea>
                        </form>
                    </tr>
                </tbody>
            </table> -->
            @endif

        </div>
    </main>
</body>
</html>
