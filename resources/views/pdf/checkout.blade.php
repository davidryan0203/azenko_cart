<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel 8 HTML to PDF Demo</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="/css/font.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
</head>
<style type="text/css">
    p{
        font-size:14px;
    }

    .borderless td, .borderless th {
        border: none;
    }

    ul > li{
        font-size:14px;
    }

    tr{
        padding-bottom:0px !important;
        margin-bottom:0px !important;
    }

    td{
        padding-bottom:0px !important;
        margin-bottom:0px !important;
    }

    .ban{
        color:#FCA318;
        font-weight: bolder;
        font-size:23px;
    }
</style>
<body class="">
    <div style="width:35%;float:left">
        <p style="color:#068edf;font-size:26px;">Official Use Only</p>
    </div>
    <div style="width:40%;float:left">
        <p style="margin-bottom:0px;padding-bottom:0px;font-size:15px;font-weight:bolder">Sales Representative: <u>{{$_SESSION['userInfo']['clickposName']}}</u></p>
        <p style="padding-top:0px;margin-top:0px;">Date: {{date('d/m/Y')}} | {{ (isset($_SESSION['branchName'])) ? $_SESSION['branchName'] : 'TLS '.str_replace('TLS ', '', $_SESSION['currentBranch']) }}</p>
    </div>
    <form class="form-inline" style="clear:both">
        <table class="table borderless">
            <thead style="background:#068edf">
                <tr class="table-primary" style="background:rgb(6, 142, 223)">
                    <td style="width:33.3333333%"></td>
                    <td style="width:33.3333333%"></td>
                    <td style="width:33.3333333%"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Rescan</strong> <input style="margin-top:.5rem" class="form-check-input custom-control-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['rescan'] == 1) ? 'checked' : '' ?> >

                            <!-- <div class="custom-control form-control-lg custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                            </div> -->
                        </p>
                    </td>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Partial Payment</strong> <input style="margin-top:.5rem" class="form-check-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['debit'] == 1) ? 'checked' : '' ?>>
                        </p>
                    </td>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Outright</strong> <input style="margin-top:.5rem" class="form-check-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['outright'] == 1) ? 'checked' : '' ?>>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Invoice Number:</strong> {{$data['invoiceNumber']}}
                        </p>
                    </td>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Customer Name:</strong> {{$data['customerName']}}
                        </p>
                    </td>
                    <td style="width:33.3333333%">
                        <p class="form-check-label" for="defaultCheck1">
                            <strong>Customer Type:</strong> {{$data['isBusiness']}}
                        </p>
                    </td>
                </tr>
                @if(isset($data['debit']) && $data['debit'] == 1 || isset($data['contact']) && $data['contact'] != '')
                    <tr>
                        @if($data['debit'] == 1)
                            <td style="width:33.3333333%">
                                <p class="form-check-label" for="defaultCheck1">
                                    <strong>Partial Payment:</strong> ${{$data['partialPayment']}}
                                </p>
                            </td>
                        @else
                            <td style="width:33.3333333%">
                                <p>&nbsp;</p>
                            </td>
                        @endif
                        @if(isset($data['contact']) && $data['contact'] != '')
                            <td style="width:66.66666666%">
                                <p class="form-check-label" for="defaultCheck1">
                                    <strong>Contact Number:</strong> {{$data['contact']}}
                                </p>
                            </td>
                        @else
                            <td style="width:66.66666666%">
                                <p>&nbsp;</p>
                            </td>
                        @endif
                    </tr>
                @endif
                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Siebel')
                        <tr style="padding-top:0px;margin-top:0px;">
                            <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                <div style="padding-top:10px;">
                                    <p class="ban" style="width: 20%;float:left;">Siebel</p>

                                    <div  style="width: 64%;float:left;padding-top:5px;">
                                        <span>BAN: <?php echo $data['ban']?></span>
                                    </div>  
                                </div>
                            </td>
                            <td colspan="3" style="margin:0px;height:0px;padding:0px;"><div style="clear:both;height:0px;margin:0px;padding:0px;"></div></td>
                        </tr>
                        @break
                    @endif
                @endforeach

                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Siebel')
                        @if($item['itemType'] == 'outright')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        <div style="width:100%;clear:both">
                                            Outright:
                                            @if($item['devices'])
                                            <p>Mobile Devices</p>
                                            <ul>
                                                @foreach($item['devices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightMobileDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['broadBandDevices'])
                                            <p>Mobile Broadband</p>
                                            <ul>
                                                @foreach($item['broadBandDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightBroadbandDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['hardwareDevices'])
                                            <p>HRO</p>
                                            <ul>
                                                @foreach($item['hardwareDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['name']) }} | ${{$device['rrp']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['aroDevices'])
                                            <p>Accessory</p>
                                            <ul>
                                                @foreach($item['aroDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['ProductType']) }} | ${{$device['ProductUnitCost']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif


                        @if($item['itemType'] == 'redemption')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        <div style="width:100%;clear:both">
                                            Telstra Plus:
                                            <ul>
                                                <li><strong>{{ ucfirst($item['product']) }}</strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'mobilePlan')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;padding-top:0px;margin-top:0px;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:40%;float:left">Mobile: <strong>Mobile Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                                40GB
                                                            @elseif($item['planType'] == 'XS')
                                                                2GB
                                                            @elseif($item['planType'] == 'Medium')
                                                                80GB
                                                            @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                                120GB
                                                            @else
                                                                180GB
                                                            @endif</strong></p>
                                            @if($item['deviceItem'])
                                                <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                                <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                            @if($item['make']) 
                                                                for {{$item['months']}} Months
                                                            @endif
                                                            @if(!$item['make']) 
                                                                BYO
                                                            @endif</strong></p>
                                            @endif
                                        </div>
                                        <br/>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            @if($item['pobo'] == false && $item['imei'] != '')
                                                <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                            @endif

                                            @if($item['pobo'] == true)
                                                <p style="width:40%;float:left">POBO</p>
                                            @endif

                                            <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                            @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        @if($item['itemType'] == 'broadBandPlan')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:40%;float:left">MBB: <strong>Data Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                            30GB
                                                        @elseif($item['planType'] == 'XS')
                                                            5GB
                                                        @elseif($item['planType'] == 'Medium')
                                                            75GB
                                                        @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                            400GB
                                                        @else
                                                            180GB
                                                        @endif</strong></p>
                                            @if($item['deviceItem'])
                                                <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                                <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                        @if($item['make']) 
                                                            for {{$item['months']}} Months
                                                        @endif
                                                        @if(!$item['make']) 
                                                            BYO
                                                        @endif</strong></p>
                                            @endif
                                        </div>
                                        <br/>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            @if($item['pobo'] == false && $item['imei'] != '')
                                                <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                            @endif

                                            @if($item['pobo'] == true)
                                                <p style="width:40%;float:left">POBO</p>
                                            @endif
                                            <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                            @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'entertainment')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:100%;float:left">Entertainment: <strong>
                                                @foreach($item['entertainment'] as $entertainment)
                                                    {{ ucfirst($entertainment['name']) }}
                                                @endforeach
                                            </strong></p>
                                        </div>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'fixed')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:33.333333%;float:left">Bundle: <strong>{{ ucfirst($item['fixed']['name']) }}</strong></p>
                                            @if($item['modem'] == 'Provided in store')
                                                <p style="width:33.333333%;float:right">Serial Number: {{$item['serialNumber'][$cartKey]}}</p>
                                            @endif
                                            <p style="width:33.333333%;float:right">Modem: {{$item['modem']}}</p>
                                        </div>
                                        @if(count($item['vas']) != 0)
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $key => $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            <p style="width:33.3333%;float:left">TV3: {{$item['tv3']}}</p>
                                            <!-- <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p> -->

                                            <div style="clear:both;width:100%;">
                                                <p style="width:33.33333%;float: left">Order Type: {{$item['recon']}}</p>
                                                @if($item['recon'] != 'New' && $item['recon'] != 'New nbn connection')
                                                    <p style="width:33.33333%;float: left">End Date: {{$item['endDate']}}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div style="clear:both;width:100%;">
                                            <p style="width:33.3333%;float:left">SOC Order Number: {{$item['socSerialNumber'][$cartKey]}}</p>
                                            <!-- <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p> -->
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'hro')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                     
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        <div style="width:100%;clear:both">
                                            {{ ($item['isBusiness'] == 1) ? 'Business' : '' }} HRO:
                                            <ul>
                                                @foreach($item['hro'] as $key => $hro)
                                                    @foreach($item['hroImei'] as $hroImeiKey => $hroImei)
                                                        
                                                        @if($hro['id'] == $hroImei['id'])
                                                            <li><strong>{{ ucfirst($hro['name']) }} - {{$hroImei['imei']}} | ${{$hro['rrp']}}</strong></li>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'miscellaneous')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                        @if(isset($item['customDescription']))
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        @else
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        @endif
                                        <div style="width:100%;clear:both">
                                            Miscellaneous:
                                            <ul>
                                                @foreach($item['selectedPromotions'] as $key => $misc)

                                                    @if($misc['name'] != 'Starter Internet Plan - 25GB' && $misc['name'] != '5G Home Internet Plan - 500GB' && $misc['name'] != 'Business Bundle Lite - 500GB')
                                                        <li><strong>{{ ucfirst($misc['name']) }} - {{$data['miscImei'][$key]}}</strong></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                            @foreach($item['selectedPromotions'] as $key => $misc)
                                @if($misc['name'] == 'Starter Internet Plan - 25GB' || $misc['name'] == '5G Home Internet Plan - 500GB' || $misc['name'] == 'Business Bundle Lite - 500GB')
                                    <tr style="padding-top:0px;margin-top:0px;">
                                        <td colspan="3" style="padding-top:0px;margin-top:0px;clear:both;">
                                            <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;clear:both;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                                <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                                <div style="width:100%">
                                                    <p style="width:33.333333%;float:left">Bundle: <strong>{{ ucfirst($misc['name']) }}</strong></p>
                                                    <p style="width:33.333333%;float:right">Modem: {{$data['miscModem'][$key]}}</p>
                                                    @if($data['miscModem'][$key] == 'Provided in store')
                                                        <p style="width:33.333333%;float:right">Serial Number: {{$data['miscSerialNumber'][$key]}}</p>
                                                    @endif
                                                </div>
                                                <div style="clear:both;width:100%;">
                                                    <p style="width:33.3333%;float:left">TV3: {{$data['miscTv3'][$key]}}</p>

                                                    <div style="clear:both;width:100%;">
                                                        <p style="width:33.33333%;float: left">Order Type: {{$data['reconMisc'][$key]}}</p>
                                                        @if($data['reconMisc'][$key] != 'New')
                                                            <p style="width:33.33333%;float: left">End Date: {{$data['miscEndDate'][$key]}}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div style="clear:both;width:100%;">
                                                    <p style="width:33.3333%;float:left">SOC Order Number: {{$data['miscSocSerialNumber'][$key]}}</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach

                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Console')
                        <tr style="padding-top:0px;margin-top:0px;">
                            <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                <div style="padding-top:0px;">
                                    <p class="ban" style="width: 20%;float:left;">Console</p>

                                    <div  style="width: 64%;float:left;padding-top:5px;">
                                        <span>BAN: <?php echo $data['banConsole']?></span>
                                    </div>  
                                </div>
                            </td>
                            <td colspan="3" style="margin:0px;height:0px;padding:0px;"><div style="clear:both;height:0px;margin:0px;padding:0px;"></div></td>
                        </tr>
                        @break
                    @endif
                @endforeach

                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Console')
                            @if($item['itemType'] == 'outright')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                                <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                            <div style="width:100%;clear:both">
                                                Outright:
                                                @if($item['devices'])
                                                <p>Mobile Devices</p>
                                                <ul>
                                                    @foreach($item['devices'] as $key => $device)
                                                        <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightMobileDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                                @if($item['broadBandDevices'])
                                                <p>Mobile Broadband</p>
                                                <ul>
                                                    @foreach($item['broadBandDevices'] as $key => $device)
                                                        <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightBroadbandDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                                @if($item['hardwareDevices'])
                                                <p>HRO</p>
                                                <ul>
                                                    @foreach($item['hardwareDevices'] as $key => $device)
                                                        <li><strong>{{ ucfirst($device['name']) }} | ${{$device['rrp']}}</strong></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                                @if($item['aroDevices'])
                                                <p>Accessory</p>
                                                <ul>
                                                    @foreach($item['aroDevices'] as $key => $device)
                                                        <li><strong>{{ ucfirst($device['ProductType']) }} | ${{$device['ProductUnitCost']}}</strong></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @if($item['itemType'] == 'mobilePlan')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;padding-top:0px;margin-top:0px;">
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                            <div style="width:100%">
                                                <p style="width:40%;float:left">Mobile: <strong>Mobile Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                                    40GB
                                                                @elseif($item['planType'] == 'XS')
                                                                    2GB
                                                                @elseif($item['planType'] == 'Medium')
                                                                    80GB
                                                                @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                                    120GB
                                                                @else
                                                                    180GB
                                                                @endif</strong></p>
                                                @if($item['deviceItem'])
                                                    <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                                    <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                                @if($item['make']) 
                                                                    for {{$item['months']}} Months
                                                                @endif
                                                                @if(!$item['make']) 
                                                                    BYO
                                                                @endif</strong></p>
                                                @endif
                                                
                                            </div>
                                            <br/>
                                            @if(isset($item['vas']))
                                                <div style="width:100%;clear:both">
                                                    <ul>
                                                        @foreach($item['vas'] as $vas)
                                                            <li>{{$vas['name']}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div style="clear:both;width:100%;">
                                                @if($item['pobo'] == false && $item['imei'] != '')
                                                    <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                                @endif

                                                @if($item['pobo'] == true)
                                                    <p style="width:40%;float:left">POBO</p>
                                                @endif

                                                <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                                @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                    <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                                @endif
                                            </div>
                                            <div style="clear:both;width:100%;">
                                                @if(isset($data['conNote']) && $data['conNote'] !=  '')
                                                    <p style="width:40%;float:left">Con Note: {{$data['conNote']}}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @if($item['itemType'] == 'broadBandPlan')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                            <div style="width:100%">
                                                <p style="width:40%;float:left">MBB: <strong>Data Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                            30GB
                                                        @elseif($item['planType'] == 'XS')
                                                            5GB
                                                        @elseif($item['planType'] == 'Medium')
                                                            75GB
                                                        @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                            400GB
                                                        @else
                                                            180GB
                                                        @endif</strong></p>
                                                @if($item['deviceItem'])
                                                    <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                                    <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                            @if($item['make']) 
                                                                for {{$item['months']}} Months
                                                            @endif
                                                            @if(!$item['make']) 
                                                                BYO
                                                            @endif</strong></p>
                                                @endif
                                            </div>
                                            <br/>
                                            @if(isset($item['vas']))
                                                <div style="width:100%;clear:both">
                                                    <ul>
                                                        @foreach($item['vas'] as $vas)
                                                        <li>{{$vas['name']}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div style="clear:both;width:100%;">
                                                @if($item['pobo'] == false && $item['imei'] != '')
                                                    <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                                @endif

                                                @if($item['pobo'] == true)
                                                    <p style="width:40%;float:left">POBO</p>
                                                @endif
                                                <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                                @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                    <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'entertainment')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                            <div style="width:100%">
                                                <p style="width:100%;float:left">Entertainment: <strong>
                                                    @foreach($item['entertainment'] as $entertainment)
                                                        {{ ucfirst($entertainment['name']) }}
                                                    @endforeach
                                                </strong></p>
                                            </div>
                                            @if(isset($item['vas']))
                                                <div style="width:100%;clear:both">
                                                    <ul>
                                                        @foreach($item['vas'] as $vas)
                                                        <li>{{$vas['name']}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'fixed')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                            <div style="width:100%">
                                                <p style="width:33.333333%;float:left">Bundle: <strong>{{ ucfirst($item['fixed']['name']) }}</strong></p>
                                                @if($item['modem'] == 'Provided in store')
                                                    <p style="width:33.333333%;float:right">Serial Number: {{$item['serialNumber'][$cartKey]}}</p>
                                                @endif
                                                <p style="width:33.333333%;float:right">Modem: {{$item['modem']}}</p>
                                            </div>
                                            @if(count($item['vas']) != 0)
                                                <div style="width:100%;clear:both">
                                                    <ul>
                                                        @foreach($item['vas'] as $key => $vas)
                                                        <li>{{$item['imei'][$key]}} | {{$vas['name']}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div style="clear:both;width:100%;">
                                                <p style="width:33.3333%;float:left">TV3: {{$item['tv3']}}</p>
                                                <!-- <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p> -->

                                                <div style="clear:both;width:100%;">
                                                    <p style="width:33.33333%;float: left">Order Type: {{$item['recon']}}</p>
                                                    @if($item['recon'] != 'New' && $item['recon'] != 'New nbn connection')
                                                        <p style="width:33.33333%;float: left">End Date: {{$item['endDate']}}</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div style="clear:both;width:100%;">
                                                <p style="width:33.3333%;float:left">SOC Order Number: {{$item['socSerialNumber'][$cartKey]}}</p>
                                                <!-- <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p> -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'hro')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                 
                                                <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                            <div style="width:100%;clear:both">
                                                HRO:
                                                <ul>
                                                    @foreach($item['hro'] as $key => $hro)
                                                        @foreach($item['hroImei'] as $hroImeiKey => $hroImei)
                                                            
                                                            @if($hro['id'] == $hroImei['id'])
                                                                <li><strong>{{ ucfirst($hro['name']) }} - {{$hroImei['imei']}} | ${{$hro['rrp']}}</strong></li>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif

                            @if($item['itemType'] == 'miscellaneous')
                                <tr style="padding-top:0px;margin-top:0px;">
                                    <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                        <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                            @if(isset($item['customDescription']))
                                                <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                            @else
                                                <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                            @endif
                                            <div style="width:100%;clear:both">
                                                Miscellaneous:
                                                <ul>
                                                    @foreach($item['selectedPromotions'] as $key => $misc)
                                                         <li><strong>{{ ucfirst($misc['name']) }} - {{$data['miscImei'][$key]}}</strong></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                    @endif
                @endforeach

                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Legacy')
                        <tr style="padding-top:0px;margin-top:0px;">
                            <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                <div style="padding-top:10px;">
                                    <p class="ban" style="width: 20%;float:left;">Legacy</p>

                                    <div  style="width: 64%;float:left;padding-top:5px;">
                                        <span>BAN: <?php echo $data['banLegacy']?></span>
                                    </div>  
                                </div>
                            </td>
                            <td colspan="3" style="margin:0px;height:0px;padding:0px;"><div style="clear:both;height:0px;margin:0px;padding:0px;"></div></td>
                        </tr>
                        @break
                    @endif
                @endforeach

                @foreach($data['quadrantCart'] as $cartKey => $item)
                    @if($item['mobileOption']['name'] == 'Legacy')
                        @if($item['itemType'] == 'outright')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        <div style="width:100%;clear:both">
                                            Outright:
                                            @if($item['devices'])
                                            <p>Mobile Devices</p>
                                            <ul>
                                                @foreach($item['devices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightMobileDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['broadBandDevices'])
                                            <p>Mobile Broadband</p>
                                            <ul>
                                                @foreach($item['broadBandDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['make']) }} {{ucfirst($device['model']) }} {{ucfirst($device['capacity']) }} - {{$outrightBroadbandDevicesImei[$key]}} | ${{$device['rrp_incl_gst']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['hardwareDevices'])
                                            <p>HRO</p>
                                            <ul>
                                                @foreach($item['hardwareDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['name']) }} | ${{$device['rrp']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            @if($item['aroDevices'])
                                            <p>Accessory</p>
                                            <ul>
                                                @foreach($item['aroDevices'] as $key => $device)
                                                    <li><strong>{{ ucfirst($device['ProductType']) }} | ${{$device['ProductUnitCost']}}</strong></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        @if($item['itemType'] == 'mobilePlan')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;padding-top:0px;margin-top:0px;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:40%;float:left">Mobile: <strong>Mobile Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                                40GB
                                                            @elseif($item['planType'] == 'XS')
                                                                2GB
                                                            @elseif($item['planType'] == 'Medium')
                                                                80GB
                                                            @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                                120GB
                                                            @else
                                                                180GB
                                                            @endif</strong></p>
                                            @if($item['deviceItem'])
                                            <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                            <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                        @if($item['make']) 
                                                            for {{$item['months']}} Months
                                                        @endif
                                                        @if(!$item['make']) 
                                                            BYO
                                                        @endif</strong></p>
                                            @endif
                                        </div>
                                        <br/>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            @if($item['pobo'] == false && $item['imei'] != '')
                                                <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                            @endif

                                            @if($item['pobo'] == true)
                                                <p style="width:40%;float:left">POBO</p>
                                            @endif

                                            <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                            @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        @if($item['itemType'] == 'broadBandPlan')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:40%;float:left">MBB: <strong>Data Plan {{$item['planType']}} @if($item['planType'] == 'Small')
                                                            30GB
                                                        @elseif($item['planType'] == 'XS')
                                                            5GB
                                                        @elseif($item['planType'] == 'Medium')
                                                            75GB
                                                        @elseif($item['planType'] == 'Large' || $item['planType'] == 'L')
                                                            400GB
                                                        @else
                                                            180GB
                                                        @endif</strong></p>
                                            @if($item['deviceItem'])
                                                <p style="width:10%;float:right"><strong>${{ $item['deviceItem']['rrp_incl_gst'] }}</strong></p>
                                                <p style="width:50%;float:right"><strong>{{ $item['make'] }} {{ $item['model'] }} {{ $item['capacity'] }} 
                                                        @if($item['make']) 
                                                            for {{$item['months']}} Months
                                                        @endif
                                                        @if(!$item['make']) 
                                                            BYO
                                                        @endif</strong></p>
                                            @endif
                                        </div>
                                        <br/>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            @if($item['pobo'] == false && $item['imei'] != '')
                                                <p style="width:40%;float:left">IMEI: {{$item['imei']}}</p>
                                            @endif

                                            @if($item['pobo'] == true)
                                                <p style="width:40%;float:left">POBO</p>
                                            @endif
                                            <p style="width:30%;float: left">Order Type: {{$item['recon']}}</p>
                                            @if(isset($item['recon']) && $item['recon'] != 'New' && $item['recon'] != 'Pre to Post')
                                                <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'entertainment')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:100%;float:left">Entertainment: <strong>
                                                @foreach($item['entertainment'] as $entertainment)
                                                    {{ ucfirst($entertainment['name']) }}
                                                @endforeach
                                            </strong></p>
                                        </div>
                                        @if(isset($item['vas']))
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $vas)
                                                    <li>{{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'fixed')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;">
                                        <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        <div style="width:100%">
                                            <p style="width:33.333333%;float:left">Bundle: <strong>{{ ucfirst($item['fixed']['name']) }}</strong></p>
                                            @if($item['modem'] == 'Provided in store')
                                                <p style="width:33.333333%;float:right">Serial Number: {{$item['serialNumber'][$cartKey]}}</p>
                                            @endif
                                            <p style="width:33.333333%;float:right">Modem: {{$item['modem']}}</p>
                                        </div>
                                        @if(count($item['vas']) != 0)
                                            <div style="width:100%;clear:both">
                                                <ul>
                                                    @foreach($item['vas'] as $key => $vas)
                                                    <li>{{$item['imei'][$key]}} | {{$vas['name']}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div style="clear:both;width:100%;">
                                            <p style="width:33.3333%;float:left">TV3: {{$item['tv3']}}</p>
                                            <!-- <p style="width:30%;float: left">End Date: {{$item['endDate']}}</p> -->

                                            <div style="clear:both;width:100%;">
                                                <p style="width:33.33333%;float: left">Order Type: {{$item['recon']}}</p>
                                                @if($item['recon'] != 'New' && $item['recon'] != 'New nbn connection')
                                                    <p style="width:33.33333%;float: left">End Date: {{$item['endDate']}}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'hro')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        <div style="width:100%;clear:both">
                                            HRO:
                                            <ul>
                                                @foreach($item['hro'] as $key => $hro)
                                                    @foreach($item['hroImei'] as $hroImeiKey => $hroImei)
                                                        
                                                        @if($hro['id'] == $hroImei['id'])
                                                            <li><strong>{{ ucfirst($hro['name']) }} - {{$hroImei['imei']}} | ${{$hro['rrp']}}</strong></li>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if($item['itemType'] == 'miscellaneous')
                            <tr style="padding-top:0px;margin-top:0px;">
                                <td colspan="3" style="padding-top:0px;margin-top:0px;">
                                    <div style="border:2px solid #068edf;margin-bottom:0px;border-radius:10px;padding-bottom: 40px;padding-left:1rem;padding-right:1rem;width:100%;">
                                        @if(isset($item['customDescription']))
                                            <p style="width:100%;color:#068edf"><strong>{{$item['customDescription']}}</strong></p>
                                        @else
                                            <p style="width:100%;color:#068edf"><strong>&nbsp;</strong></p>
                                        @endif
                                        <div style="width:100%;clear:both">
                                            Miscellaneous:
                                            <ul>
                                                @foreach($item['selectedPromotions'] as $key => $misc)
                                                     <li><strong>{{ ucfirst($misc['name']) }} - {{$data['miscImei'][$key]}}</strong></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                    <tr>
                        <td colspan="3">
                            <p class="form-check-label" for="defaultCheck1" style="width:33%;float: left">
                                <strong>Registered for Telstra Plus:</strong> <input style="margin-top:.5rem" class="form-check-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['registeredTelstraPlus'] == 1) ? 'checked' : '' ?> >
                            </p>

                            <p class="form-check-label" for="defaultCheck1" style="width:15%;float:left">
                                <strong>SCANTEK:</strong> <input style="margin-top:.5rem" class="form-check-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['scantek'] == 1) ? 'checked' : '' ?> >
                            </p>

                            <p class="form-check-label" for="defaultCheck1" style="width:10%;float:left">
                                <strong>NBA:</strong> <input style="margin-top:.5rem" class="form-check-input form-control-lg" type="checkbox" value="" id="defaultCheck1" <?php echo ($data['nba'] == 1) ? 'checked' : '' ?> >
                            </p>

                            <p class="form-check-label" for="defaultCheck1" style="width:27%;float:left;margin-top:1.2rem">
                                <strong>Case ID:</strong>{{$data['caseID']}}
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            @if($data['isBusiness'] == 'Business')
                            <p><strong>D/L:</strong> {{$data['dl']}}</p>
                            <p><strong>CRAT:</strong> {{$data['crat']}}</p>
                            <p><strong>ABN:</strong> {{$data['abn']}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="clear:both">&nbsp;</div>
                        </td>
                    </tr>
                    <!-- <tr>
                        <td style="width: 50%">
                            <p>CA Signature</p>
                            @if(isset($data['csSignature']))
                            <img src="{{$data['csSignature']}}" style="width:300px;border:2px solid gray">
                            @else
                            <input type="text" style="width:300px;border:2px solid gray">
                            @endif
                        </td>

                        <td style="width: 50%" colspan="2">
                            <div style="margin-left:15%">
                                <p style="padding-bottom: 0px;">Manager Signature</p>
                                @if(isset($data['managerSignature']))
                                <img src="{{$data['managerSignature']}}" style="width:300px;border:2px solid gray">
                                @else
                                <input type="text" style="width:300px;border:2px solid gray">
                                @endif
                            </div>
                        </td>
                    </tr> -->
            </tbody>
        </table>
    </form>

    <div style="clear:both;overflow: hidden;">
    </div>
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    <p>CA Signature</p>
                    @if(isset($data['csSignature']))
                    <img src="{{$data['csSignature']}}" style="width:300px;border:2px solid gray">
                    @else
                    <input type="text" style="width:300px;border:2px solid gray">
                    @endif
                </td>

                <td style="width: 50%" colspan="2">
                    <div style="margin-left:25%">
                        <p style="padding-bottom: 0px;">Manager Signature</p>
                        @if(isset($data['managerSignature']))
                        <img src="{{$data['managerSignature']}}" style="width:300px;border:2px solid gray">
                        @else
                        <input type="text" style="width:300px;border:2px solid gray">
                        @endif
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>