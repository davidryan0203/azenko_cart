{{--dd($data)--}}
<style type="text/css" media="all" scoped="">
     @page {
                margin: 0cm 0cm;
            }
  body{
    font-family: "Nunito", sans-serif;
    font-size:12px !important;
    margin-top: 5.5cm;
    margin-left: 1cm;
    margin-right: 1cm;
    margin-bottom: 2cm;
  }
  .grayBG{
    background-color:#B8B8B8;
    text-align: center;
    width: 20%;
  }
  h3{
    font-size:15px !important;
    font-weight: normal;
    margin:0px;
    padding:0px;
  }

  #table tbody,tr,td{
    border:2px solid #1172ba;
  }
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
}

header {
    position: fixed;
    top: 0cm;
    left: 0cm;
    right: 0cm;
    height: 4.8cm;

    /** Extra personal styles **/
    background-color: #03a9f4;
    color: white;
    text-align: center;
    line-height: 1.5cm;
    /*background-image:url('/img/Telstra-Accessory-Purchase_Banner-01.png'); 
    background-size: 200px 100px; */
}
</style>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
    <!-- Define header and footer blocks before your content -->
    <header>
        <img src="img/Telstra-Accessory-Purchase_Banner-01.png" width="100%">
    </header>
    <main>
            <div style="width: 100%">
                <!-- <div style="float:left;"><h3>Mobile Service </h3></div> -->
                <h3 style="margin-bottom: 10px;"><strong>Thank you for your time today. Below is an acknowledgement of your accessory purchase.</strong><br/> We will also attach a copy of this document to your Telstra profile for future reference.</h3>

                <h3 style="margin-bottom: 10px;"><i><strong>Customer acknowledgement:</strong></i></h3>
                <h3 style="margin-bottom: 10px;">The accessory I am purchasing as a repayment option, and the associated costs are as follows:</h3>
            </div>
            <div style="">
                <table id="table">
                    <thead>
                        <tr>
                            <th style="background-color: #001e82;border:2px solid #1172ba"></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Repayment Type</h3></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Upfront Payment</h3></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Repayment Term</h3></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Total Repayment Amount</h3></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Monthly Commitment</h3></th>
                            <th style="background-color: #001e82;color:#fff;border:2px solid #1172ba"><h3>Order Number</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['aro'] as $key => $item)
                                <!-- <tr>
                                    <td>
                                        <h3>{{(isset($item['customDescription']) && $item['customDescription'] != '') ? $item['customDescription'] : ''}}
                                            @if(empty($item['customDescription']))
                                               <?php echo "<input type='text' style='width:50%;height:20px'>" ?>
                                            @endif
                                        </h3>
                                    </td>
                                </tr> -->
                                    
                                        <tr>
                                            <td rowspan="2" style="width: 5%;text-align: center">{{$key+1}}.</td>
                                            <td>
                                                <h3>{{($item['itemType'])}}</h3>
                                            </td>

                                            <td>
                                                <p style="border:2px solid gray;width: 100%;margin:5px;">{{$item['upfrontPayment']}}</p>
                                            </td>
                                            <td><p style="border:2px solid gray;width: 100%;margin:5px;">{{$item['repaymentTerm']}} Months</p></td>
                                            <td><p style="border:2px solid gray;width: 100%;margin:5px;">${{(isset($item['aroBase'])) ? $item['aroBase'] : 0}}</p></td>
                                            <td><p style="border:2px solid gray;width: 100%;margin:5px;">${{$item['monthlyCommitment']}}</p></td>
                                            <td>
                                                <p style="border:2px solid gray;width: 100%;margin:5px;">
                                                    @if(isset($item['orderNumber']))
                                                        {{$item['orderNumber']}}
                                                    @else
                                                        &nbsp;
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <!-- <td>&nbsp;</td> -->
                                            <td>Item(s)</td>
                                            <td colspan="5">
                                                @foreach($item['aroProducts'] as $key => $aro)
                                                    
                                                    @if(isset($aro['ProductType']))
                                                        <h4 style="border:2px solid gray;margin: 5px">{{$aro['ProductType']}}</h4>
                                                    @else
                                                        <h4 style="border:2px solid gray;margin: 5px">{{$aro['name']}}</h4>
                                                        <!-- <h4 style="border:2px solid gray;margin: 5px">&nbsp;</h4> -->
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    
                                
                        @endforeach
                    </tbody>
                </table>
            </div>

        <div style="width:100%;clear:both;margin-top:20px;">
            <table style="border:none;">
                <tbody style="border:none;">
                    <tr style="border:none;">
                        <td colspan="3" style="border:none;">
                            The Repayment Option(s) will appear on my Telstra bill as a separate charge. There may be a pro-rata charge in addition to the regular monthly charge on the first bill, but the <strong>ongoing monthly cost</strong> will be as indicated in the <strong>Monthly Commitment</strong> field until the end of the specified Repayment Term.<br/><br/>
                            I understand the accessories I have purchased today are an <strong>optional add-on</strong>, are <strong><u>not free</u></strong>, <strong>not a gift with purchase</strong>, and are <strong>not included</strong> in the price of any other services I have with Telstra.<br/><br/>
                            The full terms and conditions for these purchases, including circumstances where remaining charges for the accessories may need to be paid out earlier in full, are in a separate contract to this acknowledgement, which I must read before signing the contract.
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="3" style="border:none;"><h2 style="font-size:16px !important;">Customer Details</h2></td>
                    </tr>
                    <tr style="border:none;">
                        <td style="width:40%;border:none;">
                            <form>
                                <p>Full Name</p>
                                <input value="{{$data['customerDetails']['name']}}" type="text" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%;border:none;">
                            <form>
                                <p>Contact Number</p>
                                <input value="{{$data['customerDetails']['contactNumber']}}" type="text" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%;border:none;">
                            <form>
                                <p>Date</p>
                                <input type="text" value="{{$data['customerDetails']['date']}}" style="width:100%;height:20px">
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <form>
                            <!-- <h2 style="font-size:16px !important;">Customer Signature</h2> -->
                            <!--  -->
                            @if(isset($data['signature']))
                            <img src="{{$data['signature']}}" style="padding-left:30%;width:70%;border:2px solid gray">
                            @else
                            <input type="text" style="width:100%;height:100px">
                            @endif
                        </form>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table>
                <tbody>
                    <tr style="border:none;">
                        <td colspan="3" style="border:none;"><span style="font-size:16px !important;font-weight:bold;">Telstra Representative Details</span></td>
                    </tr>
                    <tr style="border:none;">
                        <td style="width:40%;border:none;">
                            <form>
                                <p>Name</p>
                                <input type="text" value="{{$data['repDetails']['name']}}" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%;border:none;">
                            <form>
                                <p>C/D/P Number</p>
                                <input type="text" value="{{!empty($data['repDetails']['number']) ? $data['repDetails']['number'] : ''}}" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%;border:none;">
                            <form>
                                <p>Dealer Code</p>
                                <input type="text" value="{{$data['repDetails']['dealerCode']}}" style="width:100%;height:20px">
                            </form>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width:70%;border:none;">
                            <form>
                                <p>Store Name</p>
                                <input type="text" value="{{$data['repDetails']['storeName']}}" style="width:100%;height:20px">
                            </form>
                        </td>
                        <td style="width:30%;border:none;">
                            <form>
                                <p>Store Contact Number</p>
                                <input type="text" value="{{$data['repDetails']['storeContactNumber']}}" style="width:100%;height:20px">
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </main>
</body>
</html>
